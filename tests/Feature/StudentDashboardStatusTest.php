<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

/**
 * Summary of StudentDashboardStatusTest
 */
class StudentDashboardStatusTest extends TestCase
{
    public function testDashboardCanBeVisited()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/dashboard');
        $response->assertStatus(200);
    }

    public function testPersonalityTestInstructionsPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->withoutExceptionHandling()->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/test/4/instructions');
        $response->assertStatus(200);
    }

    /**
     * The test doc
     */
    public function testSessionReportPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/reports');
        $response->assertStatus(200);
    }

    public function testTestReportPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/result/report');
        $response->assertStatus(200);
    }

    public function testPaymentHistoryPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/payments');
        $response->assertStatus(200);
    }

    public function testCoachesPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/session/booking');
        $response->assertStatus(200);
    }

    public function testSessionsPageCanBeDisplayed()
    {
        $user = User::where("email", "student@gmail.com")->first();
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/user/session');
        $response->assertStatus(200);
    }
}
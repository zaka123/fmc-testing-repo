<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class StatusTest extends TestCase
{
    public function testHomePageCanBeDisplayed()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testAboutUsPageCanBeDisplayed()
    {
        $response = $this->get('/about');
        $response->assertStatus(200);
    }

    public function testConactUsPageCanBeDisplayed()
    {
        $response = $this->get('/contact');
        $response->assertStatus(200);
    }

    public function testGetStartedPageCanBeDisplayed()
    {
        $response = $this->get('/get-started');
        $response->assertStatus(200);
    }

    public function testLoginPageCanBeDisplayed()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function testSchoolStudentRegistrationPageCanBeDisplayed()
    {
        $response = $this->get('/register/Student');

        $response->assertStatus(200);
    }

    public function testUniversityStudentRegistrationPageCanBeDisplayed()
    {
        $response = $this->get('/register/University%20Student');

        $response->assertStatus(200);
    }

    public function testCareerCoachApplyScreenCanBeDisplayed()
    {
        $response = $this->get("/coach/apply");
        $response->assertStatus(200);
    }

    public function testFrequentlyAskedQuestionsScreenCanBeDisplayed()
    {
        $response = $this->get('/faqs');
        $response->assertStatus(200);
    }

    public function testHelpScreenCanBeDisplayed()
    {
        $response = $this->get('/help');
        $response->assertStatus(200);
    }

    public function testDashboardCanBeVisited()
    {
        $user = User::find(1);
        $response = $this->actingAs($user, 'web')
            ->withSession(['banned' => false])
            ->get('/dashboard');
        $response->assertStatus(200);
    }
}
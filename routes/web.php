<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BenefitController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\Counsellor;
use App\Http\Controllers\CounsellorProfileController;
use App\Http\Controllers\CounsellorSessionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DownloadResultController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\InstructionController;
use App\Http\Controllers\PaymentInfoController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SessionPaymentController;
use App\Http\Controllers\SubscribeController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\User;
use App\Http\Controllers\UserCounsellorSessionController;
use App\Http\Controllers\UserPersonalInformationController;
use App\Http\Controllers\UserProfessionalController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\UserResultController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\User\VerifyPhoneController;
use App\Http\Controllers\UserTimezoneController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('coach/register', [AuthController::class, 'counsellorRegistration'])->name("coach.apply");
Route::view('coach/apply', 'counsellor.apply')->name('counsellor.apply');
Route::get('faqs', [FrontController::class, "faqs"])->name('faqs');
// Route::get('help', [FrontController::class, 'help'])->name('help');

Route::get('about', [FrontController::class, 'about'])->name('about');
Route::get('contact', [FrontController::class, 'contact'])->name('contact');
Route::resource('contact-us', ContactUsController::class)->only(["store"]);

Route::get('counsellor-register', [AuthController::class, 'counsellorRegister']);
Route::post('counsellor-registration', [AuthController::class, 'counsellorRegister'])->name('counsellorRegister');
Route::get('school', [FrontController::class, 'school'])->name("school");
Route::get('register/{profession}', [AuthController::class, 'register']);
// added routes for new design
Route::get('get-started', [AuthController::class, 'new_signup'])->name('who-are-you');

// Route::get('get-started', [FrontController::class, 'getStarted'])->name('who-are-you');
Route::get('university', [FrontController::class, 'university'])->name("university");
Route::get('Professional', [FrontController::class, 'professional'])->name("professional");
Route::get('Parents', [FrontController::class, 'parent'])->name("parents");
Route::get('benefit/{word?}', [BenefitController::class, 'getData'])->name('benefit')->middleware(["auth", "verified"]);

Route::group(
    ['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'role:Super Admin']],
    function () {
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);
        Route::resource('users', Admin\UserController::class);
        Route::resource('users.professional', Admin\UserProfessionalController::class);
        Route::resource('counsellors', Admin\CounsellorController::class);
        Route::resource('tests', TestController::class);
        Route::resource('questions', QuestionController::class);
        Route::resource('payments', Admin\SessionPaymentController::class)->except(["edit", "create"]);
        Route::resource('benefits', BenefitController::class);
        Route::resource('instructions', InstructionController::class);
        Route::resource('videos', Admin\VideoController::class);
        Route::resource('faqs', FaqController::class);
        Route::resource('coupons', Admin\CouponController::class);
        Route::get('changeCouponStatus', [Admin\CouponController::class, 'changeCouponStatus'])->name('coupons.changeCouponStatus');
        Route::get('changeUserStatus', [Admin\UserController::class, 'changeUserStatus'])->name('users.changeUserStatus');
        Route::get('users/{id}/change-password', [Admin\UserController::class, 'changeUserPassword'])->name('users.changeUserPassword');
        Route::post('users/{id}/update-password', [Admin\UserController::class, 'updateUserPassword'])->name('users.updateUserPassword');
        Route::get('currencies', [Admin\CurrencyController::class, 'index'])->name('currencies.index');
        Route::post('currencies/store', [Admin\CurrencyController::class, 'store'])->name('currencies.store');
        Route::get('currencies/{id}/edit', [Admin\CurrencyController::class, 'edit'])->name('currencies.edit');
        Route::put('currencies/update', [Admin\CurrencyController::class, 'update'])->name('currencies.update');
        Route::delete('currencies/{id}/delete', [Admin\CurrencyController::class, 'destroy'])->name('currencies.destroy');
        Route::get('deleted/users', [Admin\TrashedUsersController::class, 'deletedUsers'])->name('trashedUsers');
        Route::delete('deleted/users/{id}', [Admin\TrashedUsersController::class, 'deletePermanently'])->name('trashedUsers.delete');
        Route::get('deleted/coaches', [Admin\TrashedUsersController::class, 'deletedCoaches'])->name('counsellors.trashed');
        Route::get('restore/users/{id}', [Admin\TrashedUsersController::class, 'restore'])->name('users.restore');
    }
);

Route::middleware(['verified', 'auth', 'change_password'])->group(function () {
    Route::get('dashboard', [DashboardController::class, "dashboard"])->name('dashboard');
    Route::get('help', [FrontController::class, 'help'])->name('help');
    Route::POST('help', [FrontController::class, 'helpStore'])->name('help.store');
    Route::POST('user/{id}/timezone/update', [UserTimezoneController::class, 'updateUserTimezone'])->name('user.timezone.update');
});

Route::get('refresh_captcha', [FrontController::class, 'refreshCaptcha']);
Route::post('subscribe', [SubscribeController::class, 'store'])->name('subscribe');
Route::prefix("user")->as("user.")->middleware(['auth', 'verified', 'role:User'])->group(function () {
    Route::POST('connect-zoom', [UserController::class, 'connectZoomAccount'])->name("zoom.account.connect");
    Route::get('session/booking', [CounsellorProfileController::class, 'index'])->name('session.booking');
    Route::get('career/coaching', [CounsellorProfileController::class, 'main'])->name('career.main');
    Route::resource('payments', SessionPaymentController::class);
    Route::resource('profile', UserProfileController::class);
    Route::resource('session', UserCounsellorSessionController::class);
    // Route::post('session/started', [UserCounsellorSessionController::class, 'sessionStarted'])->name('session.started');
    Route::post('counsellor/available-sessions', [UserCounsellorSessionController::class, 'counsellorAvailableSessions'])->name('counsellor.available-sessions');
    Route::resource('professional', UserProfessionalController::class)->except('index', 'show');
    Route::resource('video-call', User\VideoCallController::class)->only(["show"]);
    Route::get("test/{id}/info", [PaymentInfoController::class, 'index'])->name("payments.info.index");
    Route::get('benefit/{word?}', [BenefitController::class, 'getData']);
    Route::POST('stripe/payment', [SessionPaymentController::class, 'stripePayment'])->name('stripe.payment');
    // Route::POST('session/payment', [SessionPaymentController::class, 'sessionPayment'])->name('session.payment');

    // coupon implementation
    Route::post('stripe/apply_coupon', [SessionPaymentController::class, 'apply_coupon_code'])->name('stripe.apply_coupon');

    Route::get('getCounsellor/{id}', [CounsellorProfileController::class, 'getCounsellor']);
    Route::get('reports', [User\SessionReportController::class, 'index'])->name("sessions.reports");
    Route::get('checkSession/{id}', [UserCounsellorSessionController::class, 'checkSession']);
    // Route::get('getCounsellorSessions', [CounsellorSessionController::class, 'getCounsellorSessions']);
    Route::get('test/{test}', [TestController::class, 'userTestsQuestions'])->name('test');
    Route::get('test/{test}/restart', [TestController::class, 'userTestRestart'])->name('test.restart');
    Route::post('results/store', [UserResultController::class, 'store'])->name('results.store');
    Route::get('result/report', [UserResultController::class, 'report'])->name("test.report"); // test report for user
    Route::get('test/{test}/instructions', [TestController::class, 'instructions'])->name('test.instructions');

    Route::get('personal-information/{user_id}/{test_name}', [UserPersonalInformationController::class, 'getUserInformation'])->name('user.family');
    // Route::get('personal-information/{status}',[UserPersonalInformationController::class,'getUserInformation']);
    Route::get('download-personality-result/{word?}', [DownloadResultController::class, 'personality'])->name('personality.test.download');
    Route::get('download-career-result', [DownloadResultController::class, 'career'])->name('career.test.download');
    Route::get('download-skill-assessment-result', [DownloadResultController::class, 'skill_assessment'])->name('skill-assessment.test.download');

    Route::POST('profile-picture', [UserController::class, 'updateUserProfilePicture']);
    Route::POST('password/change', [UserController::class, 'changePassword'])->name('password.change');
    Route::get('verify/phone', [VerifyPhoneController::class, 'index'])->name('verify.phone');
    Route::POST('verify/phone', [VerifyPhoneController::class, 'verifyPhone'])->name('verify.code');
    Route::get('verify/resend-code/{user}', [VerifyPhoneController::class, 'resendVerificationCode'])->name('resend.code');
    Route::get('/test/{test}/report', [UserResultController::class, 'getTestReport'])->name("getTestReport");

    Route::get('settings', [User\SettingsController::class, 'index'])->name('settings.index');
    Route::post('settings/profile/update', [User\SettingsController::class, 'profileUpdate'])->name('settings.profile.update');
    Route::post('settings/password/update', [User\SettingsController::class, 'passwordUpdate'])->name('settings.password.update');
});

Route::middleware(['auth', 'verified', 'role:Counsellor'])
    ->prefix("counsellor")
    ->as("counsellor.")
    ->group(
        function () {
            Route::GET("password/change", [Admin\CounsellorController::class, 'changePassword'])->name("password.change");
            Route::POST("password/change", [Admin\CounsellorController::class, 'updatePassword'])->name("password.update");
        }
    );


Route::middleware(['auth', 'change_password', 'verified', 'role:Counsellor'])
    ->prefix("counsellor")
    ->as("counsellor.")
    ->group(
        function () {
            // Route::POST('connect-zoom', [UserController::class, 'connectZoomAccount'])->name("zoom.account.connect");
            // Route::get('/auth/zoom', [App\Http\Controllers\ZoomController::class, 'redirectToZoom'])->name("zoom.account.connect");
            Route::GET('sessions/reports', [Counsellor\SessionReportController::class, 'index'])->name("session.report.index");
            Route::GET('session/{session}/report/create', [Counsellor\SessionReportController::class, 'create'])->name("session.report.create");
            Route::POST('session/{session}/report', [Counsellor\SessionReportController::class, 'store'])->name("session.report.store");
            Route::GET('session/{session}/report/{report}', [Counsellor\SessionReportController::class, 'show'])->name("session.report.show");
            Route::GET('session/{session}/report/{report}/edit', [Counsellor\SessionReportController::class, 'edit'])->name("session.report.edit");
            Route::PUT('session/{session}/report/{report}', [Counsellor\SessionReportController::class, 'update'])->name("session.report.update");
            Route::DELETE('session/{session}/report/{report}', [Counsellor\SessionReportController::class, 'destroy'])->name("session.report.destroy");

            Route::get('sessions/history', [UserCounsellorSessionController::class, 'history'])->name('session.history');
            // Route::resource('counsellor.profile', CounsellorProfileController::class)->only(["edit", "show", "update"]);
            Route::resource('payments', SessionPaymentController::class);
            Route::resource('sessions', CounsellorSessionController::class)->except("create");
            Route::resource('user', Counsellor\UserController::class)->only("show");
            Route::resource('video-call', Counsellor\VideoCallController::class)->only(["show"]);


            // Route::get('user/{id?}', [UserController::class, 'getUserProfile']);
            Route::get('benefit/{word?}', [BenefitController::class, 'getData']);
            Route::get('getSessions', [CounsellorSessionController::class, 'getSessions']);
            Route::get('bookings', [UserCounsellorSessionController::class, 'getCounsellorSessions'])->name('session.bookings');
            Route::get('status/{id}/{status}', [UserCounsellorSessionController::class, 'changeStatus']);
            Route::post('session/started', [UserCounsellorSessionController::class, 'sessionStarted'])->name('session.started');
            Route::get('profile', [CounsellorProfileController::class, 'show'])->name('profile.show');
            Route::get('edit/profile', [CounsellorProfileController::class, 'edit'])->name('profile.edit');
            Route::POST('profile/update', [CounsellorProfileController::class, 'update'])->name('profile.update');
            Route::POST('profile/picture/store', [CounsellorProfileController::class, 'changeProfilePicture'])->name('profile.picture.store');
            Route::delete('profile/delete/{id}', [CounsellorProfileController::class, 'destroy'])->name('account.destroy');

            Route::POST('profile-picture', [UserController::class, 'updateUserProfilePicture']);

            // Route::get('session-report', [Counsellor\SessionReportController::class, 'index']);
            // Route::get('upload-session-report/{session}', [Counsellor\SessionReportController::class,'create']);
            // Route::POST('session-history', [Counsellor\SessionReportController::class,'store']);
        }
    );

// Route::get(
//     '/{path?}',
//     function ($path = null) {
//         return View::make('welcome');
//     }
// )->where('path', '.*');

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Route::get('/privacy', function () {
    return view('privacy-policy');
})->name('privacy');

Route::get('/terms-of-use', function () {
    return view('terms-of-use');
})->name('terms-of-use');

Route::get('/auth/zoom', [App\Http\Controllers\ZoomController::class, 'redirectToZoom'])->middleware(['auth', 'verified'])->name("zoom.account.connect");
Route::get('/zoom/callback', [App\Http\Controllers\ZoomController::class, 'handleZoomCallback'])->name('zoom.callback');




// Route::get('/upQuiz', function () {
//     $questions = \App\Models\Question::where('test_id', 5)->get();
//     foreach ($questions as $question) {
//         $question->update(
//             [
//                 'question_no' => $question->question_no + 59, 'test_id' => 6,
//             ]
//         );
//     }
//     dd($questions);
// });


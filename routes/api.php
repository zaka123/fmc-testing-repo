<?php

use App\Http\Controllers\API\AccessTokenController;
use App\Http\Controllers\API\CountryController;
use App\Http\Controllers\API\User;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::POST('login', [AuthController::class,'login']);
Route::get('access_token', [AccessTokenController::class,'generate_token']);
Route::get("country/{field}", [CountryController::class, 'index'])->name("api.country_info");

/****************************/
/*        USER ROUTES       */
/****************************/
Route::prefix('user')->as('api.user.')->group(
    function () {
        Route::GET("session/{session}/report/{report}", [User\SessionReportController::class, 'show'])->name("session.report.show");
    }
);

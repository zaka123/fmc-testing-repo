$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        autoplay: true,
        loop: true,
        autoplayTimeout: 5000,
        smartSpeed: 2700,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            },
            1300: {
                items: 1
            }
        }
    });
});
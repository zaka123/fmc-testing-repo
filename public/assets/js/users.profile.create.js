const questions = $('.question');
let currentQuestionNumber = 0;
$('.next')
    .on('click', () => {
        changeQuestion(1)
    });
$('.previous')
    .on('click', () => {
        changeQuestion(0)
    });
$(document).on('keypress', detectReturnKey);
$(document).on('show.bs.modal', () => {
    $(document).off('keypress');
});

$(document).on('hide.bs.modal', () => {
    $(document).on('keypress', detectReturnKey);
});

// ask another question in student profile completion
try {
    document.querySelector('.with-modal').oninput = askAnotherQuestion;
} catch {
    // do nothing
}

function detectReturnKey(e) {
    if (e.shiftKey && e.key === 'Enter') {
        changeQuestion(0)
    } else if (e.key == 'Enter') {
        e.preventDefault();
        currentQuestionNumber + 1 === questions.length ? document.forms.profileForm.submit() : changeQuestion(1)
    }
}

/*
    - flag = 0 means previous
    - falg = 1 means next
*/
function changeQuestion(flag) {
    /*
        - ensure current question is greater than or equal to 0
        - ensure current question is less than total number of questions 
    */
    if (0 <= currentQuestionNumber <= questions.length) {
        function showQuestion(q) {
            questions[q].classList.remove('d-none');
        }

        function hideQuestion(q) {
            questions[q].classList.add('d-none');
        }

        function showSubmitBtn() {
            $('.next').hide();
            $('.submit').show();
        }

        function showNextBtn() {
            $('.next').show();
            $('.submit').hide();
        }

        function disablePreviousBtn() {
            $('.previous')
                .attr("disabled", "true");
        }

        function enablePreviousBtn() {
            $('.previous')
                .removeAttr("disabled");
        }

        function validateQuestion(question) {
            if (question.classList.contains("job_question")) {
                var checkboxes = question.querySelectorAll('input[type="checkbox"]');
                var checkedOne = false;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        checkedOne = true;
                        break;
                    }
                }
                
                if (!checkedOne) {
                    toastr.error("Please select at least one option");
                    return false;
                }
            }

            const inputs = question.querySelectorAll('input');
            const selects = question.querySelectorAll('select');
            const textareas = question.querySelectorAll('textarea');
            var flag = false;
            inputs.forEach(input => {
                if (input.checkValidity()) {
                    flag = true;
                    input.classList.remove('invalid');
                } else {
                    flag = false;
                    input.classList.add('invalid');
                }
            });
            selects.forEach(select => {
                if (select.checkValidity()) {
                    flag = true;
                    select.classList.remove('invalid');
                } else {
                    flag = false;
                    select.classList.add('invalid');
                }
            });
            textareas.forEach(textarea => {
                if (textarea.checkValidity()) {
                    flag = true;
                    textarea.classList.remove('invalid');
                } else {
                    flag = false;
                    textarea.classList.add('invalid');
                }
            });
            return flag;
        }

        if (flag) { // when next is clicked
            // check validity of the current question
            if (validateQuestion(questions[currentQuestionNumber])) {
                (currentQuestionNumber + 2 === questions.length) ? showSubmitBtn() : showNextBtn();
                hideQuestion(currentQuestionNumber);
                showQuestion(currentQuestionNumber + 1);
                // try to focus next question
                try {
                    questions[currentQuestionNumber + 1].querySelector('input')
                        .focus();
                } catch {
                    try {
                        questions[currentQuestionNumber + 1].querySelector('textarea')
                            .focus();
                    } catch {
                        questions[currentQuestionNumber + 1].querySelector('select')
                            .focus();
                    }
                }
                currentQuestionNumber++; // increase question by 1
            }
        } else if (!flag && currentQuestionNumber > 0) { // when previous is clicked
            hideQuestion(currentQuestionNumber);
            showQuestion(currentQuestionNumber - 1);
            showNextBtn();
            currentQuestionNumber--; // decrease question by 1
        }
        (currentQuestionNumber === 0) ? disablePreviousBtn() : enablePreviousBtn();

    }
}

function askAnotherQuestion(e) {
    const working = 1;
    const studying = 2;
    const other = 3;
    const selected = e.target.selectedIndex;
    var modal;
    try {
        modal = document.querySelector('#staticBackdrop');
    } catch(e) {
        console.log(e);
    }

    switch (selected) {
        case working:
            question = `<label class="fs-4 fw-500">
            Why do you think you will be working 5 years from now?
                </label>
                <span class="text-muted">
                    (you&nbsp;may&nbsp;choose&nbsp;multiple)
                </span>
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" role="switch" id="future_self_reflection[1][working][1]" name="future_self_reflection[1][working][1]">
                    <label class="form-check-label" for="future_self_reflection[1][working][1]">Financial Independence</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][2]" name="future_self_reflection[1][working][2]">
                    <label class="form-check-label" for="future_self_reflection[1][working][2]">Education Requirements Fulfilled</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][3]" name="future_self_reflection[1][working][3]">
                    <label class="form-check-label" for="future_self_reflection[1][working][3]">Family Support</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][4]" name="future_self_reflection[1][working][4]">
                    <label class="form-check-label" for="future_self_reflection[1][working][4]">Societal Pressure</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][5]" name="future_self_reflection[1][working][5]">
                    <label class="form-check-label" for="future_self_reflection[1][working][5]">Lack of Vision</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][6]" name="future_self_reflection[1][working][6]">
                    <label class="form-check-label" for="future_self_reflection[1][working][6]">Other Reason</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][working][7]" name="future_self_reflection[1][working][7]">
                    <label class="form-check-label" for="future_self_reflection[1][working][7]">Prefer not to say</label>
                </div>`;
            break;
        case studying:
            question = `<label class="fs-4 fw-500">
            Why do you think you will be studying 5 years from now?
                </label>
                <span class="text-muted">
                    (you&nbsp;may&nbsp;choose&nbsp;multiple)
                </span>
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" role="switch" id="future_self_reflection[1][studying][1]" name="future_self_reflection[1][studying][1]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][1]">Lack of Vision</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][2]" name="future_self_reflection[1][studying][2]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][2]">Lack of Skills</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][3]" name="future_self_reflection[1][studying][3]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][3]">Lack of opportunities</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][4]" name="future_self_reflection[1][studying][4]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][4]">Societal Pressure</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][5]" name="future_self_reflection[1][studying][5]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][5]">Acquire Higher Education</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][6]" name="future_self_reflection[1][studying][6]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][6]">Other Reason</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" role="switch" id="future_self_reflection[1][studying][7]" name="future_self_reflection[1][studying][7]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][7]">Prefer not to say</label>
                </div>`;
            break;
        case other:
            changeQuestion(1);
            return;
        default:
            changeQuestion(1);
            return;
    }
    function validateModal() {
        if (true) {
		var checkboxes = document.querySelectorAll('#staticBackdrop input[type="checkbox"]');
                var checkedOne = false;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        checkedOne = true;
                        break;
                    }
                }
                
                if (!checkedOne) {
                    toastr.error("Please select at least one option");
                    return false;
                }
            modal.hide();
            changeQuestion(1);
        } else {
            document.querySelector('#staticBackdrop .modal-body').querySelector('select').classList.add('invalid');
        }
    }
    modal.querySelector('#staticBackdrop .modal-body').innerHTML = question;
    try {
        modal.querySelector('#staticBackdrop .modal-header').remove();  // remove header
    } catch(e) {
        console.log(e);
    }

    try {
        modal.querySelector('#staticBackdrop .modal-footer button').addEventListener('click', validateModal);
    } catch(e) {
        console.log(e);
    }
    // instanctiate modal to show
    modal = new bootstrap.Modal(modal);
    modal.show();

}

// if the user is influenced by none, then don't ask for degree of influence
document.getElementsByClassName("ambitions_2")[0].onchange = (e) => {
    const next = 1;
    const selectedOption = e.target.selectedOptions[0].value;
    if (selectedOption == "Nobody" || selectedOption == "Prefer not to say") {
        // show next question
        changeQuestion(next);
    } else {
        const degreeOfInfluencerStaticBackdrop = new bootstrap.Modal(document.getElementById("degreeOfInfluencerStaticBackdrop"));
        degreeOfInfluencerStaticBackdrop.show();
        document.querySelector("#degreeOfInfluencerStaticBackdrop .modal-footer button").onclick = (e) => {
            degreeOfInfluencerStaticBackdrop.hide();
            changeQuestion(next);
        }
    }
}

project find_me_career {
  database_type: 'MYSQL'
  note: '''
  [FindMeCareer](https://findmecareer.com)'s philosophy revolves around self-awareness. However, it is multifaceted in many ways, where the team works towards unlocking the true potential of individuals. The platform connects the experienced professionals and veterans in the field of coaching and mentoring from academia and industry to keen students and professionals for exponential growth. From a variety of experts around the globe, the best solutions for your development that fits your profile and skill set are devised. They do not just help clients choose a career, but mentor and coach them by setting smart career goals to achieve in the least amount of time. The mission is to help create professionals in all walks of life through sustained and diligent coaching and mentorship. When someone become a part of [FindMeCareer](https://findmecareer.com), they become more than clients; they become a part of an ever-growing family of professionals and students from around the world!
  '''
}

table users {
  id bigint [pk, increment]
  first_name varchar(255) [not null]
  last_name varchar(255) [not null]
  email varchar(255) [ unique, not null ]
  email_verified_at timestamp
  password varchar(255) [not null]
  profession profession_options  [not null]
  gender gender_options [not null]
  two_factor_secret text
  two_factor_recovery_codes text
  remember_token varchar(100)
  profile_picture_url text
  first_time_password_changed first_time_password_changed_options [not null, default: "yes", note: "Use for admin created users"]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  indexes {
    email
  }
  note: "Stores user's basic data"
}

table student_profiles {
  id bigint [pk, increment]
  student_id bigint [not null, ref: - users.id, note: "Student"]
  date_of_birth date [not null]
  country bigint [not null, ref: > countries.id]
  favorite_subject bigint [not null, ref: > subjects.id]
  difficult_subject bigint [not null, ref: > subjects.id]
  easiest_subject bigint [not null, ref: > subjects.id]
  average_grade_point decimal(3,2) [not null, note: "GPA"]
  deleted_at timestamp
  note: "Stores profile info of the student"
  indexes {
    student_id
    country
    favorite_subject
    difficult_subject
    easiest_subject
  }
}

table career_ambitions {
  id bigint [pk, increment]
  student_id bigint [not null, ref: - users.id, note: "Student"]
  immediate_next_step varchar(255) [not null]
  most_influence_personality varchar(255) [not null]
  same_profession_as varchar(255) [not null]
  parents_expectation_are_same varchar(255) [not null]
  reason_for_career_choice varchar(255) [not null]
  reason_for_university_degree_choice varchar(255) [not null]
  deleted_at timestamp
  note: "Stores the career ambitions of the student."
  indexes {
    student_id
  }
}

table future_self_reflection {
  id bigint [pk, increment]
  student_id bigint [not null, ref: - users.id, note: "Student"]
  five_year_forecast five_year_forecast_options
  deleted_at timestamp
  note: "Stores the future self reflections of the student."
  indexes {
    student_id
  }
}

table student_reasons_for_five_year_forcast {
  id bigint [pk, increment]
  student_id bigint [not null, ref: > users.id]
  reason_id bigint [not null]
}

table study_habits_and_routine {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  morning_study_hours decimal(2,2) [not null]
  evening_study_hours decimal(2,2) [not null]
  deleted_at timestamp
  note: "Stores the study habits and routines of the student."
  indexes {
    student_id
  }
}

table family_assistance {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  father int
  mother int
  siblings int
  relatives int
  others int
  deleted_at timestamp
  note: "Stores the family assistance info of the user."
  indexes {
    student_id
  }
}

table family_information {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  father_education varchar(255) [not null]
  mother_education varchar(255) [not null]
  father_occupation varchar(255) [not null]
  mother_occupation varchar(255) [not null]
  parents_retired varchar(255) [not null]
  deleted_at timestamp
  note: "Stores the family information of the student."
  indexes {
    student_id
  }
}

table decision_factors {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  parents_expectation int [not null]
  friends_influence int [not null]
  school_university_grades int [not null]
  favorite_subject int [not null]
  talent_skill int [not null]
  hobbies int [not null]
  scope_market_demand int [not null]
  financial_support int [not null]
  deleted_at timestamp
  note: "Stores the importance of decision factors of a student."
  indexes {
    student_id
  }
}

table acquired_skills {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  job_or_education varchar(255) [not null]
  Resume_creation varchar(255) [not null]
  cover_letter_creation varchar(255) [not null]
  interview_preparation varchar(255) [not null]
  presentation varchar(255) [not null]
  deleted_at timestamp
  note: "Stores the skills acquired by the student."
  indexes {
    student_id
  }
}

table employability_skills {
  id bigint [pk, increment]
  student_id bigint [ref: - users.id, note: "Student"]
  communication int
  teamwork int
  problem_solving int
  initiative_enterprise int
  planning_organizing int
  self_management int
  learning int
  technology int
  deleted_at timestamp
  note: "Stores the employability skills of a student."
  indexes {
    student_id
  }
}

table reasons_for_studying {
  id bigint [pk, increment]
  reason text
  deleted_at timestamp
  note: "Stores the reasons for studying."
}

table reasons_for_working {
  id bigint [pk, increment]
  reason text
  note: "Stores the reasons for working."
}

table reasons_for_home_study {
  id bigint [pk, increment]
  reason text
  note: "Stores the reasons for home study."
}

table reasons_for_no_home_study {
  id bigint [pk, increment]
  reason text
  note: "Stores the reasons for no home study."
}

table reasons_for_getting_a_job {
  id bigint [pk, increment]
  reason text
  note: "Stores the reasons for getting a job."
}

table professional_profiles {
  id bigint [pk, increment]
  professional_id bigint [ref: - users.id, note: "Student"]
  date_of_birth date [not null]
  country_id bigint [ref: > countries.id]
  experience varchar(255) [not null]
  designation varchar(255) [not null]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores profile info of the professional"
}

table counsellor_profiles {
  id bigint [pk, increment]
  counsellor_id bigint [ref: - users.id, note: "Student"]
  date_of_birth date [not null]
  country_id bigint [ref: > countries.id]
  interested_in interested_in_options [not null]
  resume_url text [not null]
  phone_number varchar(255) [not null]
  certified certified_options [not null, default: "no"]
  motivation text [not null]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores profile info of the counsellor"
  indexes {
    counsellor_id
    country_id
  }
}

table subjects {
  id bigint [pk, increment]
  name varchar(255)
  deleted_at timestamp
  note: "Stores the details of the subjects"
}

table countries {
  id bigint [pk, increment]
  name varchar(255) [not null]
  code varchar(10) [not null]
  dial_code varchar(10) [not null]
  deleted_at timestamp
  note: "Stores the country info"
}

table personality_traits {
  id bigint [pk, increment]
  type varchar(5) [not null]
  description text [not null]
  deleted_at timestamp
  note: "Stores the personality info."
}

table recommended_careers {
  id bigint [pk, increment]
  personality_trait_id bigint [ref: > personality_traits.id]
  name varchar(450)
  deleted_at timestamp
  note: "Stores the recommended careers of the personality trait."
  indexes {
    personality_trait_id
  }
}

table discouraged_careers {
  id bigint [pk, increment]
  personality_trait_id bigint [ref: > personality_traits.id]
  name varchar(450)
  deleted_at timestamp
  note: "Stores the discouraged careers of the personality trait."
  indexes {
    personality_trait_id
  }
}

table counsellor_availabilities {
  id bigint [pk, increment]
  counsellor_id bigint [ref: > users.id]
  date date [not null]
  time time [not null]
  deleted_at timestamp
  note: "Stores the counsellor available timings"
  indexes {
    counsellor_id
    ("counsellor_id", "date", "time") [unique]
  }
}

table tests {
  id bigint [pk, increment]
  name varchar(255) [not null, unique]
  start_text text
  resume_text text
  finish_text text
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
}

table test_instructions {
  id bigint [pk, increment]
  test_id bigint [not null, ref: > tests.id]
  description text [not null]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores the instructions of a test."
  indexes {
    test_id
  }
}

table test_results {
  id bigint [pk, increment]
  test_id bigint [not null, ref: > tests.id]
  test_question_id bigint [not null, ref: > test_questions.id]
  student_id bigint [not null, ref: > users.id]
  question_option_id bigint [not null, ref: > question_options.id]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores the results of a student's test."
  indexes {
    test_id
    test_question_id
    student_id
    question_option_id
    (test_id, test_question_id, student_id) [unique]
    (test_id, test_question_id, student_id, question_option_id) [unique]
  }
}

table test_questions {
  id bigint [pk, increment]
  test_id bigint [ref: > tests.id]
  category_id bigint [ref: > question_categories.id]
  sub_category_id bigint [ref: > question_sub_categories.id]
  question_number int [not null]
  statement text [not null]
  deleted_at timestamp
  note: "Stores the questions of a test."
  indexes {
    test_id
    category_id
    sub_category_id
    (test_id, question_number) [unique]
  }
}

table question_options {
  id bigint [pk, increment]
  test_question_id bigint [ref: > test_questions.id]
  option_number int [not null]
  text varchar(255) [not null]
  deleted_at timestamp
  note: "Stores the options of a question."
  indexes {
    test_question_id
    (test_question_id, option_number) [unique]
  }
}

table question_categories {
  id bigint [pk, increment]
  name varchar(255) [not null, unique]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
}

table question_sub_categories {
  id bigint [pk, increment]
  name varchar(255) [not null, unique]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
}
table sessions {
  id bigint [pk, increment]
  student_id bigint [ref: > users.id]
  counsellor_id bigint [ref: > users.id]
  counsellor_availability_id bigint [ref: - counsellor_availabilities.id]
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores the session info of student counsellor session"
  indexes {
    student_id
    counsellor_id
    counsellor_availability_id
    (student_id, counsellor_id,counsellor_availability_id) [unique]
  }
}

table session_reports {
  id bigint [pk, increment]
  session_id bigint [not null, ref: - sessions.id]
  reflections text [not null]
  warm_up text [not null]
  commitments text [not null]
  challenges text [not null]
  goal_setting text [not null]
  additional_notes text
  created_at timestamp
  updated_at timestamp
  deleted_at timestamp
  note: "Stores the reports of the sessions."
  indexes {
    session_id
  }
}

table payments {
  id bigint [pk, increment]
  target_id bigint [not null]
  student_id bigint [not null, ref: > users.id]
  purpose payment_purposes [not null]
  bank varchar(255) [not null]
  branch varchar(255)
  transaction_id varchar(500) [not null]
  amount int [not null]
  receipt_url text
  status payment_statuses [not null, default: "pending"]
  deleted_at timestamp
  note: "Stores the payment info of a session or test."
  indexes {
    target_id
    student_id
    (target_id, student_id, purpose) [unique]
  }
}

enum payment_statuses {
  "pending"
  "cancelled"
  "confirmed"
}

enum payment_purposes {
  "test"
  "session"
}

enum certified_options {
  "yes"
  "no"
}

enum gender_options {
  "male"
  "female"
  "other"
}

enum profession_options {
  "admin"
  "counsellor"
  "student"
  "professional"
  "university student"
}

enum first_time_password_changed_options {
  "yes"
  "no"
}

enum interested_in_options {
  "coaching"
  "mentoring"
  "both coaching and mentoring"
}

enum five_year_forecast_options {
  "studying"
  "working"
  "other"
}
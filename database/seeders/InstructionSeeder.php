<?php

namespace Database\Seeders;

use App\Models\Instruction;
use App\Models\Test;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstructionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 25; $i++) {
            $test = Test::inRandomOrder()->first();
            Instruction::create([
                'instruction' => $faker->paragraph(),
                'test_id' => $test->id
            ]);
        }
    }
}

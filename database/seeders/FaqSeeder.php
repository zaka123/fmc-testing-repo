<?php

namespace Database\Seeders;

use App\Models\Faq;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = file_get_contents(base_path('faqs.json'));
        $faqs = json_decode($reader);
        foreach($faqs as $faq)
            Faq::create([
                'question'  => $faq->question,
                'answer'    => $faq->answer
            ]);
    }
}

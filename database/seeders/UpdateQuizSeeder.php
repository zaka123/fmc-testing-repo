<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UpdateQuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = \App\Models\Question::where('test_id', 5)->get();
        foreach ($questions as $question) {
            $question->update(
                [
                    'question_no' => $question->question_no + 59, 'test_id' => 6,
                ]
            );
        }
    }
}

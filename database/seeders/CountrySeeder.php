<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = file_get_contents(base_path('countries.json'));
        $countries = json_decode($reader);
        foreach($countries as $country)
            Country::create((array) $country);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CareerListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            'Education' => [
                'Acedamia',
                'Teaching',
                'Counselling and guidance',
            ],
            'Health care' => [
                'Medical',
                'Dental',
                'Pharmacy',
                'Diet and Nutrition',
                'Cosmetology',
                'Pediatrics'
            ],
            'Engineering' => [
                'Civil',
                'Electrical',
                'Mechanical',
                'Civil',
                'Aerospace',
                'Nuclear',
            ],
            'Information technology' => [
                'Computer Science',
                'Computer Engineering',
                'Data analysis',
                'Data analytics',
                'Software engineering',
                'Programming',
                'Statistics',
                'Software Design',
                'Network Engineering',
                'Database administration',
            ],
            'Business administration' => [
                'Business and Strategy',
                'Administration',
                'Business and analysis',
            ],
            'Humanities' => [
                'Literature',
                'Philosophy',
                'Religion',
                'Theology',
            ],
            'Psychology' => [
                'Mental health',
                'Phychology',
                'Pcyciatry',
                'Child development',
            ],
            'Economics and finance' => [
                'Financial advising',
                'Financial analysis',
            ],
            'Science and research' => [
                'Archeology',
                'Geology',
                'Forestry',
                'Physics',
                'Ecology',
                'Paleontology',
                'Anthropology',
                'Marine Biology',
                'Egyptology',
                'Zoology',
                'Botany',
                'Astronomy',
                'Chemistry',
                'Genetics',
                'Biotechnology',
                'Environmental',
                'Mathematics',
            ],
            'Entertainment and media' => [
                'DJ',
                'Advertising',
                'Publishing',
                'Production',
                'Singing',
                'Dance',
                'Journalism',
            ],
            'Military and defence' => [
                'Public safety',
                'law inforcement',
                'Intelligence',
            ],
            'Management' => [
                'Customer care',
                'Sales',
                'Marketing',
                'consulting',
                'Human resource',
            ],
            'Sports and athletics' => [
                'Personal training',
                'Sports management',
                'coaching',
            ],
            'Fashion' => [
                'Modeling',
                'Costume design',
                'Hair styling',
                'Beauty and styling',
            ],
            'Performing arts' => [
                'Language',
                'Music',
                'Painting',
                'Comics',
                'Theatre',
            ],
            'Social services' => [
                'Sociology',
                'Political science',
                'Public administration',
            ],
            'Entrepreneurship' => [
                'N/A'
            ],
            'Government and public services' => [
                'N/A'
            ],

        ];

        if (\App\Models\CareersList::count() > 0) {
            \App\Models\CareersList::truncate();
        }
        foreach ($subjects as $key => $value) {
            foreach ($value as $sub) {
                \App\Models\CareersList::create([
                    'main_subject' => $key,
                    'sub_subject' => $sub,
                ]);
            }
        }
    }
}

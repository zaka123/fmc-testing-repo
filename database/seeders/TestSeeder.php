<?php

namespace Database\Seeders;

use App\Models\Test;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = file_get_contents(base_path('tests.json'));
        $tests = json_decode($reader, true);

        foreach($tests as $test)
            try {
                Test::create($test);
            } catch (QueryException $e) {
                echo "This test already exists.\n";
            }
    }
}

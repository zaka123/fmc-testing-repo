<?php

namespace Database\Seeders;

use App\Models\Benefit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BenefitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = file_get_contents(base_path('benefits.json'));
        $benefits = json_decode($reader);
        foreach($benefits as $benefit)
            Benefit::create([
                'title'                 =>  $benefit->title,
                'description'           =>  $benefit->description,
                'favoured_careers'      =>  $benefit->favoured_careers,
                'disFavoured_careers'    =>  $benefit->disFavoured_career,

            ]);
    }
}

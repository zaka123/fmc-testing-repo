<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SkillAssissmentTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = file_get_contents(base_path('skill-assessment.json'));
        $questions = json_decode($reader);
        foreach($questions as $question)
            Question::create((array) $question);
    }
}

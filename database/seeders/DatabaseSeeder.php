<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            BenefitsSeeder::class,
            TestSeeder::class,
            CareerExpectationsTestSeeder::class,
            InstructionSeeder::class,
            PersonalityTestSeeder::class,
            SkillAssissmentTestSeeder::class,
            FaqSeeder::class,
            CountrySeeder::class,
            TimezoneSeeder::class,
            CareerListSeeder::class,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Currency;
use Aveiv\OpenExchangeRatesApi\OpenExchangeRates;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $api = new OpenExchangeRates(getenv('OPENEXCHANGE_APP_ID'));

        // Getting currencies
        $currencies = $api->currencies(); // returns ["USD" => "United States Dollar", ...]

        // Getting latest rates
        $exchange_rates = $api->latest([
            'base' => 'USD',             // base currency
            'symbols' => [
                'USD',
                'EUR',
                'GBP',
                'PKR',
            ], // limit results to specific currencies
            'show_alternative' => true,  // include alternative currencies
        ]);

        $data = [];
        foreach ($currencies as $code => $name) {
            if (isset($exchange_rates[$code])) {
                $data[$code] = [
                    "name" => $name,
                    "exchange_rate" => $exchange_rates[$code]
                ];
            }
        }

        // save in database
        foreach ($data as $code => $currencyData) {
            Currency::create([
                'code' => $code,
                'name' => $currencyData['name'],
                'exchange_rate' => $currencyData['exchange_rate'],
            ]);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        Permission::updateOrCreate(['name' => 'questions-create']);
        Permission::updateOrCreate(['name' => 'questions-update']);
        Permission::updateOrCreate(['name' => 'questions-delete']);
        Permission::updateOrCreate(['name' => 'questions-view']);
        Permission::updateOrCreate(['name' => 'test-create']);
        Permission::updateOrCreate(['name' => 'test-view']);
        Permission::updateOrCreate(['name' => 'test-update']);
        Permission::updateOrCreate(['name' => 'test-delete']);
        Permission::updateOrCreate(['name' => 'view-counsellor']);
        Permission::updateOrCreate(['name' => 'view-user']);
        Permission::updateOrCreate(['name' => 'update-user']);
        Permission::updateOrCreate(['name' => 'delete-user']);
        Permission::updateOrCreate(['name' => 'user']);
        Role::updateOrCreate(['name' => 'Super Admin'])->givePermissionTo([
            'questions-create',
            'questions-update',
            'questions-delete',
            'questions-view',
            'test-create',
            'test-view',
            'test-update',
            'test-delete',
            'update-user',
            'delete-user'
        ]);
        Role::updateOrCreate(['name' => 'Counsellor'])->givePermissionTo(['view-user', 'test-view']);
        Role::updateOrCreate(['name' => 'User'])->givePermissionTo(['view-counsellor', 'questions-view', 'test-view']);

        $superAdmin = User::updateOrCreate([
            'email' => 'admin@gmail.com',
            'name' => 'Super',
            'gender' => 'Male',
            'last_name' => 'Admin',
            'password' => bcrypt('admin123'),
            'email_verified_at' => now()
        ]);
        $superAdminRole = Role::where('name', 'Super Admin')->first();
        $superAdmin->assignRole($superAdminRole->name);

        $Counsellor = User::updateOrCreate([
            'email' => 'counsellor@gmail.com',
            'name' => 'Counsellor',
            'last_name' => 'Shannon',
            'gender' => 'Male',
            'password' => bcrypt('admin123'),
            'email_verified_at' => now()
        ]);
        $CounsellorRole = Role::where('name', 'Counsellor')->first();
        $Counsellor->assignRole($CounsellorRole->name);

        $student = User::updateOrCreate([
            'email' => 'student@gmail.com',
            'name' => 'LWT',
            'last_name' => 'Student',
            'password' => bcrypt('admin123'),
            'profession' => 'Student',
            'gender' => 'Male',
            'password_changed' => true,
            'email_verified_at' => now()
        ]);

        $student->userProfile()->create([
            "date_of_birth" => "2003-02-04",
            "city" => "Pakistan",
            "favorite_subject" => "Mathematics",
            "easiest_subject" => "Computer Sciences",
            "difficult_subject" => "History",
            "average_grade_point" => "3",
            "father_education" => "Masters",
            "father_occupation" => "Business",
            "mother_education" => "Masters",
            "mother_occupation" => "Business",
            "parents_retired" => "Both",
        ]);

        $sql = file_get_contents(base_path('user_personal_information.sql'));
        DB::unprepared($sql);

        $userRole = Role::where('name', 'User')->first();
        $student->assignRole($userRole->name);

        $professional = User::updateOrCreate([
            'email' => 'professional@gmail.com',
            'name' => 'LWT',
            'last_name' => 'Professional',
            'password' => bcrypt('admin123'),
            'profession' => 'Professional',
            'gender' => 'Male',
            'password_changed' => true,
            'email_verified_at' => now()
        ]);

        $professional->userProfessional()->create([
            "date_of_birth" => "2003-02-04",
            "city" => "Pakistan",
            "experience" => "2+ years",
            "education_level" => "Masters",
            "organization_name" => "Last Wave Technology",
            "role" => "Developer",
        ]);
        $professional->assignRole($userRole->name);
    }
}
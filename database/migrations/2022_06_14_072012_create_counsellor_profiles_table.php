<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counsellor_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('interested_in',500)->nullable();
            $table->string('resume_upload',500)->nullable();
            $table->string('linked_in',500)->nullable();
            $table->string('phone_number')->nullable();
            $table->longText('career_couch',500)->nullable();
            $table->string('coaching_certification')->index()->nullable();
            $table->string('date_of_birth')->index()->nullable();
            $table->string('city')->index()->nullable();
            $table->foreignId('user_id')->constrained('users')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counsellor_profiles');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_reports', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->foreignId('student_id')->constrained('users')->cascadeOnDelete()->nullable();
            $table->foreignId('counsellor_id')->constrained('users')->cascadeOnDelete()->nullable();
            $table->unsignedBigInteger('session_id')->nullable();
            $table->text('report')->nullable();
            $table->text('extra')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_reports');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session_reports', function (Blueprint $table) {
            $table->renameColumn('report', 'reflections');
            $table->dropColumn('title');
            $table->text('warm_up')->nullable();
            $table->text('challenges')->nullable();
            $table->text('goal_setting')->nullable();
            $table->text('commitments')->nullable();
        });
    }
};

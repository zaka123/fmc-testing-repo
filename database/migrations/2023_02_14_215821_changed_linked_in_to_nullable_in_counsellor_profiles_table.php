<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('counsellor_profiles', function (Blueprint $table) {
            $table->string('interested_in', 500)->nullable()->change();
            $table->string('resume_upload', 500)->nullable()->change();
            $table->string('linked_in', 500)->nullable()->change();
            $table->string('phone_number')->nullable()->change();
            $table->longText('career_couch', 500)->nullable()->change();
            $table->string('coaching_certification')->nullable()->change();
            $table->string('date_of_birth')->nullable()->change();
            $table->string('country')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counsellor_profiles', function (Blueprint $table) {
            //
        });
    }
};

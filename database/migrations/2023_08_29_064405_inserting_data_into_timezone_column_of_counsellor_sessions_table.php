<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Specify the table name and column name
        $tableName = 'counsellor_sessions';
        $columnName = 'timezone';

        // Specify the default value you want to set
        $defaultValue = 'UTC';

        // Update the existing rows where the column is null
        DB::table($tableName)->whereNull($columnName)->update([$columnName => $defaultValue]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counsellor_sessions', function (Blueprint $table) {
            //
        });
    }
};

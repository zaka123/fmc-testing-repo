<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_payments', function (Blueprint $table) {
            $table->id();
            $table->string('bank')->nullable()->index();
            $table->string('branch')->nullable();
            $table->string('transaction_id')->nullable()->index();
            $table->string('amount')->nullable();
            $table->string('user_counsellor_sessions_id',500)->nullable()->index();
            $table->string('session_id',500)->nullable()->index();
            $table->foreignId('student_id')->constrained('users')->cascadeOnDelete();
            $table->string('receipt')->nullable();
            $table->string('status')->default('Pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_payments');
    }
};

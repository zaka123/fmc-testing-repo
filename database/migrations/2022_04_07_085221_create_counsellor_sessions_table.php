<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counsellor_sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('counsellor_id')->constrained('users')->cascadeOnDelete();
            $table->date('session_date')->nullable()->index();
            $table->string('session_timing')->nullable()->index();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counsellor_sessions');
    }
};

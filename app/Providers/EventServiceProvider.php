<?php

namespace App\Providers;

use App\Events\CounsellorRegistered;
use App\Events\SessionPaymentApproved;
use App\Events\SessionPaymentCreated;
use App\Events\UserContactedUs;
use App\Listeners\CreateCounsellorProfile;
use App\Listeners\CreateZoomMeeting;
use App\Listeners\ForwardContactUsForm;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class             => [
            SendEmailVerificationNotification::class,
        ],
        CounsellorRegistered::class   => [
            CreateCounsellorProfile::class,
        ],
        UserContactedUs::class        => [
            ForwardContactUsForm::class,
        ],
        SessionPaymentApproved::class => [
            CreateZoomMeeting::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}

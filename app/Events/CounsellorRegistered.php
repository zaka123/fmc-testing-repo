<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class CounsellorRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The user instance.
     *
     * @var \App\Models\User  $user
     */
    public $user;

     /**
      * The request instance.
      *
      * @var Illuminate\Http\Request  $request
      */
    public $request;

    /**
     * The resume file path.
     *
     * @var $resume
     */
    public $resume;

    /**
     * Create a new event instance.
     *
     * @param  array $data[$request, $user, $resume, $resume]
     * @return void
     */
    public function __construct(Request $request, User $user, $resume)
    {
        $this->user = $user;
        $this->request = $request;
        $this->resume = $resume;
    }
}

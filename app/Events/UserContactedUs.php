<?php

namespace App\Events;

use App\Models\ContactUs;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserContactedUs
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The order instance.
     *
     * @var \App\Models\ContactUs
     */
    public $contactUsForm;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ContactUs $contactUsForm)
    {
        $this->contactUsForm = $contactUsForm;
    }
}

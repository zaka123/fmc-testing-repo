<?php

namespace App\Events;

use App\Models\SessionPayment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SessionPaymentApproved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The session payment instance.
     *
     * @var \App\Models\SessionPayment
     */
    public $sessionPayment;

    /**
     * Create a new event instance.
     *
     * @param  \App\Models\SessionPayment
     * @return void
     */
    public function __construct(SessionPayment $sessionPayment)
    {
        $this->sessionPayment = $sessionPayment;
    }
}

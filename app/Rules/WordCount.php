<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class WordCount implements Rule
{
    /**
     * The minimum number of words.
     *
     * @var int
     */
    protected $min = 50;

    /**
     * The maximum number of words.
     *
     * @var int
     */
    protected $max = 250;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $options = [])
    {
        ($options["min"] ?? '') ? $this->min = $options["min"] : '';
        ($options["max"] ?? '') ? $this->max = $options["max"] : '';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $word_count = str_word_count($value);
        return $word_count >= $this->min && $word_count <= $this->max;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be between ' . $this->min . ' and '. $this->max . ' words.';
    }
}

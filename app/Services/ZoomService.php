<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserCounsellorSession;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use MacsiDigital\Zoom\Facades\Zoom;


class ZoomService
{
    const BASIC_ACCOUNT_TYPE = 1;

    /**
     * Create basic zoom profile for requested user
     *
     * @return bool
     */
    public static function createProfile(User $user)
    {
        try {
            return json_decode(
                Zoom::user()->create(
                    [
                        'first_name' => $user->name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                        'password' => 'secret1234',
                        'type' => self::BASIC_ACCOUNT_TYPE
                    ]
                )
            );
        } catch (\Exception $e) {
            return json_decode($e);
        }
    }

    /**
     * Create & save meeting in counsellor profile
     *
     * @param \App\Models\User $counsellor
     * @param $start_time
     */
    public static function arrangeMeeting($email, $start_time, $timezone = 'UTC')
    {
        $bookedTiming = Zoom::meeting()->make(
            [
                'topic' => sprintf("%s One-to-One Session", ucwords(env("APP_NAME"))),
                'type' => 1,
                'duration' => UserCounsellorSession::userCounsellorSessionDuration,
                'start_time' => $start_time,
                'timezone' => $timezone,
            ]
        );

        try {
            return json_decode(
                Zoom::user()
                    ->find($email)
                    ->meetings()
                    ->save($bookedTiming)
            );
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find user by email
     *
     * @param string $email
     */
    public static function findUserByEmail($email)
    {
        return Zoom::user()->find($email);
    }
}

<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\ZoomToken;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;


class OAuthZoomService
{
    // const BASIC_ACCOUNT_TYPE = 1;

    /**
     * Create & save meeting in counsellor profile
     *
     * @param \App\Models\User $counsellor
     * @param $start_time
     */
    public function arrangeMeeting($user_id, $start_time, $timezone = 'UTC')
    {
        // $zoomAccessToken = $this->getFreshAccessToken($user_id);
        $zoomAccessToken = $this->getGeneralizedZoomToken();
        if(!$zoomAccessToken){
            return null;
        }

        $meetingData = [
                'topic' => sprintf("%s One-to-One Session", ucwords(env("APP_NAME"))),
                'type' => 2,
                'duration' => UserCounsellorSession::userCounsellorSessionDuration,
                'start_time' => $start_time,
                'timezone' => $timezone,
                'password' => 'secret1234',
            ];

        try {
            // Make a POST request to create a meeting
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $zoomAccessToken, // Replace with your Zoom API access token
            ])->post('https://api.zoom.us/v2/users/me/meetings', $meetingData);

            $responseData = $response->json();

            // Check if the meeting was successfully created
            if ($response->successful() && !empty($responseData['id'])) {
                $meetingId = $responseData['id'];
                // You can store the meetingId in your database or use it as needed.
                return $responseData;
            }
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    public function meetingsList()
    {
        $accessToken = $this->getSavedAccessToken();
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get('https://api.zoom.us/v2/users/me/meetings');

        $data = $response->json();

        // Handle the response data here
        // dd($data);
        return $data;
    }

    public function getAccessToken($request)
    {
        $url = "https://zoom.us/oauth/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'grant_type'    => 'authorization_code',    # https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/
            'code'         => $request->code,
            'redirect_uri'         => config('services.zoom.redirect'),
        )));

        $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_CLIENT_ID') . ":" . getenv('ZOOM_CLIENT_SECRET'));
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $reponse = curl_exec($ch);
        $data = json_decode($reponse, true); // token will be with in this json

        // dd($data['access_token']);
        return $data;
    }

    public function refreshAccessToken($refreshToken)
    {
        $url = "https://zoom.us/oauth/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'grant_type'    => 'refresh_token',    # https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/
            'refresh_token'         => $refreshToken,
        )));

        // $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_CLIENT_ID') . ":" . getenv('ZOOM_CLIENT_SECRET'));
        $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_SDK_ID') . ":" . getenv('ZOOM_SDK_SECRET'));
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $reponse = curl_exec($ch);
        $data = json_decode($reponse, true); // token will be with in this json

        // dd($data['access_token']);
        return $data;
    }

    public function getUserInfo($user_id)
    {
        $accessToken = $this->getFreshAccessToken($user_id);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get('https://api.zoom.us/v2/users/me');

        $data = $response->json();

        // Handle the response data here
        // dd($data);
        return $data;
    }

    // public function getUserZAK($user_id)
    public function getUserZAK()
    {
        // $accessToken = $this->getFreshAccessToken($user_id);
        $accessToken = $this->getGeneralizedZoomToken();
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get('https://api.zoom.us/v2/users/me/zak');

        $data = $response->json();

        // Handle the response data here
        // dd($data);
        return $data;
    }

    public function getSavedAccessToken()
    {
        $zoomAccessToken = auth()->user()->zoomToken->access_token; // later this will be changed to counsellor zoom token
        return $zoomAccessToken;
    }

    public function getFreshAccessToken($user_id)
    {
        $data=ZoomToken::where('user_id', $user_id)->first();
        if(!$data){
            return null;
        }
        $response = $this->refreshAccessToken($data->refresh_token);
        $accessToken = $response['access_token'];
        return $accessToken;
    }

    public function getGeneralizedZoomToken(){
        $zoom = ZoomToken::orderBy('id', 'desc')->first();
        if(!$zoom){
            return null;
        }
        $response = $this->refreshAccessToken($zoom->refresh_token);
        $accessToken = $response['access_token'];
        return $accessToken;
    }
}

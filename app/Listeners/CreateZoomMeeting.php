<?php

namespace App\Listeners;

use App\Events\SessionPaymentApproved;
use Illuminate\Http\Client\ConnectionException;
use App\Mail\NewSessionMail;
use App\Models\VideoCall;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use MacsiDigital\API\Exceptions\HttpException;
use MacsiDigital\Zoom\Facades\Zoom;

class CreateZoomMeeting
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\SessionPaymentApproved $event
     * @return void
     */
    public function handle(SessionPaymentApproved $event)
    {
        $userCounsellorSession = $event->sessionPayment->userCounsellorSession;

        // if payment is made for a session & payment status is confirmed
        if ($userCounsellorSession && $event->sessionPayment->status == "Confirm") {
            $student = $event->sessionPayment->user;
            $counsellor = $userCounsellorSession->counsellor;

            // the counsellor session instance
            $session = $userCounsellorSession->session;

            /**
             * Check for student's zoom profile
             * if has not one, create one
             *
             * @return $profile
             */
            try {
                $studentZoomProfile = Zoom::user()->find($student->email);

                if (! $studentZoomProfile) {
                    $createdProfile = VideoCall::createZoomProfile($student);
                    if (! $createdProfile) {
                        return;
                    }
                }
            } catch (ConnectionException $e) {
                toastr()->error($e->getMessage());
                return;
            } catch (HttpException $e) {
                $createdProfile = VideoCall::createZoomProfile($student);
                if (! $createdProfile) {
                    return;
                }
            }


            /**
             * Check for counsellor's zoom profile
             * if has not one, create one
             *
             * @return $profile
             */
            try {
                $counsellorZoomProfile = Zoom::user()->find($counsellor->email);

                if (! $counsellorZoomProfile) {
                    $createdProfile = VideoCall::createZoomProfile($counsellor);
                    if (! $createdProfile) {
                        return;
                    }
                }
            } catch (ConnectionException $e) {
                toastr()->error($e->getMessage());
                return;
            } catch (HttpException $e) {
                $createdProfile = VideoCall::createZoomProfile($counsellor);
                if (! $createdProfile) {
                    return;
                }
            }


            // arrange a zoom meeting on given schedule
            $bookedTiming = VideoCall::arrangeZoomMeeting($session);

            // save the booked time to counsellor zoom profile
            $meeting = Zoom::user()->find($counsellor->email)->meetings()->save($bookedTiming);
            $decodedMeetingObject = json_decode($meeting);

            // save meeting info to db
            VideoCall::create(
                [
                'user_id'                       => $student->id,
                'counsellor_id'                 => $counsellor->id,
                'user_counsellor_session_id'    => $userCounsellorSession->id,
                'start_url'                     => $decodedMeetingObject->start_url,
                'join_url'                      => $decodedMeetingObject->join_url,
                ]
            );

            // send notification mail to both student and counsellor
            $mail_data = [
                'account_url' => route("counsellor.session.bookings"),
                'host' => $counsellor->full_name,
                'participant' => $student->full_name
            ];
            Mail::to($counsellor->email)->send(new NewSessionMail($mail_data));

            $mail_data = [
                'account_url' => route("user.session.index"),
                'host' => $student->full_name,
                'participant' => $counsellor->full_name
            ];
            Mail::to($student->email)->send(new NewSessionMail($mail_data));
        }
    }
}

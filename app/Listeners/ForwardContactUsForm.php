<?php

namespace App\Listeners;

use App\Events\UserContactedUs;
use App\Mail\UserContactedUsMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ForwardContactUsForm
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  App\LEvents\UserContactedUs $event
     * @return void
     */
    public function handle(UserContactedUs $event)
    {
        Mail::to(env("CONTACT_ADDRESS"))->send(new UserContactedUsMail($event->contactUsForm));
    }
}

<?php

namespace App\Listeners;

use App\Events\CounsellorRegistered;
use App\Models\CounsellorProfile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateCounsellorProfile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CounsellorRegistered $event
     * @return void
     */
    public function handle(CounsellorRegistered $event)
    {
        CounsellorProfile::create(
            [
            'interested_in'             =>  $event->request->interested_in,
            'resume_upload'             =>  $event->resume,
            'linked_in'                 =>  $event->request->linked_in,
            'career_couch'              =>  $event->request->career_couch,
            'coaching_certification'    =>  $event->request->coaching_certification,
            'date_of_birth'             =>  $event->request->date_of_birth,
            'city'                      =>  $event->request->city,
            'user_id'                   =>  $event->user->id,
            'phone_number'              =>  $event->request->phone_number,
            ]
        );
    }
}

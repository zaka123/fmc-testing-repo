<?php

namespace App\Actions\Fortify;

use App\Models\User;
// use Laravel\Jetstream\Jetstream;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;
use Twilio\Rest\Client;

// use Spatie\Permission\Models\Permission;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make(
            $input,
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => $this->passwordRules(),
                'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
            ],[
                'name.required' => 'Name is required',
                'email.required' => 'Email is required',
                'password.required' => 'Password is required',
                'password.confirmed' => 'Passwords do not match',
            ]
        )->validate();
        $user = User::create(
            [
                'name' => $input['name'],
                'email' => $input['email'],
                'profession' => 'Professional',
                'password' => Hash::make($input['password']),
                'password_changed' => true,
            ]
        );



        $role = Role::where('name', 'User')->first();
        $user->assignRole($role->name);
        return $user;
    }

    public function createOld(array $input)
    {
        Validator::make(
            $input,
            [
                'role' => ['required', 'string'],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:250'],
                'gender' => ['required', 'in:Male,Female,Prefer not to say'],
                'date_of_birth' => ['required'],
                'city' => ['required', 'exists:countries,name'],
                'favorite_subject' => ['sometimes', 'required'],
                'easiest_subject' => ['sometimes', 'required'],
                'difficult_subject' => ['sometimes', 'required'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'string', 'min:10', 'max:20', 'unique:users'],
                'verification' => ['required', 'string'],
                'password' => $this->passwordRules(),
                // 'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
                'education_level' => ['sometimes'],
                'experience' => ['sometimes'],
                'employment' => ['sometimes'],
                'organization_name' => ['sometimes'],
                'designation' => ['sometimes'],
            ],
            [
                'captcha.captcha' => 'Invalid captcha code.',
                'city.*' => 'The country field is required',

            ]
        )->validate();
        $user = User::create(
            [
                'name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'phone' => $input['phone'],
                'verification' => $input['verification'],
                'gender' => $input['gender'],
                'profession' => $input['role'],
                'password' => Hash::make($input['password']),
                'password_changed' => true,
            ]
        );

        if ($input['verification'] == 'phone') {
            $user->markEmailAsVerified();
            try {
                $token = getenv("TWILIO_AUTH_TOKEN");
                $twilio_sid = getenv("TWILIO_ACCOUNT_SID");
                $twilio_verify_sid = getenv("TWILIO_SERVICE_SID");
                $twilio = new Client($twilio_sid, $token);
                $twilio->verify->v2->services($twilio_verify_sid)
                    ->verifications
                    ->create($input['phone'], "sms");
            } catch (\Throwable $th) {
                $user->forceDelete();
                throw $th;
                // throw new \Exception('Phone number verification failed. Please try email verification.');
            }
        }

        if ($input['role'] == 'Professional') {
            $user->userProfessional()->create(
                [
                    'date_of_birth' => $input["date_of_birth"],
                    'city' => $input["city"],
                    'experience' => $input["experience"],
                    'education_level' => $input["education_level"],
                    'organization_name' => $input["organization_name"],
                    'role' => $input["designation"],
                ]
            );
        } else {
            $user->userProfile()->create(
                [
                    'date_of_birth' => $input["date_of_birth"],
                    'city' => $input["city"],
                    'favorite_subject' => $input["favorite_subject"],
                    'easiest_subject' => $input["easiest_subject"],
                    'difficult_subject' => $input["difficult_subject"],
                ]
            );
        }

        $role = Role::where('name', 'User')->first();
        $user->assignRole($role->name);
        return $user;
    }
}

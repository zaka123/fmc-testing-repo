<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserProfessional;

class UserProfessionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'date_of_birth' => 'required',
                'city' => 'required|max:250',
                'experience' => 'required|max:250',
                'education_level' => 'required|max:250',
                'organization_name' => 'required|max:250',
                'role' => 'required|max:250'
            ]
        );
        $profile = UserProfessional::create(
            [
                'user_id' => auth()->id(),
                'date_of_birth' => $request->date_of_birth,
                'city' => $request->city,
                'experience' => $request->experience,
                'education_level' => $request->education_level,
                'organization_name' => $request->organization_name,
                'role' => $request->role
            ]
        );
        if ($profile) {
            toastr()->success('User Information Updated.');
            return redirect('dashboard');
        }
        toastr()->success('User Information Updation failed.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserProfessional $userProfessional
     * @return \Illuminate\Http\Response
     */
    public function show(UserProfessional $userProfessional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserProfessional $userProfessional
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userProfessional = UserProfessional::where('user_id', $id)->first();
        $user = User::where('id', $id)->first();
        $countryNames = Country::names();
        return response()->view('users.professional.edit', compact('userProfessional', 'user', 'countryNames'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param  \App\Models\UserProfessional $userProfessional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userProfessional)
    {
        $request->validate(
            [
                'email' => 'required|email|unique:users,email,' . auth()->id(),
                'name' => 'required|max:230',
                'last_name' => 'required|max:230',
                'gender' => 'required|max:230',
                'profession' => 'required|max:230',
                'date_of_birth' => 'required|max:230',
                'city' => 'required|max:230',
                'experience' => 'required|max:230',
                'education_level' => 'required|max:230',
                'role' => 'required|max:230',
                'organization_name' => 'required|max:230',
            ]
        );
        $professional = UserProfessional::updateOrCreate(
            ['user_id' => auth()->id()],
            [
                'date_of_birth' => $request->date_of_birth,
                'city' => $request->city,
                'experience' => $request->experience,
                'education_level' => $request->education_level,
                'organization_name' => $request->organization_name,
                'role' => $request->role
            ]
        );
        $user = (new User())->updateUser($request);
        if ($professional) {
            toastr()->success('User Information Updated.');
            return redirect('dashboard');
        }
        toastr()->success('User Information Updation failed.');
        return redirect()->back();
    }

    public function updateProfessional(Request $request, $id)
    {
        $request->validate(
            [
                'email' => 'required|email|unique:users,email,' . $id,
                'name' => 'required|max:230',
                'last_name' => 'required|max:230',
                'gender' => 'required|max:230',
                'profession' => 'required|max:230',
                'date_of_birth' => 'required|max:230',
                'city' => 'required|max:230',
                'experience' => 'required|max:230',
                'education_level' => 'required|max:230',
                'role' => 'required|max:230',
                'organization_name' => 'required|max:230',
            ]
        );

        $professional = UserProfessional::updateOrCreate(
            ['user_id' => $id],
            [
                'date_of_birth' => $request->date_of_birth,
                'city' => $request->city,
                'experience' => $request->experience,
                'education_level' => $request->education_level,
                'organization_name' => $request->organization_name,
                'role' => $request->role
            ]
        );
        if ($professional) {
            $user = User::where('id', $id)->update(
                [
                    'name' => $request->input('name'),
                    'last_name' => $request->input('last_name'),
                    'email' => $request->input('email'),
                    'gender' => $request->input('gender'),
                    'profession' => $request->input('profession'),
                ]
            );
            toastr()->success('User Information Updated.');
            return redirect('dashboard');
        }
        toastr()->success('User Information Updating failed.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserProfessional $userProfessional
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        toastr()->success('User account deleted permenantly.');
        return redirect('/');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateCounsellorProfileRequest;
use App\Models\Country;
use App\Models\User;
use App\Traits\GenericTrait;
use Illuminate\Http\Request;

class CounsellorProfileController extends Controller
{
    use GenericTrait;

    public function main()
    {
        return view('users.counsellor.main');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counsellors = User::latestCounsellors();
        $profile = $counsellors->first();

        return response()->view(
            'users.counsellor.index',
            compact(
                'counsellors',
            )
        );
    }

    /**
     * getting the counsellor profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCounsellor($id)
    {
        $profile = User::role('Counsellor')->with(['counsellorProfile', 'counsellorSessions'])
            ->where('id', $id)->first();
        if (auth()->user()->hasRole('User')) {
            return response()->view('users.counsellor.profile-session', compact('profile'));
        } elseif (auth()->user()->hasRole('Super Admin')) {
            return response()->view('admin.counsellors.profile-session', compact('profile'));
        } else {
            toastr()->warning('You are not allowed to Action');
            return response()->redirect('dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CounsellorProfile $counsellorProfile
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $counsellorProfile = [
            'profile' => auth()->user()->counsellorProfile,
            'counsellor' => auth()->user(),
        ];

        return response()->view(
            'counsellor.profile.view',
            compact(
                'counsellorProfile',
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CounsellorProfile $counsellorProfile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $countries = Country::names();
        $countryDialCodes = Country::orderedDialCodes();
        $counsellorProfile = [
            'profile' => auth()->user()->counsellorProfile,
            'counsellor' => auth()->user(),
        ];
        // dd($counsellorProfile);
        return response()->view(
            'counsellor.profile.edit',
            compact(
                'counsellorProfile',
                'countries',
                'countryDialCodes',
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request      $request
     * @param  \App\Models\CounsellorProfile $counsellorProfile
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCounsellorProfileRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $inputDomain = $validated['linked_in'];
        // Check if the input domain starts with "http://" or "https://"
        if (strpos($inputDomain, "http://") === 0 || strpos($inputDomain, "https://") === 0) {
            $validated['linked_in'] = $inputDomain;
        } else {
            $validated['linked_in'] = "https://" . $inputDomain;
        }

        $counsellor = auth()->user();

        // update personal data
        $counsellor->update($validated);

        // update counsellor profile
        $counsellor->profile->update($validated);

        toastr()->success('Counsellor Information Updated');

        return redirect('dashboard');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        toastr()->success('User acount deleted permenantly.');
        return redirect('/');
    }

    public function changeProfilePicture(Request $request)
    {
        $request->validate(
            [
                'image_base64' => 'required',
            ]
        );

        $user = User::find(auth()->id());
        if ($user) {
            // Delete the old profile photo if it exists
            if ($user->profile_photo_path) {
                $this->deleteProfilePhoto($user->profile_photo_path);
            }

            // Store the new profile photo
            $input['profile_photo_path'] = $this->storeBase64($request->image_base64);
            $user->update($input);

            toastr()->success('Profile photo updated successfully.');
            return redirect()->route('dashboard');
        } else {
            toastr()->error('Something went wrong.');
        }
        return redirect()->back();
    }

    private function storeBase64($imageBase64)
    {
        list($type, $imageBase64) = explode(';', $imageBase64);
        list(, $imageBase64)      = explode(',', $imageBase64);
        $imageBase64 = base64_decode($imageBase64);
        // $imageName = time() . '.png';
        $imageName = uniqid() . '.png';
        $path = storage_path('app/public/profile-photos/') . $imageName;

        file_put_contents($path, $imageBase64);

        // return $imageName;
        return 'profile-photos/' . $imageName;
    }

    // Delete the profile photo
    private function deleteProfilePhoto($path)
    {
        $fullPath = storage_path('app/public/' . $path);

        if (file_exists($fullPath)) {
            unlink($fullPath);
        }
    }
}

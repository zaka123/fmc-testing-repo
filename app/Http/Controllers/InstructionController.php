<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInstructionRequest;
use App\Http\Requests\UpdateInstructionRequest;
use App\Models\Instruction;
use App\Models\Test;

class InstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructions = Instruction::all()
            ->groupBy('test_id');
        $tests = Test::all();
        return view('admin.instructions.index', compact('instructions', 'tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.instructions.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInstructionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInstructionRequest $request)
    {
        $instruction = Instruction::create($request->all());
        if ($instruction) {
            toastr()->success('Instruction saved successfully');
            return redirect()->route('admin.instructions.index');
        }

        toastr()->error('Something went wrong. Please try again later.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Instruction $instruction
     * @return \Illuminate\Http\Response
     */
    public function show(Instruction $instruction)
    {
        return view(
            "admin.instructions.view",
            compact(
                "instruction",
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Instruction $instruction
     * @return \Illuminate\Http\Response
     */
    public function edit(Instruction $instruction)
    {
        $tests = Test::all();
        return view(
            "admin.instructions.edit",
            compact(
                "instruction",
                "tests",
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInstructionRequest $request
     * @param  \App\Models\Instruction                     $instruction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInstructionRequest $request, Instruction $instruction)
    {
        $updated = $instruction->update($request->all());

        if ($updated) {
            toastr()->success("Record updated successfully.");
            return redirect(route("admin.instructions.index"));
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect(route("admin.instructions.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Instruction $instruction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instruction $instruction)
    {
        $instruction->delete();
        toastr()->success('Record deleted successfully.');
        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  \App\Models\Instruction $instruction
     * @return \Illuminate\Http\Response
     */
    public function restore(Instruction $instruction)
    {
        $instruction->restore();
        toastr()->success('Record restored successfully.');
        return redirect()->back();
    }
}

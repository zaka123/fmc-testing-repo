<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Models\Faq;
use Illuminate\Support\Facades\Auth;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::all();
        return view(
            'admin.faqs.index',
            compact(
                "faqs",
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.faqs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFaqRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFaqRequest $request)
    {
        $faq = Faq::create(
            [
            'question'  => $request->question,
            'answer'    => $request->answer,
            ]
        );

        if ($faq) {
            toastr()->success("Frequently Asked Question added successfully.");
            return redirect()->back();
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        return view(
            "admin.faqs.view",
            compact(
                "faq",
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        return view(
            "admin.faqs.edit",
            compact(
                "faq",
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFaqRequest $request
     * @param  \App\Models\Faq                     $faq
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFaqRequest $request, Faq $faq)
    {
        $done = $faq->update(
            [
            'question' => $request->question,
            'answer' => $request->answer
            ]
        );

        if ($done) {
            toastr()->success("Record updated successfully.");
            return redirect()->route('admin.faqs.index');
        }

        toastr()->error("Something went wrong. Please try again.");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();
        toastr()->success('Frequently Asked Question deleted successfully.');
        return redirect()->back();
    }
}

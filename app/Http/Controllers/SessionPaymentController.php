<?php

namespace App\Http\Controllers;

use App\Mail\NewSessionMail;
use App\Mail\PaymentSessionMail;
use App\Models\CounsellorSession;
use App\Models\Coupon;
use App\Models\SessionPayment;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\VideoCall;
// use App\Services\ZoomService;
use App\Services\OAuthZoomService;
use App\Traits\GenericTrait;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Exception;
use Stripe;
use Carbon\Carbon;

/**
 * Summary of SessionPaymentController
 */
class SessionPaymentController extends Controller
{
    use GenericTrait;

    protected $zoomService;

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct(OAuthZoomService $zoomService)
    {
        $this->zoomService = $zoomService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = auth()->user()->payments();

        return response()->view(
            'users.payments.index',
            compact(
                'payments',
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('users.payments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'bank' => 'required',
                'transaction_id' => 'required',
                'session' => 'required',
                'receipt' => 'required|image',
                'amount' => 'required',
                'user_counsellor_session_id' => 'required',
            ],
            [
                'session.*' => 'You must add payment for a specific session.',
                'user_counsellor_session_id.*' => 'You must add payment for a specific session.'
            ]
        );

        // upload file
        $photo = $this->uploadMedia($request->receipt);

        // reserve session for this user
        if ($request->session != "Test Payment") {
            $session = UserCounsellorSession::create(
                [
                    'user_id' => auth()->user()->id,
                    'counsellor_id' => $request->counsellor_id,
                    'session_id' => $request->session,
                    'status' => 'Confirm',
                ]
            );
        }

        $paymentCreated = SessionPayment::create(
            [
                'user_counsellor_sessions_id' => $session->id ?? $request->user_counsellor_session_id,
                'bank' => $request->bank,
                'transaction_id' => $request->transaction_id,
                'branch' => $request->branch,
                'session_id' => $request->session,
                'student_id' => auth()->user()->id,
                'status' => 'Pending',
                'amount' => $request->amount,
                'receipt' => $photo,
            ]
        );

        if ($paymentCreated) {
            toastr()->success('Payment created successfully.');

            if ($request->session != "Test Payment") {
                return redirect()->route("user.session.index");
            }

            return redirect()->back();
        }

        toastr()->warning('Payment creation failed.');
        return redirect()->back();
    }

    /**
     * Stripe payment
     *
     * @param \Illuminate\Http\Request $request
     */
    public function stripePayment(Request $request)
    {
        try {
            $request->validate(
                [
                    'stripe_name' => 'required',
                    'session' => 'required',
                    // 'card_number' => 'required|numeric|digits_between:14,20',
                    // 'ccv' => 'required|numeric|digits_between:3,4',
                    // 'expiry_month' => 'required',
                    // 'expiry_year' => 'required',
                    'stripeToken' => 'required',
                    'user_counsellor_session_id' => 'required',
                ],
                [
                    'stripe_name.*' => 'Please write the name on card.',
                    'session.*' => 'You Must add payment for a specific session.',
                    'stripeToken.*' => 'Please try again with a stable Internet connection, and your browser should support Javascript.',
                    'user_counsellor_session_id.*' => 'You Must add payment for a specific session.'
                ]
            );
            if ($request->session != "Test Payment") {
                $counsellorSession = CounsellorSession::findOrFail($request->session);
                // get meeting start time
                $meetingTime = $counsellorSession->date . ' ' . $counsellorSession->time;

                // convert start_time to UTC
                $start_time = (new DateTime($meetingTime, new DateTimeZone($counsellorSession->users->timezone)))->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d\TH:i:s') ?? null;

                // create meeting
                $meeting = $this->zoomService->arrangeMeeting($counsellorSession->users->id, $start_time, 'UTC');

                if (is_null($meeting)) {
                    toastr()->error("Something went wrong in creating meeting. Please try again later.");
                    return redirect()->back();
                }
            }

            $curreny = 'usd';
            if (session()->has('currency')) {
                $curreny = session()->get('currency');
                // convert currency to lowercase
                $curreny = strtolower($curreny);
            }

            $coupon_ID = null;
            if ($request->coupon_code_applied == "yes") {
                $coupon = Coupon::where('coupon_code', $request->coupon_code)->first();
                $coupon_ID = $coupon->id;
                if (!is_null($coupon->max_redemptions)) {
                    $coupon->update([
                        'max_redemptions' => $coupon->max_redemptions - 1,
                    ]);
                }
            }

            $payment = null;
            if ($request->coupon_code_applied == "yes" && $request->total_amount  < 1) {
                $payment = null;
            } else {
                $api = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $payment = Stripe\Charge::create(
                    [
                        "amount" => 100 * ($request->total_amount ?? 20),
                        "currency" => $curreny,
                        "source" => $request->stripeToken,
                        "description" => "Payment."
                    ]
                );
            }

            // reserve session for this user
            if ($request->session != "Test Payment") {
                $session = UserCounsellorSession::create(
                    [
                        'user_id' => auth()->user()->id,
                        'counsellor_id' => $request->counsellor_id,
                        'session_id' => $request->session,
                        'status' => 'Confirm',
                    ]
                );

                // save meeting info to db
                if (isset($meeting) && !is_null($meeting)) {
                    VideoCall::create(
                        [
                            'user_id' => auth()->id(),
                            'counsellor_id' => $request->counsellor_id,
                            'user_counsellor_session_id' => $session->id,
                            'start_url' => $meeting['start_url'],
                            'join_url' => $meeting['join_url'],
                            'meeting_id' => $meeting['id'],
                        ]
                    );
                }
            }

            if ($payment == null) {
                $paymentCreated = SessionPayment::create(
                    [
                        'user_counsellor_sessions_id' => $session->id ?? $request->user_counsellor_session_id,
                        'bank' => 'Debit/Credit Card',
                        'transaction_id' => uniqid('100%d_'),
                        'branch' => uniqid('100%d_'),
                        'session_id' => $request->session,
                        'student_id' => auth()->id(),
                        'status' => 'Confirm',
                        'amount' => $request->total_amount ?? 20,
                        'coupon_id' => $coupon_ID,
                        'currency' => $curreny
                    ]
                );
            } else {
                $paymentCreated = SessionPayment::create(
                    [
                        'user_counsellor_sessions_id' => $session->id ?? $request->user_counsellor_session_id,
                        'bank' => 'Debit/Credit Card',
                        'transaction_id' => $payment->id,
                        'branch' => $payment->source->fingerprint,
                        'session_id' => $request->session,
                        'student_id' => auth()->id(),
                        'status' => 'Confirm',
                        'amount' => $request->total_amount ?? 20,
                        'coupon_id' => $coupon_ID,
                        'currency' => $curreny
                    ]
                );
            }

            if ($paymentCreated) {
                $counsellor = User::findOrFail($request->counsellor_id);
                // send notification mail to both student and counsellor
                $mail_data = [
                    'account_url' => route("counsellor.session.bookings"),
                    'host' => $counsellor->full_name,
                    'participant' => auth()->user()->full_name
                ];
                Mail::to($counsellor->email)->send(new NewSessionMail($mail_data));

                $mail_data = [
                    'account_url' => route("user.session.index"),
                    'host' => auth()->user()->full_name,
                    'participant' => $counsellor->full_name
                ];
                Mail::to(auth()->user()->email)->send(new NewSessionMail($mail_data));
                toastr()->success('Payment created successfully.');

                if ($request->session != "Test Payment") {
                    return redirect()->route("user.session.index");
                }

                return redirect()->route("user.test.instructions", $request->user_counsellor_session_id);
            }

            toastr()->warning('Payment creation Failed.');
            return redirect()->back();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            toastr()->warning('Please try with a fresh start.');
            return redirect()->back();
        } catch (Exception $e) {
            toastr()->warning('Please try again.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SessionPayment $payment)
    {
        if ($payment->status != "Confirm") {
            if ($payment->session_id != "Test Payment") {
                $payment->userCounsellorSession->delete();
            }

            $payment->delete();
            toastr()->success("Payment record deleted successfully.");
            return redirect()->back();
        }

        toastr()->warning("Confirmed payments can't be deleted.");
        return redirect()->back();
    }


    public function apply_coupon_code(Request $request)
    {
        $curreny = 'USD';
        if (session()->has('currency')) {
            $curreny = session()->get('currency');
        }

        $totalPrice = $request->amount;
        $totalPrice = str_replace(',', '', $totalPrice);
        $totalPrice = (float)$totalPrice;

        $coupon = DB::table('coupons')
            ->where(DB::raw('BINARY `coupon_code`'), $request->coupon_code)
            ->where('currency_type', $curreny)
            ->first();

        if ($coupon) {
            $value = $coupon->discount;
            $type = $coupon->discount_type;

            if ($coupon->category == 'sessionPayment') {
                if ($coupon->status == 1) {

                    if ($coupon->expired == 1 || ($coupon->redeem_by_date != '' && $coupon->redeem_by_date < Carbon::now()->format("Y-m-d"))) {
                        $status = "error";
                        $msg = "Invalid Coupon Code";
                    } else {
                        if ($coupon->max_redemptions == null) {
                            $status = "success";
                            $msg = "Coupon Applied";
                        } else {
                            if ($coupon->max_redemptions <= 0) {
                                $status = "error";
                                $msg = "Invalid Coupon Code";
                            } else {
                                $status = "success";
                                $msg = "Coupon Applied";
                            }
                        }
                    }
                } else {
                    $status = "error";
                    $msg = "Invalid Coupon Code";
                }
            } else {
                $status = "error";
                $msg = "Invalid Coupon Code";
            }
        } else {
            $status = "error";
            $msg = "Invalid Coupon Code";
        }

        $discount = 0;
        if ($status == 'success') {
            if ($type == 'Fixed') {
                if ($value > $totalPrice) {
                    $status = "error";
                    $msg = "Invalid Coupon Code";

                    return response()->json([
                        'status' => $status,
                        'msg' => $msg
                    ]);
                } else {
                    $totalPrice = $totalPrice - $value;
                    $discount = $value;
                }
            }
            if ($type == 'Percent') {
                if ($value > 100) {
                    $status = "error";
                    $msg = "Invalid Coupon Code";

                    return response()->json([
                        'status' => $status,
                        'msg' => $msg
                    ]);
                } else {
                    $newPrice = ($value / 100) * $totalPrice;
                    $totalPrice = round($totalPrice - $newPrice);
                    $discount = $newPrice;
                }
            }
        }

        return response()->json([
            'status' => $status,
            'msg' => $msg,
            'totalPrice' => number_format($totalPrice, 2),
            'currency' => $curreny,
            'discount' => number_format($discount, 2)
        ]);
    }
}

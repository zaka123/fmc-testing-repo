<?php

namespace App\Http\Controllers;

use App\Models\Timezone;
use Illuminate\Http\Request;

class UserTimezoneController extends Controller
{
    // public function index()
    // {
    //     $timezones = Timezone::all();
    // }
    public function updateUserTimezone(Request $request, $id)
    {
        auth()->user()->timezone = $request->timezone;
        auth()->user()->save();
        toastr()->success("Timezone Updated Successfully.");
        return redirect()->back();
    }
}

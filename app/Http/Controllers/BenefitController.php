<?php

namespace App\Http\Controllers;

use App\Models\Benefit;
use Exception;
use Illuminate\Http\Request;

class BenefitController extends Controller
{
    public function getData($slug)
    {
        $benefit = Benefit::where('title', strtoupper($slug))->first();
        // remove html tags from description
        // $benefit->description = strip_tags($benefit->description);
        return $benefit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = Benefit::orderBy('id', 'desc')->get();
        return view('admin.benefit.index', compact('benefits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.benefit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate(
            [
            'title' => 'required|max:250|string',
            'description' => 'string|required',
            'favoured_careers' => 'required|string',
            'disFavoured_career' => 'required|string'
            ]
        );
        try {
            $benefit = Benefit::create(
                [
                'title' => strtoupper($request->title), 'description' => $request->description,
                'favoured_careers' => $request->favoured_careers, 'disFavoured_careers' => $request->disFavoured_career
                ]
            );
            toastr()->success('Record saved successfully');
            return redirect(route('admin.benefits.index'));
        } catch (Exception $e) {
            toastr()->error('something went wrong please try again');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function show(Benefit $benefit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $benefit = Benefit::where('id', $id)->first();
            return view('admin.benefit.edit', compact('benefit'));
        } catch (Exception $e) {
            toastr()->error('something went wrong,please try again');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Benefit      $benefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate(
            [
            'title' => 'required|max:250|string',
            'description' => 'string|required',
            'favoured_careers' => 'required|string',
            'disFavoured_career' => 'required|string'
            ]
        );
        try {
            $benefit = Benefit::where('id', $id)->update(
                [
                'title' => strtoupper($request->title), 'description' => $request->description,
                'favoured_careers' => $request->favoured_careers, 'disFavoured_careers' => $request->disFavoured_career
                ]
            );
            toastr()->success('Record saved successfully');
            return redirect(route('admin.benefits.index'));
        } catch (Exception $e) {
            toastr()->error('something went wrong please try again');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Benefit $benefit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Benefit::where('id', $id)->delete();
            toastr()->success('Record removed successfully');
            return redirect()->back();
        } catch (Exception $e) {
            toastr()->error('Record removing failed');
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use toastr;
use App\Models\Test;
use App\Models\Question;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::orderBy('id', 'ASC')->get();
        $tests = Test::orderBy('id', 'DESC')->get();
        return view('admin.questions.index', compact('questions', 'tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreQuestionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionRequest $request)
    {
        $question = new Question;
        $question->question = $request->question;
        $question->category = $request->category;
        $question->sub_category = $request->sub_category;
        $question->question_sub = $request->question_sub;
        $question->question_no = $request->question_no;
        $question->test_id = $request->tests;
        $question->option = $request->option;
        $question->option_second = $request->option_second;
        $question->option_third = $request->option_third;
        $question->option_third = $request->option_third;
        $question->option_fourth = $request->option_fourth;
        $question->option_five = $request->option_five;
        $question->save();
        if ($question) {
            toastr()->success('Question Added Successfully.');
            return redirect()->back();
        }
        toastr()->warning('Question addition failed.');
        return redirect()->back()->with('fail', 'Question addition failed.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tests = Test::orderBy('id', 'DESC')->get();
        $question = Question::where('id', $id)->first();
        if ($question) {
            return view('admin.questions.edit', compact('tests', 'question'));
        }
        toastr()->warning('Question Record not found.');
        return redirect('admin/questions');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateQuestionRequest $request
     * @param  \App\Models\Question                     $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request)
    {
        $question = Question::where('id', $request->question_id)
            ->update(
                [
                'question' => $request->question, 'question_no' => $request->question_no, 'test_id' => $request->tests,
                'option' => $request->option, 'option_second' => $request->option_second,
                'option_third' => $request->option_third,
                'option_fourth' => $request->option_fourth,
                'option_five' => $request->option_five,
                'category' => $request->category,
                'sub_category' => $request->sub_category,
                'question_sub' => $request->question_sub,
                ]
            );
        if ($question) {
            toastr()->success('Question Record Updated.');
            return redirect('admin/questions');
        }
        toastr()->warning('Question Record updating failed.');
        return redirect('admin/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::where('id', $id)->first();
        if (!$question) {
            toastr()->warning('Question not found.');
            return redirect()->back();
        }
        $question->delete();
        toastr()->success('Question Deleted.');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\CareerAmbition;
use App\Models\DecisionInfluencer;
use App\Models\PersonalHabits;
use App\Models\User;
use App\Models\UserPersonalInformation;
use App\Models\UserProfile;
use Illuminate\Http\Request;

class UserPersonalInformationController extends Controller
{
    public function personalHabit(Request $request)
    {
        foreach ($request->answer as $key => $answer) {
            $userResult[] = UserPersonalInformation::updateOrInsert(
                [
                    'test_name' => $request->test_name,
                    'user_id' => auth()->id(),
                    'question_no' => intval($key)
                ],
                ['answer' => $answer]
            );
        }
        toastr()->success('Information Updated');
        return redirect('dashboard');
    }

    /**
     * Get Family Information of requested user
     */
    public function getUserInformation($user_id, $test)
    {
        $user = User::findOrFail($user_id);
        $info = $user->personalInformation($test);
        // dd($info->where('question_no', 1)->first()->answer);

        switch ($test) {
            case 'Personal':
                $profile = $user->userProfile;
                $info = $info[0]->answer;

                return view('users.personal.family', compact('info', 'profile'));

            case 'career':
                $decision = $user->personalInformation('decision');
                $reasons = UserPersonalInformation::reasonsForGettingJob;
                return view('users.personal.career-ambitions', compact('info', 'decision', 'reasons'));

            case 'future_self_reflection':
                $reasons = null;
                $forcast = (!is_array($info[0]->answer)) ? "Other" : ($info[0]->answer == "Working" ? "Working" : "Studying");
                if ($forcast == "Working") {
                    $reasons = UserPersonalInformation::reasonsForWorking;
                } elseif ($forcast == "Studying") {
                    $reasons = UserPersonalInformation::reasonsForStudying;
                }

                return view('users.personal.future-self-reflection', compact('info', 'reasons', 'forcast'));

            default:
                return view('users.personal.study-habits-and-routine', compact('info'));
        }
    }
}
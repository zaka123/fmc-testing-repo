<?php

namespace App\Http\Controllers;

use App\Models\CounsellorSession;
use App\Http\Requests\StoreCounsellorSessionRequest;
use App\Http\Requests\UpdateCounsellorSessionRequest;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;

class CounsellorSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $sessions = CounsellorSession::getCounsellorSessions(auth()->id());
        return view('counsellor.sessions.index', compact('sessions'));
    }

    public function getSessions()
    {
        $sessions = CounsellorSession::getCounsellorSessions(auth()->id());
        $data = array();
        foreach ($sessions as $session) {
            $data[] = array(
                'id'   => $session["id"],
                'title'   => nl2br($session["session_timing"]),
                'start'   => $session["session_date"]
            );
        }
        echo json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCounsellorSessionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCounsellorSessionRequest $request)
    {
        $data_time = $request->date . ' ' . $request->time;
        $session_time = (new DateTime($data_time, new DateTimeZone(auth()->user()->timezone)))->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i');

        $old_record = CounsellorSession::where('session_timing', $session_time)->where('counsellor_id', auth()->id())->get();
        if (count($old_record) > 0) {
            toastr()->warning('Session time already exist.');
            return redirect()->back();
        }
        $session = CounsellorSession::create(
            [
                'session_timing'    => $session_time,
                'counsellor_id'     => auth()->id(),
                'timezone' => 'UTC',
            ]
        );

        if ($session) {
            toastr()->success('Session time added successfully.');
            return redirect()->back();
        }

        toastr()->warning('Something went wrong. Please try again later.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CounsellorSession $counsellorSession
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(CounsellorSession $session)
    {
        if ($session->userCounsellorSession->isNotEmpty()) {
            toastr()->warning("Sessions that have bookings can't be updated.");
            return redirect()->back();
        }
        $timeInput = $session->time_for_HTML;

        // Split the time into hours and minutes
        list($hours, $minutes) = explode(':', $timeInput);

        // Convert hours to 12-hour format and determine AM/PM
        $ampm = 'AM';
        if ((int)$hours >= 12) {
            $ampm = 'PM';
        }
        if ((int)$hours > 12) {
            $hours = (int)$hours - 12;
        }
        // $data = [$hours, $minutes, $ampm];
        // dd($data);


        return view('counsellor.sessions.edit', compact('session','hours','minutes','ampm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCounsellorSessionRequest $request
     * @param  \App\Models\CounsellorSession                     $counsellorSession
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCounsellorSessionRequest $request, $id)
    {
        $data_time = $request->date . ' ' . $request->time;
        $session_time = (new DateTime($data_time, new DateTimeZone(auth()->user()->timezone)))->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i');

        $old_record = CounsellorSession::where('session_timing', $session_time)->where('counsellor_id', auth()->id())->get();
        if (count($old_record) > 0) {
            toastr()->warning('Session time already exist.');
            return redirect()->back();
        }
        $session = CounsellorSession::where('id', $id)->where('counsellor_id', auth()->id())->update(
            [
                'session_timing' => $session_time,
                'timezone' => 'UTC',
            ]
        );
        if ($session) {
            toastr()->success('Session time updated successfully.');
            return redirect(url('counsellor/sessions'));
        }
        toastr()->warning('Something went wrong. Please try again later.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CounsellorSession $counsellorSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(CounsellorSession $session)
    {
        if ($session->userCounsellorSession->isNotEmpty()) {
            toastr()->warning("Sessions that have bookings can't be deleted.");
            return redirect()->back();
        }

        if ($session->delete()) {
            toastr()->success('Session time deleted Successfully.');
            return redirect()->back();
        }
        toastr()->warning('Session time deletion failed.');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\CounsellorProfile;
use App\Models\Country;
use App\Models\Currency;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserProfessional;
use App\Models\UserProfile;
use Aveiv\OpenExchangeRatesApi\OpenExchangeRates;
use GeoIp2\Database\Reader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class DashboardController extends Controller
{
    public function dashboard(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('User')) {
            return $this->user($request);
        } elseif ($user->hasRole('Counsellor')) {
            return $this->counsellor();
        } elseif ($user->hasRole('Super Admin')) {
            return $this->admin();
        }
    }

    /**
     * Student Dashboard
     */
    // comment out for new design, when uncomment, rename it back to user()
    // public function userOld()
    // {
    //     // // updating currencies
    //     // $db_currencies_codes = [];
    //     // $db_currencies = Currency::pluck('code')->values()->toArray();
    //     // foreach ($db_currencies as $key => $code) {
    //     //     $db_currencies_codes[] = $code;
    //     // }
    //     // $api = new OpenExchangeRates(getenv('OPENEXCHANGE_APP_ID'));
    //     // // Getting latest rates
    //     // $exchange_rates = $api->latest([
    //     //     'base' => 'USD',             // base currency
    //     //     'symbols' => $db_currencies_codes, // limit results to specific currencies
    //     //     'show_alternative' => true,  // include alternative currencies
    //     // ]);
    //     // // Getting currencies
    //     // $currencies = $api->currencies(); // returns ["USD" => "United States Dollar", ...]
    //     // $data = [];
    //     // foreach ($currencies as $code => $name) {
    //     //     if (isset($exchange_rates[$code])) {
    //     //         $data[$code] = [
    //     //             "name" => $name,
    //     //             "exchange_rate" => $exchange_rates[$code]
    //     //         ];
    //     //     }
    //     // }
    //     // // saving currencies in database
    //     // foreach ($data as $code => $currencyData) {
    //     //     Currency::updateOrCreate(['code' => $code], $currencyData);
    //     // }

    //     // getting country names
    //     $countryNames = Country::names();
    //     $user = auth()->user();

    //     if ($user->verification == 'phone' && !$user->phone_verified) {
    //         return redirect()->route("user.verify.phone");
    //     }

    //     if ($user->profession == "Professional" && is_null($user->userProfessional)) {
    //         return view('users.professional.create', get_defined_vars());
    //     } elseif ($user->profession != "Professional" && !$user->profile_completed) {
    //         return view("users.profile.create", get_defined_vars());
    //     }

    //     $sessions               = $user->latestSessions();
    //     $upcomingSessions       = $user->upcomingSessions();
    //     $personalityTestResults = $user->personalityTestResults();
    //     $reports                = $user->latestReports();
    //     $personalityTest = \App\Models\Test::findOrFail(4);
    //     $careerExpectationTest = \App\Models\Test::findOrFail(5);
    //     $skillAssessmentTest = \App\Models\Test::findOrFail(6);

    //     // professional dashboard
    //     if ($user->profession == "Professional") {
    //         $userProfile                         = $user->userProfessional;
    //         $skillAssessmentTestResults          = $user->skillAssessmentTestResults();
    //         $careerExpectationsTestResults       = $user->getTestResults(5);
    //         $careerExpectationsTestResultsKeys   = $careerExpectationsTestResults >= 99 ? array_keys($careerExpectationsTestResults) : null;
    //         $careerExpectationsTestResultsValues = $careerExpectationsTestResults >= 99 ? array_values($careerExpectationsTestResults) : null;

    //         $headings = [
    //             "Competition",
    //             "Freedom",
    //             "Management",
    //             "Life&nbsp;Balance",
    //             "Organization&nbsp;Membership",
    //             "Expertise",
    //             "Learning",
    //             "Entrepreneurship"
    //         ];

    //         $careerExpectationsTestResultsHTML = $careerExpectationsTestResults >= 99 ? array_combine($careerExpectationsTestResultsKeys, $headings) : null;

    //         return view(
    //             'users.professional.index',
    //             compact(
    //                 'careerExpectationsTestResults',
    //                 'personalityTestResults',
    //                 'reports',
    //                 'careerExpectationsTestResultsHTML',
    //                 'careerExpectationsTestResultsKeys',
    //                 'careerExpectationsTestResultsValues',
    //                 'sessions',
    //                 'skillAssessmentTestResults',
    //                 'upcomingSessions',
    //                 'user',
    //                 'userProfile',
    //                 'personalityTest',
    //                 'careerExpectationTest',
    //                 'skillAssessmentTest',
    //             )
    //         );
    //     }

    //     $userProfile = $user->userProfile;

    //     return view(
    //         'users.index',
    //         compact(
    //             'personalityTestResults',
    //             'reports',
    //             'sessions',
    //             'upcomingSessions',
    //             'user',
    //             'userProfile',
    //             'personalityTest',
    //             'careerExpectationTest',
    //             'skillAssessmentTest',
    //         )
    //     );
    // }

    // added for new design
    public function user($request)
    {
        $ip = $request->ip();  // uncomment this line for production
        // $ip = '101.50.64.10'; // this line will be removed later in production
        $currency = $this->getCurrencyByIpLocation($ip);
        session()->put('currency', $currency);

        // getting country names
        $countryNames = Country::names();
        $user = auth()->user();

        $sessions               = $user->latestSessions();
        $upcomingSessions       = $user->upcomingSessions();
        $personalityTestResults = $user->personalityTestResults();
        $reports                = $user->latestReports();
        $personalityTest = \App\Models\Test::findOrFail(4);
        $careerExpectationTest = \App\Models\Test::findOrFail(5);
        $skillAssessmentTest = \App\Models\Test::findOrFail(6);

        // professional dashboard
        $userProfile                         = $user->userProfessional;
        $skillAssessmentTestResults          = $user->skillAssessmentTestResults();
        $careerExpectationsTestResults       = $user->getTestResults(5);
        $careerExpectationsTestResultsKeys   = $careerExpectationsTestResults >= 99 ? array_keys($careerExpectationsTestResults) : null;
        $careerExpectationsTestResultsValues = $careerExpectationsTestResults >= 99 ? array_values($careerExpectationsTestResults) : null;

        $headings = [
            "Competition",
            "Freedom",
            "Management",
            "Life&nbsp;Balance",
            "Organization&nbsp;Membership",
            "Expertise",
            "Learning",
            "Entrepreneurship"
        ];

        $careerExpectationsTestResultsHTML = $careerExpectationsTestResults >= 99 ? array_combine($careerExpectationsTestResultsKeys, $headings) : null;

        return view(
            'users.professional.dashboard',
            compact(
                'careerExpectationsTestResults',
                'personalityTestResults',
                'reports',
                'careerExpectationsTestResultsHTML',
                'careerExpectationsTestResultsKeys',
                'careerExpectationsTestResultsValues',
                'sessions',
                'skillAssessmentTestResults',
                'upcomingSessions',
                'user',
                'userProfile',
                'personalityTest',
                'careerExpectationTest',
                'skillAssessmentTest',
            )
        );
    }

    /**
     * Counsellor Dashboard
     */
    public function counsellor()
    {
        $counsellor        = auth()->user();
        $counsellorProfile = $counsellor->counsellorProfile;
        $latestSessions    = $counsellor->latestSessions(3);
        $sessions          = $counsellor->recentlyRegisteredSessions(4);
        $upcomingSessions  = $counsellor->upcomingSessions(3);

        return view(
            'counsellor.index',
            compact(
                'counsellor',
                'counsellorProfile',
                'latestSessions',
                'sessions',
                'upcomingSessions',
            )
        );
    }

    /**
     * Admin Dashboard
     */
    public function admin()
    {
        $counsellors = User::latestCounsellors(8);
        // $students    = User::latestStudents(8);
        $students    = User::orderBy('created_at', 'desc')
            ->take(8)
            ->get();
        // dd($students);
        return view(
            'admin.index',
            compact(
                'counsellors',
                'students'
            )
        );
    }

    public function getCurrencyByIpLocation($ip)
    {
        $countries_with_gbp_currency = [
            "United Kingdom",
            "Great Britain",
            "British Antarctic Territory",
            "Falkland Islands",
            "Gibraltar",
            "Guernsey",
            "Isle of Man",
            "Jersey",
            "Saint Helena",
            "Ascension",
            "Tristan da Cunha",
            "South Georgia",
            "South Sandwich Islands"
        ];
        $countries_with_eur_currency = [
            "Austria",
            "Belgium",
            "Croatia",
            "Cyprus",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Ireland",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Netherlands",
            "Portugal",
            "Slovakia",
            "Slovenia",
            "Spain"
        ];

        $currency = 'USD';
        try {
            $filePath = config('services.maxmind_db.path');
            if (file_exists($filePath)) {

                $reader = new Reader($filePath);
                // $record = $reader->country($request->ip());
                // $record = $reader->country('128.101.101.101'); // 'US'
                // $record = $reader->country('185.228.92.10'); // 'PK'
                $record = $reader->country($ip);

                if (isset($record->country->name)) {
                    if ($record->country->name == 'Pakistan') {
                        $currency = 'PKR';
                    } elseif (in_array($record->country->name, $countries_with_gbp_currency)) {
                        $currency = 'GBP';
                    } elseif (in_array($record->country->name, $countries_with_eur_currency)) {
                        $currency = 'EUR';
                    } else {
                        $currency = 'USD';
                    }
                }
            }
        } catch (\GeoIp2\Exception\AddressNotFoundException $ex) {
            // return "IP Address not found in the database.";
            $currency = 'USD';
        } catch (\MaxMind\Db\InvalidDatabaseException $e) {
            // return "Invalid database file.";
            $currency = 'USD';
        }
        return $currency;
    }
}

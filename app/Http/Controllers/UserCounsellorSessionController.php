<?php

namespace App\Http\Controllers;

use App\Mail\NewSessionMail;
use App\Models\CounsellorSession;
use App\Models\Currency;
use App\Models\User;
use Nette\Utils\Json;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\UserCounsellorSession;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Validator;

class UserCounsellorSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessions = auth()->user()->sessions();

        return response()->view(
            'users.sessions.index',
            compact(
                'sessions',
            )
        );
    }

    public function history()
    {
        $sessions = auth()->user()->sessionHistory();
        return view(
            'counsellor.sessions.history',
            compact(
                'sessions',
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate(
            [
                'session_id'    => 'required|numeric',
                'counsellor_id' => 'required|numeric',
            ],
            [
                'session_id.*'      => 'Please Select a slot from listed',
                'counsellor_id.*'   => 'Please Select a slot from listed',
            ]
        );
        if (!$data) {
            return response($data);
        }

        $result = UserCounsellorSession::create(
            [
                'session_id'    => $request->session_id,
                'counsellor_id' => $request->counsellor_id,
                'user_id'       => auth()->id(),
                'status'        => 'Unpaid'
            ]
        );
        if ($result) {
            $counsellor = User::where('id', $request->counsellor_id)->first();
            $mail_data = [
                'counsellor_name'   => $counsellor->full_name,
                'student_name'      => auth()->user()->full_name,
            ];
            Mail::to($counsellor->email)->send(new NewSessionMail($mail_data));

            toastr()->success('Session Booking Request Submitted');
            return redirect('user/session');
        }
        toastr()->error('Session Booking Request Submission failed');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserCounsellorSession $userCounsellorSession
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $counsellor_id)
    {
        $counsellor_sessions = CounsellorSession::where('counsellor_id', $counsellor_id)
            ->where('session_timing', $request->date)->get();

        return view(
            'users.sessions.view',
            [
                'sessions' => $counsellor_sessions,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserCounsellorSession $userCounsellorSession
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCounsellorSession $userCounsellorSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request          $request
     * @param  \App\Models\UserCounsellorSession $userCounsellorSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCounsellorSession $userCounsellorSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserCounsellorSession $userCounsellorSession
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = UserCounsellorSession::where('id', $id)->first();
        if ($result) {
            if ($result->delete()) {
                toastr()->success('Booking deleted.');
                return redirect()->back();
            }
            toastr()->warning('Booking Deletion Failed.');
            return redirect()->back();
        }
    }

    public function getCounsellorSessions()
    {
        // $user_sessions = UserCounsellorSession::where('status','!=','Unpaid')->where('counsellor_id',auth()->id())->get();
        $user_sessions = auth()->user()->sessions();
        return view('counsellor.sessions.user-sessions', compact('user_sessions'));
    }

    public function changeStatus($id, $status)
    {
        (new UserCounsellorSession())->changeStatus($status, $id);
        toastr()->success('Booking status changed to ' . $status);
        return redirect()->back();
    }

    // public function sessionStarted(Request $request)
    // {
    //     $session = UserCounsellorSession::where('id', $request->id)->first();
    //     $sessionStartTime = Carbon::parse($session->session->session_timing);
    //     $sessionEndTime = Carbon::parse($session->session->session_timing)->addMinutes(UserCounsellorSession::userCounsellorSessionDuration);
    //     $isStarted = Carbon::now() >= $sessionStartTime && Carbon::now() <= $sessionEndTime;
    //     return response()->json($isStarted);
    // }

    public function counsellorAvailableSessions(Request $request)
    {
        try {
            $selectedDate = $request->input('selectedDate');
            $date = Carbon::parse($selectedDate)->format('Y-m-d');


            $counselorId = $request->input('counselorId');

            $counsellor = User::where('id', $counselorId)->first();
            // $counselorSessions = $counsellor->counsellorSessions;
            $counselorSessions = CounsellorSession::where('counsellor_id', $counselorId)->whereDate('session_timing', $date)->get();

            // return response()->json($counselorSessions);

            // Format sessions for display
            $formattedSessions = [];
            foreach ($counselorSessions as $session) {
                if ($session->canBeBooked()) {
                    $formattedSession = [
                        'counsellor' => $counsellor,
                        'session' => $session,
                        'date' => (new DateTime($session->session_timing, new DateTimeZone($session->timezone)))
                            ->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y'),
                        'time' => (new DateTime($session->session_timing, new DateTimeZone($session->timezone)))
                            ->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('h:i A'),
                        'booking_link' => route('user.payments.info.index', $session->id) . '?session=true',
                    ];
                    $formattedSessions[] = $formattedSession;
                }
            }

            // currency conversion logic here
            if (session()->has('currency')) {
                $user_currency = session()->get('currency');
            } else {
                $user_currency = 'USD';
            }
            // $amount = Currency::where('code', $user_currency)->where('charged_for', 'Session')->first()->charges;
            $currency = Currency::where('code', $user_currency)->where('charged_for', 'Session')->first();
            if(!$currency || $currency == null) {
                $amount = 20;
            } else {
                $amount = $currency->charges;
            }


            // Pass formatted sessions to the view
            return response()->json([
                'success' => true,
                'sessions' => $formattedSessions,
                'counselorId' => $counselorId,
                'amount' => number_format(round($amount, 0), 2),
                'currency' => $user_currency
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\SessionPayment;
use Illuminate\Http\Request;
use Aveiv\OpenExchangeRatesApi\OpenExchangeRates;
use Illuminate\Support\Facades\Http;

class CurrencyConverterController extends Controller
{
    public function convert(Request $request)
    {
        // $response = Http::get('https://ipinfo.io/' . $request->ip() . '/json');
        // $geolocationData = $response->json();
        // dd($geolocationData);

        return view('test');
        // dd(session('currency'));
        $api = new OpenExchangeRates(getenv('OPENEXCHANGE_APP_ID'));

        // Getting currencies
        $currencies = $api->currencies(); // returns ["USD" => "United States Dollar", ...]

        $api->currencies([
            'show_alternative' => true, // include alternative currencies
            'show_inactive' => true,    // include historical/inactive currencies
        ]);


        // Getting latest rates
        $api->latest(); // returns ["USD" => 1.0, ...]

        $latest_rates = $api->latest([
            'base' => 'USD',             // base currency
            'symbols' => [
                'CNY',
                'USD',
                'EUR',
                'JPY',
                'GBP',
                'CAD',
                'AUD',
                'NZD',
                'INR',
                'PKR',
                'CHF',
                'BRL',
                'MXN',
                'KWD',
                'OMR',
                'SAR',
                'AED',
            ], // limit results to specific currencies
            'show_alternative' => true,  // include alternative currencies
        ]);
        // $dollar = $latest_rates['USD'];
        $pkr = $latest_rates['PKR'];
        $converted = 10 * $pkr;
        // round the converted value
        $converted = round($converted);
        dd($converted);

        // Currency conversion (Not supported for free plan)
        // $api->convert(99.99, 'USD', 'EUR'); // returns the converted value (from USD to EUR)
    }

    public function changeCurrency(Request $request)
    {
        $api = new OpenExchangeRates(getenv('OPENEXCHANGE_APP_ID'));
        $latest_rates = $api->latest([
            'base' => 'USD',             // base currency
            'symbols' => [
                'CNY',
                'USD',
                'EUR',
                'JPY',
                'GBP',
                'CAD',
                'AUD',
                'NZD',
                'INR',
                'PKR',
                'CHF',
                'BRL',
                'MXN',
                'KWD',
                'OMR',
                'SAR',
                'AED',
            ], // limit results to specific currencies
            'show_alternative' => true,  // include alternative currencies
        ]);

        $currency = $latest_rates[session('currency')];
        $converted =  $currency * SessionPayment::sessionPaymentAmount;
        // round the converted value
        $converted = round($converted);
        dd($converted);
        return response()->json($converted);
    }
}

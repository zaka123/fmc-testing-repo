<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCounsellorProfileRequest;
use App\Models\Country;
use App\Traits\GenericTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Mail;

class AuthController extends Controller
{
    use GenericTrait;

    /**
     * Send Counsellor to Admin
     *
     * @param  \App\Http\Requests\StoreCounsellorProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function counsellorRegister(StoreCounsellorProfileRequest $request)
    {
        $validated = $request->validated();
        $validated["i_phone_number"] = sprintf("%s %s", $validated["dial_code"], $validated["phone_number"]);

        Mail::send(
            'mail.counsellor.welcome',
            ['data' => $validated],
            function ($message) use ($validated) {
                $message->to($validated['email'])->subject("Welcome To FMC");
            }
        );

        Mail::send(
            'mail.admin.counsellor.apply',
            ['data' => $validated],
            function ($message) use ($validated) {
                $message->to(env("CAREER_ADDRESS"));
                $message->subject("Counsellor Apply to FMC");
                $message->attachData(
                    $validated["resume"]->get(),
                    $validated["resume"]->getClientOriginalName(),
                    [
                        'mime' => $validated['resume']->getClientMimeType()
                    ]
                );
            }
        );

        toastr()->success(
            "Your form has been submitted successfully.\
            For further actions please check your email"
        );

        return redirect("/");
    }

    public function register($profession = null)
    {
        if(auth()->check()) {
            return redirect()->route("dashboard");
        }
        $countryNames = Country::names();
        if (strtolower($profession) == "professional") {
            $view = 'auth.register-professional';
        } else {
            $view = 'auth.register-student';
        }

        return view($view, get_defined_vars());
    }

    // new signup for new design
    public function new_signup()
    {
        return view('auth.new_signup');
    }

    public function counsellorRegistration()
    {
        $countryNames = Country::pluck('name')->all();
        $countryDialCodes = Country::orderedDialCodes();

        return view(
            "auth.counsellor-registration",
            get_defined_vars()
        );
    }
}

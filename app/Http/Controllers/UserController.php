<?php

namespace App\Http\Controllers;

use App\Actions\Fortify\PasswordValidationRules;
use App\Models\CareerAmbition;
use App\Models\CounsellorProfile;
use App\Models\DecisionInfluencer;
use App\Models\PersonalHabits;
use App\Models\SessionPayment;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserPersonalInformation;
use App\Models\UserProfessional;
use App\Models\UserProfile;
use App\Models\UserResult;
use App\Models\VideoCall;
use App\Services\ZoomService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;
use MacsiDigital\Zoom\Facades\Zoom;
use Nette\Utils\Json;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    use PasswordValidationRules;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($role)
    {
        $users = User::getRoleBasedUser($role);
        if ($role == "Counsellor") {
            return view('admin.counsellors.index', compact('users'));
        }
        return view('admin.users.index', compact('users'));
    }

    /**
     * Update user profile Photo
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserProfilePicture(Request $request)
    {
        $request->validate(
            [
            'photo'=>'nullable|mimes:jpg,jpeg,png|max:1024'
            ],
            [
            'photo.max'=>'Photo size must be less than 1 MB'
            ]
        );
        $user = User::where('id', auth()->id())->first()->updateProfilePhoto($request->photo);
        toastr()->success('Profile photo Updated.');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $User
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        if ($user && $user->hasRole('User')) {
            $userProfile = UserProfile::where('user_id', $id)->first();
            $sessions = UserCounsellorSession::where('user_id', $id)->get();
            $skillAssessmentTestResult = (new UserResult())->skillAssessmentTestResult($id);
            $getCareerTestResult = (new UserResult())->getCareerTestResult($id, 5);
            $getPersonalityTestResult = (new UserResult())->getUsersResult($id, 4);
            return view(
                'admin.users.view',
                [
                'user' => $user, 'userProfile' => $userProfile, 'sessions' => $sessions,
                'skillAssessmentTestResult' => $skillAssessmentTestResult, 'getCareerTestResult' => $getCareerTestResult,
                'getPersonalityTestResult' => $getPersonalityTestResult
                ]
            );
        }
        return view('admin.counsellors.view', ['user' => $user]);
    }

    public function getUserProfile($user_id)
    {
        // ensure that the requested user is registered with counsellor
        // $counsellor_id = auth()->user()->id;
        // UserCounsellorSession::where('counsellor_id', $counsellor_id)->where('user_id', $user_id)->get();
        $user = User::where('id', $user_id)->first();

        if (!empty($user) && $user->profession=="Professional") {
            $userProfile = UserProfessional::where('user_id', $user_id)->first();
        } elseif (!empty($user) || !is_null($user)) {
            $userProfile = UserProfile::where('user_id', $user_id)->first();

            // get family info
            $familyInfo = get_object_vars(
                json_decode(
                    UserPersonalInformation::where('user_id', $user_id)
                        ->where('test_name', 'Personal')
                        ->first()
                        ->toArray()["answer"]
                )
            );

            // get career ambitions
            $careerAmbitions = UserPersonalInformation::where('user_id', $user_id)
                ->where('test_name', 'career')
                ->get()
                ->toArray();

            // get decision (career)
            $decision = UserPersonalInformation::where('user_id', $user_id)
                ->where('test_name', 'decision')
                ->get()
                ->toArray();

            // get study habits and routine
            $studyHabitsAndRoutine = UserPersonalInformation::where('user_id', $user_id)
                ->where('test_name', 'study_habits_and_routine')
                ->get()
                ->toArray();

            // get future self reflection
            $futureSelfReflection = UserPersonalInformation::where('user_id', $user_id)
                ->where('test_name', 'future_self_reflection')
                ->get()
                ->toArray();

            $data = compact('familyInfo', 'careerAmbitions', 'decision', 'studyHabitsAndRoutine', 'futureSelfReflection');
        } else {
            toastr()->warning('User profile is not complete');
            return redirect()->back();
        }

        $sessions = UserCounsellorSession::where('user_id', $user_id)->get();
        $skillAssessmentTestResult = (new UserResult())->skillAssessmentTestResult(6, $user_id);
        $getCareerTestResult = (new UserResult())->getCareerTestResult(5, $user_id);
        $getPersonalityTestResult = (new UserResult())->getUsersResult(4, $user_id);

        $data = array_merge(
            $data ?? [],
            compact(
                'user',
                'userProfile',
                'sessions',
                'skillAssessmentTestResult',
                'getCareerTestResult',
                'getPersonalityTestResult'
            )
        );

        return view('counsellor.users.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $User
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        // dd($user);
        if ($user->profession== "Student") {
            $userProfile = UserProfile::where('user_id', $id)->first();
            return view('admin.users.edit', compact('user', 'userProfile'));
        } elseif ($user->profession=="Professional") {
            $userProfessional = UserProfessional::where('user_id', $id)->first();
            return view('admin.users.profile-edit', compact('user', 'userProfessional'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $User
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->delete();
        if (!$user) {
            toastr()->warning('User Deletion Failed.');
            return redirect()->back();
        }
        toastr()->success('User Deleted.');
        return redirect()->back();
    }

    public function videoCall($student_id)
    {
        $student = User::where('id', $student_id)->role('User')->first();
        if (!$student) {
            toastr()->warning('User Profile not found.');
        }

        $counsellor = Zoom::user()->find(auth()->user()->email);
        if (!$counsellor) {
            $counsellor_profile = (new UserProfile())->addProfileZoom(auth()->user()->name, auth()->user()->last_name, auth()->user()->email);
            if (!$counsellor_profile) {
                toastr()->success('You dont have a valid email');
                return redirect()->back();
            }
            toastr()->success('Your Profile has been set, Please verify from email.');
        }

        $student_profile = Zoom::user()->find($student->email);
        if (!$student_profile) {
            $userZoomProfile =  (new UserProfile())->addProfileZoom($student->name, $student->last_name, $student->email);
            if (!$userZoomProfile) {
                toastr()->success($student->email . ' is invalid. Try again with with a valid email.');
                return redirect()->back();
            }
        }
        try {
            $create_meeting = Zoom::meeting()->make(
                [
                'topic' => 'New meeting',
                'type' => 1,
                'duration'=>60,
                'start_time' => now(),
                ]
            );
            $meeting = Zoom::user()->find(auth()->user()->email)->meetings()->save($create_meeting);
            $users = json_decode($meeting);
            // dd($users);
            // dd(str_replace('https://us05web.zoom.us/s/','',$users->start_url));
            if ($users) {
                Mail::raw(
                    'Your session is starting please click <a href="'. $users->join_url.'"> here or the  link, Password is: '.$users->password.'   '.$users->join_url,
                    function ($message) use ($student) {
                        $message->subject('Your Session is starting');
                        $message->from(env('MAIL_USERNAME'), env('APP_NAME'));
                        $message->to($student->email);
                    }
                );
                  toastr()->success('Please Click on Start and wait for User to join the Session.');
                  return view('counsellor.meeting', ['link'=> str_replace('https://www.us05web.zoom.us/s/', '', $users->start_url)]);
            }
        } catch (\MacsiDigital\API\Exceptions\HttpException $error) {
            toastr()->warning('There is some problem in generating Meeting. Please check your email and accept Invitation if not accepted.');
            return redirect('counsellor/bookings');
        }
    }

    /**
     * Connect zoom account
     *
     * @param Illuminate\Http\Request $request
     * @return ?
     */
    public function connectZoomAccount(Request $request)
    {
        $student     = auth()->user();
        $zoomProfile = ZoomService::findUserByEmail($student->email);
        if (! $zoomProfile) {
            $zoomProfile = ZoomService::createProfile($student);
            if ($zoomProfile) {
                CounsellorProfile::where('user_id',auth()->user()->id)->update([
                    'zoom_account'=>1
                ]);
                toastr()->success(sprintf("An invitation email has been sent to your email address %s", $student->email));
            } else {
                toastr()->warning("Your email address is blocked by zoom.");
            }
        } else {
            CounsellorProfile::where('user_id',auth()->user()->id)->update([
                'zoom_account'=>1
            ]);
            toastr()->success("Zoom connected successfully.");
        }
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\SessionPayment;
use App\Models\Test;
use App\Models\CounsellorSession;
use App\Models\Currency;
use Illuminate\Http\Request;

class PaymentInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  App\Models\Test $test
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $isSession = $request->query('session');
        if ($isSession) {
            $session = CounsellorSession::findOrFail($id);

            // redirect user with error if the session has been by some other user
            if (!$session->canBeBooked()) {
                toastr()->warning("The session you are trying to book has been booked. Please try another session.");
                return redirect()->route("user.session.booking");
            }

            $data['session'] = $session;

            // $amount = SessionPayment::sessionPaymentAmount;
            $page = 'session';

            // currency conversion logic here
            if (session()->has('currency')) {
                $user_currency = session()->get('currency');
            } else {
                $user_currency = 'USD';
            }
            // $amount = Currency::where('code', $user_currency)->where('charged_for', 'Session')->first()->charges;
            $currency = Currency::where('code', $user_currency)->where('charged_for', 'Session')->first();
            if(!$currency || $currency == null) {
                $amount = 20;
            } else {
                $amount = $currency->charges;
            }

        } else {
            $test = Test::findOrFail($id);
            switch ($test->id) {
                case 4:
                    $amount = SessionPayment::personalityTestPayment;
                    $page = 'personality';
                    break;

                case 5:
                    $amount = SessionPayment::careerExpectationsTestPayment;
                    $page = 'career-expectations';
                    break;

                default:
                    $amount = SessionPayment::skillAssessmentTestPayment;
                    $page = 'skill-assessment';
                    break;
            }

            $data['test'] = $test;
        }

        // // currency conversion logic here
        // if(session()->has('currency')) {
        //     $user_currency = session()->get('currency');
        // } else {
        //     $user_currency = 'USD';
        // }
        // $exchange_rate = $this->getExchangeRate($user_currency);
        // if (!$exchange_rate || $exchange_rate == null) {
        //     $exchange_rate = 1;
        // }
        // $amount = $amount * $exchange_rate;
        // // dd($amount);

        $data['amount'] = round($amount, 0); //$amount;

        return view("users.payments.info." . $page, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getExchangeRate($currency_code)
    {
        $currency = Currency::where('code', $currency_code)->first();
        if ($currency) {
            return $currency->exchange_rate;
        }
        return null;
    }
}

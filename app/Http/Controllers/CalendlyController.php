<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Http;

class CalendlyController extends Controller
{
    public function redirectToCalendly()
    {
        $calendlyClientId = getenv('CALENDLY_CLIENT_ID');
        $redirectUri = getenv('CALENDLY_REDIRECT_URI');

        $calendlyAuthUrl = "https://calendly.com/oauth/authorize?response_type=code&client_id=$calendlyClientId&redirect_uri=$redirectUri";

        return redirect($calendlyAuthUrl);
    }

    public function handleCalendlyCallback(Request $request)
    {
        $code = $request->query('code');
        $state = $request->query('state');

        // Exchange the code for an access token
        $calendlyClientId = getenv('CALENDLY_CLIENT_ID');
        $calendlyClientSecret = getenv('CALENDLY_CLIENT_SECRET');
        $redirectUri = getenv('CALENDLY_REDIRECT_URI');

        $response = Http::post('https://calendly.com/oauth/token', [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $calendlyClientId,
            'client_secret' => $calendlyClientSecret,
            'redirect_uri' => $redirectUri,
        ]);

        $accessToken = $response->json()['access_token'];

        // Store the access token securely for future use
        $request->session()->put('calendly_access_token', $accessToken);
        toastr()->success('Calendly OAuth authentication successful.');
        return redirect('/')->with('success', 'Calendly OAuth authentication successful.');
    }

    public function createMeeting()
    {
        // Retrieve the access token from your storage
        $accessToken = session()->get('calendly_access_token');

        $url = 'https://api.calendly.com/users/me';

        $client = new Client();

        $response = $client->get($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        $data = json_decode($response->getBody(), true);
        dd($data);



        // $curl = curl_init();

        // curl_setopt_array($curl, [
        //     CURLOPT_URL => "https://api.calendly.com/users/me",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_HTTPHEADER => [
        //         "Authorization: Bearer " . $accessToken,
        //         "Content-Type: application/json"
        //     ],
        // ]);

        // curl_setopt_array($curl, [
        //     CURLOPT_URL => "https://api.calendly.com/event_types",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_HTTPHEADER => [
        //       "Authorization: Bearer " . $accessToken,
        //       "Content-Type: application/json"
        //     ],
        //   ]);

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // // Handle the response and error checking
        // if ($err) {
        //     echo "cURL Error #:" . $err;
        // } else {
        //     echo $response;
        // }

    }

    // Testing Zoom S2S API
    public function getAccessToken()
    {
        $url = "https://zoom.us/oauth/token?grant_type=account_credentials&account_id=" . getenv('ZOOM_ACCOUNT_ID');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'grant_type'    => 'client_credentials',    # https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/
            'scope'         => '',
        )));

        $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_CLIENT_ID') . ":" . getenv('ZOOM_CLIENT_SECRET'));
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $data = curl_exec($ch);
        $auth = json_decode($data, true); // token will be with in this json

        // dd($auth['access_token']);

        return $auth['access_token'];
    }

    public function getUsers()
    {
        $accessToken = $this->getAccessToken();
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get('https://api.zoom.us/v2/users');

        $data = $response->json();

        // Handle the response data here
        dd($data);
        return $data;
    }

    public function createZoomUser($accessToken, $email, $firstName, $lastName)
    {
        // $accessToken = $this->getAccessToken();
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json', // Set the content type to JSON
        ])->post('https://api.zoom.us/v2/users', [
            'email' => $email,
            'first_name' => $firstName,
            'last_name' => $lastName,
        ]);

        $data = $response->json();

        // Handle the response data here
        return $data;
    }
}

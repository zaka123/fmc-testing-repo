<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\PaymentSessionMail;
use App\Models\SessionPayment;
use App\Models\UserCounsellorSession;
use App\Models\VideoCall;
use App\Services\OAuthZoomService;
// use App\Services\ZoomService;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Console\Migrations\RollbackCommand;
use Illuminate\Database\Events\TransactionRolledBack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SessionPaymentController extends Controller
{

    protected $zoomService;

    public function __construct(OAuthZoomService $zoomService)
    {
        $this->zoomService = $zoomService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = SessionPayment::latestPayments();

        return response()->view('admin.payments.index', compact('payments'));
    }

    /**
     * Display the specified resource.
     *
     * @param  sessionPayment $sessionPayment
     * @return \Illuminate\Http\Response
     */
    public function show(sessionPayment $payment)
    {
        // dd($payment->coupon->coupon_code);
        return response()->view("admin.payments.view", compact("payment"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\SessionPayment $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SessionPayment $payment)
    {
        DB::beginTransaction();
        $updated = $payment->update(
            [
                'status' => 'Confirm'
            ]
        );
        if ($updated) {
            $counsellorSession = $payment->counsellorSession;
        // convert session_timing to UTC
        $start_time = ( new DateTime($counsellorSession->session_timing))->format('Y-m-d\TH:i:s');
            // create meeting
            $meeting = $this->zoomService->arrangeMeeting($counsellorSession->users->id, $start_time, 'UTC');

            if (is_null($meeting)) {
                DB::rollBack();
                toastr()->error("Something went wrong in creating meeting. Please try again later.");
                return redirect()->back();
            }

            VideoCall::create(
                [
                    'user_id' => $payment->user->id,
                    'counsellor_id' => $payment->counsellorSession->counsellor_id,
                    'user_counsellor_session_id' => $payment->userCounsellorSession->id,
                    'start_url' => $meeting['start_url'],
                    'join_url' => $meeting['join_url'],
                    'meeting_id' => $meeting['id'],
                ]
            );

            DB::commit();


            // send success email
            Mail::to($payment->user->email)->send(
                new PaymentSessionMail(
                    [
                        'student_name' => $payment->user->full_name,
                        'status' => "Confirm",
                    ]
                )
            );

            // (new UserCounsellorSession())->changeStatus("Confirm", $payment->user_counsellor_sessions_id);

            toastr()->success("Payment approved successfully.");
            return redirect()->route("admin.payments.index");
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SessionPayment $payment)
    {
        if ($payment->status != "Confirm") {
            if ($payment->session_id != "Test Payment") {
                $payment->userCounsellorSession->delete();
            }

            $payment->delete();
            toastr()->success("Payment record deleted successfully.");
            return redirect()->back();
        }

        toastr()->warning("Confirmed payments can't be deleted.");
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserProfessional;
use Illuminate\Http\Request;

class UserProfessionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, UserProfessional $professional)
    {
        if (! $user->hasRole("User") && ! $user->profession == "Professional") {
            abort(404);
        }

        $personalityTestResults = $user->personalityTestResults();
        $careerExpectationsTestResults = $user->careerExpectationsTestResults();
        $skillAssessmentTestResults = $user->skillAssessmentTestResults();
        $sessions = $user->sessions();
        $userProfile = $professional;

        return view(
            'admin.users.professional.view',
            compact(
                "careerExpectationsTestResults",
                "personalityTestResults",
                "sessions",
                "skillAssessmentTestResults",
                "user",
                "userProfile",
            )
        );
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserProfessional $request, UserProfessional $userProfessional)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

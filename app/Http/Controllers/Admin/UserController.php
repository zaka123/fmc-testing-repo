<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserPersonalInformation;
use App\Models\UserProfessional;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::latestStudents(); // commented out for new design
        $users = User::orderBy('id', 'desc')->get();
        return view(
            'admin.users.index',
            compact(
                'users',
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    // public function show(User $user)
    // {
    //     if (!$user->hasRole("User") || $user->hasRole("Super Admin") || $user->profession == "Professional") {
    //         abort(404);
    //     }

    //     $personalityTestResults = $user->personalityTestResults();
    //     $sessions = $user->sessions();
    //     $userProfile = $user->userProfile;

    //     $familyInfo = $user->personalInformation('Personal');
    //     if ($familyInfo && isset($familyInfo[0])) {

    //         $familyInfo = $familyInfo[0]->answer;
    //     }
    //     $careerAmbitions = $user->personalInformation('career');
    //     $reasonsForGettingJob = UserPersonalInformation::reasonsForGettingJob;
    //     $decision = $user->personalInformation('decision');
    //     $studyHabitsAndRoutine = $user->personalInformation('study_habits_and_routine');
    //     $futureSelfReflection = $user->personalInformation('future_self_reflection');
    //     $reasons = null;
    //     $forcast = "Other";
    //     if ($futureSelfReflection && isset($futureSelfReflection[0])) {
    //         $forcast = (!is_array($futureSelfReflection[0]->answer)) ? "Other" : ($futureSelfReflection[0]->answer == "Working" ? "Working" : "Studying");
    //     }
    //     if ($forcast == "Working") {
    //         $reasons = UserPersonalInformation::reasonsForWorking;
    //     } elseif ($forcast == "Studying") {
    //         $reasons = UserPersonalInformation::reasonsForStudying;
    //     }

    //     // dd($careerAmbitions[1]->answer);

    //     return view(
    //         'admin.users.view',
    //         compact(
    //             "careerAmbitions",
    //             "decision",
    //             "familyInfo",
    //             "forcast",
    //             "futureSelfReflection",
    //             "personalityTestResults",
    //             "reasons",
    //             "reasonsForGettingJob",
    //             "sessions",
    //             "studyHabitsAndRoutine",
    //             "user",
    //             "userProfile",
    //         )
    //     );
    // }

    public function show(User $user)
    {
        if (! $user->hasRole("User") && ! $user->profession == "Professional") {
            abort(404);
        }

        $personalityTestResults = $user->personalityTestResults();
        $careerExpectationsTestResults = $user->careerExpectationsTestResults();
        $skillAssessmentTestResults = $user->skillAssessmentTestResults();
        $sessions = $user->sessions();
        $userProfile = $user->userProfile;

        return view('admin.users.professional.view',
            compact(
                "careerExpectationsTestResults",
                "personalityTestResults",
                "sessions",
                "skillAssessmentTestResults",
                "user",
                "userProfile",
            )
        );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if ($user->profession == "Professional") {
            $userProfile = UserProfessional::where('user_id', $user->id)->first();
            return view(
                "admin.users.professional.edit",
                compact(
                    "user",
                    "userProfile",
                )
            );
        }

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        return view(
            "admin.users.edit",
            compact(
                "user",
                "userProfile",
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User         $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $deleted = $user->delete();

        if ($deleted) {
            toastr()->success("User trashed successfully.");
            return redirect()->back();
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect()->back();
    }


    

    /**
     * Restore the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function restore(User $user)
    {
        //
    }

    public function changeUserStatus(Request $request)
    {
        $user = User::findOrFail($request->user_id);

        $success = $user->update([
            'status' => $request->status
        ]);

        if ($success) {
            $msg = 'Status change successfully.';
        } else {
            $msg = 'Sorry something went wrong';
        }

        return response()->json(['msg' => $msg]);
    }

    public function changeUserPassword($id)
    {
        $user = User::findOrFail($id);
        return view("admin.users.change-password", compact("user"));
    }

    public function updateUserPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);
        $user = User::findOrFail($request->id);
        $user->password = Hash::make($request->password);
        $user->save();
        toastr()->success("Password changed successfully.");
        return redirect()->route('admin.users.index');

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCouponRequest;
use App\Models\Coupon;
use App\Models\Currency;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('id', 'DESC')->get();
        foreach ($coupons as $coupon) {
            if ($coupon->redeem_by_date != '' && $coupon->redeem_by_date < Date::today()->format("Y-m-d") && $coupon->expired == 0) {
                $coupon->update([
                    'expired' => 1
                ]);
            }
        }
        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::all();
        return view('admin.coupons.create', compact('currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCouponRequest $request)
    {

        try {
            // get validated data
            $validated = $request->validated();

            if (array_key_exists("test_category", $validated) && $validated['test_category'] != '') {
                $validated['category'] = $validated['test_category'];
            }

            $validated['note'] = strip_tags($validated['note']);
            // create coupon
            $coupon =  Coupon::create($validated);

            toastr()->success("Coupon created successfully.");
            return redirect()->route("admin.coupons.index");
        } catch (Exception $ex) {
            toastr()->error("Sorry something went wrong. Please try again.");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        return view("admin.coupons.show", compact("coupon"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        $currencies = Currency::all();
        return view("admin.coupons.edit", compact("coupon", "currencies"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        try {
            // get validated data
            $validated = $request->validate(
                [
                    'coupon_name' => 'required|min:3|max:30',
                    'discount_type' => 'required',
                    'category' => 'required',
                    'discount' => 'required|numeric',
                    'coupon_code' => 'required|min:3|max:30|unique:coupons,coupon_code,' . $coupon->id,
                    'test_category' => 'nullable',
                    'max_redemptions' => 'nullable|numeric',
                    'status' => 'nullable',
                    'note' => 'nullable',
                    'currency_type' => 'required',
                    'redeem_by_date' => [
                        'nullable',
                        'date_format:Y-m-d',
                        // 'after_or_equal:' . Date::today()->format("Y-m-d"),
                    ],
                ],
            );

            if (array_key_exists("test_category", $validated) && $validated['test_category'] != '') {
                $validated['category'] = $validated['test_category'];
            }
            $validated['note'] = strip_tags($validated['note']);
            if ($validated['redeem_by_date'] != '' && $validated['redeem_by_date'] >= Date::today()->format("Y-m-d")) {
                $validated['expired'] = 0;
            }
            // update coupon
            $success = $coupon->update($validated);

            if ($success) {
                toastr()->success("Coupon updated successfully.");
                return redirect()->route("admin.coupons.index");
            } else if (!$success) {
                toastr()->error("Sorry something went wrong. Please try again.");
                return redirect()->back();
            }
        } catch (Exception $ex) {
            toastr()->error("Sorry something went wrong. Please try again.");
            return redirect()->back();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        toastr()->success("Coupon deleted successfully.");
        return redirect()->route("admin.coupons.index");
    }

    public function changeCouponStatus(Request $request)
    {
        $coupon = Coupon::find($request->coupon_id);
        // dd($coupon);
        $success = $coupon->update([
            'status' => $request->status
        ]);
        if ($success) {
            $msg = 'Status change successfully.';
        } else {
            $msg = 'Sorry something went wrong';
        }
        return response()->json(['msg' => $msg]);
    }
}

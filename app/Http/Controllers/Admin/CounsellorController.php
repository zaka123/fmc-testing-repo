<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCounsellorProfileRequest;
use App\Http\Requests\Counsellor\StoreFirstTimePasswordResetRequest;
use App\Models\Country;
use App\Models\User;
use App\Traits\GenericTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class CounsellorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counsellors = User::latestCounsellors();
        return view(
            'admin.counsellors.index',
            compact(
                'counsellors',
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.counsellors.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCounsellorProfileRequest $request)
    {
        // get validated data
        $validated = $request->validated();

        // verfiy counsellor by default
        $validated["email_verified_at"] = now();
        $validated["name"]              = $validated["first_name"];
        $validated["password"]          = Hash::make($validated["password"]);

        $inputDomain = $validated['linked_in'];
        // Check if the input domain starts with "http://" or "https://"
        if (strpos($inputDomain, "http://") === 0 || strpos($inputDomain, "https://") === 0) {
            $validated['linked_in'] = $inputDomain;
        } else {
            $validated['linked_in'] = "https://" . $inputDomain;
        }

        // create counsellor account
        $counsellor =  User::create($validated);

        // assign a role
        $role = Role::where('name', 'Counsellor')->first();
        $counsellor->assignRole($role->name);

        // create counsellor profile
        $profile = $counsellor->profile()->create($validated);


        // send verification email
        // $user->sendEmailVerificationNotification();
        // toastr()->success('Registration completed, Please verify your email to continue.');
        toastr()->success("Coach profile successfully.");
        return redirect()->route("admin.counsellors.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $counsellor)
    {
        if (! $counsellor->hasRole("Counsellor")) {
            abort(404);
        }

        $sessions = $counsellor->sessions();
        $connectedUsers = $counsellor->recentlyRegisteredSessions(null);
        return view(
            "admin.counsellors.view",
            compact(
                "counsellor",
                "connectedUsers",
                "sessions",
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $counsellor)
    {
        if (! $counsellor->hasRole("Counsellor")) {
            abort(404);
        }
        $countries = Country::names();
        // dd($counsellorProfile);
        return response()->view('admin.counsellors.edit',
            compact(
                'counsellor',
                'countries',
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User         $user
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, User $counsellor)
    {
        // validate input data...
        $validated = $request->validate([
            'name' => 'required',
            'last_name'  => 'required',
            'email' => ['required', 'email', 'max:255', 'unique:users,email,' . $counsellor->id],
        ]);

        // update personal data
        $counsellor->update($validated);
        $counsellor->profile->update([
            'country' => $request->country,
        ]);

        toastr()->success('Counsellor Information Updated');
        return redirect()->route('admin.counsellors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function changePassword()
    {
        return view("counsellor.reset-password");
    }

    public function updatePassword(StoreFirstTimePasswordResetRequest $request)
    {
        $validated = $request->validated();

        $counsellor = Auth::user();
        $counsellor->password = Hash::make($validated["password"]);
        $counsellor->password_changed = true;
        $counsellor->save();
        toastr()->success("Password updated successfully.");
        return redirect()->route("dashboard");
    }
}

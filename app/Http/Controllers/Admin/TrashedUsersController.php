<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CounsellorProfile;
use App\Models\CounsellorSession;
use App\Models\SessionPayment;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserResult;
use App\Models\VideoCall;
use App\Models\ZoomToken;
use Illuminate\Http\Request;

class TrashedUsersController extends Controller
{
    public function deletedCoaches()
    {
        $role = 'Coaches';
        $users = User::onlyTrashed()
            ->whereHas('roles', function ($query) {
                $query->where('name', 'counsellor');
            })
            ->get();
        return view('admin.users.trashed', compact('users', 'role'));
    }
    public function deletedUsers()
    {
        $role = 'Students';
        $users = User::onlyTrashed()
            ->whereHas('roles', function ($query) {
                $query->where('name', 'user');
            })
            ->get();
        return view('admin.users.trashed', compact('users', 'role'));
    }

    public function deletePermanently($id)
    {
        $user = User::onlyTrashed()->find($id);

        if ($user) {
            // find user_counsellor_sessions of the user
            $user_counsellor_sessions = UserCounsellorSession::where('user_id', $user->id)->get();
            if ($user_counsellor_sessions) {
                foreach ($user_counsellor_sessions as $user_counsellor_session) {
                    // find video call of the user_counsellor_session
                    $video_call = VideoCall::where('user_counsellor_session_id', $user_counsellor_session->id)->first();
                    // delete video call
                    if ($video_call) {
                        $video_call->forceDelete();
                    }
                    // find payment of the user_counsellor_session
                    $payment = SessionPayment::where('user_counsellor_sessions_id', $user_counsellor_session->id)->first();
                    // delete payment
                    if ($payment) {
                        $payment->forceDelete();
                    }
                    // find counsellor session of the user_counsellor_session
                    $cousellor_session = CounsellorSession::find($user_counsellor_session->session_id);
                    // delete counsellor session
                    if ($cousellor_session) {
                        $cousellor_session->forceDelete();
                    }
                    $user_counsellor_session->forceDelete();
                }
            }
            // find zoom token of the user
            $zoomToken = ZoomToken::where('user_id', $user->id)->first();
            // delete zoom token
            if ($zoomToken) {
                $zoomToken->forceDelete();
            }

            $results = UserResult::where('students_id', $user->id)->get();
            if ($results) {
                foreach ($results as $result) {
                    $result->forceDelete();
                }
            }

            $deleted = $user->forceDelete();

            if ($deleted) {
                toastr()->success("User deleted permanently.");
                return redirect()->back();
            }
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect()->back();
    }

    public function restore($id)
    {
        User::withTrashed()->find($id)->restore();
        toastr()->success("Record restored successfully.");
        return back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Aveiv\OpenExchangeRatesApi\OpenExchangeRates;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function index()
    {
        $currencies = Currency::all();
        return view('admin.currencies.index', compact('currencies',));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'code' => 'required|string|max:3|min:3|unique:currencies,code,',
            'name' => 'required',
            'charges' => 'required|numeric',
            'charged_for' => 'required',
        ]);

        $validated['code'] = strtoupper($validated['code']);
        $currency = Currency::create($validated);
        if ($currency) {
            toastr()->success('Currency added successfully');
            return back();
        }
        toastr()->error('Something went wrong, please try again');
        return back();
    }

    public function edit($id)
    {
        $currency = Currency::find($id);
        return response()->json($currency);
        // return view('admin.currencies.edit', compact('currency'));
    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'code' => 'required|string|max:3|min:3|unique:currencies,code,' . $request->id,
            'name' => 'required',
            'charges' => 'required|numeric',
            'charged_for' => 'required',
        ]);
        $validated['code'] = strtoupper($validated['code']);
        $currency = Currency::find($request->id);
        $currency->update($validated);
        if ($currency) {
            toastr()->success('Currency updated successfully');
            return back();
        }
        toastr()->error('Something went wrong, please try again');
        return back();
    }

    public function destroy($id)
    {
        $currency = Currency::find($id);
        $currency->delete();
        if ($currency) {
            toastr()->success('Currency deleted successfully');
            return back();
        }
        toastr()->error('Something went wrong, please try again');
        return back();
    }



    // public function updateCurrencies()
    // {
    //     $db_currencies_codes = [];
    //     $db_currencies = Currency::pluck('code')->values()->toArray();
    //     foreach ($db_currencies as $key => $code) {
    //         $db_currencies_codes[] = $code;
    //     }

    //     $api = new OpenExchangeRates(getenv('OPENEXCHANGE_APP_ID'));

    //     // Getting latest rates
    //     $exchange_rates = $api->latest([
    //         'base' => 'USD',             // base currency
    //         'symbols' => $db_currencies_codes, // limit results to specific currencies
    //         'show_alternative' => true,  // include alternative currencies
    //     ]);

    //     // Getting currencies
    //     $currencies = $api->currencies(); // returns ["USD" => "United States Dollar", ...]

    //     $data = [];
    //     foreach ($currencies as $code => $name) {
    //         if (isset($exchange_rates[$code])) {
    //             $data[$code] = [
    //                 "name" => $name,
    //                 "exchange_rate" => $exchange_rates[$code]
    //             ];
    //         }
    //     }

    //     // save in database
    //     foreach ($data as $code => $currencyData) {
    //         Currency::updateOrCreate(['code' => $code], $currencyData);
    //     }

    //     toastr()->success('Currencies updated successfully');
    //     return back();
    // }
}

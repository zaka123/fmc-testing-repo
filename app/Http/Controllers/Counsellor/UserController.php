<?php

namespace App\Http\Controllers\Counsellor;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserCounsellorSession;
use App\Models\UserPersonalInformation;
use App\Models\UserProfessional;
use App\Models\UserProfile;
use App\Models\UserResult;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(User $user)
    {
        if (!$user->hasRole("User") || $user->hasRole("Super Admin")) {
            abort(404);
        }

        $personalityTestResults = $user->personalityTestResults();
        $sessions = $user->sessions();
        $userProfile = $user->profile;

        $familyInfo = $user->personalInformation('Personal');
        $familyInfo = $familyInfo[0]->answer ?? null;
        $careerAmbitions = $user->personalInformation('career');
        $reasonsForGettingJob = UserPersonalInformation::reasonsForGettingJob;
        $decision = $user->personalInformation('decision');
        $studyHabitsAndRoutine = $user->personalInformation('study_habits_and_routine') ?? null;
        $futureSelfReflection = $user->personalInformation('future_self_reflection') ?? null;
        $reasons = null;
        $forcast = null;
        if (count($futureSelfReflection)) {
            $forcast = (!is_array($futureSelfReflection[0]->answer)) ? "Other" : ($futureSelfReflection[0]->answer == "Working" ? "Working" : "Studying");
            if ($forcast == "Working") {
                $reasons = UserPersonalInformation::reasonsForWorking;
            } elseif ($forcast == "Studying") {
                $reasons = UserPersonalInformation::reasonsForStudying;
            }
        }

        return view(
            'counsellor.users.view',
            compact(
                "careerAmbitions",
                "decision",
                "familyInfo",
                "forcast",
                "futureSelfReflection",
                "personalityTestResults",
                "reasons",
                "reasonsForGettingJob",
                "sessions",
                "studyHabitsAndRoutine",
                "user",
                "userProfile",
            )
        );
    }

}
<?php

namespace App\Http\Controllers\Counsellor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Counsellor\StoreSessionReportRequest;
use App\Http\Requests\Counsellor\UpdateSessionReportRequest;
use App\Models\SessionReport;
use App\Models\UserCounsellorSession;
use App\Policies\Counsellor\SessionReportPolicy;
use Illuminate\Http\Request;

class SessionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = auth()->user()->reports()->get();
        
        return view(
            'counsellor.sessions.reports.index',
            compact(
                'reports',
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @return \Illuminate\Http\Response
     */
    public function create(UserCounsellorSession $session)
    {
        if (auth()->user()->id != $session->counsellor->id) {
            abort(403);
        } elseif ($session->report_uploaded) {
            toastr()->warning('Report for this session already exists.');
            return redirect()->back();
        }

        return view(
            'counsellor.sessions.reports.create',
            compact(
                'session',
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Counsellor\StoreSessionReportRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSessionReportRequest $request, UserCounsellorSession $session)
    {
        if (auth()->user()->id != $session->counsellor->id) {
            abort(403);
        }

        $validated = $request->validated();

        // create a fresh report with validated data
        $report = new SessionReport($validated);
        
        // pass hooks to other models
        $report->counsellor_id = $session->counsellor_id;
        $report->session_id    = $session->id;
        $report->student_id    = $session->user_id;

        // save the report
        $report->save();

        toastr()->success('Report uploaded successfully.');

        return redirect()->route('counsellor.session.history');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function show(UserCounsellorSession $session, SessionReport $report)
    {
        return view(
            "counsellor.sessions.reports.view",
            compact(
                "session",
                "report",
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SessionReport $report
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCounsellorSession $session, SessionReport $report)
    {
        return view(
            "counsellor.sessions.reports.edit",
            compact(
                "session",
                "report",
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Counsellor\UpdateSessionReportRequest $request
     * @param  \App\Models\UserCounsellorSession                        $session
     * @param  \App\Models\SessionReport                                $report
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSessionReportRequest $request, UserCounsellorSession $session, SessionReport $report)
    {
        $validated = $request->validated();

        // update validated data
        $report->update($validated);

        toastr()->success('Report updated successfully.');

        return redirect()->route('counsellor.session.history');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCounsellorSession $session, SessionReport $report)
    {
        $report->delete();

        toastr()->success("Report deleted successfully.");
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GeoIp2\Database\Reader;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        // Note: currently the getCurrencyByIpLocation functionality is implemented in DashboardController

        // $ip = $request->ip();
        // $currency = $this->getCurrencyByIpLocation($ip);
        // session()->put('currency', $currency);

        // return view('welcome');
        if (Auth::check()) {
            return redirect('/dashboard');
        }
        return redirect('/get-started');
    }

    public function getCurrencyByIpLocation($ip)
    {
        $countries_with_gbp_currency = [
            "United Kingdom",
            "Great Britain",
            "British Antarctic Territory",
            "Falkland Islands",
            "Gibraltar",
            "Guernsey",
            "Isle of Man",
            "Jersey",
            "Saint Helena",
            "Ascension",
            "Tristan da Cunha",
            "South Georgia",
            "South Sandwich Islands"
        ];
        $countries_with_eur_currency = [
            "Austria",
            "Belgium",
            "Croatia",
            "Cyprus",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Ireland",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Netherlands",
            "Portugal",
            "Slovakia",
            "Slovenia",
            "Spain"
        ];

        $currency = 'USD';
        try {
            $filePath = config('services.maxmind_db.path');
            if (file_exists($filePath)) {

                $reader = new Reader($filePath);
                // $record = $reader->country($request->ip());
                // $record = $reader->country('128.101.101.101'); // 'US'
                // $record = $reader->country('185.228.92.10'); // 'PK'
                $record = $reader->country($ip);

                if (isset($record->country->name)) {
                    if ($record->country->name == 'Pakistan') {
                        $currency = 'PKR';
                    } elseif (in_array($record->country->name, $countries_with_gbp_currency)) {
                        $currency = 'GBP';
                    } elseif (in_array($record->country->name, $countries_with_eur_currency)) {
                        $currency = 'EUR';
                    } else {
                        $currency = 'USD';
                    }
                }
            }
        } catch (\GeoIp2\Exception\AddressNotFoundException $ex) {
            // return "IP Address not found in the database.";
            $currency = 'USD';
        } catch (\MaxMind\Db\InvalidDatabaseException $e) {
            // return "Invalid database file.";
            $currency = 'USD';
        }
        return $currency;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Benefit;
use App\Models\UserResult;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;

class DownloadResultController extends Controller
{
    /**
     * Download personality test result in pdf.
     */
    public function personality($word = null)
    {
        try {
            $result = (new UserResult())->checkUserTestStatus(auth()->id(), 4);
            $wordcombination = Benefit::where('title', $word)->first();
            $pdf = Pdf::loadView('users.results.personality-pdf', compact('result', 'wordcombination'));
            return $pdf->download('personality-report.pdf');
        } catch (Exception $e) {
            toastr()->error('Something went wrong.');
            return redirect()->back();
        }
    }

    /**
     * Download career expectations questionaire test result in pdf.
     */
    public function career()
    {
        try {
            $result = (new UserResult())->checkUserTestStatus(auth()->id(), 5);
            $pdf = Pdf::loadView('users.results.career-pdf', compact('result'));
            return $pdf->download('career-expectation-report.pdf');
        } catch (Exception $e) {
            toastr()->error('Something went wrong.');
            return redirect()->back();
        }
    }

    /**
     * Download skill assessment test result in pdf.
     */
    public function skill_assessment()
    {
        try {
            $result = (new UserResult())->checkUserTestStatus(auth()->id(), 6);
            $pdf = Pdf::loadView('users.results.skill-assessment-pdf', compact('result'));
            return $pdf->download('skill-assessment-report.pdf');
        } catch (Exception $e) {
            toastr()->error('Something went wrong.');
            return redirect()->back();
        }
    }
}

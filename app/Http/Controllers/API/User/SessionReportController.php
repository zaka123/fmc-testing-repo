<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\SessionReportResource;
use App\Models\SessionReport;
use App\Models\UserCounsellorSession;
use Illuminate\Http\Request;

class SessionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @return \Illuminate\Http\Response
     */
    public function create(UserCounsellorSession $session)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \Illuminate\Http\Request          $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserCounsellorSession $session)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function show(UserCounsellorSession $session, SessionReport $report)
    {
        return new SessionReportResource($report);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCounsellorSession $session, SessionReport $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request          $request
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCounsellorSession $session, SessionReport $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserCounsellorSession $session
     * @param  \App\Models\SessionReport         $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCounsellorSession $session, SessionReport $report)
    {
        //
    }
}

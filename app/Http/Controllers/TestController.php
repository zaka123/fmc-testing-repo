<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\Question;
use App\Http\Requests\StoreTestRequest;
use App\Http\Requests\UpdateTestRequest;
use App\Models\SessionPayment;
use App\Models\User;
use App\Models\UserResult;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::orderBy('test_name', 'ASC')->get();
        return view('admin.tests.index', compact('tests'));
    }


    public function skillAssessmentTest()
    {
        $id = 6;
        $payment = (new SessionPayment())->getTestPayment($id);
        if (!$payment) {
            return view('users.payments.create');
        }
        $user_id = auth()->id();
        // check for result
        $result = (new UserResult())->checkUserTestStatus($user_id, $id);
        if ($result > 99) {
            return view('users.instructions.skill-assessment', compact('result'));
        }
        // get questions
        $questions = Question::with(
            [
                'userResult' => function ($query) use ($user_id, $id) {
                    $query->where('students_id', $user_id)->where('test', $id);
                }
            ]
        )->where('test_id', $id)->orderBy('question_no', 'ASC')->get();
        if (count($questions) > 0) {
            $lastAttempted = UserResult::where('test', $id)->where('students_id', $user_id);
            $lastAttemptedQuestion = $lastAttempted->orderBy('question', 'desc')->pluck('question')->first();
            $lastAnswered = $lastAttempted->count();
            $total = $lastAnswered / count($questions);
            $total = $total * 100;
            return view('users.tests.skill-assessment', compact('questions', 'total', 'lastAnswered', 'lastAttemptedQuestion'));
        }
        toastr()->warning('Test record not found.');
        return redirect()->back();
    }

    public function instructions(Test $test)
    {
        if (auth()->user()->profession != "Professional" && ($test->id == 5 || $test->id == 6)) {
            toastr()->warning("This feature is only available for professionals");
            return redirect()->back();
        }
        $instructions = $test->instructions;
        $result = auth()->user()->getTestResults($test->id);

        return view(
            'users.instructions.index',
            compact(
                'instructions',
                'result',
                'test',
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTestRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestRequest $request)
    {
        $test = Test::create(['test_name' => $request->test_name]);
        if ($test) {
            toastr()->success('Test Created successfully.');
            return redirect()->back();
        }
        toastr()->warning('Test creation Failed.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Test $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        $questions = $test->questions;
        // dd($questions[0]->option_five);
        return view(
            "admin.tests.view",
            compact(
                "questions",
                "test",
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Test $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTestRequest $request
     * @param  \App\Models\Test                     $test
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestRequest $request, Test $test)
    {
        $updated = $test->update(
            $request->all()
        );

        if ($updated) {
            toastr()->success("Record updated successfully.");
            return redirect()->route("admin.tests.index");
        }

        toastr()->error("Something went wrong. Please try again later.");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Test $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        $deleted = $test->delete();
        if ($deleted) {
            toastr()->success('Test deleted successfully.');
        } else {
            toastr()->error("Something went wrong. Please try again later.");
        }
        return redirect()->back();
    }

    public function userTestsQuestions(Test $test)
    {
        $id = $test->id;

        if (auth()->user()->profession != "Professional" && ($id == 5 || $id == 6)) {
            toastr()->warning("This feature is only available for professionals.");
            return redirect()->back();
        }

        $payment = (new SessionPayment())->getTestPayment($id);

        /**
        * Make test free

        if (!$payment) {
        return view('users.payments.create', compact('id'));
        } elseif ($payment->status=="Pending") {
        return view('users.payments.confirmation-alert');
        } elseif ($payment->status == "Cancel") {
        toastr()->warning('Your payment has been cancelled by admin. Please repay');
        return view('users.payments.create', compact('id'));
        }
        */

        $user = User::findOrFail(auth()->id());
        $user_id = $user->id;

        // check for result
        $result = (new UserResult())->checkUserTestStatus($user_id, $id);
        if ($result > 99) {
            return redirect()->route("user.test.report");
        }

        // get questions
        $questions = Question::with(
            [
                'userResult' => function ($query) use ($user_id, $id) {
                    $query->where('students_id', $user_id)->where('test', $id);
                }
            ]
        )->where('test_id', $id)->orderBy('question_no', 'ASC')->get();
        if (count($questions) > 0) {
            $lastAnswered = $user->getLastAttemptedQuestionNumber($test->id);
            $total = $user->getTestProgress($test->id);
            return view(
                'users.tests.view',
                compact(
                    'lastAnswered',
                    'questions',
                    'test',
                    'total',
                )
            );
        }

        toastr()->warning('Test record not found.');
        return redirect()->back();
    }

    public function userTestRestart(Test $test)
    {
        $test_id = $test->id;
        $user = User::findOrFail(auth()->id());
        $user_id = $user->id;

        // check for result
        $result = UserResult::where('test', $test_id)->where('students_id', $user_id)->get();
        foreach ($result as $record) {
            $record->delete();
        }
        // dd($result);
        return redirect()->route("user.test", $test_id);
    }
}

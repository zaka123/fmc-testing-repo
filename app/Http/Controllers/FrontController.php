<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    /**
     * Display a listing of frequently asked questions
     *
     * @return view
     */
    public function faqs()
    {
        $faqs = Faq::all();

        $faqGroups = $faqs->chunk(5);
        return view(
            'faqs',
            compact(
                "faqGroups",
            )
        );
    }

    /**
     * Display help page
     */
    public function help()
    {
        return view('help');
    }

    public function helpStore(Request $request)
    {
        // Validate the form data
        $validatedData = $request->validate([
            'email' => 'nullable|email',
            'phone' => 'nullable',
            'question' => 'required',
            'info' => 'required',
            'date' => 'required|date',
            'category' => 'required',
            'faqs' => 'required',
        ]);

        // dd($validatedData);
        $validatedData['name'] = auth()->user()->name . ' ' . auth()->user()->last_name;
        // Send email using Laravel's Mail facade
        Mail::send('mail.help', ['data' => $validatedData], function ($message) use ($validatedData) {
            $message->to('noreply@findmecareer.com')
                ->subject('Help Request');
        });
        toastr()->success("Your request has been submitted successfully!");
        return redirect()->back();
    }

    /**
     * Display about page
     */
    public function about()
    {
        return view("about");
    }

    public function contact()
    {
        $countryDialCodes = Country::orderedDialCodes();
        return view("contact", get_defined_vars());
    }

    public function school()
    {
        if (auth()->check()) {
            return redirect()->route("dashboard");
        }
        return view("school");
    }

    public function university()
    {
        if (auth()->check()) {
            return redirect()->route("dashboard");
        }
        return view("university");
    }

    public function professional()
    {
        if (auth()->check()) {
            return redirect()->route("dashboard");
        }
        return view("professional");
    }

    public function getStarted()
    {
        if (auth()->check()) {
            return redirect()->route("dashboard");
        }
        return view("whoAreYou");
    }

    public function parents()
    {
        return view("parents");
    }
}

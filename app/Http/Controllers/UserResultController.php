<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Benefit;
use App\Models\CareersList;
use App\Models\UserResult;
use Illuminate\Http\Request;
use App\Models\SessionPayment;
use Barryvdh\DomPDF\Facade\Pdf;
use DOMDocument;
use Illuminate\Support\Facades\Validator;

class UserResultController extends Controller
{

    public function report()
    {
        $paymentObj = new SessionPayment();

        // for student and professional
        $personalityTestPayment = $paymentObj->getTestPayment(4);
        $personalityTest = \App\Models\Test::find(4);
        $careerExpectationTest = \App\Models\Test::find(5);
        $skillAssessmentTest = \App\Models\Test::find(6);

        // for professional only
        if (auth()->user()->profession == "Professional") {
            $skillAssessmentTestPayment = $paymentObj->getTestPayment(6);
            $skillAssessmentTestResults = auth()->user()->getTestResults(6);

            $careerExpectationsTestResults = auth()->user()->getTestResults(5);
            $careerExpectationsTestPayment = $paymentObj->getTestPayment(5);
            // foreach($careerExpectationsTestResults as $key=>$value) {
            //     echo sprintf("%s %s\n", $key, $value);
            // }
            // dd('hi');

            $data = compact(
                'careerExpectationsTestPayment',
                'careerExpectationsTestResults',
                'skillAssessmentTestPayment',
                'skillAssessmentTestResults',
            );
        }

        return view(
            'users.tests.report',
            array_merge(
                $data ?? [],
                compact(
                    'personalityTestPayment',
                    'personalityTest',
                    'careerExpectationTest',
                    'skillAssessmentTest',
                )
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeOld(Request $request)
    {
        $result = UserResult::updateOrCreate(
            [
                'question' => $request->question,
                'test' => $request->test_id,
                'students_id' => auth()->id()
            ],
            [
                'answer' => $request->answer,
            ]
        );

        if (!$result) {
            return response(false);
        }
        return response(true);
    }

    public function store(Request $request)
    {
        $tasks = $request->input('tasks');
        foreach ($tasks as $task) {
            UserResult::create(
                [
                    'question' => $task['question'],
                    'test' => $task['test_id'],
                    'students_id' => auth()->id(),
                    'answer' => $task['answer'],
                ]
            );
        }
        return response()->json([
            'msg' => "Data saved successfully",
        ]);
    }

    public function getTestReport($test)
    {
        if ($test == 'career-expectations') {
            return view('users.results.career');
        } else if ($test == 'skill-assessment') {
            return view('users.results.skill-assessments');
        } else if ($test == 'personality') {
            $personalityTestResults = auth()
                ->user()
                ->getTestResults(4);

            $wordCombination = '';
            $personalityTestResults['extroversion'] > 20 ? ($wordCombination = 'S') : ($wordCombination = 'R');
            $personalityTestResults['neuroticism'] > 20 ? ($wordCombination .= 'L') : ($wordCombination .= 'C');
            $personalityTestResults['conscientiousness'] > 20 ? ($wordCombination .= 'O') : ($wordCombination .= 'U');
            $personalityTestResults['agreeableness'] > 20 ? ($wordCombination .= 'A') : ($wordCombination .= 'E');
            $personalityTestResults['opennessToExperience'] > 20 ? ($wordCombination .= 'I') : ($wordCombination .= 'N');

            $benefit = Benefit::where('title', strtoupper($wordCombination))->first();
            // dd($benefit->description);


            $html = $benefit->description;
            // Create a DOMDocument object
            $dom = new DOMDocument();
            $dom->loadHTML($html);

            // Get all li elements
            $li_elements = $dom->getElementsByTagName('li');

            $items = array();

            // Iterate through each li element to get the text content
            foreach ($li_elements as $li) {
                // Get the text content of the li element
                $text_content = $li->textContent;
                // Remove special characters and trailing spaces
                $text_content = str_replace('\u{A0}', '', $text_content); // Remove '\u{A0}'
                $text_content = trim($text_content); // Trim trailing spaces
                // Add to the items array
                $items[] = $text_content;
            }
            // dd($items);

            $careersToChoose = [];

            foreach ($items as $value) {
                // extract sub_subjects from CareersList for main_subject as $value
                $careers = CareersList::where('main_subject', $value)->get();

                // Group the careers by main_subject
                $careersToChoose[$value] = $careers;
            }

            // dd($careersToChoose);

            return view('users.results.personality', compact('careersToChoose', 'wordCombination'));
        } else {
            abort(404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserProfileRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Models\Country;
use App\Models\User;
use App\Models\UserPersonalInformation;
use App\Models\UserProfile;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('users.profile.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserProfileRequest $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserProfileRequest $request)
    {
        $userProfile = auth()->user()->userProfile;
        if ($userProfile) {
            $userProfile->update($request->validated());
        } else {
            auth()->user()->userProfile()->create($request->validated());
        }
        UserPersonalInformation::storePersonalInformation($request->personal, 'Personal');
        UserPersonalInformation::storePersonalInformation($request->ambitions, 'career');
        UserPersonalInformation::storePersonalInformation($request->influencer, 'decision');
        UserPersonalInformation::storePersonalInformation($request->study_habits_and_routine, 'study_habits_and_routine');
        UserPersonalInformation::storePersonalInformation($request->future_self_reflection, 'future_self_reflection');
        toastr()->success('Profile Completed successfully.');
        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserProfile $userProfile
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = User::find(auth()->id());
        // if (!$user->hasRole("User") || $user->hasRole("Super Admin") || $user->profession == "Professional")
        if (!$user->hasRole("User") || $user->hasRole("Super Admin"))
        {
            abort(404);
        }

        if($user->profession == "Professional"){
            $personalityTestResults = $user->personalityTestResults();
        $careerExpectationsTestResults = $user->careerExpectationsTestResults();
        $skillAssessmentTestResults = $user->skillAssessmentTestResults();
        $sessions = $user->sessions();
        $userProfile = $user->userProfessional;

        return view(
            'users.professional.show',
            compact(
                "careerExpectationsTestResults",
                "personalityTestResults",
                "sessions",
                "skillAssessmentTestResults",
                "user",
                "userProfile",
            )
        );
            // return view('users.professional.show', compact('user'));
        }

        $personalityTestResults = $user->personalityTestResults();
        $sessions = $user->sessions();
        $userProfile = $user->userProfile;

        $familyInfo = $user->personalInformation('Personal');
        if ($familyInfo && isset($familyInfo[0])) {

            $familyInfo = $familyInfo[0]->answer;
        }
        $careerAmbitions = $user->personalInformation('career');
        $reasonsForGettingJob = UserPersonalInformation::reasonsForGettingJob;
        $decision = $user->personalInformation('decision');
        $studyHabitsAndRoutine = $user->personalInformation('study_habits_and_routine');
        $futureSelfReflection = $user->personalInformation('future_self_reflection');
        $reasons = null;
        $forcast = "Other";
        if ($futureSelfReflection && isset($futureSelfReflection[0])) {
            $forcast = (!is_array($futureSelfReflection[0]->answer)) ? "Other" : ($futureSelfReflection[0]->answer == "Working" ?    "Working" : "Studying");
        }
        if ($forcast == "Working") {
            $reasons = UserPersonalInformation::reasonsForWorking;
        } elseif ($forcast == "Studying") {
            $reasons = UserPersonalInformation::reasonsForStudying;
        }

        return view(
            'users.profile.show',
            compact(
                "careerAmbitions",
                "decision",
                "familyInfo",
                "forcast",
                "futureSelfReflection",
                "personalityTestResults",
                "reasons",
                "reasonsForGettingJob",
                "sessions",
                "studyHabitsAndRoutine",
                "user",
                "userProfile",
            )
        );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserProfile $userProfile
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function edit($userProfile)
    {
        $user = User::where('id', auth()->id())->first();
        $userProfile = UserProfile::where('id', $userProfile)->where('user_id', auth()->id())->first();
        $countryNames = Country::names();
        if ($userProfile) {
            return response()->view('users.profile.edit', compact('userProfile', 'user', 'countryNames'));
        }

        toastr()->warning('Profile Information mismatched.');
        return redirect('dashboard');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserProfileRequest $request
     * @param  \App\Models\UserProfile                     $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserProfileRequest $request, $userProfile)
    {
        $update = UserProfile::where('user_id', auth()->id())->where('id', $userProfile)->update(
            [
                'date_of_birth' => $request->date_of_birth,
                'city' => $request->city,
                'favorite_subject' => $request->favorite_subject,
                'easiest_subject' => $request->easiest_subject,
                'difficult_subject' => $request->difficult_subject,
                'average_grade_point' => $request->average_grade_point,
                'father_education' => $request->father_education,
                'father_occupation' => $request->father_occupation,
                'mother_education' => $request->mother_education,
                'mother_occupation' => $request->mother_occupation,
                'parents_retired' => $request->parents_retired,
            ]
        );
        $user = (new User())->updateUser($request);
        if ($update) {
            toastr()->success('Profile Information updated successfully.');
            return redirect('dashboard');
        }
        toastr()->warning('Profile Information updating Failed.');
        return redirect()->back();
    }

    public function updateProfile(Request $request, $id = null)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            toastr()->warning('Record not found');
            return redirect()->back();
        }
        $request->validate(
            [
                'date_of_birth' => 'required',
                'city' => 'required',
                'favorite_subject' => 'required',
                'easiest_subject' => 'required',
                'difficult_subject' => 'required',
                'average_grade_point' => 'required',
                'father_education' => 'required',
                'father_occupation' => 'required',
                'mother_education' => 'required',
                'mother_occupation' => 'required',
                'parents_retired' => 'required',
                'name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email,' . $user->id ?? '',
                'gender' => 'required',
                'profession' => 'required',
            ]
        );
        $update = UserProfile::where('user_id', $user->id ?? '')->update(
            [
                'date_of_birth' => $request->date_of_birth,
                'city' => $request->city,
                'favorite_subject' => $request->favorite_subject,
                'easiest_subject' => $request->easiest_subject,
                'difficult_subject' => $request->difficult_subject,
                'average_grade_point' => $request->average_grade_point,
                'father_education' => $request->father_education,
                'father_occupation' => $request->father_occupation,
                'mother_education' => $request->mother_education,
                'mother_occupation' => $request->mother_occupation,
                'parents_retired' => $request->parents_retired,
            ]
        );
        if ($update) {
            $user = User::where('id', $user->id ?? '')->update(
                [
                    'name' => $request->input('name'),
                    'last_name' => $request->input('last_name'),
                    'email' => $request->input('email'),
                    'gender' => $request->input('gender'),
                    'profession' => $request->input('profession'),
                ]
            );
            toastr()->success('Profile Information updated successfully.');
            return redirect('dashboard');
        }
        toastr()->warning('Profile Information updating Failed.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserProfile $userProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        toastr()->success('User account deleted permenantly.');
        return redirect('/');
    }
}

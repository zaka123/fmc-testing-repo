<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVideoCallRequest;
use App\Http\Requests\UpdateVideoCallRequest;
use App\Models\VideoCall;
use App\Services\OAuthZoomService;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

class VideoCallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVideoCallRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoCallRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VideoCall $videoCall
     * @return \Illuminate\Http\Response
     */
    public function show(VideoCall $videoCall)
    {
        $zoomService = new OAuthZoomService();
        // $zakToken = $zoomService->getUserZAK($videoCall->counsellor_id);
        $zakToken = '';

        // Define your API Key and Secret
        $apiKey = getenv('ZOOM_SDK_ID');
        $apiSecret = getenv('ZOOM_SDK_SECRET');

        $role = 0;

        $key = $apiKey;
        $secret = $apiSecret;
        $meeting_number = $videoCall->meeting_id;
        // $role = $role;
        $token = array(
            "sdkKey" => $key,
            "mn" => $meeting_number,
            "role" => $role,
            "iat" => time(),
            "exp" => time() + 3600, //60 seconds as suggested
            "tokenExp" => time() + 3600,
        );
        $encode = \Firebase\JWT\JWT::encode($token, $secret, 'HS256');
        $jwtToken = $encode;

        return view("users.zoom.meeting", compact("videoCall", "jwtToken", "role",));
        // return view("users.video-call.view",compact("videoCall"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VideoCall $videoCall
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoCall $videoCall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateVideoCallRequest $request
     * @param  \App\Models\VideoCall                     $videoCall
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVideoCallRequest $request, VideoCall $videoCall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VideoCall $videoCall
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoCall $videoCall)
    {
        //
    }

}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class VerifyPhoneController extends Controller
{
    public function index()
    {
        return view('auth.verify-phone');
    }

    public function verifyPhone(Request $request)
    {
        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
        ]);
        $user = auth()->user();
        $data['phone_number'] = $user->phone;
        // dd($data);
        try {
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_ACCOUNT_SID");
            $twilio_verify_sid = getenv("TWILIO_SERVICE_SID");
            $twilio = new Client($twilio_sid, $token);

            $verification = $twilio->verify->v2->services($twilio_verify_sid)
                ->verificationChecks
                ->create(['code' => $data['verification_code'], 'to' => $data['phone_number']]);
            if ($verification->valid) {
                $user->phone_verified = true;
                $user->save();
                toastr()->success('Phone number verified successfully');
                return redirect()->route('dashboard')->with(['message' => 'Phone number verified']);
            }
            return back()->with('error', 'Invalid verification code entered!');
        } catch (\Throwable $th) {
            throw $th;
        }
        return back()->with('error', 'Someting went wrong. Please try again!');
    }

    public function resendVerificationCode(User $user)
    {
        try {
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_ACCOUNT_SID");
            $twilio_verify_sid = getenv("TWILIO_SERVICE_SID");
            $twilio = new Client($twilio_sid, $token);
            $twilio->verify->v2->services($twilio_verify_sid)
                ->verifications
                ->create($user->phone, "sms");
            return back()->with('status', 'A code has been sent to your phone number.');
        } catch (\Throwable $th) {
            throw $th;
            // throw new \Exception('Phone number verification failed. Please try email verification.');
        }
        return back();
    }
}

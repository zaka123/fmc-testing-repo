<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view('users.settings.index');
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:100|min:2',
            'email' => 'required|email',
        ]);
        try {
            $user = auth()->user();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            Profile::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'address' => $request->address,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'zip_code' => $request->zip_code,
            ]);

            return redirect()->back()->with('success', 'Profile updated successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong, please try again later');
        }
    }

    public function passwordUpdate(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required',

        ]);
        // try {
            $user = auth()->user();

            if (!password_verify($request->current_password, $user->password)) {
                return redirect()->back()->with('error', 'Current password is incorrect');
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return redirect()->back()->with('success', 'Password updated successfully');
        // } catch (\Exception $e) {
        //     return redirect()->back()->with('error', 'Something went wrong, please try again later');
        // }
    }
}

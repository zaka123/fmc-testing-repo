<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\SessionReport;
use App\Models\UserCounsellorSession;
use Exception;
use Illuminate\Http\Request;

class SessionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $reports = $user->reports;

        return view('users.sessions.reports.index',
            compact(
                'reports',
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($session_id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Http\Response
     */
    public function show(SessionReport $sessionReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Http\Response
     */
    public function edit(SessionReport $sessionReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SessionReport $sessionReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(SessionReport $sessionReport)
    {
        //
    }
}

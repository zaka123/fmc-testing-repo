<?php

namespace App\Http\Controllers;

use App\Models\ZoomToken;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ZoomController extends Controller
{
    public function redirectToZoom()
    {
        // check if user is logged in
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        // Generate a unique state parameter to prevent CSRF attacks
        $state = uniqid();

        // Save the state parameter to the session for validation later
        session(['zoom_oauth_state' => $state]);

        // Redirect the user to Zoom's OAuth authorization URL
        $zoomAuthUrl = 'https://zoom.us/oauth/authorize?' . http_build_query([
            'client_id'     => getenv('ZOOM_SDK_ID'),
            'redirect_uri'  => getenv('ZOOM_SDK_REDIRECT'),
            // 'client_id'     => config('services.zoom.client_id'),
            // 'redirect_uri'  => config('services.zoom.redirect'),
            'response_type' => 'code',
            'scope'         => '',
            // 'state'         => $state,
        ]);

        return redirect()->away($zoomAuthUrl);
    }

    public function handleZoomCallback(Request $request)
    {
        $response = $this->getAccessToken($request);
        // dd($response);
        if ($response) {
            $response['user_id'] = auth()->user()->id;
            // create if user id does not exist otherwise update
            $zoomToken = ZoomToken::updateOrCreate(
                ['user_id' => auth()->user()->id],
                $response
            );
        }
        toastr()->success('Zoom account connected successfully');
        // Redirect or return a success response
        return redirect('/dashboard')->with('success', 'Zoom account connected successfully');
    }

    public function getAccessToken($request)
    {
        $url = "https://zoom.us/oauth/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'grant_type'    => 'authorization_code',    # https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/
            'code'         => $request->code,
            // 'redirect_uri'         => config('services.zoom.redirect'),
            'redirect_uri'         => getenv('ZOOM_SDK_REDIRECT'),
        )));

        // $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_CLIENT_ID') . ":" . getenv('ZOOM_CLIENT_SECRET'));
        $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_SDK_ID') . ":" . getenv('ZOOM_SDK_SECRET'));
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $reponse = curl_exec($ch);
        $data = json_decode($reponse, true); // token will be with in this json

        // dd($data['access_token']);
        return $data;
    }

    public function refreshAccessToken($refreshToken)
    {
        $url = "https://zoom.us/oauth/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'grant_type'    => 'refresh_token',    # https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/
            'refresh_token'         => $refreshToken,
        )));

        // $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_CLIENT_ID') . ":" . getenv('ZOOM_CLIENT_SECRET'));
        $headers[] = "Authorization: Basic " . base64_encode(getenv('ZOOM_SDK_ID') . ":" . getenv('ZOOM_SDK_SECRET'));
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $reponse = curl_exec($ch);
        $data = json_decode($reponse, true); // token will be with in this json

        // dd($data['access_token']);
        return $data;
    }

    public function scheduleMeeting()
    {
        // check if user is logged in
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        $yourZoomAccessToken = auth()->user()->zoomToken->access_token; // later this will be changed to counsellor zoom token
        // $yourZoomAccessToken = session('zoom_access_token');
        dd($yourZoomAccessToken);
        // Define your meeting data
        $meetingData = [
            'topic' => 'One on one session', // Set your meeting topic
            'type' => 2, // 2 for a scheduled meeting
            'start_time' => now()->addHours(1)->format('Y-m-d\TH:i:s'), // Schedule the meeting for one hour from now
            'duration' => 30, // Meeting duration in minutes
            'timezone' => 'Asia/Karachi', // Set the timezone
            // 'password' => '', // Set a meeting password
            'agenda' => 'Discuss career opportunities', // Meeting agenda
        ];

        try {
            // Make a POST request to create a meeting
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $yourZoomAccessToken, // Replace with your Zoom API access token
            ])->post('https://api.zoom.us/v2/users/me/meetings', $meetingData);

            $responseData = $response->json();

            // Check if the meeting was successfully created
            if ($response->successful() && !empty($responseData['id'])) {
                $meetingId = $responseData['id'];
                // You can store the meetingId in your database or use it as needed.
                toastr()->success('Meeting scheduled successfully');
                // Redirect or return a success response
                return redirect()->back()->with('success', 'Meeting scheduled successfully');
            } else {
                toastr()->error('Failed to schedule meeting');
                // Handle errors
                return redirect()->back()->with('error', 'Failed to schedule meeting');
            }
        } catch (\Exception $e) {
            // Handle exceptions
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function meetingsList()
    {
        $accessToken = $this->getFreshAccessToken();
        // dd($accessToken);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken['access_token'],
        ])->get('https://api.zoom.us/v2/users/me/meetings');

        $data = $response->json();

        // Handle the response data here
        // dd($data);
        return $data;
    }

    public function getUsers($request)
    {
        $accessToken = $this->getSavedAccessToken();
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get('https://api.zoom.us/v2/users');

        $data = $response->json();

        // Handle the response data here
        dd($data);
        return $data;
    }

    public function getSavedAccessToken()
    {
        $zoomAccessToken = auth()->user()->zoomToken->access_token; // later this will be changed to counsellor zoom token
        return $zoomAccessToken;
    }

    public function getFreshAccessToken()
    {
        $data = ZoomToken::where('user_id', 214)->first();
        $response = $this->refreshAccessToken($data->refresh_token);

        // $accessToken = $response['access_token'];
        // dd($accessToken);
        return $response;
    }

    public function generateJWTtoken()
    {
        // Define your API Key and Secret
        $apiKey = getenv('ZOOM_SDK_ID');
        $apiSecret = getenv('ZOOM_SDK_SECRET');

        // Create a payload for the JWT token
        $payload = [
            "appKey"=> $apiKey,
            "sdkKey"=> $apiSecret,
            "mn"=> 123456,
            "role"=> 0,
            "iat"=> 1646937553,
            "exp"=> 1646944753,
            "tokenExp"=> 1646944753
            // 'iss' => $apiKey,
            // 'exp' => strtotime('+1 hour'), // Set the expiration time (1 hour from now)
        ];

        // Generate the JWT token
        $jwtToken = JWT::encode($payload, $apiSecret, 'HS256');

        return $jwtToken;
    }

    public function getZoomJwtToken()
{
    $jwtToken = $this->generateJWTtoken();

    return response()->json(['signature' => $jwtToken]);
}

    public function generateToken()
    {
        // $meetingNumber = $request->input('meetingNumber');
        // $role = $request->input('role');
        $apiKey = getenv('ZOOM_SDK_ID');
        $apiSecret = getenv('ZOOM_SDK_SECRET');

        $iat = now()->subSeconds(30)->getTimestamp();
        $exp = $iat + 60 * 60 * 2;

        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT',
        ];

        $payload = [
            "appKey"=> $apiKey,
            "sdkKey"=> $apiSecret,
            'mn' => 121319,
            'role' => 0,
            'iat' => $iat,
            'exp' => $exp,
            'tokenExp' => $iat + 60 * 60 * 2,
        ];

        $base64UrlHeader = base64_encode(json_encode($header));
        $base64UrlPayload = base64_encode(json_encode($payload));
        $signature = hash_hmac('sha256', "$base64UrlHeader.$base64UrlPayload", $apiSecret, true);
        $base64UrlSignature = base64_encode($signature);

        return response()->json([
            'signature' => $base64UrlSignature,
            // 'signature' => base64_decode( $base64UrlSignature),
        ]);
    }
}

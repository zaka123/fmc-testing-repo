<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class SessionReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'additional_notes' => $this->additional_notes,
            'challenges'       => $this->challenges,
            'commitments'      => $this->commitments,
            'goal_setting'     => $this->goal_setting,
            'reflections'      => $this->reflections,
            'warm_up'          => $this->warm_up,
            'counsellor_name'  => $this->counsellor->full_name,
            'date_uploaded'    => $this->date_uploaded,
            'counsellor_profile_picture' => $this->counsellor->profile_photo,
        ];
    }
}

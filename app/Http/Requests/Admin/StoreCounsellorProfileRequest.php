<?php

namespace App\Http\Requests\Admin;

use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCounsellorProfileRequest extends FormRequest
{
    use PasswordValidationRules;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('Super Admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:250'],
            'phone_number'  => ['nullable', 'required_with:dial_code'],
            'dial_code'     => ['nullable', 'required_with:phone_number'],
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'linked_in'     => ['nullable', 'string'],
            'interested_in' => ['required', 'string', 'in:Coaching,Mentoring,Both Coaching and Mentoring'],
            'gender'        => ['nullable', 'in:Male,Female,Other'],
            'country'       => ['nullable', 'exists:countries,name'],
            'password'      => $this->passwordRules(),
            'role'          => ['required', 'in:Counsellor'],
            'password_confirmation' => ['required', 'same:password'],
        ];
    }
}

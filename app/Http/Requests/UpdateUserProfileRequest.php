<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_of_birth' => 'required',
            'city' => 'required',
            'favorite_subject' => 'required',
            'easiest_subject' => 'required',
            'difficult_subject' => 'required',
            'average_grade_point' => 'required',
            'father_education' => 'required',
            'father_occupation' => 'required',
            'mother_education' => 'required',
            'mother_occupation' => 'required',
            'parents_retired' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.auth()->id(),
            'gender' => 'required',
            'profession' => 'required',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;

class UpdateCounsellorSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole("Counsellor");
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:' . Date::today()->format("Y-m-d"),
            ],
            'time' => ['required'],
        ];

        if ($this->input('date') === Date::today()->format("Y-m-d")) {
            $rules['time'][] = 'required';
            $rules['time'][] = 'after_or_equal:' . Date::now()->format("H:i:s");
        }

        return $rules;
    }
}

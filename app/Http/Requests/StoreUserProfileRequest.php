<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'average_grade_point' => ['sometimes', 'required'],
            'father_education' => ['sometimes', 'required'],
            'father_occupation' => ['sometimes', 'required'],
            'mother_education' => ['sometimes', 'required'],
            'mother_occupation' => ['sometimes', 'required'],
            'parents_retired' => ['sometimes', 'required', 'in:Father,Mother,Both,Neither,Prefer not to say'],
            'ambitions' => ['required', 'array'],
            'personal' => ['required', 'array'],
            'influencer' => ['required', 'array'],
        ];
    }
}
<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCounsellorProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole("Counsellor");
    }

    /**
     * Handle a passed validation attempt.
     *
     * @return void
     */
    protected function passedValidation()
    {
        //
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'career_couch'           => 'string|required|min:200',
            'country'                => ['required', 'exists:countries,name'],
            // 'coaching_certification' => ['required'],
            'email'                  => ['required', 'email', 'max:255', 'unique:users,email,' . auth()->id()],
            'gender'                 => ['required'],
            'interested_in'          => ['required'],
            'last_name'              => ['required'],
            'linked_in'              => ['nullable'],
            'name'                   => ['required'],
            'phone_number'           => ['nullable'],
            'dial_code'              => ['nullable'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The first name field is required'
        ];
    }
}

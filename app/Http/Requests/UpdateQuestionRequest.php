<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id'=>'required',
            'question_no'=>'required',
            'question'=>'required',
            'tests'=>'required',
            'category' => 'nullable|string|max:250',
            'sub_category' => 'nullable|string|max:250',
            'question_sub' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return[
            'question_id.*'=>'Please Try with another question.',
            'tests.*'=>'Please Select a test from Dropdown.',
        ];
    }
}

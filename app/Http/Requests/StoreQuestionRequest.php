<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question'=>'required',
            'question_no'=>'required',
            'tests'=>'required',
            'category'=>'nullable|string|max:250',
            'sub_category'=>'nullable|string|max:250',
            'question_sub'=>'nullable|string',
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Http\FormRequest;

class StorePreLoginRequest extends FormRequest
{
    use PasswordValidationRules;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:250'],
            'gender'        => ['required', 'in:Male,Female,Prefer not to say'],
            'date_of_birth' => ['required'],
            'city'          => ['required', 'exists:countries,name'],
            'favorite_subject'    => ['required_if:role,!=,' . 'Professional'],
            'easiest_subject'     => ['required_if:role,!=,' . 'Professional'],
            'difficult_subject'   => ['required_if:role,!=,' . 'Professional'],
            'average_grade_point' => ['required_if:role,!=,' . 'Professional'],
            'father_education'    => ['required_if:role,!=,' . 'Professional'],
            'father_occupation'   => ['required_if:role,!=,' . 'Professional'],
            'mother_education'    => ['required_if:role,!=,' . 'Professional'],
            'mother_occupation'   => ['required_if:role,!=,' . 'Professional'],
            'parents_retired'     => ['required_if:role,!=,' . 'Professional', 'in:Father,Mother,Both,Neither,Prefer not to say'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'role'     => ['required', 'string'],
            // 'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
            'education_level'    => ['required_if:role,==,' . 'Professional'],
            'experience'         => ['required_if:role,==,' . 'Professional'],
            'organization_name'  => ['nullable'],
            'designation'        => ['nullable'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'city.*' => 'The country field is required',
        ];
    }
}

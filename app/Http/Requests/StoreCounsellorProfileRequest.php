<?php

namespace App\Http\Requests;

use App\Rules\WordCount;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\File;

class StoreCounsellorProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            throw new AuthorizationException('You are logged into another account.');
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:250'],
            'phone_number'  => ['nullable', 'required_with:dial_code'],
            'email'         => ['required', 'string', 'email', 'max:255'],
            'linked_in'     => ['nullable', 'url'],
            'interested_in' => ['required', 'string', 'in:Coaching,Mentoring,Both Coaching and Mentoring'],
            'career_couch'  => ['required', new WordCount],
            'gender'        => ['nullable', 'in:Male,Female,Other'],
            'country'       => ['nullable', 'exists:countries,name'],
            'dial_code'     => ['nullable', 'required_with:phone_number', 'exists:countries,dial_code'],
            // 'coaching_certification' => ['nullable', 'in:on,off'],
            'resume'        => [
                'required',
                File::types(['doc', 'docx', 'pdf'])
                    ->max(10 * 1024),
            ],
        ];
    }

    /**
     * ...
     */
    public function messages()
    {
        return [
            'name.required' => 'The first name field is required',
            'resume.max'    => 'The resume must not be greater than 10MB',
        ];
    }
}

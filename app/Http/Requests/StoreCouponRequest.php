<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\Rule;

class StoreCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole("Super Admin");
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'coupon_name'    => ['required', 'string', 'max:30'],
            'coupon_code'     => ['required', 'unique:coupons,coupon_code,'. $this->coupon, 'max:30'],
            // 'coupon_code' => [
            //     'required',
            //     'max:30',
            //     Rule::unique('coupons', 'coupon_code')->ignore($this->coupon)
            // ],
            'discount_type'  => ['required'],
            'discount'         => ['required', 'numeric'],
            'category'  => ['required'],
            'test_category'  => ['nullable'],
            'max_redemptions'  => ['nullable', 'numeric'],
            'status'  => ['nullable'],
            'note'  => ['nullable'],
            'currency_type'  => ['required'],
            'redeem_by_date' => [
                'nullable',
                'date_format:Y-m-d',
                'after_or_equal:' . Date::today()->format("Y-m-d"),
            ],
        ];
    }

    /**
     * ...
     */
    public function messages()
    {
        return [
            'discount_type.required' => 'Please select a discount type',
        ];
    }
}

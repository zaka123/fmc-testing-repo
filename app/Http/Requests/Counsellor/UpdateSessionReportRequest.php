<?php

namespace App\Http\Requests\Counsellor;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSessionReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'warm_up'      => ['required', 'string'],
            'reflections'  => ['required', 'string'],
            'extra'        => ['nullable', 'string'],
            'challenges'   => ['required', 'string'],
            'goal_setting' => ['required', 'string'],
            'commitments'  => ['required', 'string'],
        ];
    }
}

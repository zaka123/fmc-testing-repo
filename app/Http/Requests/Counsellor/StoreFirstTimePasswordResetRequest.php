<?php

namespace App\Http\Requests\Counsellor;

use App\Actions\Fortify\PasswordValidationRules;
use App\Rules\NotEqualOldPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreFirstTimePasswordResetRequest extends FormRequest
{
    use PasswordValidationRules;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole("Counsellor");
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'password'              => [$this->passwordRules(), new NotEqualOldPassword],
            'password_confirmation' => ['required', 'same:password'],
        ];
    }
}

<?php

namespace App\Http\Requests\Counsellor;

use App\Rules\WordCount;
use Illuminate\Foundation\Http\FormRequest;

class StoreSessionReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'challenges'   => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
            'commitments'  => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
            'extra'        => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
            'goal_setting' => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
            'reflections'  => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
            'warm_up'      => [
                'nullable',
                new WordCount(['max' => 300,])
            ],
        ];
    }
}

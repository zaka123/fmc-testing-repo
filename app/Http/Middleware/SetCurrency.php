<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetCurrency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->has('currency')) {
            // Set user's preferred currency in session or cookie
            session(['currency' => $request->input('currency')]);
        } elseif (session('currency')) {
            // Use previously selected currency from session or cookie
        } else {
            // Default to a base currency
            session(['currency' => 'USD']);
        }

        return $next($request);
    }
}

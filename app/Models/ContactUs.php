<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Cast\Attribute;

class ContactUs extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'dial_code',
        'message',
    ];

    public function phoneNumber(): Attribute
    {
        return Attribute::make(
            get: fn () => sprintf("%s %s", $this->dial_code, $this->phone_number),
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareersList extends Model
{
    use HasFactory;

    protected $table = 'careers_list';

    protected $fillable = [
        'main_subject',
        'sub_subject',
    ];
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CounsellorProfile extends Model
{
    use HasFactory;

    /**
     * Attributes that are mass assignable
     *
     * @var array $fillable
     */
    protected $fillable = [
        'career_couch',
        'coaching_certification',
        'country',
        'date_of_birth',
        'dial_code',
        'interested_in',
        'linked_in',
        'phone_number',
        'resume_upload',
    ];

    public function getAgeAttribute() // Get age from date of birth
    {
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }

    public function certified(): Attribute
    {
        if ($this->coaching_certification == "on") {
            return Attribute::make(
                get: fn () => '<span class="badge bg-success">Certified</span>',
            );
        }
        return Attribute::make(get: fn () => '<span class="badge bg-secondary">Not certified</span>',);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->whereHasRole('Counsellor');
    }

    public function getCityAttribute()
    {
        return $this->country;
    }

    /**
     * Returns counsellor of this profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function counsellor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getInterestedInAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getResumeUploadAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getIPhoneNumberAttribute()
    {
        $phone_number = $this->phone_number != 'N/A' ? preg_replace('/^\+\d+\s+/', '', $this->phone_number ?? '') : '';
        return $phone_number ? $this->dial_code . $phone_number : 'N/A';
    }

    public function getlinkedInAttribute($value)
    {
        return $value ?? '#';
    }

    public function getlinkedInForHTMLAttribute($value)
    {
        return $value ?? '';
    }

    public function getCareerCouchAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getCoachingCertificationAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getDateOfBirthAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getCountryAttribute($value)
    {
        return $value ?? 'N/A';
    }

    public function getPhoneNumberAttribute($value)
    {
        return $value ? preg_replace('/^\+\d+\s+/', '', $value) : 'N/A';
    }

    public function getPhoneNumberForHTMLAttribute($value)
    {
        return $value ? preg_replace('/^\+\d+\s+/', '', $value) : '';
    }
}

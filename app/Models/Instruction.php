<?php

namespace App\Models;

use App\Models\Test;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instruction extends Model
{
    use HasFactory;

    protected $fillable = ['test_id', 'instruction'];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerAmbition extends Model
{
    use HasFactory;

    public function userPersonalInformation()
    {
        return $this->hasOne(UserPersonalInformation::class, 'question_no');
    }

    public function userCareerAmbition($id)
    {
        $careerAmbition = CareerAmbition::with(
            ['userPersonalInformation' => function ($query) use ($id) {
                $query->where('test_name', 'career')->where('user_id', $id);
            }]
        )->get();
        return $careerAmbition;
    }
}

<?php

namespace App\Models;

use App\Traits\GenericTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionReport extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GenericTrait;
    
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'warm_up',
        'reflections',
        'extra',
        'challenges',
        'goal_setting',
        'commitments',
    ];
    
    /**
     * Returns the student of this report
     *
     * @return App\Models\User
     */
    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }

    
    /**
     * Returns the counsellor of this report
     *
     * @return App\Models\User
     */
    public function counsellor()
    {
        return $this->belongsTo(User::class, 'counsellor_id');
    }

    /**
     * Returns the session of this report
     *
     * @return App\Models\UserCounsellorSession
     */
    public function session()
    {
        return $this->belongsTo(UserCounsellorSession::class, 'session_id');
    }

    /****************************/
    /*        Attributes        */
    /****************************/
    
    /**
     * Get the human readable date of the report
     *
     * @return string
     */
    public function getDateUploadedAttribute()
    {
        return $this->created_at->format("d-m-Y");
    }

    /**
     * Interact with the session report warm up attribute.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function warmUp(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
        );
    }

    /**
     * Interact with the session report reflections attribute.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function reflections(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ?? 'N/A',
        );
    }

    /**
     * Interact with the session report additional notes attribute.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function additionalNotes(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->extra ?? 'No additional notes given',
        );
    }

    /**
     * Interact with the session report warm up attribute.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function challenges(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
        );
    }

    public function goalSetting(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
        );
    }

    public function commitments(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
        );
    }
}

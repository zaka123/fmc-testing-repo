<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'coupon_name',
        'coupon_code',
        'discount_type',
        'discount',
        'max_redemptions',
        'category',
        'redeem_by_date',
        'status',
        'expired',
        'note',
        'currency_type',
    ];

    public function sessionPayment()
    {
        return $this->hasMany(SessionPayment::class);
    }
}

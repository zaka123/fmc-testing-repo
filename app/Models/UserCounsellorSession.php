<?php

namespace App\Models;

use App\Mail\SessionStatus;
use App\Models\SessionPayment;
use App\Models\VideoCall;
use App\Traits\GenericTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class UserCounsellorSession extends Model
{
    use HasFactory;
    use GenericTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'counsellor_id',
        'session_id',
        'status',
        'time',
        'date'
    ];

    // duration of a session (minutes)
    const userCounsellorSessionDuration = 50;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'session_timing' => 'datetime',
    ];

    /**
     * Returns the user of this session
     *
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

    /**
     * Returns the counsellor of this session
     *
     * @return App\Models\User
     */
    public function counsellor()
    {
        return $this->belongsTo(User::class, 'counsellor_id')->withDefault();
    }

    /**
     * Returns the counsellor timing of this session
     *
     * @return App\Models\CounsellorSession
     */
    public function session()
    {
        return $this->belongsTo(CounsellorSession::class, 'session_id')->withDefault();
    }

    /**
     * Returns report of this session
     *
     * @return App\Models\SessionReport
     */
    public function report()
    {
        return $this->hasOne(SessionReport::class, 'session_id');
    }

    /**
     * Determines whether report is uploaded or not
     *
     * @return bool
     */
    public function getReportUploadedAttribute()
    {
        return (bool) $this->report;
    }

    /**
     * Returns the payment object of this session
     *
     * @return App\Models\SessionPayment
     */
    public function payment()
    {
        return $this->hasOne(SessionPayment::class, 'user_counsellor_sessions_id')
            ->where('session_id', "!=", "Test Payment");
    }

    /**
     * Returns the video call of this session
     *
     * @return \App\Models\VideoCall
     */
    public function videoCall()
    {
        return $this->hasOne(VideoCall::class, 'user_counsellor_session_id');
    }

    /**
     * Returns true if the meeting is ahead
     *
     * @return bool
     */
    public function getIsStartingAttribute()
    {
        return Carbon::now()->setTimezone('UTC') <= $this->session->session_timing;
    }

    /**
     * Returns true if the meeting has started
     *
     * @return bool
     */
    public function getHasStartedAttribute()
    {
        return Carbon::now()->setTimezone('UTC') >= Carbon::parse($this->session->session_timing) && Carbon::now()->setTimezone('UTC') <= Carbon::parse($this->session->session_timing)->addMinutes($this::userCounsellorSessionDuration);
    }

    /**
     * Returns true if the meeting has started
     *
     * @return bool
     */
    public function getHasEndedAttribute()
    {
        return Carbon::now()->setTimezone('UTC') >= Carbon::parse($this->session->session_timing)->addMinutes($this::userCounsellorSessionDuration);
    }

    // just for testing purpose
    // public function getTestAttribute()
    // {
    //     // return Carbon::now() >= Carbon::parse($this->session->session_timing)->addMinutes($this::userCounsellorSessionDuration);
    //     return Carbon::now()->setTimezone('UTC');
    // }

    public function getSessionStatusAttribute()
    {
        if ($this->status == "Unpaid") {
            return "<span class='bg-secondary text-white badge'>unpaid</span>";
        } elseif ($this->status == "Pending") {
            return "<span class='bg-primary text-white badge'>pending</span>";
        } elseif ($this->status == "Confirm") {
            return "<span class='bg-success text-white badge'>confirmed</span>";
        }
    }

    public function changeStatus($status, $id)
    {
        $update =  UserCounsellorSession::where('id', $id)->first();
        if ($update) {
            $update->update(['status' => $status]);
        }
        try {
            $mail_data = [
                'counsellor_name' => $update->counsellor->full_name ?? 'unknown',
                'status'          => $status,
                'time'            => $update->session->session_time,
                'student_name'    => $update->user->full_name
            ];
            $result = Mail::to($update->user->email)->send(new SessionStatus($mail_data));
        } catch (\Exception $e) {
            return false;
        }

        return $update;
    }
}

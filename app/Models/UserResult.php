<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserResult extends Model
{
    use HasFactory;

    protected $casts = [
        'question' => 'integer',
    ];

    protected $fillable = [
        'answer',
        'students_id',
        'test',
        'question'
    ];

    public function userPersonalityResultStates($student_id)
    {
        $questions = Question::where('test_id', 4)->count();
        $personality = UserResult::where('test', 4)->where('students_id', $student_id)->count();
        $result = ((int) $personality / $questions) * 100;
        return $result;
    }

    public function checkUserTestStatus($user_id, $test_id)
    {
        $questions = Question::where('test_id', $test_id)->count();
        $answered = UserResult::where('test', $test_id)->where('students_id', $user_id)->count();

        if ($questions > 0) {
            $result = ((int) $answered / $questions) * 100;
            if (($result == 100 || $result == "100") && $test_id == 4) {
                $result = $this->getUsersResult($test_id, $user_id);
            } elseif (($result == 100 || $result == "100") && $test_id == 5) {
                $result = $this->getCareerTestResult($test_id, $user_id);
            } elseif (($result == 100 || $result == "100") && $test_id == 6) {
                // $result = $this->skillAssessmentTestResult($test_id, $user_id);
                $result = $this->combinedTestsResult($test_id, $user_id);
            }

            return $result;
        }

        return false;
    }

    public function getUsersResult($test_id, $user_id)
    {
        $extroversion = array(20);
        $agreeableness = array(14);
        $conscientiousness = array(14);
        $neuroticism = array(38);
        $opennessToExperience = array(8);
        $result = UserResult::where('test', $test_id)
            ->where('students_id', $user_id)
            ->orderBy('question', 'ASC')
            ->get();

        foreach ($result as $record) {
            if ($record->question == 1 || $record->question == "1") {
                $extroversion[$record->question] = $record->answer;
            } elseif ($record->question == 6 || $record->question == "6") {
                $extroversion[$record->question] = -$record->answer;
            } elseif ($record->question == 11 || $record->question == "11") {
                $extroversion[$record->question] = $record->answer;
            } elseif ($record->question == 16 || $record->question == "16") {
                $extroversion[$record->question] = -$record->answer;
            } elseif ($record->question == 21 || $record->question == "21") {
                $extroversion[$record->question] = $record->answer;
            } elseif ($record->question == 26 || $record->question == "26") {
                $extroversion[$record->question] = $record->answer;
            } elseif ($record->question == 31 || $record->question == "31") {
                $extroversion[$record->question] = -$record->answer;
            } elseif ($record->question == 36 || $record->question == "36") {
                $extroversion[$record->question] = -$record->answer;
            } elseif ($record->question == 41 || $record->question == "41") {
                $extroversion[$record->question] = $record->answer;
            } elseif ($record->question == 46 || $record->question == "46") {
                $extroversion[$record->question] = -$record->answer;
            }
            // here the extroversion formula ends
            elseif ($record->question == 2 || $record->question == "2") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 7 || $record->question == "7") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 12 || $record->question == "12") {
                $agreeableness[$record->question] = -$record->answer;
            } elseif ($record->question == 17 || $record->question == "17") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 27 || $record->question == "27") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 32 || $record->question == "32") {
                $agreeableness[$record->question] = -$record->answer;
            } elseif ($record->question == 37 || $record->question == "37") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 42 || $record->question == "42") {
                $agreeableness[$record->question] = $record->answer;
            } elseif ($record->question == 47 || $record->question == "47") {
                $agreeableness[$record->question] = $record->answer;
            }
            // agreeableness formula ends here
            elseif ($record->question == 3 || $record->question == "3") {
                $conscientiousness[$record->question] = $record->answer;
            } elseif ($record->question == 8 || $record->question == "8") {
                $conscientiousness[$record->question] = -$record->answer;
            } elseif ($record->question == 13 || $record->question == "13") {
                $conscientiousness[$record->question] = $record->answer;
            } elseif ($record->question == 18 || $record->question == "18") {
                $conscientiousness[$record->question] = -$record->answer;
            } elseif ($record->question == 23 || $record->question == "23") {
                $conscientiousness[$record->question] = $record->answer;
            } elseif ($record->question == 28 || $record->question == "28") {
                $conscientiousness[$record->question] = -$record->answer;
            } elseif ($record->question == 33 || $record->question == "33") {
                $conscientiousness[$record->question] = $record->answer;
            } elseif ($record->question == 38 || $record->question == "38") {
                $conscientiousness[$record->question] = -$record->answer;
            } elseif ($record->question == 43 || $record->question == "43") {
                $conscientiousness[$record->question] = $record->answer;
            } elseif ($record->question == 48 || $record->question == "48") {
                $conscientiousness[$record->question] = $record->answer;
            }
            // conscientiousness formula ends here
            elseif ($record->question == 4 || $record->question == "4") {
                $neuroticism[$record->question] = $record->answer;
            } elseif ($record->question == 9 || $record->question == "9") {
                $neuroticism[$record->question] = $record->answer;
            } elseif ($record->question == 14 || $record->question == "14") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 19 || $record->question == "19") {
                $neuroticism[$record->question] = $record->answer;
            } elseif ($record->question == 24 || $record->question == "24") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 29 || $record->question == "29") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 34 || $record->question == "34") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 39 || $record->question == "39") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 44 || $record->question == "44") {
                $neuroticism[$record->question] = -$record->answer;
            } elseif ($record->question == 49 || $record->question == "49") {
                $neuroticism[$record->question] = -$record->answer;
            }
            // neuroticism formula ends here
            elseif ($record->question == 5 || $record->question == "5") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 10 || $record->question == "10") {
                $opennessToExperience[$record->question] = -$record->answer;
            } elseif ($record->question == 15 || $record->question == "15") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 20 || $record->question == "20") {
                $opennessToExperience[$record->question] = -$record->answer;
            } elseif ($record->question == 25 || $record->question == "25") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 35 || $record->question == "35") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 40 || $record->question == "40") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 45 || $record->question == "45") {
                $opennessToExperience[$record->question] = $record->answer;
            } elseif ($record->question == 50 || $record->question == "50") {
                $opennessToExperience[$record->question] = $record->answer;
            }
            // opennessToExperience formula ends here
        }

        return [
            'extroversion' => array_sum($extroversion),
            'agreeableness' => array_sum($agreeableness),
            'conscientiousness' => array_sum($conscientiousness),
            'neuroticism' => array_sum($neuroticism),
            'opennessToExperience' => array_sum($opennessToExperience)
        ];
    }

    /**
     * @return array
     */
    public function getCareerTestResult($test_id, $user_id)
    {
        $competition = array();
        $freedom = array();
        $management = array();
        $lifeBalance = array();
        $organisationMembership = array();
        $expertise = array();
        $learning = array();
        $entrepreneurship = array();
        $result = UserResult::where('test', $test_id)
            ->where('students_id', $user_id)
            ->orderBy('question', 'ASC')
            ->get();

        foreach ($result as $key => $record) {
            if ($record->question == 1 || $record->question == 11 || $record->question == 19) {
                $competition[$key] = (int) $record->answer;
            } elseif ($record->question == 2 || $record->question == 21 || $record->question == 23) {
                $freedom[$key] = (int) $record->answer;
            } elseif ($record->question == 3 || $record->question == 7 || $record->question == 14) {
                $management[$key] = (int) $record->answer;
            } elseif ($record->question == 4 || $record->question == 6 || $record->question == 16) {
                $lifeBalance[$key] = (int) $record->answer;
            } elseif ($record->question == 9 || $record->question == 12 || $record->question == 17) {
                $organisationMembership[$key] = (int) $record->answer;
            } elseif ($record->question == 13 || $record->question == 20 || $record->question == 22) {
                $expertise[$key] = (int) $record->answer;
            } elseif ($record->question == 5 || $record->question == 10 || $record->question == 18) {
                $learning[$key] = (int) $record->answer;
            } elseif ($record->question == 8 || $record->question == 15 || $record->question == 24) {
                $entrepreneurship[$key] = (int) $record->answer;
            }
        }

        $results = [
            'Competition' => round((array_sum($competition) / 12) * 100, 2),
            'Freedom' => round((array_sum($freedom) / 12) * 100, 2),
            'Management' => round((array_sum($management) / 12) * 100, 2),
            'Life&nbsp;Balance' => round((array_sum($lifeBalance) / 12) * 100, 2),
            'Organization&nbsp;Membership' => round((array_sum($organisationMembership) / 12) * 100, 2),
            'Expertise' => round((array_sum($expertise) / 12) * 100, 2),
            'Learning' => round((array_sum($learning) / 12) * 100, 2),
            'Entrepreneurship' => round((array_sum($entrepreneurship) / 12) * 100, 2),
        ];
        arsort($results);
        return $results;
    }

    public function skillAssessmentTestResult($test_id = null, $user_id = null)
    {
        $inMeetings = 0;
        $outsideMeetings = 0;
        $speaking = 0;
        $writing = 0;
        $organizing = 0;
        $creativity = 0;
        $totalInMeetings = UserResult::whereIn('question', [1, 2, 3, 4, 5, 6, 8, 7, 9, 1, 2, 3, 4, 5, 6, 8, 7, 9])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $inMeetings = round(((int) $totalInMeetings / 45) * 100, 2);

        $totaloutsideMeetings = UserResult::whereIn('question', [10, 11, 12, 13, 14])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $outsideMeetings = round(((int) $totaloutsideMeetings / 25) * 100, 2);

        $totalspeaking = UserResult::whereIn('question', [15, 16, 17, 18, 19, 20, 21, 22, 23, 24])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $speaking = round(((int) $totalspeaking / 50) * 100, 2);

        $totalwriting = UserResult::whereIn('question', [25, 26, 27, 28, 29, 30])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $writing = round(((int) $totalwriting / 30) * 100, 2);

        $totalorganizing = UserResult::whereIn('question', [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $organizing = round(((int) $totalorganizing / 70) * 100, 2);

        $totalcreativity = UserResult::whereIn('question', [45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $creativity = round(((int) $totalcreativity / 75) * 100, 2);
        return [
            'inMeetings' => $inMeetings,
            'outsideMeetings' => $outsideMeetings,
            'speaking' => $speaking,
            'writing' => $writing,
            'organizing' => $organizing,
            'creativity' => $creativity
        ];
    }

    public function combinedTestsResult($test_id = null, $user_id = null)
    {
        $inMeetings = 0;
        $outsideMeetings = 0;
        $speaking = 0;
        $writing = 0;
        $organizing = 0;
        $creativity = 0;
        $totalInMeetings = UserResult::whereIn('question', [1, 2, 3, 4, 5, 6, 8, 7, 9, 1, 2, 3, 4, 5, 6, 8, 7, 9])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $inMeetings = round(((int) $totalInMeetings / 45) * 100, 2);

        $totaloutsideMeetings = UserResult::whereIn('question', [10, 11, 12, 13, 14])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $outsideMeetings = round(((int) $totaloutsideMeetings / 25) * 100, 2);

        $totalspeaking = UserResult::whereIn('question', [15, 16, 17, 18, 19, 20, 21, 22, 23, 24])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $speaking = round(((int) $totalspeaking / 50) * 100, 2);

        $totalwriting = UserResult::whereIn('question', [25, 26, 27, 28, 29, 30])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $writing = round(((int) $totalwriting / 30) * 100, 2);

        $totalorganizing = UserResult::whereIn('question', [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $organizing = round(((int) $totalorganizing / 70) * 100, 2);

        $totalcreativity = UserResult::whereIn('question', [45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59])->where('test', 6)->where('students_id', $user_id)->sum('answer');
        $creativity = round(((int) $totalcreativity / 75) * 100, 2);

        // for career expectation assessment report
        $competition = array();
        $freedom = array();
        $management = array();
        $lifeBalance = array();
        $organisationMembership = array();
        $expertise = array();
        $learning = array();
        $entrepreneurship = array();
        $result = UserResult::where('test', 6)
            ->where('students_id', $user_id)
            ->orderBy('question', 'ASC')
            ->get();

        foreach ($result as $key => $record) {
            if ($record->question == 60 || $record->question == 70 || $record->question == 78) {
                $competition[$key] = (int) $record->answer;
            } elseif ($record->question == 61 || $record->question == 80 || $record->question == 82) {
                $freedom[$key] = (int) $record->answer;
            } elseif ($record->question == 62 || $record->question == 66 || $record->question == 75) {
                $management[$key] = (int) $record->answer;
            } elseif ($record->question == 63 || $record->question == 65 || $record->question == 75) {
                $lifeBalance[$key] = (int) $record->answer;
            } elseif ($record->question == 68 || $record->question == 71 || $record->question == 76) {
                $organisationMembership[$key] = (int) $record->answer;
            } elseif ($record->question == 72 || $record->question == 79 || $record->question == 81) {
                $expertise[$key] = (int) $record->answer;
            } elseif ($record->question == 64 || $record->question == 69 || $record->question == 77) {
                $learning[$key] = (int) $record->answer;
            } elseif ($record->question == 67 || $record->question == 74 || $record->question == 83) {
                $entrepreneurship[$key] = (int) $record->answer;
            }
        }

        return [
            'inMeetings' => (int) round($inMeetings, 0),
            'outsideMeetings' => (int) round($outsideMeetings, 0),
            'speaking' => (int) round($speaking, 0),
            'writing' => (int) round($writing, 0),
            'organizing' => (int) round($organizing, 0),
            'creativity' => (int) round($creativity, 0),

            // for career expectation assessment report
            'Competition' => round((array_sum($competition) / 12) * 100, 2),
            'Freedom' => round((array_sum($freedom) / 12) * 100, 2),
            'Management' => round((array_sum($management) / 12) * 100, 2),
            'Life&nbsp;Balance' => round((array_sum($lifeBalance) / 12) * 100, 2),
            'Organization&nbsp;Membership' => round((array_sum($organisationMembership) / 12) * 100, 2),
            'Expertise' => round((array_sum($expertise) / 12) * 100, 2),
            'Learning' => round((array_sum($learning) / 12) * 100, 2),
            'Entrepreneurship' => round((array_sum($entrepreneurship) / 12) * 100, 2),
        ];
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use MacsiDigital\Zoom\Facades\Zoom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserProfile extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'average_grade_point',
        'city',
        'date_of_birth',
        'difficult_subject',
        'easiest_subject',
        'father_education',
        'father_occupation',
        'favorite_subject',
        'mother_education',
        'mother_occupation',
        'parents_retired',
        'user_id'
    ];

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }

    public function addProfileZoom($name, $last_name, $email)
    {
        try {
            $meeting = Zoom::user()->create(
                [
                'first_name' => $name,
                'last_name'  => $last_name,
                'email'      => $email,
                'password'   => 'secret1234'
                ]
            );
        } catch (\Exception $e) {
            $meeting = json_decode($e);
        }
        return $meeting;
    }
}

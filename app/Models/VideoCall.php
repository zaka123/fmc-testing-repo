<?php

namespace App\Models;

use App\Models\User;
use App\Models\UserCounsellorSession;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MacsiDigital\Zoom\Facades\Zoom;

class VideoCall extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'counsellor_id',
        'user_counsellor_session_id',
        'start_url',
        'join_url',
        'attended',
        'meeting_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'attended' => 'boolean',
    ];

    /**
     * Returns the student of this meeting
     *
     * @return \App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Returns the counsellor of this meeting
     *
     * @return \App\Models\User
     */
    public function counsellor()
    {
        return $this->belongsTo(User::class, 'counsellor_id');
    }

    /**
     * Returns the starting link to this meeting
     *
     * @return string
     */
    public function getStartingLinkAttribute()
    {
        return $this->start_url;
    }

    /**
     * Returns the joining link to this meeting
     *
     * @return string
     */
    public function getJoiningLinkAttribute()
    {
        return $this->join_url;
    }

    /**
     * Returns true if the meeting is attended
     * otherwise false
     *
     * @return bool
     */
    public function userCounsellorSession()
    {
        return $this->belongsTo(UserCounsellorSession::class, 'user_counsellor_session_id');
    }

    /**
     * Returns true if the meeting is attended
     * otherwise false
     *
     * @return bool
     */
    public function getstatusAttribute()
    {
        return $this->attended;
    }

    /**
     * Creates the zoom profile
     *
     * @return
     */
    public static function createZoomProfile($student)
    {
        try {
            return json_decode(Zoom::user()->create(
                [
                    'first_name' => $student->name,
                    'last_name' => $student->last_name,
                    'email' => $student->email,
                    'password' => 'secret1234'
                ]
            )
            );
        } catch (\Exception $e) {
            return json_decode($e);
        }
    }

    /**
     * Arranges zoom meeting on a specific time
     *
     * @return
     */
    public static function arrangeZoomMeeting($data)
    {
        return Zoom::meeting()->make(
            [
                'topic' => $data->topic ?? env("APP_NAME") . " Meeting",
                'type' => $data->type ?? 1,
                'duration' => $data->duration ?? UserCounsellorSession::userCounsellorSessionDuration,
                'start_time' => $data->session_timing,
                'timezone' => env("TIME_ZONE"),
            ]
        );
    }
}

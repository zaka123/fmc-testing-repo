<?php

namespace App\Models;

use App\Events\SessionPaymentApproved;
use App\Models\UserCounsellorSession;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionPayment extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_counsellor_sessions_id',
        'bank',
        'branch',
        'transaction_id',
        'session_id',
        'student_id',
        'receipt',
        'status',
        'amount',
        'currency',
        'coupon_id',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        // 'created' => SessionPaymentApproved::class,
        // 'updated' => SessionPaymentApproved::class,
    ];

    const personalityTestPayment = 500; // persontality test payment amount
    const careerExpectationsTestPayment = 1000; // career expectations test payment amount
    const skillAssessmentTestPayment = 1500; // skill assessment test payment amount
    // const sessionPaymentAmount = 5000; // session payment amount in PKR
    const sessionPaymentAmount = 17; // session payment amount in USD

    public function paymentStatus(): Attribute
    {
        if ($this->status == "Unpaid") {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-secondary">unpaid</span>',
            );
        } elseif ($this->status == "Pending") {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-primary">pending</span>',
            );
        } elseif ($this->status == "Confirm") {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-success">confirmed</span>',
            );
        } elseif ($this->status == "Cancel") {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-warning">cancelled</span>',
            );
        } else {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-danger">Not Verified</span>',
            );
        }
    }

    public function paymentType(): Attribute
    {
        if ($this->session_id == "Test Payment") {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-primary">Test</span>',
            );
        } else {
            return Attribute::make(
            get: fn($value) => '<span class="badge bg-success">Session</span>',
            );
        }
    }

    public function paymentPurpose(): Attribute
    {
        if (is_numeric($this->session_id)) {
            return Attribute::make(
            get: fn() => '<span class="badge bg-primary">Session Payment</span>',
            );
        }

        switch ($this->user_counsellor_sessions_id) {
            case 4:
                return Attribute::make(
                get: fn() => '<span class="badge bg-success">Personality Test</span>',
                );

            case 5:
                return Attribute::make(
                get: fn() => '<span class="badge bg-secondary">Career Expectations Test</span>',
                );


            default:
                return Attribute::make(
                get: fn() => '<span class="badge bg-info">Skill Assessment Test</span>',
                );
        }
    }

    /**
     * Returns true if the payment is confirmed
     * otherwise false
     *
     * @return bool
     */
    public function getIsApprovedAttribute()
    {
        return $this->status == "Confirm";
    }

    /**
     * Returns the user counsellor session of this payment
     *
     * @return App\Models\UserCounsellorSession
     */
    public function userCounsellorSession()
    {
        return $this->belongsTo(UserCounsellorSession::class, 'user_counsellor_sessions_id')
            ->where("session_id", "!=", "Test Payment");
    }

    public function getTestPayment($id = null)
    {
        $payment = SessionPayment::where('student_id', auth()->id())
            ->where('user_counsellor_sessions_id', $id)
            ->where('session_id', 'Test Payment')
            ->first();
        return $payment;
    }

    public static function latestPayments()
    {
        return self::with('user')->orderBy('id', 'DESC')->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function counsellorSession()
    {
        return $this->belongsTo(CounsellorSession::class, 'session_id');
    }

    public function coupon(){
        return $this->belongsTo(Coupon::class);
    }
}

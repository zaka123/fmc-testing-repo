<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DecisionInfluencer extends Model
{
    use HasFactory;

    public function userPersonalInformation()
    {
        return $this->hasOne(UserPersonalInformation::class, 'question_no');
    }

    public function userDecisionInfluencer($id)
    {
        $decisionInfluencer = DecisionInfluencer::with(
            ['userPersonalInformation' => function ($query) use ($id) {
                $query->where('test_name', 'decision')->where('user_id', $id);
            }]
        )->get();
        return $decisionInfluencer;
    }
}

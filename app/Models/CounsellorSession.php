<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CounsellorSession extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'counsellor_id',
        'session_date',
        'session_timing',
        'description',
        'timezone',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'session_timing' => 'datetime',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'counsellor_id')->withDefault();
    }

    public function userCounsellorSession()
    {
        return $this->hasMany(UserCounsellorSession::class, 'session_id');
    }

    public function payment()
    {
        return $this->hasOne(SessionPayment::class, 'session_id');
    }

    public static function getCounsellorSessions($id)
    {
        return self::where('counsellor_id', $id)->orderBy('session_timing', 'DESC')->get();
    }

    /**
     * Returns the formatted date of the session
     *
     * @return
     */
    public function date(): Attribute
    {
        return Attribute::make(
		get: fn() => (new DateTime($this->session_timing, new DateTimeZone('UTC')))->setTimezone(new DateTimeZone($this->users->timezone))->format('d-m-Y'),
	);
    }

    /**
     * Returns the formatted time of the session
     *
     * @return
     */
    public function time(): Attribute
    {
        return Attribute::make(
            get: fn() => (new DateTime($this->session_timing, new DateTimeZone('UTC')))->setTimezone(new DateTimeZone($this->users->timezone))->format('h:i A'),
        );
    }

    /**
     * Returns the formatted date of the session for html
     *
     * @return
     */
    public function getDateForHTMLAttribute()
    {
        // return $this->session_timing->format('Y-m-d');
        return (new DateTime($this->session_timing, new DateTimeZone('UTC')))->setTimezone(new DateTimeZone(auth()->user()->timezone))->format('Y-m-d');
    }

    /**
     * Returns the formatted time of the session for html
     *
     * @return
     */
    public function getTimeForHTMLAttribute()
    {
        // return $this->session_timing->format('H:i:s');
        return (new DateTime($this->session_timing, new DateTimeZone('UTC')))->setTimezone(new DateTimeZone(auth()->user()->timezone))->format('H:i:s');
    }

    /**
     * Returns true if this session has no users
     * otherwise false
     *
     * @return bool
     */
    public function canBeBooked()
    {
        return ! $this->hasMany(UserCounsellorSession::class, 'session_id')->exists();
    }
}

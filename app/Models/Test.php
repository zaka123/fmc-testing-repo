<?php

namespace App\Models;

use App\Models\Instruction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['test_name'];

    public function questions()
    {
        return $this->hasMany(Question::class, 'test_id');
    }

    public function userTestsStatistics($user_id)
    {
        $result = array();
        $personality_test = UserResult::where('test', 4)->where('students_id', $user_id)->count();
        $result['1'] = ($personality_test/50) *100;

        $career_ambition_test = UserResult::where('test', 5)->where('students_id', $user_id)->count();
        $result['2'] = ($career_ambition_test / 24) * 100;

        $career_ambition_test = UserResult::where('test', 6)->where('students_id', $user_id)->count();
        $result['3'] = ($career_ambition_test / 59) * 100;

        return $result;
    }

    /**
     * Display a list of this instructions
     */
    public function instructions()
    {
        return $this->hasMany(Instruction::class);
    }
}

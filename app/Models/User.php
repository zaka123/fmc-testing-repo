<?php

namespace App\Models;

use App\Models\SessionPayment;
use App\Models\UserResult;
use App\Traits\GenericTrait;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use MacsiDigital\Zoom\Facades\Zoom;
use Spatie\Permission\Traits\HasRoles;

/**
 * Summary of User
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use SoftDeletes;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    use GenericTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'email_verified_at',
        'gender',
        'last_name',
        'name',
        'password',
        'password_changed',
        'profession',
        'status',
        'profile_photo_path',
        'phone',
        'verification',
        'phone_verified'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password_changed' => 'boolean',
        'status' => 'boolean',
        'phone_verified'=> 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'gender' => null,
    ];

    /**
     * Returns recently created nth students
     *
     * @param  int $count which defaults to all
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function latestStudents($count = null)
    {
        return self::role("User")
            ->whereHas('userProfile')
            ->orWhereHas('userProfessional')
            ->latest()
            ->take($count)
            ->get();
    }

    /**
     * Returns recently created nth counsellors
     *
     * @param  int $count which defaults to all
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function latestCounsellors($count = null)
    {
        return self::role('Counsellor')
            ->whereHas('counsellorProfile')
            ->latest()
            ->take($count)
            ->get();
    }

    /**
     * Returns the profile of this counsellor
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function counsellorProfile()
    {
        return $this->hasOne(CounsellorProfile::class);
    }

    /**
     * Returns the profile of this professional
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userProfessional()
    {
        return $this->hasOne(UserProfessional::class, 'user_id');
    }

    /**
     * Returns the profile of this student
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userProfile()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }

    /**
     * Returns profile of the user
     *
     * @return ?
     */
    public function profile()
    {
        if ($this->hasRole("Counsellor")) {
            return $this->counsellorProfile();
        } elseif ($this->hasRole("User")) {
            if ($this->profession == "Professional") {
                return $this->userProfessional();
            } else {
                return $this->userProfile();
            }
        }

        throw new \Exception("Admins do not have profiles.");
    }

    public function address()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    /**
     * Returns a list of recently registered unique (by users) sessions
     *
     * @param  int $count
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function recentlyRegisteredSessions($count = 3)
    {
        return $this->hasMany(UserCounsellorSession::class, 'counsellor_id')
            ->whereRelation(
                'payment',
                function ($query) {
                    $query->where('status', 'Confirm');
                }
            )
            ->where('status', 'Confirm')
            ->latest()
            ->take($count)
            ->get()
            ->unique('user_id');
    }

    /**
     * Returns the session history of this user/counsellor
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function sessionHistory()
    {
        if ($this->hasRole("User")) {
            return $this->hasMany(UserCounsellorSession::class, 'user_id')
                ->where('status', 'Confirm')
                ->whereRelation('session', 'session_timing', '<', date('Y-m-d H:i'))
                ->latest()
                ->get();
        } else {
            return $this->hasMany(UserCounsellorSession::class, 'counsellor_id')
                ->where('status', 'Confirm')
                ->whereRelation('payment', 'status', 'Confirm')
                ->whereRelation('session', 'session_timing', '<', date('Y-m-d H:i'))
                ->latest()
                ->get();
        }
    }

    /**
     * Returns a list of sessions that are paid and attended
     *
     * @param  int $count
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function latestSessions($count = 3)
    {
        $obj_id = $this->hasRole("User") ? "user_id" : "counsellor_id";
        return $this->hasMany(UserCounsellorSession::class, $obj_id)
            ->whereRelation('session', 'session_timing', '<', date('Y-m-d H:i'))
            ->where("status", "Confirm")
            ->latest()
            ->take($count)
            ->get();
    }

    /**
     * Returns a list of sessions that are coming
     *
     * @param  int $count
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function upcomingSessions($count = 3)
    {
        $obj_id = $this->hasRole("User") ? "user_id" : "counsellor_id";
        return $this->hasMany(UserCounsellorSession::class, $obj_id)
            ->whereRelation('session', 'session_timing', '>', date('Y-m-d H:i'))
            ->where('status', 'Confirm')
            // ->whereHas(
            //     'payment',
            //     function (Builder $query) {
            //         $query->where('status', 'Confirm');
            //     }
            // )
            ->latest()
            ->take($count)
            ->get();
    }

    /**
     * Returns the all the available timings of this counsellor
     * on which counsellor has no meetings
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function counsellorSessions()
    {
        return $this->hasMany(CounsellorSession::class, 'counsellor_id')
            ->with(
                'userCounsellorSession',
                function ($query) {
                    $query->where('status', '!=', 'Confirm');
                }
            )
            ->where('session_timing', '>=', Carbon::now()->addHours(48));
            // ->where('session_timing', '>=', Carbon::now());
    }

    /**
     * Returns all sessions of this user/counsellor
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function sessions()
    {
        if ($this->hasRole("User")) {
            return $this->hasMany(UserCounsellorSession::class, 'user_id')
                ->latest()
                ->get()
                ->unique('session_id');
        }

        return $this->hasMany(UserCounsellorSession::class, 'counsellor_id')
            ->whereHas(
                'payment',
                function (Builder $query) {
                    $query->where('status', 'Confirm');
                }
            )
            ->latest()
            ->get()
            ->unique("session_id");
    }

    /**
     * Returns all payments made by this student
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function payments()
    {
        return $this->hasMany(SessionPayment::class, 'student_id')
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Returns a list of session reports of this user/counsellor sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        $obj_id = $this->hasRole("User") ? 'student_id' : 'counsellor_id';

        return $this->hasMany(SessionReport::class, $obj_id);
    }

    public function updateUser($request)
    {
        $user = User::where('id', auth()->id())->update(
            [
                'name' => $request->input('name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email'),
                'gender' => $request->input('gender'),
                'profession' => $request->input('profession'),
            ]
        );
        return $user;
    }

    /**
     * Returns personal information of this student
     *
     * @param  string $test_name
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function personalInformation($test)
    {
        return $this->hasMany(UserPersonalInformation::class, 'user_id')
            ->where('test_name', $test)
            ->get();
    }

    /**
     * Returns true if this user has paid for the test
     * otherwise false
     *
     * @return bool
     */
    public function hasPaidFor($test_id, $session_id = "Test Payment")
    {
        return $this->hasOne(SessionPayment::class, 'student_id')
            ->where("user_counsellor_sessions_id", $test_id)
            ->where("session_id", $session_id)
            ->exists();
    }

    /**
     * Returns the specified result object of this user
     *
     * @return mixed
     */
    public function getTestResults($test_id)
    {
        return (new UserResult())->checkUserTestStatus($this->id, $test_id);
    }

    /**
     * Returns the personality test results of this user
     */
    public function personalityTestResults()
    {
        return (new UserResult())->checkUserTestStatus($this->id, 4);
    }

    /**
     * Returns the career expectations test results of this user
     */
    public function careerExpectationsTestResults()
    {
        return (new UserResult())->checkUserTestStatus($this->id, 5);
    }

    /**
     * Returns the skill assessment test results of this user
     */
    public function skillAssessmentTestResults()
    {
        return (new UserResult())->checkUserTestStatus($this->id, 6);
    }

    /**
     * Returns the latest reviews of the student
     *
     * @param  int $count
     * @return \Illuminate\Support\Collection
     */
    public function latestReports($count = 4)
    {
        return $this->hasMany(SessionReport::class, 'student_id')->orderBy('created_at', 'desc')->take($count)->get();
    }

    public function results()
    {
        return $this->hasMany(UserResult::class, 'students_id');
    }

    /**
     * Returns the number of last attempted question of the specified test
     *
     * @param int $test = 4
     * @return int
     */
    public function getLastAttemptedQuestionNumber($test = 4)
    {
        return $this->results()
            ->where('test', $test)
            ->count();
    }

    public function getTestProgress($test)
    {
        $completedQuestions = $this->results()
            ->where('test', $test)
            ->count();
        $totalQuestions = Test::findOrFail($test)->questions->count();
        $progress = $totalQuestions > 0 ? $completedQuestions / $totalQuestions * 100 : 0;
        return round($progress, 2);
    }

    /**
     * Summary of hasZoomAccount
     * @return mixed
     */
    public function hasZoomAccount()
    {
        return Zoom::user()->find($this->email);
    }

    public function profilePhoto(): Attribute
    {
        if ($this->profile_photo_path != null) {
            return Attribute::make(
            get: fn() => 'storage/' . $this->profile_photo_path,
            ); }
        return Attribute::make(get: fn() => 'assets/img/userProfile.png', ); } /**
                * Interact with the user's full name attribute
                *
                * @return \Illuminate\Database\Eloquent\Casts\Attribute
                */
    public function fullName(): Attribute {
        return Attribute::make(
        get: fn() => ucwords($this->first_name . ' ' . $this->last_name),
        ); } /**
           * Interact with the user's name attribute
           *
           * @return \Illuminate\Database\Eloquent\Casts\Attribute
           */
    public function name(): Attribute {
        return Attribute::make(
        get: fn($value) => ucwords(strtolower($value)),
        set: fn($value) => strtolower($value)
        ); } /**
           * Interact with the user's first name attribute
           *
           * @return \Illuminate\Database\Eloquent\Casts\Attribute
           */
    public function firstName(): Attribute {
        return Attribute::make(
        get: fn() => ucwords(strtolower($this->name)),
        set: fn($value) => $this->setAttribute('name', strtolower($value))
        ); } /**
           * Interact with the user's last name attribute
           *
           * @return \Illuminate\Database\Eloquent\Casts\Attribute
           */
    public function lastName(): Attribute {
        return Attribute::make(
        get: fn($value) => ucwords(strtolower($value)),
        set: fn($value) => strtolower($value)
        ); } /**
           * Interact with the user's email attribute
           *
           * @return \Illuminate\Database\Eloquent\Casts\Attribute
           */
    public function email(): Attribute {
        return Attribute::make(
        get: fn($value) => strtolower($value),
        set: fn($value) => strtolower($value),
        ); }
    public function verified(): Attribute {
        if ($this->email_verified_at != null) {
            return Attribute::make(
            get: fn() => '<span class="badge badge-success">Verified</span>',
            ); }
        return Attribute::make(get: fn() => '<span class="badge badge-danger">Not Verified</span>', ); }
    public function getProfileCompletedAttribute() {
        return (
            $this->userProfile
            && $this->personalInformation("Personal")->isNotEmpty()
            && $this->personalInformation("career")->isNotEmpty()
            && $this->personalInformation("decision")->isNotEmpty()
            && $this->personalInformation("study_habits_and_routine")->isNotEmpty()
            && $this->personalInformation("future_self_reflection")->isNotEmpty()
        );
    }

    public function zoomToken()
    {
        return $this->hasOne(ZoomToken::class);
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserProfessional extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','date_of_birth','city','experience','education_level','organization_name','role'];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }

    public function getRoleAttribute($value) {
        return $value ?? 'N/A';
    }
    
    public function getOrganizationNameAttribute($value) {
        return $value ?? 'N/A';
    }
}

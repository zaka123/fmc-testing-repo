<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalHabits extends Model
{
    use HasFactory;

    public function userPersonalInformation()
    {
        return $this->hasOne(UserPersonalInformation::class, 'question_no');
    }

    public function userPersonalHabits($id)
    {
        $personalHabits = PersonalHabits::with(
            ['userPersonalInformation'=>function ($query) use ($id) {
                $query->where('test_name', 'Personal')->where('user_id', $id);
            }]
        )->get();
        return $personalHabits;
    }
}

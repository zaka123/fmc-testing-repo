<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPersonalInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'test_name',
        'user_id',
        'question_no',
        'answer'
    ];

    /**
     * Reasons for getting a job
     */
    const reasonsForGettingJob = [
        1 => "Which is dynamic, interesting, varied",
        2 => "With social prestige and admired by others",
        3 => "With a stable salary",
        4 => "Where you can learn continuously",
        5 => "Which allows me to be original and creative",
        6 => "Prefer not to say",
    ];

    /**
     * Reasons for working
     */
    const reasonsForWorking = [
        1 => "Financial Independence",
        2 => "Education Requirements Fulfilled",
        3 => "Family Support",
        4 => "Societal Pressure",
        5 => "Lack of Vision",
        6 => "Other Reason",
        7 => "Prefer not to say",
    ];

    /**
     * Reasons for studying
     */
    const reasonsForStudying = [
        1 => "Lack of Vision",
        2 => "Lack of Skills",
        3 => "Lack of opportunities",
        4 => "Societal Pressure",
        5 => "Acquire Higher Education",
        6 => "Other Reason",
        7 => "Prefer not to say",
    ];

    public function getAnswerAttribute($value)
    {
        if (is_numeric($value)) {
            return $value;
        }

        $data = json_decode($value);
        if (json_last_error() === JSON_ERROR_NONE) {
            return (array) $data;
        }

        return (string) $value;
    }

    public static function storePersonalInformation($request, $test_name)
    {
        $userResult = array();
        foreach ($request as $key => $answer) {
            if (is_array($answer)) {
                $userResult[] = UserPersonalInformation::updateOrInsert(
                    [
                        'test_name' => $test_name,
                        'user_id' => auth()->id(),
                        'question_no' => intval($key)
                    ],
                    [
                        'answer' => json_encode($answer)
                    ]
                );
            } else {
                $userResult[] = UserPersonalInformation::updateOrInsert(
                    [
                        'test_name' => $test_name,
                        'user_id' => auth()->id(),
                        'question_no' => intval($key)
                    ],
                    [
                        'answer' => $answer
                    ]
                );
            }
        }

        return $userResult;
    }
}
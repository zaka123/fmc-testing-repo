<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['question_no','category','sub_category','question_sub','question','option','option_second','option_third','option_fourth','test_id', 'option_five'];
    
    public function tests()
    {
        return $this->belongsTo(Test::class, 'test_id')->withDefault();
    }

    public function userResult()
    {
        return $this->hasOne(UserResult::class, 'question', 'question_no');
    }
}

<?php

namespace App\Policies\Counsellor;

use App\Models\SessionReport;
use App\Models\User;
use App\Models\UserCounsellorSession;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionReportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, SessionReport $sessionReport)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User                  $counsellor
     * @param  \App\Models\UserCounsellorSession $session
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $counsellor, UserCounsellorSession $session)
    {
        return true;
        return $counsellor->id == $session->counsellor->id;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, SessionReport $sessionReport)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, SessionReport $sessionReport)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, SessionReport $sessionReport)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\SessionReport $sessionReport
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, SessionReport $sessionReport)
    {
        //
    }
}

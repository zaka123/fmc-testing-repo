<?php

namespace App\Policies;

use App\Models\SessionPayment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionPaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return ($user->hasRole("Super Admin") || $user->hasRole("User"));
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, SessionPayment $sessionPayment)
    {
        return $user->hasRole("Super Admin");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasRole("User");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, SessionPayment $sessionPayment)
    {
        return ($user->hasRole("Super Admin") && $sessionPayment->status == "Confirm");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, SessionPayment $sessionPayment)
    {
        return (($user->hasRole("Super Admin") || $user->id == $sessionPayment->user->id) && $sessionPayment->status != "Confirm");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, SessionPayment $sessionPayment)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\SessionPayment $sessionPayment
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, SessionPayment $sessionPayment)
    {
        return false;
    }
}

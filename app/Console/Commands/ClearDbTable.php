<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearDbTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'table:clear {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the specified database table';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = $this->argument('table');

        DB::table($table)->truncate();

        $this->info("Table '$table' has been cleared.");
    }
}

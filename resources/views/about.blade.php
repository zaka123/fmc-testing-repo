@extends('layouts.new_app', ['pagename' => 'About us'])
@section('css')
    <style>
        .text-special {
            color: #749BC2 !important;
        }

        .row {
            width: 100%;
            margin-top: 80px;
            z-index: -1;
        }

        @media(max-width:768px) {
            .row {
                flex-direction: column-reverse;
                margin-top: 15px !important;
            }
        }

        .about_text p {
            text-align: justify;
            font-size: 17px;
            font-weight: 400;
        }

        .about_text h3 {
            font-family: myFont;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-6 about_text mt-5 mt-md-1">
            <h3 class="text-uppercase text-special"><b>Our Story</b></h3>
            <p class="text-secondary">Find Me Career’s philosophy revolves around self-awareness. However, it is multifaceted
                in many ways, where the team works towards unlocking the true potential of individuals. The platform
                connects the experienced professionals and veterans in the field of coaching and mentoring from academia and
                industry to keen students and professionals for exponential growth.
                From a variety of experts around the globe, the best solutions for your development that fits your profile
                and skill set are devised. They do not just help clients choose a career, but mentor and coach them by
                setting smart career goals to achieve in the least amount of time.
                The mission is to help create professionals in all walks of life through sustained and diligent coaching and
                mentorship.
                When someone become a part of FindMeCareer, they become more than clients; they become a part of an
                ever-growing family of professionals and students from around the world!</p>
        </div>
        <div class="col-12 col-md-6 " style="height: 400px">
            <div style="width: 100%;height:100%">
                <img src="{{asset('new_design_assets/img/new/about_us.svg')}}" alt="" width="100%" height="100%" style="max-width: 100%;object-fit:contain">
            </div>
        </div>
    </div>
@endsection

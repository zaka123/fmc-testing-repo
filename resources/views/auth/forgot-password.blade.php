@extends('layouts.navbar')
@section('css')
    <style>
        label {
            color: var(--special) !important
        }
        .hoverChhanger:hover{
            background-color:black!important;
        }
    </style>
@endsection
@section('content')
    <div class="container pb-5 d-flex align-items-center justify-content-center" style="min-height:82vh">
        <div class="card" style="width:35rem;--bs-card-border-color:var(--special)">
            <h5 class="card-header bg-special">Did you forget password?</h5>
            <div class="card-body">
                <div class="card-text mb-3 text-muted">
                    No problem. Just let us know your email address and we will email you a <code>password reset link</code>
                    that will allow you to choose a new one.
                </div>
                <form method="POST" action="{{ route('password.email') }}" id="resetPasswordForm">
                    @csrf
                    <div class="form-floating">
                        <input id="email" class="form-control" type="email" name="email" value="{{ old('email') }}"
                            required="" autofocus="" placeholder="Enter your email">
                        <label for="email" class="form-label">Enter your email:</label>
                    </div>
                </form>
                @if (session('status')=='We have emailed your password reset link!')
                    <div class="my-2 text-info" >
                        {{-- {{ session('status') }} --}}
                        A password reset link has been sent to your email, please check for spam and wait for 5 minutes before resending new email.
                    </div>
                @endif
            </div>
            <button class="card-footer btn btn-sm bg-special text-white hoverChhanger" form="resetPasswordForm">Email Password Reset Link</button>
        </div>
    </div>
@endsection
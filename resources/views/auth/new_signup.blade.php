@extends('layouts.new_app', ['pagename' => 'Signup'])
@section('css')
@endsection
@section('content')
    <div class="wrapper_div w-100 h-100 d-flex flex-column flex-md-row gap-md-5 ">
        <div class="tagline position-relative">
            <div class="text">
                <h3><span class="multiple-text"></span></h3>
            </div>
            <div class="hero_img  position-absolute" style="right: -80px">
                <img src="{{ asset('new_design_assets/img/new/hero_img.svg') }}" alt="">
            </div>
        </div>
        <div class="login">
            <h3><b>Sign up</b></h3>
            <p>Already have an account? <a href="{{ route('login') }}">Sign in</a></p>
            <form class="" action="{{ route('register') }}" method="post" id="newSignupForm">
                @csrf
                <div class="input active">
                    <label>Name</label>
                    <svg width="17" height="17" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"
                        style="margin-top: 1px;margin-left:1px">
                        <path fill="#749bc2"
                            d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0S96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z" />
                    </svg>
                    <input type="text" name="name" id="name" value="{{ old('name') }}">
                    @error('name')
                        <small class="text-danger">{{ $message }}</small>
                        <script>
                            document.querySelector('.active').classList.remove('active');
                        </script>
                    @enderror
                </div>
                <div class="input active">
                    <label>Email</label>
                    <svg width="20" height="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#749bc2"
                            d="M20 4H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2zm0 4.7l-8 5.334L4 8.7V6.297l8 5.333l8-5.333V8.7z" />
                    </svg>
                    <input type="email" name="email" id="email" value="{{ old('email') }}">
                    @error('email')
                        <small class="text-danger">{{ $message }}</small>
                        <script>
                            document.querySelector('.active').classList.remove('active');
                        </script>
                    @enderror
                </div>
                <div class="input active">
                    <label>Password</label>
                    <svg class="psw_lock" width="15" height="15" viewBox="0 0 448 512"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill="#749bc2"
                            d="M144 144v48h160v-48c0-44.2-35.8-80-80-80s-80 35.8-80 80zm-64 48v-48C80 64.5 144.5 0 224 0s144 64.5 144 144v48h16c35.3 0 64 28.7 64 64v192c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V256c0-35.3 28.7-64 64-64h16z" />
                    </svg>
                    <svg id="pswEye" width="14" height="14" viewBox="0 0 576 512"
                        xmlns="http://www.w3.org/2000/svg" class="eye">
                        <path fill="#6b7280"
                            d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0a144 144 0 1 1-288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z" />
                    </svg>
                    <input type="password" name="password" id="password">
                    @error('password')
                        <small class="text-danger">{{ $message }}</small>
                        <script>
                            document.querySelector('.active').classList.remove('active');
                        </script>
                    @enderror
                </div>
                <div class="input active">
                    <label>Confirm Password</label>
                    <svg class="psw_lock" width="15" height="15" viewBox="0 0 448 512"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill="#749bc2"
                            d="M144 144v48h160v-48c0-44.2-35.8-80-80-80s-80 35.8-80 80zm-64 48v-48C80 64.5 144.5 0 224 0s144 64.5 144 144v48h16c35.3 0 64 28.7 64 64v192c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V256c0-35.3 28.7-64 64-64h16z" />
                    </svg>
                    <svg id="pswConEye" width="14" height="14" viewBox="0 0 576 512"
                        xmlns="http://www.w3.org/2000/svg" class="eye">
                        <path fill="#6b7280"
                            d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0a144 144 0 1 1-288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z" />
                    </svg>
                    <input type="password" name="password_confirmation" id="password_confirmation">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="terms" id="terms"
                            @if (old('terms')) checked @endif>
                        Accept <a href="/terms-of-use">terms &&nbsp;conditions</a>
                    </label>
                    @error('terms')
                        <p class="text-danger" style="font-size: 14px">{{ $message }}</p>
                        <script>
                            document.querySelector('.checkbox').style.marginBottom = "5px";
                        </script>
                    @enderror
                </div>
                <button class="signup">Signup</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#pswEye').mousedown(function() {
                $('#password').attr({
                    type: 'text'
                });
            });
            $('#pswEye').mouseup(function() {
                $('#password').attr({
                    type: 'password'
                });
            });

            $('#pswConEye').mousedown(function() {
                $('#password_confirmation').attr({
                    type: 'text'
                });
            });
            $('#pswConEye').mouseup(function() {
                $('#password_confirmation').attr({
                    type: 'password'
                });
            });
        });
        // running text animation
        var typed = new Typed(".multiple-text", {
            strings: ["Believe,&nbsp;Achieve,<br> Thrive", "Meet Your  Career<br> Coach",
                "Your Path to Successful<br> Career", "Empower Your <br>Professional Growth",
                "Crafting Careers,<br> Fulfilling Dreams"
            ],
            typeSpeed: 100,
            backDelay: 4000,
            loop: true
        })
    </script>
@endsection

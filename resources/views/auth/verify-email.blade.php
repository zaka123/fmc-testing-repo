@extends('layouts.navbar')
@section('css')
    <style>
        body>nav.navbar.navbar-expand-lg.navbar-light.mb-5.pt-3.sticky-top {
            margin-bottom: 0 !important
        }

        body {
            background-image: url("/assets/icons/index.svg") !important;
            background-size: cover !important;
            background-color: rgb(108 187 222 / 0.4) !important
        }

        .container>.card {
            translate: 0 -15%
        }
    </style>
@endsection
@section('content')
    <div class="container d-flex align-items-center justify-content-center" style="min-height:89.5vh">
        <div class="card shadow" style="width:35rem">
            <h4 class="card-header bg-special">Attention!</h4>
            <div class="card-body">
                <div class="card-text mb-3">
                    Thanks for signing up! Before getting started, could you verify your email address by clicking on the
                    link we just emailed to you? If you didn't receive the email, we will gladly send you another.
                </div>
                @if (session('status') == 'verification-link-sent')
                    <div class="card-text-mb-3 fs-6">
                        A new verification link has been sent to the email address you provided during registration.
                    </div>
                @endif
            </div>
            <div class="card-footer">
                <button class="btn btn-sm me-2 btn-outline-success" type="submit" form="_resend">Resend Verification
                    Email</button>
                <button class="btn btn-outline-danger btn-sm" type="submit" form="_logout">Logout</button>
                <form method="POST" action="{{ url('email/verification-notification') }}" id="_resend">@csrf</form>
                <form method="POST" action="{{ route('logout') }}" id="_logout">@csrf</form>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.new_app', ['pagename' => 'Sign in'])

@section('content')
    <div class="wrapper_div w-100 h-100 d-flex flex-column flex-md-row gap-md-5 ">
        <div class="tagline position-relative">
            <div class="text">
                <h3><span class="multiple-text"></span></h3>
            </div>
            <div class="hero_img  position-absolute" style="right: -80px">
                <img src="{{ asset('new_design_assets/img/new/hero_img.svg') }}" alt="">
            </div>
        </div>
        <div class="login pt-5">
            <h3><b>Sign in</b></h3>
            <p>Don't have account? <a href="/get-started">Signup</a></p>
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="input active">
                    <label>Email</label>
                    <svg width="20" height="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#749bc2"
                            d="M20 4H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2zm0 4.7l-8 5.334L4 8.7V6.297l8 5.333l8-5.333V8.7z" />
                    </svg>
                    <input type="email" name="email" id="email" value="{{ old('email') }}" autofocus="true">
                    @error('email')
                        <small class="text-danger">{{ $message }}</small>
                        <script>
                            document.querySelector('.active').classList.remove('active');
                        </script>
                    @enderror
                </div>
                <div class="input active">
                    <label>Password</label>
                    <svg class="psw_lock" width="15" height="15" viewBox="0 0 448 512"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill="#749bc2"
                            d="M144 144v48h160v-48c0-44.2-35.8-80-80-80s-80 35.8-80 80zm-64 48v-48C80 64.5 144.5 0 224 0s144 64.5 144 144v48h16c35.3 0 64 28.7 64 64v192c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V256c0-35.3 28.7-64 64-64h16z" />
                    </svg>
                    <svg id="pswEye" width="14" height="14" viewBox="0 0 576 512"
                        xmlns="http://www.w3.org/2000/svg" class="eye">
                        <path fill="#6b7280"
                            d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0a144 144 0 1 1-288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z" />
                    </svg>
                    <input type="password" name="password" id="password" minlength="8">
                    @error('password')
                        <small class="text-danger">{{ $message }}</small>
                        <script>
                            document.querySelector('.active').classList.remove('active');
                        </script>
                    @enderror
                </div>
                <div class="checkbox ">
                    <input type="checkbox" id="keepmelogin" name="remember">
                    <label class="" for="keepmelogin" style="color:#749BC2"> Keep me logged in </label><br>
                    <a href="{{ route('password.request') }}" class="">Forgot Password?</a>
                </div>
                <button class="signup">Sign in</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#pswEye').mousedown(function() {
                $('#password').attr({
                    type: 'text'
                });
            });
            $('#pswEye').mouseup(function() {
                $('#password').attr({
                    type: 'password'
                });
            });
        });
        // running text animation
        var typed = new Typed(".multiple-text", {
            strings: ["Believe,&nbsp;Achieve,<br> Thrive", "Meet Your  Career<br> Coach",
                "Your Path to Successful<br> Career", "Empower Your <br>Professional Growth",
                "Crafting Careers,<br> Fulfilling Dreams"
            ],
            typeSpeed: 100,
            backDelay: 4000,
            loop: true
        })
    </script>
@endsection

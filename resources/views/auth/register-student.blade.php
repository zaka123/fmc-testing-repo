@extends('layouts.navbar', ['title' => 'Register - '])
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/css/intlTelInput.min.css" rel="stylesheet">
    <style>
        :root {
            --border-color: #0C2A68;
        }

        [for="_4483afe6"]::before {
            content: "*";
            color: red;
            position: absolute;
            margin-left: -10px;
        }

        [for="6dd6d341"]::before {
            content: "*";
            color: red;
            position: absolute;
            margin-left: -10px;
        }

        body {
            background-color: #c4e4f2;
            background-image: url('/assets/img/curve.png') !important
        }

        body>nav.navbar.navbar-expand-lg.navbar-light.mb-5.pt-3.sticky-top {
            margin-bottom: 0 !important
        }

        #footer>div {
            display: none;
        }

        @media (min-width:1200px) {
            .swiper-wrapper {
                min-height: 40vh;
            }
        }

        @media (max-width:499px) {
            body>div:nth-child(2) {
                min-height: 73vh
            }
        }

        @media (min-width:500px) {
            body>div:nth-child(2) {
                min-height: 74.6vh
            }
        }

        @media(min-width: 1400px) {
            .pt-xxl-6 {
                padding-top: 6rem !important;
            }
        }

        @media (min-width: 768px) {
            body>div:nth-child(2) {
                min-height: 82vh
            }
        }

        input:not([type="checkbox"]),
        .form-control:focus,
        .form-select:focus,
        .form-select {
            /* box-shadow: 0px 3px 3px 2px #6CBBDE !important; */
            border-width: 0.5px;
            border-style: solid;
            border-color: var(--border-color);
            border-radius: 8px !important
        }

        input:valid,
        input:valid:focus {
            border-width: 2px;
            border-style: solid;
            --border-color: var(--bs-success)
        }

        .btn-check:checked+.btn {
            --bs-btn-active-bg: var(--bs-dark);
            --bs-btn-active-border-color: var(--bs-dark);
            color: white !important;
        }

        .btn-info:focus {
            box-shadow: none
        }

        .invalid {
            --border-color: red
        }

        input[type=radio] {
            width: 23px;
            height: 18px;
        }

        @media (min-width: 1200px) and (min-height: 700px) {
            footer {
                position: absolute !important;
                bottom: 0 !important;
                width: 100%;
            }
        }

        .swiper-wrapper {
            align-items: center;
        }

        .swiper-slide:not(.swiper-slide-active) {
            visibility: hidden !important
        }

        html {
            user-select: none
        }

        .error-message {
            color: red;
            font-size: 14px;
            margin-top: 5px;
        }

        .hoverChangerLook:hover {
            background-color: black !important;
            color: white !important;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <form class="swiper" action="{{ route('register') }}" method="post" autocomplete="nope">
            @csrf
            <input type="hidden" name="role" @if (old('role')) value="{{ old('role') }}" @endif
                value="{{ $profession ?? 'Student' }}">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <label for="firstName" class="fw-500 fs-5">What is your first name?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <input id="firstName" autocomplete="foo" class="form-control" type="text" name="first_name"
                                value="{{ old('first_name') }}" required>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button class="btn btn-sm bg-special previous disabled" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <label for="lastName" class="fw-500 fs-5">What is your last name?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <input id="lastName" class="form-control" type="text" name="last_name"
                                value="{{ old('last_name') }}" required>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <div class="text-center">
                                <label class="fw-500 fs-5">Your gender?</label>
                                <div class="genders">
                                    <div class="row justify-content-center g-2">
                                        <div class="col-auto">
                                            <input class="btn-check" {{ old('gender') == 'Male' ? 'checked' : '' }}
                                                onclick="showquestion(1)" type="radio" id="male" name="gender"
                                                value="Male" required>
                                            <label for="male"
                                                class="btn text-dark btn-sm btn-outline-primary hoverChangerLook"
                                                style="border-color:rgb(146 151 153)">
                                                <i class="fa-solid fs-5 fa-mars"></i><br>
                                                Male
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <input class="btn-check" {{ old('gender') == 'Female' ? 'checked' : '' }}
                                                onclick="showquestion(1)" type="radio" id="female" name="gender"
                                                value="Female" required>
                                            <label for="female"
                                                class="btn text-dark btn-sm btn-outline-primary hoverChangerLook"
                                                style="border-color:rgb(146 151 153)">
                                                <i class="fa-solid fs-5 fa-venus"></i><br>
                                                Female
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <input class="btn-check"
                                                {{ old('gender') == 'Prefer not to say' ? 'checked' : '' }}
                                                onclick="showquestion(1)" type="radio" id="other" name="gender"
                                                value="Prefer not to say" required>
                                            <label for="other"
                                                class="btn text-dark btn-sm btn-outline-primary hoverChangerLook"
                                                style="border-color:rgb(146 151 153)">
                                                <i class="fa-solid fs-5 fa-transgender"></i><br>
                                                Prefer not to say
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="error-gender" class="error-message"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <label for="date_of_birth" class="fw-500 fs-5">What is your Date of Birth?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <input type="date" name="date_of_birth" required id="date_of_birth"
                                max="{{ date(' Y-m-d') }}" value="{{ old('date_of_birth') }}"
                                class="@error('date_of_birth') is-invalid @enderror form-control">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="fw-500 text-center fs-5">Thanks for the information, <span
                                    class="70cf9770 fw-bold text-special"></span>, let’s move forward.</div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <label for="city" class="fw-500 fs-5">Which country are you from?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="city" id="city" required
                                data-error-message="Please select your country"
                                class="@error('city') is-invalid @enderror form-select">
                                <option value="">Please select your country</option>
                                @foreach ($countryNames as $country)
                                    <option @selected(old('city') == $country)>{{ $country }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="fw-500 text-center fs-5"><span class="70cf9771 fw-bold text-special"></span> We
                                would appreciate it if you could tell us more about yourself so we can better assist you!
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row offset-lg-4">
                        <div class="col-lg-7">
                            <label for="favorite_subject" class="fs-5 fw-500">Growing up, what has been your favourite
                                subject in school?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <input name="favorite_subject" required placeholder="Favorite subject" id="favorite_subject"
                                value="{{ old('favorite_subject') }}"
                                class="@error('favorite_subject') is-invalid @enderror form-control">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="fw-500 text-center fs-5">
                                That seems very interesting, we have the right mentors for you to strengthen around that
                                subject!
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <div class="question px-0">
                                <label for="easiest_subject" class="fs-5 fw-500">What subject you have found the easiest
                                    in school?</label>
                                <input name="easiest_subject" placeholder="Easiest subject" required id="easiest_subject"
                                    value="{{ old('easiest_subject') }}"
                                    class="@error('easiest_subject') is-invalid @enderror form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question px-0">
                                <div class="row">
                                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                                        <label for="difficult_subject" class="fs-5 fw-500">Oh wow, you’re smart then! Ok,
                                            on
                                            the contrary, which subject do you find difficult or boring?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <input name="difficult_subject" placeholder="Difficult subject" required
                                            id="difficult_subject" value="{{ old('difficult_subject') }}"
                                            class="@error('difficult_subject') is-invalid @enderror form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <div class="question px-0">
                                <div class="fw-500 text-center fs-5">
                                    We’re pretty sure it isn’t only you who found that subject a little tricky, haha! Do not
                                    worry though, we have got you covered!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <div class="question px-0">
                                <label for="email" class="fw-500 fs-5">Your email address?</label>
                                <input id="email" autocomplete="off" value="{{ old('email') }}"
                                    class="form-control @error('email') is-invalid @enderror" type="email" name="email" required readonly
                                    onfocus="this.removeAttribute('readonly');">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-4" style="text-align:center">
                            <label for="phone" class="fw-500 fs-5">Your phone number?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4 text-center">
                            <input id="phoneNo" class="form-control @error('phone') is-invalid @enderror" type="tel" name="phoneNo"
                                value="{{ old('phone') }}" required>
                            <input id="phone" type="hidden" name="phone" value="{{ old('phone') }}">
                            <div id="error-phone" class="text-danger"></div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook"
                                        type="button" style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- verify by phone or email --}}

                <div class="swiper-slide verification">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center">
                                <label class="fw-500 fs-5">How would you like to verify yourself?</label>
                                <div class="verification mt-2">
                                    <div class="row justify-content-center">
                                        <div class="col-auto px-1">
                                            <input class="btn-check" onclick="showquestion(1)"
                                                @if (old('verification') == 'email') checked @endif type="radio"
                                                id="verify_by_email" name="verification" value="email" required>
                                            <label for="verify_by_email" class="btn text-dark btn-sm btn-outline-primary hoverChangerLook"
                                                style="border-color:rgb(146 151 153)">
                                                Via Email
                                            </label>
                                        </div>
                                        <div class="col-auto px-1">
                                            <input class="btn-check" onclick="showquestion(1)"
                                                @if (old('verification') == 'phone') checked @endif type="radio"
                                                id="verify_by_phone" name="verification" value="phone" required>
                                            <label for="verify_by_phone" class="btn text-dark btn-sm btn-outline-primary hoverChangerLook"
                                                style="border-color:rgb(146 151 153)">
                                                Via Phone
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="error-verification" class="error-message"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)" class="btn btn-sm bg-special previous hoverChangerLook"
                                        type="button" style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button onclick="showquestion(1)" class="btn btn-sm bg-special next hoverChangerLook" type="button"
                                        style="width:72px">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {{-- end of verify by phone or email --}}

                <div class="swiper-slide pt-xxl-2">
                    <div class="row justify-content-center mt-3">
                        <div class="col-lg-4">
                            <div class="question px-0">
                                <div class="position-relative">
                                    <label for="password" class="fw-500 fs-5">Choose your password?</label>
                                    <input id="password" autocomplete="off" oninput="checkpasswordlength()"
                                        class="form-control" type="password" name="password" required readonly
                                        onfocus="this.removeAttribute('readonly');">
                                    <i class="fa-solid fa-eye position-absolute text-secondary pswd-icon"
                                        style="cursor:pointer;bottom: 10px; right:10px"></i>
                                </div>
                                <div id="error-password" class="text-danger d-none">Password must be at least 8
                                    characters</div>
                                <div class="my-3">
                                    <label for="password_confirmation" class="fw-500 fs-5">Confirm your password</label>
                                    <input id="password_confirmation" autocomplete="off" oninput="checkpassword()"
                                        class="password_confirmation form-control" type="password"
                                        name="password_confirmation" required readonly
                                        onfocus="this.removeAttribute('readonly');">
                                    <div class="error text-danger d-none">Passwords don't match</div>
                                </div>
                                <div class="form-group text-justify">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="6dd6d341" required>
                                        <label class="form-check-label ps-1" for="6dd6d341">
                                            I certify that I am over 18 and accept the
                                            <a href="#" class="text-decoration-none">Terms</a> &
                                            <a href="#" class="text-decoration-none">Conditions.</a>
                                            Learn more about how we process your data in our
                                            <a href="#" class="text-decoration-none">Privacy Policy, Cookie</a> and
                                            our
                                            <a href="#" class="text-decoration-none">Rules and Profiles
                                                Visibility</a>.
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="_4483afe6" required>
                                        <label class="form-check-label ps-1" for="_4483afe6">
                                            I consent to the processing of my
                                            <a href="#" class="text-decoration-none">sensitive data</a>
                                            and the use of
                                            <a href="#" class="text-decoration-none">Safe Message Filters</a> by
                                            FIND ME CAREER to provide me the service.
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="ed4e55ce">

                                        <label class="form-check-label ps-1" for="ed4e55ce">
                                            <label class="form-check-label" for="ed4e55ce">

                                                I would like to receive via email commercial offers relating to products or
                                                services provided by partners of FINDMECAREER.com International Limited. You
                                                may opt-out at any time in your settings.
                                            </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center mb-3">
                        <div class="col-12">
                            <div class="d-flex mt-3">
                                <div class="ms-auto me-1">
                                    <button onclick="showquestion(0)"
                                        class="btn btn-sm bg-special previous hoverChangerLook" type="button"
                                        style="width:72px">Previous</button>
                                </div>
                                <div class="me-auto ms-1">
                                    <button class="btn btn-sm bg-special save hoverChangerLook" type="submit"
                                        style="width: 72px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('script')
    <script src="https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/intlTelInput.min.js"></script>
    <script>
        var slider, documentClicked = false;
        var phoneInput;
        $(document).ready(function() {
            slider = new Swiper('.swiper', {
                allowTouchMove: false,
                notify: {
                    enabled: true
                },
                autoplay: {
                    delay: 5000, // set autoplay delay time if needed
                    disableOnInteraction: false // set to true if you want to disable autoplay when user interacts with slider
                }
            });
            slider.autoplay.stop();

            const phoneInputField = document.querySelector("#phoneNo");
            phoneInput = window.intlTelInput(phoneInputField, {
                utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/utils.js",
            });

            // $("#firstName").focus();
            $("#firstName").on('change', function() {
                $('.70cf9770').html(this.value.charAt(0).toUpperCase() + this.value.slice(1).toLowerCase());
                $('.70cf9771').html(this.value.charAt(0).toUpperCase() + this.value.slice(1).toLowerCase() +
                    '!');
            });
        });

        document.addEventListener('keypress', e => {
            if (e.key === 'Enter') {
                e.preventDefault();
                showquestion(1);
            }
        });

        function showquestion(next) {
            if (next) {
                if (slider.slides.length + 1 === slider.activeIndex) {
                    document.forms[0].submit();
                    return;
                } else {
                    var verificationIsValid = true;
                    var genderIsValid = true;
                    var emailIsValid = true;
                    var questionIsValid = false;
                    var activeSlide = $(".swiper-slide-active");
                    var formControls = activeSlide.find("input, select");

                    // phone number validation
                    var phoneNo = formControls.filter('input[name="phoneNo"]');

                    if (phoneNo.length > 0) {
                        if (!phoneInput.isValidNumber()) {
                            $('#error-phone').text('Please enter a valid phone number.');
                            phoneNo.focus();
                            return;
                        } else {
                            $('#error-phone').text('');
                            $('#phone').val(phoneInput.getNumber());
                        }
                    }

                    formControls.each(function() {
                        if (this.checkValidity()) {
                            questionIsValid = true;
                            emailIsValid = true;
                            genderIsValid = true;
                            verificationIsValid = true;

                            // Clear any existing error messages
                            $(this).siblings(".error-message").remove();
                        } else {
                            questionIsValid = false;

                            if (this.getAttribute("type") === "email") {
                                emailIsValid = false;
                            } else if (this.getAttribute("name") === "gender") {
                                genderIsValid = false;
                            }
                            else if (this.getAttribute("name") === "verification") {
                                verificationIsValid = false;
                            }
                            else {
                                // Display error message below the input
                                var errorMessage = this.getAttribute("data-error-message") ||
                                    "This field is required.";
                                $(this).siblings(".error-message").remove();
                                $(this).after("<div class='error-message'>" + errorMessage + "</div>");
                            }

                        }
                    });

                    if (questionIsValid || formControls.length < 1) {
                        slider.slideNext();

                        // Wait for the slide transition to complete before setting focus
                        setTimeout(function() {
                            var nextSlide = $(".swiper-slide-active");
                            var nextFormControls = nextSlide.find("input, select");

                            // Find the first input element that is visible and set focus to it
                            var firstVisibleInput = nextFormControls.filter(':visible').first();
                            if (firstVisibleInput.length > 0) {
                                firstVisibleInput.focus();
                            }

                            // Remove readonly attribute from the second input element
                            var password_confirmation = nextFormControls.filter('[name="password_confirmation"]')
                                .first(); // Change the selector as needed
                            password_confirmation.removeAttr('readonly');
                        }, 500); // Adjust the delay (milliseconds) to wait for the transition to complete

                        return;
                    } else {
                        if (!emailIsValid) {
                            // Display email validation error below the email input
                            var emailInput = formControls.filter("[type='email']").first();
                            var emailErrorMessage = emailInput.attr("data-error-message") ||
                                "Please enter a valid email address.";
                            emailInput.siblings(".error-message").remove();
                            emailInput.after("<div class='error-message'>" + emailErrorMessage + "</div>");
                        }

                        if (!genderIsValid) {
                            // Display error message
                            $("#error-gender").text("Please select your gender.");
                        }
                        if (!verificationIsValid) {
                            // Display error message
                            $("#error-verification").text("Please select a verification method.");
                        }

                        // Find the first input element that is invalid and set focus to it
                        var invalidInput = formControls.filter(function() {
                            return !this.checkValidity();
                        }).first();

                        if (invalidInput.length > 0) {
                            invalidInput.focus();
                        }
                        return;
                    }
                }
            } else {
                slider.slidePrev();
                return;
            }
        }


        function checkpassword() {
            const password = document.querySelector('[name="password"]').value;
            const confirmation = document.querySelector('[name="password_confirmation"]').value;
            const next = document.querySelector('.next');
            const save = document.querySelector(".save");
            if (password == confirmation) {
                next.classList.remove('disabled');
                save.classList.remove('disabled');
                document.querySelector('.error').classList.add('d-none');
            } else {
                save.classList.add('disabled');
                document.querySelector('.error').classList.remove('d-none')
            }

        }

        function checkpasswordlength() {
            const password = document.querySelector('[name="password"]').value;
            const next = document.querySelector('.next');
            const save = document.querySelector(".save");
            if (password.length >= 8) {
                next.classList.remove('disabled');
                save.classList.remove('disabled');
                document.querySelector('#error-password').classList.add('d-none');
            } else {
                save.classList.add('disabled');
                document.querySelector('#error-password').classList.remove('d-none')
            }

        }

        var clicked = false;

        $('.pswd-icon').on('click', (e) => {

            if (!clicked) {
                $('#password').attr({
                    type: 'text'
                });
                $('#password_confirmation').attr({
                    type: 'text'
                });
                $(e.target).addClass('fa-eye-slash').removeClass('fa-eye');
                clicked = true;
            } else {
                $('#password').attr({
                    type: 'password'
                });
                $('#password_confirmation').attr({
                    type: 'password'
                });
                $(e.target).addClass('fa-eye').removeClass('fa-eye-slash');
                clicked = false;
            }
        });

        document.addEventListener('DOMContentLoaded', function() {
            var todaysDate = new Date(); // Gets today's date
            var year = todaysDate.getFullYear(); // YYYY
            var month = ("0" + (todaysDate.getMonth() + 1))
                .slice(-2); // MM
            var day = ("0" + todaysDate.getDate())
                .slice(-2); // DD
            var maxDate = (year + "-" + month + "-" + day);
            $('input[type="date"]').attr({
                max: maxDate
            });
        });

        document.querySelector('form').onsubmit = (e) => {
            e.preventDefault();
            // const phoneNumber = phoneInput.getNumber();
            // $('#phone').val(phoneNumber);
            e.target.checkValidity() ? e.target.submit() : toastr.error(
                "Something went wrong. Please refresh the page. And try again")
        }

        document.onclick = () => {
            if (!documentClicked) {
                document.forms[0].reset();
                documentClicked = true;
                slider.slideTo(0);
            }
        }
    </script>

    <script>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}");
            @endforeach
        @endif
    </script>
@endpush

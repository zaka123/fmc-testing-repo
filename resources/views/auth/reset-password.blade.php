@extends('layouts.navbar')
@section('css')
    <style>
        label {
            color: var(--special) !important
        }
        .hoverChhanger:hover{
            background-color:black!important;
        }
    </style>
@endsection
@section('content')
    <div class="container pb-5 d-flex align-items-center justify-content-center" style="min-height:82vh">
        <div class="card" style="width:35rem;--bs-card-border-color:var(--special)">
            <h5 class="card-header bg-special">Change Password Form</h5>
            <div class="card-body">
                <form method="POST" action="{{ route('password.update') }}" id="resetPasswordForm">
                    @csrf
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">

                    <div class="form-floating mb-3">
                        <input id="email" class="form-control" type="email" name="email" value="{{ old('email', $request->email) }}"
                            required readonly>
                        <label for="email" class="form-label">Your email address:</label>
                        @error('email')<div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-floating mb-3">
                        <input id="password" class="form-control" type="password" name="password"
                             placeholder="Enter your password" required autofocus>
                        <label for="password" class="form-label">Enter your password:</label>
                        @error('password')<div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-floating mb-3">
                        <input id="password_confirmation" class="form-control" type="password" name="password_confirmation"
                            placeholder="Enter your password" required>
                        <label for="password_confirmation" class="form-label">Confirm your password:</label>
                        {{-- @error('password.password_confirmation')<div class="text-danger">{{ $message }}</div> @enderror --}}
                    </div>

                </form>

            </div>
            <button class="card-footer btn btn-sm bg-special text-white hoverChhanger" form="resetPasswordForm">Change Password</button>
        </div>
    </div>
@endsection

@extends('layouts.navbar', ['title' => 'Register - '])
@section('css')
<style>
    :root {
        --border-color: #0C2A68;
    }

    body {
        background-color: #c4e4f2;
        background-image: url('/assets/img/curve.png') !important
    }

    body>nav.navbar.navbar-expand-lg.navbar-light.mb-5.pt-3.sticky-top {
        margin-bottom: 0 !important
    }

    #footer>div {
        display: none;
    }

    @media (min-width:1200px) {
        .swiper-wrapper {
            min-height: 40vh;
        }
    }

    @media (max-width:499px) {
        body>div:nth-child(2) {
            min-height: 73vh
        }
    }

    @media (min-width:500px) {
        body>div:nth-child(2) {
            min-height: 74.6vh
        }
    }

    @media (min-width: 768px) {
        body>div:nth-child(2) {
            min-height: 82vh
        }
    }

    input:not([type="checkbox"]),
    .form-control:focus,
    .form-select:focus,
    .form-select {
        /* box-shadow: 0px 3px 3px 2px #6CBBDE !important; */
        border-width: 0.5px;
        border-style: solid;
        border-color: var(--border-color);
        border-radius: 8px !important
    }

    input:valid,
    input:valid:focus {
        border-width: 2px;
        border-style: solid;
        --border-color: var(--bs-success)
    }

    .btn-check:checked+.btn {
        --bs-btn-active-bg: var(--bs-dark);
        --bs-btn-active-border-color: var(--bs-dark);
        color: white !important;
    }

    .btn-info:focus {
        box-shadow: none
    }

    .invalid {
        --border-color: red
    }

    input[type=radio] {
        width: 23px;
        height: 18px;
    }

    @media (min-width: 1200px) and (min-height: 700px) {
        footer {
            position: absolute !important;
            bottom: 0 !important;
            width: 100%;
        }
    }

    .swiper-wrapper {
        align-items: center;
    }
</style>
@endsection
@section('content')
<div class="container">
    <form class="swiper" action="{{ route('register') }}" method="post">
        @csrf
        <input type="hidden" name="role" @if (old('role')) value="{{ old('role') }}" @endif
            value="{{ $profession ?? 'Student' }}">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <label for="firstName" class="fw-500 fs-5">What is your first name?</label>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <input id="firstName" class="form-control" type="text" name="first_name"
                            value="{{ old('first_name') }}" required autofocus>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button class="btn btn-sm bg-special previous disabled" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <label for="lastName" class="fw-500 fs-5">What is your last name?</label>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <input id="lastName" class="form-control" type="text" name="last_name"
                            value="{{ old('last_name') }}" required>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="text-center">
                            <label class="fw-500 fs-5">Your gender</label>
                            <div class="genders">
                                <div class="row justify-content-center">
                                    <div class="col-auto">
                                        <input class="btn-check" {{ old('gender')=='Male' ? 'checked' : '' }}
                                            onclick="showquestion(1)" type="radio" id="male" name="gender" value="Male"
                                            required>
                                        <label for="male" class="btn text-dark btn-sm btn-outline-primary"
                                            style="border-color:rgb(146 151 153)">
                                            <i class="fa-solid fs-5 fa-mars"></i><br>
                                            Male
                                        </label>
                                    </div>
                                    <div class="col-auto">
                                        <input class="btn-check" {{ old('gender')=='Female' ? 'checked' : '' }}
                                            onclick="showquestion(1)" type="radio" id="female" name="gender"
                                            value="Female" required>
                                        <label for="female" class="btn text-dark btn-sm btn-outline-primary"
                                            style="border-color:rgb(146 151 153)">
                                            <i class="fa-solid fs-5 fa-venus"></i><br>
                                            Female
                                        </label>
                                    </div>
                                    <div class="col-auto">
                                        <input class="btn-check" {{ old('gender')=='Prefer not to say' ? 'checked' : ''
                                            }} onclick="showquestion(1)" type="radio" id="other" name="gender"
                                            value="Prefer not to say" required>
                                        <label for="other" class="btn text-dark btn-sm btn-outline-primary"
                                            style="border-color:rgb(146 151 153)">
                                            <i class="fa-solid fs-5 fa-transgender"></i><br>
                                            Prefer not to say
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <label for="date_of_birth" class="fw-500 fs-5">What is your Date of Birth?</label>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <input type="date" name="date_of_birth" required id="date_of_birth" max="{{ date(' Y-m-d') }}"
                            value="{{ old('date_of_birth') }}"
                            class="@error('date_of_birth') is-invalid @enderror form-control">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="fw-500 text-center fs-5">Thanks for the information, <span
                                class="70cf9770 fw-bold text-special"></span>, let’s move forward.</div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <label for="city" class="fw-500 fs-5">Which country are you from?</label>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <select name="city" id="city" required class="@error('city') is-invalid @enderror form-select"
                            autofocus>
                            <option value="">Please select your country</option>
                            @foreach ($countryNames as $country)
                            <option @selected(old('city')==$country)>{{ $country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="fw-500 text-center fs-5"><span class="70cf9771 fw-bold text-special"></span> We
                            would appreciate it if you could tell us more about yourself so we can better assist you!
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row offset-lg-4">
                    <div class="col-lg-7">
                        <label for="favorite_subject" class="fs-5 fw-500">Growing up, what has been your favourite
                            subject in school?</label>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <input name="favorite_subject" required placeholder="Favorite subject"
                            pattern="^[aA-zZ]+ ?([aA-zZ]+)? ?([aA-zZ]+)?$" id="favorite_subject"
                            value="{{ old('favorite_subject') }}"
                            class="@error('favorite_subject') is-invalid @enderror form-control">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="fw-500 text-center fs-5">
                            That seems very interesting, we have the right mentors for you to strengthen around that
                            subject!
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="question px-0">
                            <label for="easiest_subject" class="fs-5 fw-500">What would you say is the subject you
                                have
                                found the easiest in school?</label>
                            <input name="easiest_subject" placeholder="Easiest subject"
                                pattern="^[aA-zZ]+ ?([aA-zZ]+)? ?([aA-zZ]+)?$" required id="easiest_subject"
                                value="{{ old('easiest_subject') }}"
                                class="@error('easiest_subject') is-invalid @enderror form-control">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question px-0">
                            <div class="row">
                                <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                                    <label for="difficult_subject" class="fs-5 fw-500">Oh wow, you’re smart then! Ok,
                                        on
                                        the
                                        contrary, which subject do you find difficult or boring?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <input name="difficult_subject" placeholder="Difficult subject"
                                        pattern="^[aA-zZ]+ ?([aA-zZ]+)? ?([aA-zZ]+)?$" required id="difficult_subject"
                                        value="{{ old('difficult_subject') }}"
                                        class="@error('difficult_subject') is-invalid @enderror form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="question px-0">
                            <div class="fw-500 text-center fs-5">
                                We’re pretty sure it isn’t only you who found that subject a little tricky, haha! Do not
                                worry though, we have got you covered!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question px-0">
                            <div class="row">
                                <div class="col-md-8 col-lg-5 offset-md-2 offset-lg-4">
                                    <label for="average_grade_point" class="fs-5 fw-500">How would you describe
                                        yourself
                                        as a student? Ranging from A to D</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="average_grade_point" placeholder="GPA: 3.4" required
                                        id="average_grade_point" value="{{ old('average_grade_point') }}"
                                        class="@error('average_grade_point') is-invalid @enderror form-select">
                                        <option value="">Please select your answer</option>
                                        <option>A-Grade Student (80% and above)</option>
                                        <option>B-Grade Student (70-80%)</option>
                                        <option>C-Grade Student (60-70%)</option>
                                        <option>D-Grade Student (50-60%)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="question px-0">
                            <div class="fw-500 text-center fs-5">Perfect, we now have the basic information to choose
                                the right mentor for you. Let’s get some more information for the final touches.</div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question px-0">
                            <div class="row">
                                <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                                    <label for="father_education" class="fs-5 fw-500">What level of education did your
                                        father get?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="father_education" required id="father_education"
                                        class="@error('father_education') is-invalid @enderror form-select">
                                        <option value="">Select an option</option>
                                        <option>Not qualified</option>
                                        <option>Primary Education</option>
                                        <option>Secondary Education</option>
                                        <option>Vocational Training</option>
                                        <option>Bachelors</option>
                                        <option>Masters</option>
                                        <option>PhD</option>
                                        <option>Prefer not to say</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question">
                            <div class="row">
                                <div class="col-md-8 col-lg-7 offset-lg-4">
                                    <label for="father_occupation" class="fs-5 fw-500">What occupation or field does
                                        your
                                        father work in?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="father_occupation" required id="father_occupation"
                                        class="@error('father_occupation') is-invalid @enderror form-select">
                                        <option value="">Select an option</option>
                                        <option>Unemployed</option>
                                        <option>Unskilled</option>
                                        <option>Semi-skilled job</option>
                                        <option>High skilled job</option>
                                        <option>Business</option>
                                        <option>Prefer not to say</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question">
                            <div class="row">
                                <div class="col-md-8 col-lg-6 offset-lg-4">
                                    <label for="mother_education" class="fs-5 fw-500">What level of education did your
                                        mother get?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="mother_education" required id="mother_education"
                                        class="@error('mother_education') is-invalid @enderror form-select">
                                        <option value="">Select an option</option>
                                        <option>Not qualified</option>
                                        <option>Primary Education</option>
                                        <option>Secondary Education</option>
                                        <option>Vocational Training</option>
                                        <option>Bachelors</option>
                                        <option>Masters</option>
                                        <option>PhD</option>
                                        <option>Prefer not to say</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question">
                            <div class="row">
                                <div class="col-lg-7 offset-lg-4">
                                    <label for="mother_occupation" class="fs-5 fw-500">What occupation or field does
                                        your
                                        mother work in?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="mother_occupation" required id="mother_occupation"
                                        class="@error('mother_occupation') is-invalid @enderror form-select">
                                        <option value="">Select an option</option>
                                        <option>Unemployed</option>
                                        <option>Unskilled</option>
                                        <option>Semi-skilled job</option>
                                        <option>High skilled job</option>
                                        <option>Business</option>
                                        <option>Prefer not to say</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="question">
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <label class="fs-5 fw-500" for="parents_retired">Are your parents retired?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4">
                                    <select name="parents_retired" required id="parents_retired"
                                        class="@error('mother_occupation') is-invalid @enderror form-select">
                                        <option value="">Select an option</option>
                                        <option>Father</option>
                                        <option>Mother</option>
                                        <option>Both</option>
                                        <option>Neither</option>
                                        <option>Prefer not to say</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="fw-500 text-center fs-5">That will be all for now, <span
                                class="70cf9771 fw-bold text-special"></span> Congratulations, your account has been
                            created successfully. It is now time for you to start your journey with FindMeCareer and
                            take your professional development to the NEXT LEVEL!</div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="question px-0">
                            <label for="email" class="fw-500 fs-5">Your email address?</label>
                            <input id="email" value="{{ old('email') }}" class="form-control" type="email" name="email"
                                required>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button"
                                    style="width:72px">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row justify-content-center mt-3">
                    <div class="col-lg-4">
                        <div class="question px-0">
                            <div class="position-relative mb-3">
                                <label for="password" class="fw-500 fs-5">Choose your password?</label>
                                <input id="password" class="form-control" type="password" name="password" required>
                                <i class="fa-solid fa-eye position-absolute text-secondary pswd-icon"
                                    style="cursor:pointer;bottom: 10px; right:10px"></i>
                            </div>
                            <div class="mb-3">
                                <label for="password_confirmation" class="fw-500 fs-5">Confirm your password</label>
                                <input id="password_confirmation" oninput="checkpassword()"
                                    class="password_confirmation form-control" type="password"
                                    name="password_confirmation" required>
                                <div class="error text-danger d-none">Passwords don't match</div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="6dd6d341" required>
                                    <label class="form-check-label" for="6dd6d341">
                                        I certify that I am over 18 and accept the
                                        <a href="#" class="text-decoration-none">Terms</a> &
                                        <a href="#" class="text-decoration-none">Conditions.</a>
                                        Learn more about how we process your data in our
                                        <a href="#" class="text-decoration-none">Privacy Policy, Cookie</a> and
                                        our
                                        <a href="#" class="text-decoration-none">Rules and Profiles
                                            Visibility</a>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="4483afe6" required>
                                    <label class="form-check-label" for="4483afe6">
                                        I consent to the processing of my
                                        <a href="#" class="text-decoration-none">sensitive data</a>
                                        and the use of
                                        <a href="#" class="text-decoration-none">Safe Message Filters</a>
                                        by Match to provide me the service
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="ed4e55ce" required>
                                    <label class="form-check-label" for="ed4e55ce">
                                        I would like to receive via email commercial offers relating to products or
                                        services provided by partners of Match.com International Limited. See our
                                        <a href="#" class="text-decoration-none">partners</a>
                                        You may opt-out at any time in your settings.
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mb-3">
                    <div class="col-12">
                        <div class="d-flex mt-3">
                            <div class="ms-auto me-1">
                                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" type="button"
                                    style="width:72px">Previous</button>
                            </div>
                            <div class="me-auto ms-1">
                                <button class="btn btn-sm bg-special save" type="submit"
                                    style="width: 72px;">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('footer.user')
@endsection
@push('script')
<script>
    var slider;
    $(document).ready(function () {
        slider = new Swiper('.swiper', {
            allowTouchMove: false,
            notify: {
                enabled: true
            },
        });

        $("#firstName").on('change', function () {
            $('.70cf9770').html(this.value.charAt(0).toUpperCase() + this.value.slice(1).toLowerCase());
            $('.70cf9771').html(this.value.charAt(0).toUpperCase() + this.value.slice(1).toLowerCase() +
                '!');
        });
    });
</script>
@endpush
@section('script')
<script>
    document.addEventListener('keypress', e => {
        if (e.key === 'Enter') {
            e.preventDefault();
            showquestion(1);
        }
    });


    function showquestion(next) {
        if (next) {
            if (slider.slides.length + 1 === slider.activeIndex) {
                document.forms[0].submit();
                return;
            } else {
                var questionIsValid = false;
                var activeSlide = $(".swiper-slide-active");
                var formControls = activeSlide.find("input, select");

                formControls.each(function() {
                    if (this.checkValidity()) {
                        questionIsValid = true;
                    } else {
                        questionIsValid = false;
                    }
                });

                if (questionIsValid || formControls.length < 1) {
                    slider.slideNext();
                    return;
                } else {
                    toastr.error("Please fill in this field.");
                    return;
                }
            }
        } else {
            slider.slidePrev();
            return;
        }
    }

    function checkpassword() {
        const password = document.querySelector('[name="password"]').value;
        const confirmation = document.querySelector('[name="password_confirmation"]').value;
        const next = document.querySelector('.next');
        const save = document.querySelector(".save");
        if (password == confirmation) {
            next.classList.remove('disabled');
            save.classList.remove('disabled');
            document.querySelector('.error').classList.add('d-none');
        } else {
            save.classList.add('disabled');
            document.querySelector('.error').classList.remove('d-none')
        }

    }

    var clicked = false;

    $('.pswd-icon').on('click', (e) => {

        if (!clicked) {
            $('#password').attr({ type: 'text' });
            $(e.target).addClass('fa-eye-slash').removeClass('fa-eye');
            clicked = true;
        } else {
            $('#password').attr({ type: 'password' });
            $(e.target).addClass('fa-eye').removeClass('fa-eye-slash');
            clicked = false;
        }
    });

    document.addEventListener('DOMContentLoaded', function () {
        var todaysDate = new Date(); // Gets today's date
        var year = todaysDate.getFullYear(); // YYYY
        var month = ("0" + (todaysDate.getMonth() + 1))
            .slice(-2); // MM
        var day = ("0" + todaysDate.getDate())
            .slice(-2); // DD
        var maxDate = (year + "-" + month + "-" + day);
        $('input[type="date"]').attr({ max: maxDate });
    });

    document.querySelector('form').onsubmit = (e) => {
        e.preventDefault();
        e.target.checkValidity() ? e.target.submit() : toastr.error("Something went wrong. Please refresh the page. And try again")
    }

</script>
@endsection
{{-- @extends('layouts.navbar', ['pagename' => 'Apply | ']) --}}
@extends('layouts.new_app', ['pagename' => 'Apply | '])
@section('css')
    <style>
        .position-absolute {
            top: 42px;
        }

        input::placeholder {
            color: #749BC2 !important;
        }

        .col-md-12::-webkit-scrollbar {
            width: 5px;
            height: 8px;
            background-color: #F3F5F7;
        }

        .col-md-12::-webkit-scrollbar-thumb {
            background-color: #749BC2;
            border-radius: 100px !important;
        }

        @media(max-width:768px) {
            .col-md-12 {
                margin-top: 10px !important;
            }
        }

        nav {
            position: fixed;
            z-index: +99;
            width: 92%;
        }

        #career_couch::placeholder {
            color: #749BC2 !important;
        }

        .form-control:focus,
        .form-control,
        .form-select,
        .form-select:focus,
        div:has(> [name="resume"]) {
            box-shadow: 0px 3px 6px #749BC2 !important;
        }

        .card {
            /* box-shadow: 0px 1px 12px #0C2A68; */
            /* border: 0.4px solid #6cbbde !important */
            border: 0px !important;
        }

        /* yes no css */
        .switch {
            position: relative;
            display: inline-block;
            top: 23%;
            width: 80px;
            scale: 0.7;
            height: 30px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: grey;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "No";
            width: fit-content;
            left: 3px;
            bottom: 3px;
            padding: 0px 3px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #749BC2;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #749BC2;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(44px);
            -ms-transform: translateX(44px);
            transform: translateX(44px);
            content: "Yes";
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .apply:hover {
            background-color: black !important;
            color: white !important;
        }
        i{
            color: #749BC2;
        }
        .bg-special{
            background-color: #749BC2 !important;
        }
    </style>
@endsection
@section('content')
    <div class="container" style="height: 70vh!important">
        <div class="row">
            <div class="col-md-12 mt-5"
                style="height: 80vh!important;overflow:auto; scrollbar-width: thin;
            scrollbar-color: #749BC2 transparent;">
                <div class="card mb-5 mt-5 ">
                    <div class="card-body w-100">
                        <form action="{{ route('counsellorRegister') }}" class="needs-validation w-100" id="counsellorForm"
                            method="post" enctype="multipart/form-data" novalidate>
                            @csrf
                            <div class="row g-5 w-100 ">
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label for="first_name" class="form-label fw-500">First Name<span
                                                class="text-danger">*</span></label>
                                        <input id="first_name" type="text"
                                            class="form-control shadow ps-5 @error('first_name') is-invalid @enderror"
                                            name="first_name" value="{{ old('first_name') }}" required>
                                        <i class="fa-solid fa-user  position-absolute ms-3"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label for="last_name" class="form-label fw-500">Last Name<span
                                                class="text-danger">*</span></label>
                                        <input id="last_name" type="text"
                                            class="form-control shadow ps-5 @error('last_name') is-invalid @enderror"
                                            name="last_name" value="{{ old('last_name') }}" required>
                                        <i class="fa-solid fa-user position-absolute ms-3"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 d-flex">
                                    <div class="form-group">
                                        <label for="dial_code" class="form-label fw-500">Phone</label>
                                        <input name="dial_code" list="dial_code_list" id="dial_code" placeholder="+92"
                                            class="form-control @error('dial_code') is-invalid @enderror"
                                            style="width: 5rem" autocomplete="country-code">
                                        <datalist id="dial_code_list">
                                            @foreach ($countryDialCodes as $code)
                                                <option @selected(old('dial_code') == $code)>{{ $code }}</option>
                                            @endforeach
                                        </datalist>
                                        </select>
                                    </div>
                                    <div class="position-relative ps-2 w-100">
                                        <label for="phone_number"
                                            class="form-label fw-500 text-white user-select-none">Phone</label>
                                        <input id="phone_number" placeholder="300-000 0000" type="tel"
                                            class="form-control shadow ps-5 @error('phone_number') is-invalid @enderror"
                                            name="phone_number" value="{{ old('phone_number') }}">
                                        <i class="fa-solid fa-phone  position-absolute ms-3"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label for="email" class="form-label fw-500">Email<span
                                                class="text-danger">*</span></label>
                                        <input id="email" type="email"
                                            class="form-control shadow ps-5 @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" required>
                                        <i class="fa-solid fa-envelope position-absolute ms-3"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label for="linked_in" class="form-label fw-500">Linkedin</label>
                                        <input id="linked_in" type="url"
                                            class="form-control shadow ps-5 @error('linked_in') is-invalid @enderror"
                                            name="linked_in" value="{{ old('linked_in') }}">
                                        <i class="fa-brands fa-linkedin-in  position-absolute ms-3"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label class="form-label fw-500" for="interested_in">What are youinterested in?<span
                                                class="text-danger">*</span></label>
                                        <select class="form-select shadow @error('interested_in') is-invalid @enderror"
                                            name="interested_in" id="interested_in" required>
                                            <option value="">Please select your answer</option>
                                            <option @selected(old('interested_in') == 'Coaching')>Coaching</option>
                                            <option @selected(old('interested_in') == 'Mentoring')>Mentoring</option>
                                            <option value="Both Coaching and Mentoring" @selected(old('interested_in') == 'Both Coaching and Mentoring')>Both
                                            </option>
                                        </select>
                                        <div class="invalid-feedback">Please an option from the list.</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label for="country" class="form-label fw-500">Country</label>
                                        <select name="country" id="country"
                                            class="form-select shadow @error('country') is-invalid @enderror">
                                            <option value="">Please select your country</option>
                                            @foreach ($countryNames as $country)
                                                <option>{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative">
                                        <label class="form-label fw-500" for="gender">Gender</label>

                                        <select class="form-select shadow @error('gender') is-invalid @enderror"
                                            name="gender" id="gender">

                                            <option value="">Please select your gender</option>
                                            <option @selected(old('gender') == 'Male')>Male</option>
                                            <option @selected(old('gender') == 'Female')>Female</option>
                                            <option @selected(old('gender') == 'Other')>Other</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="career_couch" class="form-label fw-500">Why you want to become a career
                                        coach<span class="text-danger">*</span> <small>(250 words)</small></label>
                                    <textarea id="career_couch" class=" shadow form-control @error('career_couch') is-invalid @enderror"
                                        name="career_couch" rows="5" required cols="112" minlength="250" style="resize:none;">{{ old('career_couch') }}</textarea>
                                    <div class="invalid-feedback">This field is required.</div>
                                    <div id="chars_typed" class="text-end">0 / 250</div>
                                </div>
                                {{-- <div class="col-md-12">
                                <span class="fw-500">Do you have career/life coaching certificate:</span>
                                <label class="switch">
                                    <input type="checkbox" @checked(old('coaching_certification')) name="coaching_certification" id="certificate">
                                    <div class="slider round">
                                        <span class="on"></span>
                                        <span class="off"></span>
                                    </div>
                                </label>
                            </div> --}}
                                <div class="col-12 col-md-6">
                                    <label for="resume" class="form-label fw-500">Upload Resume/CV<span
                                            class="text-danger">*</span></label>
                                    <div class="text-center p-5 mydive rounded">
                                        <label for="resume" class="fw-500" style="cursor: pointer;">
                                            Upload Resume/CV<br>
                                            <i class="fa-solid fa-arrow-up-from-bracket fa-2x "></i><br>
                                            Choose file
                                        </label>
                                        <input type="file" name="resume" id="resume" class="form-control"
                                            required accept="application/msword, application/pdf" hidden>
                                        <div class="invalid-feedback">This field is required.</div>
                                        <div class="text-muted text-truncate" id="filename"></div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn bg-special text-white apply" type="submit"
                                        name="button">Apply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        // show uploaded file name
        document.querySelector('#resume').onchange = () => {
            $('#filename').html(document.querySelector('#resume')['files'][0].name);
        }

        // update number of words typed
        const chars_typed = document.querySelector('#chars_typed');
        $(document).ready(function() {
            $('#career_couch').bind('keypress input', function() {
                var words = this.value.trim().split(" ").length;
                if (words > 250) {
                    toastr.error("career coach must not be greater than 250 words");
                    this.value = this.value.substring(0, this.value.lastIndexOf(" "));
                    chars_typed.textContent = `250 / 250`;
                } else {
                    chars_typed.textContent = `${words} / 250`;
                }

            });
        });
    </script>
    <script>
        (() => {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            const forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.from(forms).forEach(form => {
                form.addEventListener('submit', event => {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
        })()
    </script>
@endpush

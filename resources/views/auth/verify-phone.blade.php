@extends('layouts.navbar')
@section('css')
<style>
    body>nav.navbar.navbar-expand-lg.navbar-light.mb-5.pt-3.sticky-top {
        margin-bottom: 0 !important
    }

    body {
        background-image: url("/assets/icons/index.svg") !important;
        background-size: cover !important;
        background-color: rgb(108 187 222 / 0.4) !important
    }
    .container > .card {
        translate: 0 -15%
    }
</style>
@endsection
@section('content')
<div class="container d-flex align-items-center justify-content-center" style="min-height:89.5vh">
    <div class="card shadow" style="width:35rem">
        <h4 class="card-header bg-special">Attention!</h4>
        <div class="card-body">
            <div class="card-text mb-3">
                Thanks for signing up! Before getting started, could you verify your phone number by entering the
                code that we have sent to your phone number: <strong>{{ auth()->user()->phone }}</strong>? If you didn't receive the code, we will gladly send you another.
            </div>
            <form method="POST" action="{{ route('user.verify.code') }}" id="_verify">
                @csrf
                <div class="form-floating">
                    <input id="verification_code" class="form-control" type="number" name="verification_code" value="{{ old('verification_code') }}"
                        required autofocus placeholder="Enter code:">
                    <label for="verification_code" class="form-label">Enter code:</label>
                </div>
                @error('verification_code')<div class="my-2 text-danger" >{{ $message }}</div>@enderror
            </form>
            @if (session('status')=='A code has been sent to your phone number.')
                <div class="my-2 text-info" >
                    {{-- {{ session('status') }} --}}
                    A code has been sent to your phone number.
                </div>
            @endif
            @if (session('error'))
                <div class="my-2 text-danger" >
                    {{ session('error') }}
                </div>
            @endif
        </div>
        <div class="card-footer">
            <button class="btn btn-sm me-2 btn-outline-success" type="submit" form="_verify">Verify</button>
            <a class="btn btn-outline-danger btn-sm" href="{{ route('user.resend.code', auth()->user()) }}" >Resend Code</a>
        </div>
    </div>
</div>
@endsection

@extends('layouts.navbar', ["title" => "Register - "])
@section('css')
<style>
    :root {
        --border-color: #0C2A68;
    }

    body {
        background-color: #c4e4f2;
        background-image: url('/assets/img/curve.png') !important
    }

    body>nav.navbar.navbar-expand-lg.navbar-light.mb-5.pt-3.sticky-top {
        margin-bottom: 0 !important
    }

    #footer>div {
        display: none;
    }

    @media (min-width:500px) {
        body>div:nth-child(2) {
            min-height: 74.6vh
        }
    }

    @media (min-width: 768px) {
        body > div:nth-child(2) {
            min-height: 82vh
        }
    }


    @media (max-width:499px) {
        body>div:nth-child(2) {
            min-height: 73vh
        }
    }

    body {
        transition: transform 0.5s
    }

    input:not([type="checkbox"]),
    .form-control:focus {
        box-shadow: 0px 3px 3px 2px #6CBBDE !important;
        border-width: 0.5px;
        border-style: solid;
        border-color: var(--border-color);
        border-radius: 8px !important
    }

    input:valid,
    input:valid:focus {
        border-width: 2px;
        border-style: solid;
        --border-color: var(--bs-success)
    }

    /* input icon */
    input+i {
        top: 45px;
        left: 20px
    }

    .btn-check:checked+.btn {
        --bs-btn-active-bg: var(--bs-dark);
        --bs-btn-active-border-color: var(--bs-dark);
        color: white !important;
    }

    .btn-info:focus {
        box-shadow: none
    }

    .invalid {
        --border-color: red
    }

    input[type=radio] {
        width: 23px;
        height: 18px;
    }

    .question:not(:first-of-type) {
        display: none;
    }

    .question:has(input) {
        margin-top: -4.5rem;
    }

    .question {
        transition-property: display !important;
        transition-duration: 1000ms !important;
    }

    @media (min-width: 1200px) and (min-height: 700px) {
        footer {
            position: absolute !important;
            bottom: 0 !important;
            width: 100%;
        }
    }
</style>
@endsection
@section('content')
<div class="container d-flex align-items-center pe-0">
    <form id="form" class="row px-3 px-lg-0 w-100 justify-content-center mb-3" action="{{ route('register') }}" method="POST">
        @csrf
        <div class="question col-md-8 col-lg-4 position-relative px-0">
            <label for="firstName">
                <strong>
                    <h4 class="fw-500">What is your first name?</h4>
                </strong>
            </label>
            <input id="firstName" class="form-control ps-5 shadow" type="text" name="first_name" value="{{ old('first_name') }}" required autofocus>
            <i class="fa-solid fa-user position-absolute text-secondary"></i>
        </div>
        <div class="question col-md-8 col-lg-4 position-relative px-0">
            <label for="lastName">
                <strong>
                    <h4 class="fw-500">What is your last name?</h4>
                </strong>
            </label>
            <input id="lastName" class="form-control ps-5 shadow" type="text" name="last_name" value="{{ old('last_name') }}" required>
            <i class="fa-solid fa-user position-absolute text-secondary"></i>
        </div>
        <input type="hidden" name="role" @if (old('role')) value="{{ old('role') }}" @endif value="{{ $profession ?? 'Student' }}">
        <div class="question col-md-8 col-lg-4">
            <div class="col-lg-8 offset-lg-2 text-center">
                <label>
                    <strong>
                        <h4 class="fw-500">Your gender</h4>
                    </strong>
                </label>
                <div class="genders">
                    <div class="row justify-content-center">
                        <div class="col-auto">
                            <input class="btn-check" {{ old("gender") == "Male" ? 'checked' : '' }} onclick="showquestion(1)" type="radio" id="male" name="gender" value="Male" required>
                            <label for="male" class="btn text-dark btn-sm btn-outline-primary" style="border-color:rgb(146 151 153)">
                                <i class="fa-solid fs-5 fa-mars"></i><br>
                                Male
                            </label>
                        </div>
                        <div class="col-auto">
                            <input class="btn-check" {{ old("gender") == "Female" ? 'checked' : '' }} onclick="showquestion(1)" type="radio" id="female" name="gender" value="Female" required>
                            <label for="female" class="btn text-dark btn-sm btn-outline-primary" style="border-color:rgb(146 151 153)">
                                <i class="fa-solid fs-5 fa-venus"></i><br>
                                Female
                            </label>
                        </div>
                        <div class="col-auto">
                            <input class="btn-check" {{ old("gender") == "Other" ? 'checked' : '' }} onclick="showquestion(1)" type="radio" id="other" name="gender" value="Other" required>
                            <label for="other" class="btn text-dark btn-sm btn-outline-primary" style="border-color:rgb(146 151 153)">
                                <i class="fa-solid fs-5 fa-transgender"></i><br>
                                Other
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="question col-md-8 col-lg-4 position-relative px-0">
            <label for="email">
                <strong>
                    <h4 class="fw-500">Your email address?</h4>
                </strong>
            </label>
            <input id="email" value="{{ old('email') }}" class="form-control ps-5 shadow" type="email" name="email" required>
            <i class="fa-solid fa-envelope position-absolute text-secondary"></i>
        </div>
        <div class="question col-md-8 col-lg-4 px-0 mt-5">
            <div class="mb-3 position-relative">
                <label for="password">
                    <strong>
                        <h4 class="fw-500">Choose your password?</h4>
                    </strong>
                </label>
                <input id="password" class="form-control ps-5 shadow" type="password" name="password" required>
                <i class="fa-solid fa-lock position-absolute text-secondary"></i>
                <i class="fa-solid fa-eye position-absolute text-secondary pswd-icon" style="cursor:pointer;bottom: 10px; right:10px"></i>
            </div>
            <div class="position-relative mb-3">
                <label for="password_confirmation">
                    <strong>
                        <h4 class="fw-500">Confirm your password</h4>
                    </strong>
                </label>
                <input id="password_confirmation" oninput="checkpassword()" class="password_confirmation form-control ps-5 shadow" type="password" name="password_confirmation" required>
                <i class="fa-solid fa-lock position-absolute text-secondary"></i>
                <div class="error text-danger d-none">Passwords don't match</div>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="6dd6d341" required>
                    <label class="form-check-label" for="6dd6d341">
                        I certify that I am over 18 and accept the 
                        <a href="#" class="text-decoration-none">Terms</a> & 
                        <a href="#" class="text-decoration-none">Conditions.</a> 
                        Learn more about how we process your data in our 
                        <a href="#" class="text-decoration-none">Privacy Policy, Cookie</a> and our 
                        <a href="#" class="text-decoration-none">Rules and Profiles Visibility</a>
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="4483afe6" required>
                    <label class="form-check-label" for="4483afe6">
                        I consent to the processing of my 
                        <a href="#" class="text-decoration-none">sensitive data</a> 
                        and the use of 
                        <a href="#" class="text-decoration-none">Safe Message Filters</a> 
                        by Match to provide me the service
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="ed4e55ce" required>
                    <label class="form-check-label" for="ed4e55ce">
                        I would like to receive via email commercial offers relating to products or services provided by partners of Match.com International Limited. See our 
                        <a href="#" class="text-decoration-none">partners</a> 
                        You may opt-out at any time in your settings.
                    </label>
                </div>
            </div>
        </div>
        <div class="d-flex mt-3">
            <div class="ms-auto me-1">
                <button onclick="showquestion(0)" class="btn btn-sm bg-special previous" disabled type="button" style="width:72px">Previous</button>
            </div>
            <div class="me-auto ms-1">
                <button onclick="showquestion(1)" class="btn btn-sm bg-special next" type="button" style="width:72px">Next</button>
                <button class="btn btn-sm bg-special disabled save" type="submit" style="width: 72px;display:none">Submit</button>
            </div>
        </div>
    </form>
</div>
@include('footer.user')
@endsection
@section('script')
<script>
// remove primary footer
$('#footer').remove();


var question = 0;
const questions = $('.question').length;

// show next question on return key
document.addEventListener('keypress', e => {
    if (e.key === 'Enter') {
        e.preventDefault();
        questions > question + 1 ? showquestion(1) : $('#form').submit()
    }
});


function showquestion(next) {

    /**
     * show next question when flag is 1
     * otherwise show previous question
     */

    if (next) {
        var questionIsValid = false;
        document.querySelectorAll(`.question:nth-of-type(${question + 1}) input`).forEach(input => {
            questionIsValid = input.checkValidity();
            questionIsValid ? $(input).removeClass('invalid') : $(input).addClass('invalid')
        });

        if (questionIsValid) {
            $('.question').eq(question).hide(); // hide current question
            $('.question').eq(++question).show(); // show next question

        } else {
            toastr.error('Please fill the input to continue.');
        }

    } else {
        $('.question').eq(question).hide(); // hide current question
        $('.question').eq(--question).show(); // show previous question
    }

    // Focus next question
    try {
        document.querySelectorAll('.question')[question].querySelector('input').focus();
    } catch (e) {
        // do nothing for now
    }
    // hide next btn and show save when last question appears
    if (question + 1 == questions) {
        $('.next').hide();
        $('.save').show();
    } else {
        $('.next').show();
        $('.save').hide();
    }

    // disable previous btn on first question
    question === 0 ? $('.previous').attr({ disabled: '' }) : $('.previous').removeAttr('disabled')
}

function checkpassword() {
    const password = document.querySelector('[name="password"]').value;
    const confirmation = document.querySelector('[name="password_confirmation"]').value;
    const next = document.querySelector('.next');
    const save = document.querySelector(".save");
    if (password == confirmation) {
        next.classList.remove('disabled');
        save.classList.remove('disabled');
        document.querySelector('.error').classList.add('d-none');
    } else {
        save.classList.add('disabled');
        document.querySelector('.error').classList.remove('d-none')
    }

}

var clicked = false;

$('.pswd-icon').on('click', (e) => {

    if (!clicked) {
        $('#password').attr({ type: 'text' });
        $(e.target).addClass('fa-eye-slash').removeClass('fa-eye');
        clicked = true;
    } else {
        $('#password').attr({ type: 'password' });
        $(e.target).addClass('fa-eye').removeClass('fa-eye-slash');
        clicked = false;
    }
});

document.addEventListener('DOMContentLoaded', function() {
    var todaysDate = new Date(); // Gets today's date
    var year = todaysDate.getFullYear(); // YYYY
    var month = ("0" + (todaysDate.getMonth() + 1))
        .slice(-2); // MM
    var day = ("0" + todaysDate.getDate())
        .slice(-2); // DD
    var maxDate = (year + "-" + month + "-" + day);
    $('input[type="date"]').attr({ max: maxDate });
});

document.querySelector('#form').onsubmit = (e) => {
    e.preventDefault();
    e.target.checkValidity() ? e.target.submit() : toastr.error("Something went wrong. Please refresh the page. And try again")
}

</script>
@endsection

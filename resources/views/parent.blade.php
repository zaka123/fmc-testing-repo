@extends('layouts.navbar', ['title' => "Parents Looking for their child"])
@section('css')
<style>
body {
    background-color: rgba(108, 187, 222, 20%) !important;
    background-image: url('/assets/icons/index.svg');
    background-size: cover;
}

</style>
@endsection
@section('content')
<div class="container mb-3 mb-lg-0" style="min-height:86vh">
    <div class="row align-items-center g-3">
        <div class="col-md-10">
            <h3 class="text-special">
                MAKE YOUR JOURNEY/TRANSITION FROM EDUCATION TO EMPLOYEMENT SEAMLESSLY WITH OUR CAREER EXPERTS!
            </h3>
        </div>
        <div class="col-lg-7">
            <p class="fw-light">
                Are you worried about your child career? Have you identified your child interests? Are you worried his interests does not mach with your career choice or inspirations?
                <br>
                Our experts help you to get rid of these worrying by talking to you and your child, identifying his interests and skills, finding the career path that matches his interest and your inspiratins.
                <br>
                Our experts makes personalized plan with set targets and provide relevant advice to you and your child for succesfull career.
            </p>
        </div>
        <div class="col-lg-5">
            <img src="{{asset('assets/img/Parents.png')}}" class="img-fluid" alt="job interviewer">
        </div>
        <div class="col-12">
            <a href="{{url('register','Parent')}}" class="btn bg-special rounded-3">
                Let's get start
            </a>
        </div>
    </div>
</div>
@include('newsletter_form')
@endsection

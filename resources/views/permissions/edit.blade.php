<form action="{{route('admin.permissions.store')}}" method="post">
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label for="permission-name">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="role">Role</label>
            <select name="role" id="role" class="form-control">
                @foreach($roles as $role)
                <option value="{{$role->id}}">{{$role->name ?? ''}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</form>
@push('script')
<script>

</script>
@endpush
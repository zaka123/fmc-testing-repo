@section('page-title')
Permissions
@endsection
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Permissions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="bg-white px-4 py-4 rounded">
            <div class="row">
                <div class="col-md-12 justify-content-end">
                    <button type="button" class="bg-primary btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <i class="fa fa-solid fa-plus"></i>
                    </button>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Name</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $key=>$permission)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$permission->name ?? '' }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="#" class="text-white btn btn-sm btn-warning">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <form action="{{route('admin.permissions.destroy',$permission->id)}}" method="post">
                                            @csrf @method('DELETE')
                                            <button type="submit" class="bg-danger btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @section('modal')
    <form action="{{route('admin.permissions.store')}}" method="post">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="permission-name">Name</label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                <select name="role" id="role" class="form-control">
                    @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name ?? ''}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="bg-primary btn btn-primary">Save changes</button>
        </div>
    </form>
    @endsection
    @include('extra.modal',['modal_title'=>'Add Permissions'])
</x-app-layout>
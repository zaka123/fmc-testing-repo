{{-- @extends('layouts.navbar', ['title' => 'Terms of use - ']) --}}
@extends('layouts.new_app')
@section('css')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap');

        h1 {
            font-size: 2.5rem !important;
        }

        @media(max-width:1200px) {
            h1 {
                font-size: calc(1.375rem + 1.5vw) !important;
            }
        }

        p {
            font-family: 'Roboto', sans-serif;
        }

        @media(max-width:768px) {
            .container-fluid {
                margin-top: 5px !important;
            }

            p {
                text-align: justify !important;
            }

            .col-12 {
                padding: 10px !important;
            }
        }

        .row {
            overflow: auto
        }

        .row {
            width: 97vw!important;
            height: 80vh !important;
            scrollbar-width: thin;
            scrollbar-color: #749BC2 transparent;
        }

        .row::-webkit-scrollbar {
            width: 5px;
            height: 8px;
            background-color: #F3F5F7;
        }

        .row::-webkit-scrollbar-thumb {
            background-color: #749BC2;
            border-radius: 100px !important;
        }

        .row {
            --bs-gutter-x: 1rem !important;
        }
    </style>
@endsection
@section('content')
    <div class="main mt-5">
        <div class="row g-5">
            <div class="col-12">
                <h1>Website Terms and Conditions of Use</h1>
                <p>Last updated: July 25, 2023</p>

                <h2>1. Terms</h2>

                <p>By accessing this Website, accessible from https://findmecareer.com/, you are agreeing to be bound by
                    these Website Terms and Conditions of Use and agree that you are responsible for the agreement with any
                    applicable local laws. If you disagree with any of these terms, you are prohibited from accessing this
                    site. The materials contained in this Website are protected by copyright and trade mark law.</p>

                <h2>2. Use License</h2>

                <p>Permission is granted to temporarily download one copy of the materials on Find Me Career's Website for
                    personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of
                    title, and under this license you may not:</p>

                <ul>
                    <li>modify or copy the materials;</li>
                    <li>use the materials for any commercial purpose or for any public display;</li>
                    <li>attempt to reverse engineer any software contained on Find Me Career's Website;</li>
                    <li>remove any copyright or other proprietary notations from the materials; or</li>
                    <li>transferring the materials to another person or "mirror" the materials on any other server.</li>
                </ul>

                <p>This will let Find Me Career to terminate upon violations of any of these restrictions. Upon termination,
                    your viewing right will also be terminated and you should destroy any downloaded materials in your
                    possession whether it is printed or electronic format.</p>

                <h2>3. Disclaimer</h2>

                <p>All the materials on Find Me Career's Website are provided "as is". Find Me Career makes no warranties,
                    may it be expressed or implied, therefore negates all other warranties. Furthermore, Find Me Career does
                    not make any representations concerning the accuracy or reliability of the use of the materials on its
                    Website or otherwise relating to such materials or any sites linked to this Website.</p>

                <h2>4. Limitations</h2>

                <p>Find Me Career or its suppliers will not be hold accountable for any damages that will arise with the use
                    or inability to use the materials on Find Me Career's Website, even if Find Me Career or an authorize
                    representative of this Website has been notified, orally or written, of the possibility of such damage.
                    Some jurisdiction does not allow limitations on implied warranties or limitations of liability for
                    incidental damages, these limitations may not apply to you.</p>

                <h2>5. Revisions and Errata</h2>

                <p>The materials appearing on Find Me Career's Website may include technical, typographical, or photographic
                    errors. Find Me Career will not promise that any of the materials in this Website are accurate,
                    complete, or current. Find Me Career may change the materials contained on its Website at any time
                    without notice. Find Me Career does not make any commitment to update the materials.</p>

                <h2>6. Links</h2>

                <p>Find Me Career has not reviewed all of the sites linked to its Website and is not responsible for the
                    contents of any such linked site. The presence of any link does not imply endorsement by Find Me Career
                    of the site. The use of any linked website is at the user's own risk.</p>

                <h2>7. Site Terms of Use Modifications</h2>

                <p>Find Me Career may revise these Terms of Use for its Website at any time without prior notice. By using
                    this Website, you are agreeing to be bound by the current version of these Terms and Conditions of Use.
                </p>

                <h2>8. Your Privacy</h2>

                <p>Please read our Website Terms and Conditions of Use.</p>

                <h2>9. Governing Law</h2>

                <p>Any claim related to Find Me Career's Website shall be governed by the laws of pk without regards to its
                    conflict of law provisions.</p>
                <h1>Changes to this Website Terms and Conditions of Use</h1>
                <p>We may update Our Website Terms and Conditions of Use from time to time. We will notify You of any
                    changes by posting the new Website Terms and Conditions of Use on this page.</p>
                <p>We will let You know via email and/or a prominent notice on Our Service, prior to the change becoming
                    effective and update the &quot;Last updated&quot; date at the top of this Website Terms and Conditions
                    of Use.</p>
                <p>You are advised to review this Website Terms and Conditions of Use periodically for any changes. Changes
                    to this Website Terms and Conditions of Use are effective when they are posted on this page.</p>
                <h1>Contact Us</h1>
                <p>If you have any questions about this Website Terms and Conditions of Use, You can contact us:</p>
                <ul>
                    <li>By email: contact@findmecareer.com</li>
                </ul>
            </div>
        </div>
    </div>
    </div>
@endsection

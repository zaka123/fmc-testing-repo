<style>
    .lookBack:hover {
        color: #6CBBDE !important;
    }
</style>
<footer class="mt-5 text-bg-dark px-4 py-3 onTheFloor position-sticky w-100 bottom-0 ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                    class="d-inline d-md-none"><br></span>All rights reserved</div>
        </div>
    </div>
</footer>

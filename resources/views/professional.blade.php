@extends('layouts.navbar',  ['title' => "Working Professional"])
@section('css')
<style>
body {
    background-color: rgba(108, 187, 222, 20%) !important;
    background-image: url('/assets/icons/index.svg');
    background-size: cover;
}
.startedChanger:hover{
    background-color:black!important;
}

</style>
@endsection
@section('content')
<div class="container pt-lg-5 mb-3 mb-lg-0" style="min-height:86vh">
    <div class="row align-items-center gx-3">
        <div class="col-lg-7">
            <h3 class="text-special text-uppercase">
                get help for career transition and growth from our career experts
            </h3>
            <p class="fw-light text-justify">
                Are you bored in your current role and looking for career transition or your role doesn’t fit with your interest or attitude? Our career experts help you to find the suitable role based on your skills and interest and provide your step-by-step guidance to achieve your career aspirations.
            </p>
            <p>
                <a href="{{url('register','Professional')}}" class="d-none d-lg-inline-block btn bg-special startedChanger rounded-3">Let's get Started</a>
                <a href="{{url('register','Professional')}}" class="d-lg-none btn btn-sm bg-special startedChanger rounded-3">Let's get Started</a>
            </p>
        </div>
        <div class="col-lg-5">
            <img src="{{asset('assets/img/consultant_3.png')}}" class="img-fluid" alt="job interviewer">
        </div>
    </div>
</div>
@include('newsletter_form')
@endsection

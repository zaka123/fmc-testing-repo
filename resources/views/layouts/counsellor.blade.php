<!DOCTYPE html>
<html lang="en" class="fontawesome-i2svg-active fontawesome-i2svg-complete">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TDNQXVZ');
    </script>
    <!-- End Google Tag Manager -->
    <title>{{ $pagename ?? 'Coach' }} | {{ config('app.name') }}</title>
    <!-- Bootstrap Icons -->
    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <!-- our css -->
    <link rel="stylesheet" href="{{ asset('assets/css/user.css') }}">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Datatables -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/plugins/dataTables/css/jQuery.dataTables.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap5.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.bootstrap5.css') }}">

    <script src="{{ asset('assets/js/jquery-3.6.0.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/DataTables-1.13.1/js/dataTables.bootstrap5.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/Responsive-2.4.0/js/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/Responsive-2.4.0/js/responsive.bootstrap5.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>

    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('style/plugins/toastr/toastr.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('style/plugins/toastr/css/toastr.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('style/plugins/summernote/summernote-bs5.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
    <link rel="stylesheet" href="{{ asset('style/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <style>
        .inner {
            min-height: auto !important;
        }

        #DataTables_Table_0_filter {
            text-align: left !important;
        }

        @media (min-height: 700px) {
            .main {
                min-height: 87.2vh !important
            }
        }

        body {
            overflow-x: hidden !important
        }

        .cropImage {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin: 0 auto;

        }

        .previewCropImage {
            text-align: center;
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
        }

        .modal-cropImage {
            max-width: 700px !important;
        }
    </style>

    @yield('css')
    <script src="{{ asset('assets/js/8e5f520b45.js') }}"></script>
</head>

<body style="background-image:none">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDNQXVZ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @yield('counsellor-content')
    <!-- profile update modal -->
    <div class="modal fade" id="changeProfilePicture" tabindex="-1" role="dialog"
        aria-labelledby="changeProfilePictureTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-black" id="changeProfilePictureTitle">Upload Picture</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ url('counsellor/profile-picture') }}" class="needs-validation"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body border-0 ">
                        <input class="form-control" type="file" required id="photo" name="photo">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets/js/8e5f520b45.js') }}"></script>
    <script src="{{ asset('style/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('style/plugins/toastr/js/toastr.min.js') }}"></script>
    <script src="{{ asset('style/plugins/summernote/summernote-bs5.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
    <script>
        var $modal = $('#changePictureModal');
        var image = document.getElementById('image');
        var cropper;

        /*------------------------------------------
        --------------------------------------------
        Image Change Event
        --------------------------------------------
        --------------------------------------------*/
        // $("body").on("change", ".image", function(e) {
        //     var files = e.target.files;
        //     var done = function(url) {
        //         image.src = url;
        //         $modal.modal('show');
        //     };

        //     var reader;
        //     var file;
        //     var url;

        //     if (files && files.length > 0) {
        //         file = files[0];

        //         if (URL) {
        //             done(URL.createObjectURL(file));
        //         } else if (FileReader) {
        //             reader = new FileReader();
        //             reader.onload = function(e) {
        //                 done(reader.result);
        //             };
        //             reader.readAsDataURL(file);
        //         }
        //     }
        // });

        $("body").on("change", ".image", function(e) {
            var files = e.target.files;

            // Check if a file was selected
            if (!files || files.length === 0) {
                // No file selected, you can show an error message here if needed
                return;
            }

            var validImageTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

            var file = files[0];

            // Check if the selected file is a valid image type
            if (validImageTypes.includes(file.type)) {
                // Continue with the image processing logic

                var done = function(url) {
                    image.src = url;
                    $modal.modal('show');
                };

                var reader;
                var url;

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            } else {
                // Show an error message for invalid image type
                // alert('Please select a valid image file (JPEG, JPG, PNG, GIF).');
                toastr.error("Please select a valid image file (JPEG, JPG, PNG, GIF).");
                // Clear the input field
                $(this).val('');
            }
        });

        /*------------------------------------------
        --------------------------------------------
        Show Model Event
        --------------------------------------------
        --------------------------------------------*/
        $modal.on('shown.bs.modal', function() {
            var aspectRatio = image.naturalWidth / image.naturalHeight;
            cropper = new Cropper(image, {
                // aspectRatio: 1,
                aspectRatio: aspectRatio,
                viewMode: 3,
                preview: '.previewCropImage'
            });
        }).on('hidden.bs.modal', function() {
            cropper.destroy();
            cropper = null;
        });

        /*------------------------------------------
        --------------------------------------------
        Crop Button Click Event
        --------------------------------------------
        --------------------------------------------*/
        $("#crop").click(function() {
            canvas = cropper.getCroppedCanvas({
                width: 160,
                height: 160,
            });

            canvas.toBlob(function(blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    var base64data = reader.result;
                    $("input[name='image_base64']").val(base64data);
                    // $(".show-image").show();
                    // $(".show-image").attr("src",base64data);
                    // $("#changePictureModal").modal('toggle');

                    // Submit the form
                    $("#changePictureForm").submit();
                }
            });
        });
    </script>
    <script>
        toastr.options = {
            "closeButton": true,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </script>
    <script>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}");
            @endforeach
        @endif
    </script>
    @stack('script')
    <script>
        function myFunction() {
            var myModal = new bootstrap.Modal(document.getElementById("exampleModal"));
            myModal.show();
        }
    </script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });

        $(document)
            .ready(() => {
                $('.payments')
                    .DataTable({
                        "responsive": true,
                        "lengthChange": false,
                        "autoWidth": false,
                        "ordering": false,
                        "pagingType": "simple",
                        "pageLength": 5,
                        dom: '<"title pt-3"<"filter"f>>rt<"row"<"col-12 col-md-6"i><"col-12 col-md-6"p>>'
                    });

                // fix height of cards
                let height = $('.latest-sessions.card')
                    .height();
                $('.upcoming-sessions')
                    .height(height);

            });
    </script>
    <script>
        $(function() {
            $(document)
                .scroll(function() {
                    var $nav = $(".navbar.fixed-top");
                    $nav.toggleClass('scrolled', $(this)
                        .scrollTop() > $nav.height());
                });
        });
    </script>
    <script>
        if (matchMedia("(max-width: 768px)").matches) {
            try {
                $("#DataTables_Table_0_filter > label > input")
                    .attr({
                        placeholder: 'Search'
                    });
                console.log('placeholder added');
                $("#DataTables_Table_0_filter > label")
                    .html($("#DataTables_Table_0_filter > label input")[0])
                console.log($("#DataTables_Table_0_filter > label input"));
            } catch {
                // do nothing
                console.log('something went wrong.');
            }
        }
    </script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (() => {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            const forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.from(forms)
                .forEach(form => {
                    form.addEventListener('submit', event => {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()

        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/64ef1a8ca91e863a5c10a3ce/1h92vvdv2';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    @toastr_render
</body>

</html>

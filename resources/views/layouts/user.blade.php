<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TDNQXVZ');
    </script>
    <!-- End Google Tag Manager -->
    <title>{{ $pagename ?? 'Student' }} | {{ config('app.name') }}</title>
    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-icons.css') }}">
    <!-- our css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/color-calendar/dist/css/theme-basic.css" />
    <link rel="stylesheet" href="{{ asset('assets/css/user.css') }}">
    <!-- external libraries -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
        integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('style/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css"
        integrity="sha384-4LISF5TTJX/fLmGSxO53rV4miRxdg84mZsxmO8Rx5jGtp/LbrixFETvWa5a6sESd" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="{{ asset('assets/js/jquery-3.6.0.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/DataTables-1.13.1/js/dataTables.bootstrap5.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/Responsive-2.4.0/js/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('style/plugins/datatables-bs5/Responsive-2.4.0/js/responsive.bootstrap5.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>

    <style>
        @media (min-width: 1200px) and (min-height: 900px) {
            footer {
                position: absolute !important;
                bottom: 0 !important;
                width: 83%;
            }
        }

        body {
            overflow-x: hidden !important
        }

        /* coach session info styles */
        .coach_session_info {
            background: #FFFFFF 0% 0% no-repeat padding-box !important;
            box-shadow: 0px 3px 6px #00000029 !important;
            border-radius: 5px !important;
            opacity: 1;
        }


        .heading {
            font-weight: 700;
        }

        .line {
            width: 100%;
            margin: auto;
            background-color: rgba(128, 128, 128, 0.568);
            height: 2px;
            margin: 15px 0px;
        }

        .session-icon {
            display: flex;
            align-items: center;
            justify-content: start;
            gap: 10px;
            margin: 20px 0px;
        }

        .session-icon__icon {
            width: 45px;
            height: 45px;
            background-color: #496C8C;
            color: white;
            font-size: 21px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 50%;
        }

        .session-icon__icon--small {
            width: 35px;
            height: 35px;
            background-color: #496C8C;
            color: white;
            font-size: 16px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 50%;
        }

        .session-icon__time {
            padding: 0;
            margin: 0;
            font-weight: 600;
            font-family: 'Montserrat', sans-serif !important;
            width: 70%
        }

        .first-part {
            border-right: 1px solid rgba(190, 186, 186, 0.637);
        }

        .second-part {
            border-right: 1px solid rgba(190, 186, 186, 0.637);
        }

        .color-calendar {
            width: 100% !important;
            box-shadow: none !important;
            margin: 20px 0px;
        }

        .calendar__day-box {
            background-color: #496C8C !important;
            border-radius: 50% !important;
            box-shadow: none !important;
        }

        .session-location {
            background: #729CC4 0% 0% no-repeat padding-box;
            border-radius: 9px;
            opacity: 1;
            border: 0;
            height: 40px;
            color: white;
            cursor: pointer;
            width: 70%;
            padding-left: 35px;
        }

        .session-location:focus,
        .session-location:active {
            box-shadow: none;
            outline: none;
            border: none;
        }

        .calendar__header {
            position: relative;
        }

        .calendar__header::after {
            content: "";
            width: 90%;
            height: 2px;
            background-color: rgba(128, 128, 128, 0.233);
            position: absolute;
            bottom: 0;
            left: 5%;

        }

        @media(max-width:768px) {
            .selected-date {
                margin-top: 30px !important;
            }
        }

        .available-time {
            width: 100%;
            border: 2px solid #496C8C;
            border-radius: 32px;
            opacity: 1;
            height: 45px;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            transition: all 0.3s ease-in-out;
            /* display: none; */
        }

        .available-time.active {
            width: 50% !important;
        }

        .confirm {
            background: #496C8C 0% 0% no-repeat padding-box;
            border-radius: 32px;
            opacity: 1;
            border: 0;
            height: 45px;
            display: none;
        }

        .confirm.active {
            width: 50%;
            display: block;
        }

        #showMoreBtn {
            margin-right: auto;
            background-color: #F1F1F1;
        }

        @media(max-width:768px) {
            #showMoreBtn {
                margin: auto;
            }
        }

        /* couch session styles ends here */
        /* payments styles starts here */
        .payment-info {
            background: #FFFFFF 0% 0% no-repeat padding-box !important;
            box-shadow: 0px 3px 6px #00000029 !important;
            border-radius: 5px !important;
            opacity: 1;
        }

        .payment-info-main {
            background: #FFFFFF 0% 0% no-repeat padding-box;
            border: 1px solid #707070;
            border-radius: 15px;
        }

        @media(min-width:768px) {
            .payment-info-main {
                border-right: none;
            }
        }



        .payment-info__right {
            background: #496C8C 0% 0% no-repeat padding-box;
            border-radius: 15px;
            opacity: 1;
        }

        @media(max-width:768px) {
            .payment-info__right {
                width: 92%;
                margin: auto;
            }
        }

        .payment-info__left--child {
            border: 1px solid #707070;
            border-radius: 15px;
            opacity: 1;
            margin: 15px 0px;
        }

        .payment-info__left input[type='radio'] {
            accent-color: rgba(113, 111, 211, 0.363) !important;
            width: 20px;
            height: 20px;
        }

        .legend {
            font-size: 12px;
            font-weight: 700;
            font-family: 'Montserrat', sans-serif !important;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
            line-height: 1;
            margin-top: 20px;
        }

        .input {
            background: #FCFCFC 0% 0% no-repeat padding-box;
            border: 0.5px solid #707070;
            border-radius: 6px;
            opacity: 0.92;
            width: 100%;
            height: 34px;
            padding-left: 15px
        }

        .payment-info_left--third .form-control {
            padding-left: 5px !important;
        }

        .payment-info_left--third .form-control:focus {
            outline: 1px solid black;
            box-shadow: none;
            border: 1px solid black;
        }

        .coupan {
            background: #FCFCFC 0% 0% no-repeat padding-box;
            border: 0.5px solid #707070;
            border-radius: 6px;
            opacity: 0.92;
            padding-left: 15px
        }

        .button {
            background: #729CC4 0% 0% no-repeat padding-box;
            border-radius: 26px;
            opacity: 1;
            border: none;
            padding: 8px 0px;
        }

        #credit-list1,
        #credit-list2 {
            display: none;
        }

        /* payments styles ends here */
    </style>

    @yield('css')
</head>
@php $active = $pagename ?? '' @endphp

<body class="m-0 p-0">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDNQXVZ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <main>
        @yield('modal')
        <!-- time zone modal -->
        @php
            $timezones = App\Models\Timezone::all();
        @endphp
        <!-- timezone update modal -->
        <div class="modal fade" id="changeTimezoneModal" tabindex="-1" role="dialog"
            aria-labelledby="changeTimezoneModalTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-black" id="changeTimezoneModalTitle">Change Timezone</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" action="{{ route('user.timezone.update', auth()->id()) }}"
                        class="needs-validation">
                        @csrf
                        <div class="modal-body border-0 ">
                            <select class="form-select select2bs4" name="timezone" id="timezone" required>
                                <option value="">Select timezone</option>
                                @foreach ($timezones as $timezone)
                                    <option value="{{ $timezone->timezone }}"
                                        @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                        {{ $timezone->timezone }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary closeChanger"
                                data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary saveChanger">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- modal for the payments end here --}}
        <!-- Change Profile Picture Modal -->
        @include('extra.profile-picture-modal')

        <!-- Error messages -->
        @if (auth()->user()->email_verified_at == null)
            <div class="container">
                <div class="row">
                    <div class="alert alert-warning alert-dismissible mt-3 fade show" role="alert">
                        You haven't verified your email. Please verify it.
                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                            aria-label="Close"></button>
                    </div>
                </div>
            </div>
        @endif
        @yield('user-content')
    </main>
    {{-- external libraries js links --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
    integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- js for sidebar menu_item_special starts here --}}
    <script script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    @stack('script')
    <script>
        var specialItem = document.querySelectorAll('.menu_item_special');
        specialItem.forEach((item) => {
            if (item.classList.contains('active')) {
                item.addEventListener('mouseenter', function() {
                    item.style.backgroundColor = 'transparent';
                })
            }
        })
        // js for the popup for profile and other starts here
        $(function() {
            // Reusable function to handle toggle behavior
            function toggleElement(selector, list) {
                const elementSelector = $(selector);
                const elementList = $(list);
                elementSelector.on("click", function(e) {
                    elementList.toggle("drop", {
                        direction: "vertical"
                    }, 700);
                    // Prevent the click event from propagating to the document body
                    e.stopPropagation();
                });
                // Close the element when clicking outside
                $(document).on("click", function(e) {
                    if (!elementList.is(e.target) && !elementSelector.is(e.target) && elementList.has(e
                            .target).length === 0) {
                        elementList.hide("drop", {
                            direction: "vertical"
                        }, 700);
                    }
                });
            }
            // Call the function for each element
            toggleElement("#userInfoButton", "#userInfo");
            // js for animating the username is here
            var animatedName = document.getElementById('userName');
            window.addEventListener('load', function() {
                animatedName.classList.add('animate__flipInX');
            })
        });

        //js for the preloader gost elements
        var skeletons = document.querySelectorAll('.skeleton');
        window.addEventListener('load', function() {
            skeletons.forEach((skeleton) => {
                skeleton.classList.remove('skeleton');
            })
        })
    </script>
    {{-- js for sidebar menu_item_special ends here --}}
    <script>
        (() => {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            const forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.from(forms).forEach(form => {
                form.addEventListener('submit', event => {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
        })()
    </script>
    <script>
        function setUpComingSessionCardHeight() {
            // fix height of cards
            if (window.matchMedia('(min-width: 776px)').matches) {
                setTimeout(() => {
                    let upComingSessionCardHeight = $('.upcoming-sessions').height();
                    let latestSessionCardHeight = $('.latest-sessions.card').height();
                    $('.upcoming-sessions')
                        .animate({
                            height: latestSessionCardHeight
                        }, 800);
                }, 1500)
            } else {
                $('.upcoming-sessions').animate({
                    height: 'unset'
                }, 800);
            }
        }


        // save datatable for later use
        var dataTable;
        $(document)
            .ready(() => {
                try {
                    dataTable = initializeDataTable();
                } catch {
                    // do nothing
                }

                // set card height
                setUpComingSessionCardHeight();

                // reset card height on window resize
                $(window)
                    .on('resize', () => {
                        setUpComingSessionCardHeight()
                    });

                if (matchMedia("(max-width: 768px)").matches) {
                    try {
                        $("#DataTables_Table_0_filter > label > input")
                            .attr({
                                placeholder: 'Search'
                            });
                        $("#DataTables_Table_0_filter > label")
                            .html($("#DataTables_Table_0_filter > label input"))
                    } catch {
                        // do nothing
                    }
                }
            });
    </script>
    <script>
        function change(arg) {
            $('#save_changes').show();
            $('#save_changes').attr('form', arg);
        }
    </script>
    <script src="{{ asset('assets/js/8e5f520b45.js') }}"></script>
    <script src="{{ asset('style/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('style/plugins/toastr/js/toastr.min.js') }}"></script>
    <script>
        toastr.options = {
            "closeButton": true,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </script>
    {{-- <script>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}");
            @endforeach
        @endif
    </script> --}}
    <script>
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/64ef1a8ca91e863a5c10a3ce/1h92vvdv2';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    @toastr_render
</body>

</html>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <p><b>{{ __("First Name") }}: </b>{{ $data["first_name"] }}</p>
    <p><b>{{ __("Last Name") }}: </b>{{ $data["last_name"] }}</p>
    <p><b>{{ __("Phone") }}: </b>{{ $data["i_phone_number"] ?? 'N/A' }}</p>
    <p><b>{{ __("Email") }}: </b>{{ $data["email"] }}</p>
    <p><b>{{ __("Linkedin") }}: </b>{{ $data["linked_in"] ?? 'N/A' }}</p>
    <p><b>{{ __("What are you interested in") }}: </b>{{ $data["interested_in"] }}</p>
    <p><b>{{ __("Country") }}: </b>{{ $data["country"] ?? 'N/A' }}</p>
    <p><b>{{ __("Gender") }}: </b>{{ $data["gender"] ?? 'N/A' }}</p>
    {{-- <p><b>{{ __("Do you have career/life coaching certificate") }}: </b>{{ ($data["coaching_certification"] ?? '') == 'on' ? 'Yes' : 'No' }}</p> --}}
    <p><b>{{ __("Why you want to become a career coach") }}: </b>{{ $data["career_couch"] }}</p>
    <p><b>{{ __("Uploaded Resume/CV") }}: </b></p>
</body>
</html>

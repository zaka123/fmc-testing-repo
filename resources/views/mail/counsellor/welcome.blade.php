<!DOCTYPE html>
<html>

<body>
    <p>Hi {{ $data["first_name"] }},</p>
    <p>Thanks for your interest in joining FIND ME CAREER for the Career coach role.</p>
    <p>We’ve received your application which our recruitment team will review as quickly as possible.</p>
    <p>If you’d like to contact us in the meantime, please email <a href="mailto:careers@findmecareer.com">careers@findmecareer.com</a>.</p>
    <br>
    <br>
    <br>
    <p>Kind Regards,</p>
    <p>The FMC Talent Team</p>
</body>

</html>

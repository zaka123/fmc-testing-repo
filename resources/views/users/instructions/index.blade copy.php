@extends('layouts.user', ['pagename' => $test->test_name])
@section('css')
    @if ($test)
        @if ($test->id == 4)
            <style>
                .personality-test {
                    color: var(--special) !important;
                    font-weight: 500;
                }

                .changerHandlerLook:hover {
                    background-color: black !important;
                }

                .scoreChanger:hover {
                    background-color: black !important;
                }

                .blackishChanger:hover {
                    background-color: black !important;
                }
            </style>
        @elseif($test->id == 5)
            <style>
                .career-expectations-test {
                    color: var(--special) !important;
                    font-weight: 500;
                }

                .changerHandlerLook:hover {
                    background-color: black !important;
                }

                .scoreChanger:hover {
                    background-color: black !important;
                }

                .blackishChanger:hover {
                    background-color: black !important;
                }
            </style>
        @else
            <style>
                .skill-assessment-test {
                    color: var(--special) !important;
                    font-weight: 500;
                }

                .changerHandlerLook:hover {
                    background-color: black !important;
                }

                .scoreChanger:hover {
                    background-color: black !important;
                }

                .blackishChanger:hover {
                    background-color: black !important;
                }
            </style>
        @endif
    @endif
@endsection

@section('user-content')
    <!-- modals here  -->
    <div>
        @php
            $testResults = auth()
                ->user()
                ->getTestResults(4);
            $skillAssessmentTestResults = auth()
                ->user()
                ->getTestResults(6);
            $careerExpectationsTestResults = auth()
                ->user()
                ->getTestResults(5);
        @endphp
        <div class="modal fade" id="restartTestModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
            aria-labelledby="restartTestModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="restartTestModalLabel">Attention!</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>If you restart the assessment, you wouldn't be able to see your previous results.</p>
                        <p>Are you sure you want to proceed?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary blackishChanger"
                            data-bs-dismiss="modal">No</button>
                        <a id="restartLink" {{-- href="{{ url('user/test/4/restart') }}" --}} class="btn btn-sm btn-danger redChanger">Yes</a>
                    </div>
                </div>
            </div>
        </div>

        {{-- @if ($testResults >= 99 && $test->id == 4)
            <div class="modal fade" id="testScoreStaticBackdrop{{ $test->id }}" data-bs-backdrop="static" tabindex="-1"
                aria-labelledby="testScoreStaticBackdrop{{ $test->id }}Label" aria-hidden="true">
                @include('users.results.personality')
            </div>
        @endif

        @if ($skillAssessmentTestResults >= 99 && $test->id == 6)
            <div class="modal fade" id="testScoreStaticBackdrop{{ $test->id }}" data-bs-backdrop="static" tabindex="-1"
                aria-labelledby="testScoreStaticBackdrop{{ $test->id }}Label" aria-hidden="true">
                @include('users.results.skill-assessments')
            </div>
        @endif

        @if ($careerExpectationsTestResults >= 99 && $test->id == 5)
            <div class="modal fade" id="testScoreStaticBackdrop{{ $test->id }}" data-bs-backdrop="static" tabindex="-1"
                aria-labelledby="testScoreStaticBackdrop{{ $test->id }}Label" aria-hidden="true">
                <!-- resutls dialog -->
                @include('users.results.career')
            </div>
        @endif --}}
    </div>

    <!-- modals end  -->

    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12 main">
                        <div class="floating"></div>
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="card border-0 py-3 px-3 px-lg-auto bg-transparent">
                                    <div class="card-body">
                                        <div class="row inner">
                                            <div class="col-10 offset-1">
                                                <div class="py-4">
                                                    <h3 class="mb-4">Instructions!</h3>
                                                    <ul class="instructions">
                                                        @forelse($instructions as $instruction)
                                                            <li class="text-muted">{{ $instruction->instruction }}</li>
                                                        @empty
                                                            <li class="text-muted">No instructions found</li>
                                                        @endforelse
                                                    </ul>
                                                    <div id="testsButtonsDiv" class="text-center">
                                                        @if (is_array($result))
                                                            {{-- <button class="btn px-4 px-md-5 btn-sm text-light scoreChanger"
                                                                style="background-color: #00c2cb" data-bs-toggle="modal"
                                                                data-bs-target="#testScoreStaticBackdrop{{ $test->id }}">
                                                                View score
                                                            </button> --}}
                                                            <a href="{{ route('user.getTestReport', 'personality') }}" class="btn px-4 px-md-5 btn-sm text-light scoreChanger"
                                                            style="background-color: #00c2cb">View Score</a>

                                                            <button type="button"
                                                                class="btn restart-test px-4 px-md-5 bg-secondary text-light btn-sm blackishChanger"
                                                                onclick="openRestartModal({{ $test->id }})">
                                                                Restart Assessment
                                                            </button>
                                                        @elseif($result > 0 && $result < 100)
                                                            <a class="btn px-4 px-md-5 btn-sm text-light changerHandlerLook"
                                                                href="{{ route('user.test', $test->id) }}"
                                                                style="background-color: #4caf50">
                                                                Resume Assessment
                                                            </a>
                                                        @else
                                                            <a class="btn px-4 px-md-5 btn-sm text-light changerHandlerLook"
                                                                href="{{ route('user.test', $test->id) }}"
                                                                style="background-color: #00c2cb">
                                                                Start Assessment
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-12"> --}}
                    @include('footer.user')
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script>
            // Update the button text to "Resume Assessment" if the test is in progress
            test = parseInt("{{ $test->id }}");
            var taskName = 'tasks-' + test;
            let tasks = JSON.parse(localStorage.getItem(taskName));

            if (tasks !== null) {
                /* Update the button text to "Resume Assessment" */
                $('#testsButtonsDiv').html('');
                $('#testsButtonsDiv').html(`<a class="btn px-4 px-md-5 btn-sm text-light changerHandlerLook"
                                href="{{ route('user.test', $test->id) }}"
                                style="background-color: #4caf50">
                                Resume Assessment
                            </a>`);
            }

            // Showing warning message when the test is restarting
            function openRestartModal(testNumber) {
                // creating restart link
                const restartLink = document.getElementById('restartLink');
                restartLink.href = `/user/test/${testNumber}/restart`;

                const modal = new bootstrap.Modal(document.getElementById('restartTestModal'));
                modal.show();
            }
        </script>
    @endpush
@endsection

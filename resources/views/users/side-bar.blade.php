<style>
    @media (max-width: 576px) {
        .col-6 {
            width: 50% !important
        }
    }

    @media (min-width: 576px) {
        .col-sm-5 {
            width: 41.66666667% !important
        }
    }

    @media (min-width: 768px) {
        .col-md-4 {
            width: 33.333333% !important
        }
    }

    @media (min-width: 992px) {
        .col-lg-2 {
            width: 16.666667% !important
        }
    }

    a {
        transition: all ease 0.7s;
    }

    .sidebar-link {
        border-radius: 24px;
        margin: auto;
        width: 85%;
    }

    @media(max-width:668px) {
        .sidebar-link {
            width: 100%;
        }
    }

    .sidebar-link:hover {
        background-color: rgba(221, 226, 224, 0.3);
        border-radius: 24px;
    }

    a.active {
        box-shadow: inset 0px 7px 6px #00000029;
        outline: 1px solid #FFFFFF;
        border-radius: 24px;
    }

    a i {
        color: #749BC2;
    }

    .latest_special_bg {
        background-color: #729CC4 !important;
    }

    .profile_image_container {
        border-radius: 50%;
        margin: auto;
    }

    .logo_img {
        border: 1px solid #FFF;
        box-shadow: 0px 3px 6px #00000029 !important;
        object-fit: cover;
        max-width: 100%;
    }

    .menu_item_special {
        display: flex;
        align-items: center;
        justify-content: start;
        text-decoration: none;
    }

    .circular-icon {
        width: 30px;
        height: 30px;
        box-shadow: 2px 3px 6px #00000063;
        background-color: white;
        border-radius: 50%;
    }

    .smallest_font {
        font-size: 14px;
        margin: 0;
    }
</style>
<div class="col-6 col-sm-5 col-md-4 col-lg-2 offcanvas-start offcanvas-lg latest_special_bg" id="sidebar"
    style="--bs-offcanvas-width:auto;box-shadow: 1px 0px 0px 1px #00000026">
    <div class="sticky-top side-bar vh-100 overflow-auto">
        <div class="px-3">
            <div class="row row-cols-1 g-2">
                {{-- this div is for the logo image --}}
                <div class="col">
                    <div class="my-4">
                        <a href="{{ url('/') }}" class="d-block m-auto w-fit-content"
                            style="background: #fafafa;padding:3px 3px">
                            <img src="{{ asset('new_design_assets/img/logo.png') }}" alt="Brand"
                                class="img-fluid border border-1">
                        </a>
                    </div>
                </div>
                {{-- this div is for the profile picture --}}
                <div class="mt-3  text-center profile_image_container" style="height: 150px;width:150px">
                    <img src="{{ asset(auth()->user()->profile_photo ?? 'new_design_assets/img/user.svg') }}"
                        alt="profile pic" class="rounded-circle logo_img" width="100%" height="100%">
                </div>
                {{-- this div is for the user name --}}
                <div class="my-3">
                    <p class="special_monstrate_text text-center border border-1 w-fit-content m-auto px-4 rounded-3">
                        {{ auth()->user()->name }}</p>
                </div>
                {{-- this anchor is for the home button --}}
                <a href="/" class="menu_item_special justify-content-center gap-2 p-2  bg-white my-3 rounded-4"
                    style="box-shadow: 2px 5px 6px #0000006B;">
                    <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#496c8c"
                            d="M4 13h6a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1m-1 7a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1zm10 0a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-7a1 1 0 0 0-1-1h-6a1 1 0 0 0-1 1zm1-10h6a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1h-6a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1" />
                    </svg>
                    <p class="special_monstrate_text p-0 m-0" style="color: #496c8c;font-size:18px;">
                        Home</p>
                </a>
                {{-- this anchor is for the career caoching --}}
                {{-- <a href="{{ url('user/session/booking') }}"
                    class="menu_item_special @if (url()->current() == url('user/session/booking')) active @endif sidebar-link gap-2 p-1 my-2 mt-3">
                    <i class="fa-solid fa-arrows-down-to-people flex-all-center circular-icon"></i>
                    <p class="special_monstrate_text  smallest_font pt-1">
                        Career Coaching</p>
                </a> --}}
                <a href="{{ url('user/career/coaching') }}"
                    class="menu_item_special @if (url()->current() == url('user/career/coaching') || url()->current() == url('user/session/booking')) active @endif sidebar-link gap-2 p-1 my-2 mt-3">
                    <i class="fa-solid fa-arrows-down-to-people flex-all-center circular-icon"></i>
                    <p class="special_monstrate_text  smallest_font pt-1">
                        Career Coaching</p>
                </a>
                {{-- this anchor is for the assessments --}}
                <a href="{{ route('user.test.report') }}"
                    class="menu_item_special @if (url()->current() == route('user.test.report') || url()->current() == url('user/test/skill-assessment/report')) active @endif sidebar-link gap-2 p-1 my-2">
                    <i class="fa-solid fa-file-invoice flex-all-center circular-icon"></i>
                    <p class="special_monstrate_text  smallest_font pt-1">
                        Assessments</p>
                </a>
                {{-- this anchor is for the assessments --}}
                <a href="{{ route('user.test.instructions', 4) }}"
                    class="menu_item_special @if (url()->current() == route('user.test.instructions', 4) || url()->current() == url('user/test/personality/report')) active @endif sidebar-link gap-2 p-1 my-2">
                    <i class="fa-solid fa-graduation-cap  flex-all-center circular-icon"></i>
                    <p class="special_monstrate_text  smallest_font pt-1">
                        Career Explorer</p>
                </a>
            </div>
        </div>
    </div>
</div>

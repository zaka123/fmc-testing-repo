<form action="{{ route("user.zoom.account.connect") }}" method="post" onsubmit="this.querySelector('button').setAttribute('disabled', true);">
    @csrf
    <button class="btn w-100 btn-primary" data-bs-toggle="tooltip" data-bs-title="Connect zoom account to attend sessions.">
        Connect Zoom
    </button>
</form>

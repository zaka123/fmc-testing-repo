@extends('layouts.user',['pagename'=>'Dashboard'])
@section('user-content')

<div class="container-fluid px-0">
    <div class="row g-1">
        <!-- side bar -->
        @include('users.side-bar')

        <!-- main central contents column -->
        <div class="col-lg-8" style="background-color: rgba(112,112,112,7%);">
            <h3 class="bg-white text-special p-3 sticky-top d-flex">Meeting</h3>
            <iframe allow="microphone; camera" style="border: 0; height: 500%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/join/87098112519" frameborder="0"></iframe>
        </div>
    </div>
</div>
@endsection
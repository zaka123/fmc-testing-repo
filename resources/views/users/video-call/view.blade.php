@extends('layouts.user', ['pagename' => 'One to One Session'])
@section('user-content')
<div class="container-fluid px-0">
    <div class="row g-1">
        <!-- side bar here  -->
        @include('users.side-bar')
        <!--/ side bar -->
        <div class="col-md-10">
            <div class="row g-0">
                <div class="col-12 sticky-top bg-white">
                    <div class="text-end pb-3 px-3 d-flex">
                        <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas" data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                        <div class="ms-auto">@include('navbars.user')</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="main d-flex align-items-center" style="min-height: 83.3vh;">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-10 col-lg-8">
                                <div class="card shadow" style="--bs-card-border-color:var(--special)">
                                    <h5 class="card-header text-center text-white bg-special">
                                        Your Meeting is ready
                                    </h5>
                                    <div class="card-body">
                                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                    </div>
                                    <div class="card-footer text-center bg-special">
                                        <a href="{{ $videoCall->joining_link }}" target="_blank" class="btn btn-dark btn-sm">Join Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @include('footer.user')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

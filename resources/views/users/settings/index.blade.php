@extends('layouts.user', ['pagename' => 'Settings'])
@section('css')
    <style>
        .settings__sidebar-item__wrapper {
            display: flex;
            align-items: center;
            justify-content: end;
            margin: 8px 0;
        }

        @media(max-width:768px) {
            .settings__sidebar-item__wrapper {
                justify-content: center;
            }
        }

        .settings__sidebar--item {
            padding: 18px;
            background: #F7FAFD 0% 0% no-repeat padding-box;
            border: 1px solid #707070;
            border-radius: 13px;
            opacity: 1;
            cursor: pointer;
        }

        .settings__sidebar--item.active {
            background: #729CC4 0% 0% no-repeat padding-box;
        }

        .settings__sidebar--item.active>* {
            color: white
        }

        .settings__content--header {
            display: flex;
            align-items: center;
            justify-content: space-around;
            padding: 12px 0;
            background: #FFFFFF 0% 0% no-repeat padding-box;
            border: 1px solid #707070;
            border-radius: 13px;
            opacity: 1;
        }


        .settings__content--header-img {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 20px;
        }

        .settings__content--header-img img {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            box-shadow: 0px 3px 6px #00000029;
            border: 2px solid #FFFFFF;
            opacity: 1;
        }

        .settings__content--body {
            background: rgba(247, 250, 253, 0.67) 0% 0% no-repeat padding-box;
            border: 1px solid #707070;
            border-radius: 13px;
        }

        .settings__content--header-button button {
            border: none;
            background: #729CC4 0% 0% no-repeat padding-box;
            border-radius: 9px;
            opacity: 1;
            padding: 0 7px;
        }

        @media(max-width:768px) {
            .settings__content--header {
                flex-direction: column;
            }

            .settings__content--header-img {
                margin-left: 8px
            }
        }

        .legend {
            font-size: 12px;
            font-weight: 700;
            font-family: 'Montserrat', sans-serif !important;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
            line-height: 1;
            margin-top: 20px;
        }

        .input {
            background: #FCFCFC 0% 0% no-repeat padding-box;
            border: 0.5px solid #707070;
            border-radius: 6px;
            opacity: 0.92;
            width: 100%;
            height: 34px;
            padding-left: 15px
        }

        .update-button {
            background: #496C8C 0% 0% no-repeat padding-box;
            border-radius: 9px;
            opacity: 1;
            color: white;
            border: none;
            width: 100%;
            padding: 7px;
        }

        .helping-text {
            font-size: 14px;
            color: black;
            font-weight: 500
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <div class="col-lg-10">
                <div class="row g-0">
                    @include('includes.header')
                    <div class="col-12">
                        <div class="row px-5">
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('error') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>
                            @endif
                        </div>
                        <div class="row g-0">
                            <div class="settings__sidebar col-md-4 pt-5">
                                {{-- for account settings --}}
                                <div class="settings__sidebar-item__wrapper">
                                    <div class="settings__sidebar--item" id="accountSettings">
                                        <h2 class="spacial_bold_monstrate_text fs-5  p-0 m-0" style="font-weight: 600">
                                            Account Settings</h2>
                                        <p class="special_monstrate_text  p-0 m-0 helping-text">Details
                                            about
                                            your
                                            personal information</p>
                                    </div>
                                </div>
                                {{-- for passwords settings --}}
                                <div class="settings__sidebar-item__wrapper">
                                    <div class="settings__sidebar--item" id="passwordSettings">
                                        <h2 class="spacial_bold_monstrate_text fs-5 p-0 m-0" style="font-weight: 600">
                                            Password & Security</h2>
                                        <p class="special_monstrate_text p-0 m-0 helping-text">Details
                                            about
                                            your
                                            personal information</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 p-md-5 p-3">
                                {{-- account settings contents --}}
                                <div class="settings__content" id="accountSettingsContent">
                                    <div class="settings__content--wrapper mt-2">
                                        <div class="settings__content--header">
                                            <div class="settings__content--header-img">
                                                <img src="{{ asset(auth()->user()->profile_photo ?? 'new_design_assets/img/random-coach.svg') }}"
                                                    alt="">
                                                <div>
                                                    <h2 class="spacial_bold_monstrate_text fs-5  p-0 m-0"
                                                        style="font-weight: 700">Upload a new photo</h2>
                                                    <p class="special_monstrate_text text-dark fw-medium p-0 m-0"
                                                        style="font-size: 14px">profile-pic.jpg</p>
                                                </div>
                                            </div>
                                            <div class="settings__content--header-button">
                                                <button class="spacial_bold_monstrate_text text-white fs-5"
                                                    data-bs-toggle="modal" data-bs-target="#profilePictureModal"
                                                    style="font-weight: 500">Upload</button>
                                            </div>
                                        </div>
                                        <div class="settings__content--body mt-4 px-md-5 px-3 py-4">
                                            <h2 class="spacial_bold_monstrate_text fs-5  p-0 m-0" style="font-weight: 700">
                                                Change user information here</h2>
                                            <form action="{{ route('user.settings.profile.update') }}" method="post">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <legend class="legend">Full Name*</legend>
                                                        <input type="text" class="input" name="name" id=""
                                                            value="{{ auth()->user()->full_name }}" required>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">Email Address*</legend>
                                                        <input type="email" class="input" name="email" id=""
                                                            value="{{ auth()->user()->email }}" required>
                                                    </div>
                                                    <div class="col-12">
                                                        <legend class="legend">Address</legend>
                                                        <input type="text" class="input" name="address" id=""
                                                            value="{{ auth()->user()->address->address ?? '' }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">City</legend>
                                                        <input type="text" class="input" name="city" id=""
                                                            value="{{ auth()->user()->address->city ?? '' }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">State/Province</legend>
                                                        <input type="text" class="input" name="state" id=""
                                                            value="{{ auth()->user()->address->state ?? '' }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">Zip Code</legend>
                                                        <input type="number" class="input" name="zip_code" id=""
                                                            value="{{ auth()->user()->address->zip_code ?? '' }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">Country</legend>
                                                        <input type="text" class="input" name="country"
                                                            id=""
                                                            value="{{ auth()->user()->address->country ?? '' }}">
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit"
                                                            class="update-button spacial_bold_monstrate_text text-white fs-5 mt-5"
                                                            style="font-weight: 500">Update information</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- password settings contents --}}
                                <div class="settings__content" id="passwordSettingsContent">
                                    <div class="settings__content--wrapper">
                                        <div class="settings__content--body mt-2 px-md-5 px-3 py-4">
                                            <h2 class="spacial_bold_monstrate_text fs-5  p-0 m-0"
                                                style="font-weight: 700">
                                                Change Password here</h2>
                                            <form action="{{ route('user.settings.password.update') }}" method="post">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <legend class="legend">Current Password*</legend>
                                                        <input type="password" class="input" name="current_password"
                                                            id="" required>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <legend class="legend">New Password*</legend>
                                                        <input type="password" class="input" name="password"
                                                            id="" required>
                                                    </div>
                                                    <div class="col-12">
                                                        <legend class="legend">Confirm Password*</legend>
                                                        <input type="password" class="input"
                                                            name="password_confirmation" id="" required>
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit"
                                                            class="update-button spacial_bold_monstrate_text text-white fs-5 mt-5"
                                                            style="font-weight: 500">Update password</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $("#accountSettingsContent").show();
            $("#accountSettings").addClass('active');
            $("#passwordSettingsContent").hide();
            $("#accountSettings").click(function() {
                $("#accountSettingsContent").show();
                $("#accountSettings").addClass('active');
                $("#passwordSettings").removeClass('active');
                $("#passwordSettingsContent").hide();
            });
            $("#passwordSettings").click(function() {
                $("#accountSettingsContent").hide();
                $("#passwordSettings").addClass('active');
                $("#accountSettings").removeClass('active');
                $("#passwordSettingsContent").show();
            });
        });
    </script>
@endpush

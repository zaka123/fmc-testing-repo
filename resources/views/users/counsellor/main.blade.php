@extends('layouts.user', ['pagename' => 'Coaches'])
@section('css')
    <style>
        .counsellorz {
            color: var(--special) !important;
            font-weight: 500;
        }

        #more {
            display: none;
        }

        #readBtn {
            text-decoration: none;
        }

        @media (max-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        @media (min-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        /* Medium devices (tablets, 768px and up) */
        @media (min-width: 768px) {
            .my-image {
                width: 100px !important;
            }
        }

        /* Large devices (desktops, 992px and up) */
        @media (min-width: 992px) {
            .my-image {
                width: 150px !important;
            }
        }

        .profile {
            cursor: pointer !important;
        }

        .inner::before {
            top: 69%;
        }

        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .scrollmenu {
            white-space: nowrap;
            overflow-x: auto;
        }

        .scrollmenu::-webkit-scrollbar {
            width: 1px !important;
        }

        .scrollmenu::-webkit-scrollbar-track {
            background: lightgrey;
            width: 1px !important;
            border-radius: 1rem;

        }

        .scrollmenu::-webkit-scrollbar-thumb {
            background: #888;
            border-radius: 10px;
        }

        .scrollmenu::-webkit-scrollbar-thumb:hover {

            background: #555;
        }

        .fontChanger:hover {
            background-color: black !important;
        }

        .closeChanger:hover {
            background-color: black !important;
        }

        .saveChanger:hover {
            background-color: rgb(53, 53, 175) !important;
        }

        .helping_list_item {
            list-style: none;
            font: normal normal 600 17px/19px;
            font-family: 'Montserrat', sans-serif !important;
            letter-spacing: 0px;
            color: black;
            opacity: 1;
            position: relative;
            margin: 20px 0px
        }

        .helping_list_item::before {
            content: "\f058";
            font-family: 'Font Awesome 5 Free';
            font-weight: 100;
            color: #496C8C;
            position: absolute;
            left: -30px;
        }

        .career-image {
            object-fit: fill;
            margin: auto;
            display: block
        }

        @media(max-width:992px) {
            .career-image {
                width: 90%;
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <div class="col-lg-10">
                <div class="row g-0">
                    @include('includes.header')
                    <div class="col-12">
                        <div class="main" style="min-height:91vh">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="m-auto" style="width: 90%">
                                        <img src="{{ asset('new_design_assets/img/career-show.svg') }}" alt=""
                                            width="400" height="300" class="career-image">
                                        <h2 class="text-center spacial_bold_monstrate_text my-3" style="font-style: italic">
                                            CAREER COACHING</h2>
                                        <p class="m-auto special_monstrate_text text-dark" style="text-align:justify">
                                            Welcome to our Career Coaching hub, your dedicated space for unlocking
                                            professional growth and fulfillment. Our expert coaches are here to empower you
                                            with personalized guidance tailored to your unique goals and challenges. Whether
                                            you're seeking direction, aiming for advancement, or navigating a career
                                            transition, our one-on-one sessions provide the support and insights you need to
                                            thrive. Let's embark on this transformative journey together.</p>
                                        <a href="{{ url('user/session/booking') }}" class="anchor-button my-4 m-auto">Start
                                            Your Journey</a>
                                        <hr style="background-color: rgb(59, 58, 58);height:3px">
                                        <h4 class="special_monstrate_text text-dark fw-bolder fs-6">We can help you if:</h4>
                                        <ul class="helping_list">
                                            <li class="helping_list_item">Personalized career assessments to identify
                                                strengths and areas for development.</li>
                                            <li class="helping_list_item">Tailored coaching sessions focused on your
                                                specific career goals and challenges.</li>
                                            <li class="helping_list_item">Guidance on navigating career transitions, such as
                                                job changes or promotions.</li>
                                            <li class="helping_list_item">Strategies for improving leadership skills and
                                                enhancing professional effectiveness.</li>
                                            <li class="helping_list_item">Support in developing a compelling personal brand
                                                and networking effectively.</li>
                                            <li class="helping_list_item">Assistance in crafting a winning resume, cover
                                                letter, and LinkedIn profile.</li>
                                            <li class="helping_list_item">Mentorship and advice from seasoned professionals
                                                in your field.</li>
                                            <li class="helping_list_item">Ongoing support and accountability to ensure you
                                                stay on track with your career objectives.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.user', ['pagename' => 'Coaches'])
@section('css')
    <style>
        .counsellorz {
            color: var(--special) !important;
            font-weight: 500;
        }

        #more {
            display: none;
        }

        #readBtn {
            text-decoration: none;
        }

        @media (max-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        @media (min-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        /* Medium devices (tablets, 768px and up) */
        @media (min-width: 768px) {
            .my-image {
                width: 100px !important;
            }
        }

        /* Large devices (desktops, 992px and up) */
        @media (min-width: 992px) {
            .my-image {
                width: 150px !important;
            }
        }

        .profile {
            cursor: pointer !important;
        }

        .inner::before {
            top: 69%;
        }

        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .scrollmenu {
            white-space: nowrap;
            overflow-x: auto;
        }

        .scrollmenu::-webkit-scrollbar {
            width: 1px !important;
        }

        .scrollmenu::-webkit-scrollbar-track {
            background: lightgrey;
            width: 1px !important;
            border-radius: 1rem;

        }

        .scrollmenu::-webkit-scrollbar-thumb {
            background: #888;
            border-radius: 10px;
        }

        .scrollmenu::-webkit-scrollbar-thumb:hover {

            background: #555;
        }

        .fontChanger:hover {
            background-color: black !important;
        }

        .closeChanger:hover {
            background-color: black !important;
        }

        .saveChanger:hover {
            background-color: rgb(53, 53, 175) !important;
        }

        .coach_card_wrapper {
            display: flex;
            align-items: center;
            justify-content: center
        }

        @media(max-width:768px) {
            .coach_card_wrapper {
                padding: 0px 6px;
            }
        }

        .coach_card {
            height: 250px;
            width: 240px;
            background: #F7FAFD 0% 0% no-repeat padding-box;
            box-shadow: 0px 3px 6px #00000030;
            border-radius: 9px;
            opacity: 1;
        }

        .coach_card_header {
            width: 100%;
            height: 50%;
            display: flex;
            align-items: center;
            justify-content: center
        }

        .coach_card_header img {
            height: 90%;
            width: 50%;
            max-width: 50%;
            object-fit: cover;
            border-radius: 100%;

        }

        .coach_card_body {
            width: 100%;
            height: 30%;
        }

        .coach_card_footer {
            width: 100%;
            height: 20%;
        }

        .sessions_history {
            font-size: 12px;
            font-weight: 600;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 5px;
            background: #ECECEC 0% 0% no-repeat padding-box;
            border-radius: 7px;
            opacity: 1;
            width: fit-content;
            padding: 1px 10px;
            margin: auto;
        }

        .coachPrevBtn,
        .coachNextBtn {
            cursor: pointer;
            top: 150px;
            z-index: 99
        }

        .coachNextBtn {
            right: 4px;
        }

        .coachPrevBtn {
            left: 4px;
        }

        .single_coach_data {
            background: #F7FAFD 0% 0% no-repeat padding-box;
            box-shadow: 0px 3px 6px #00000029;
            border-radius: 9px;
            opacity: 1;
        }

        .first_data {
            display: flex;
            align-items: center;
            gap: 3px;
        }

        .first_data i {
            width: 27px;
            height: 27px;
            background: #496C8C 0% 0% no-repeat padding-box;
            border-radius: 3px;
            opacity: 1;
            color: white;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 16px;
            margin-right: 8px
        }

        .first_data h3 {
            font-size: 15px !important;
        }

        .first_data p {
            font-size: 15px;
            font-weight: 600
        }

        .owl-dots {
            position: absolute;
            bottom: 0%;
            width: fit-content;
            left: 50%;
            transform: translateX(-50%);
            display: flex;
            align-items: center;
            gap: 10px;
        }

        @media(max-width:768px) {
            .owl-dots {
                bottom: -25px;
            }
        }

        .owl-dots.disabled {
            display: block !important;
        }

        .owl-dot span {
            background: rgba(128, 128, 128, 0.452);
            display: block;
            width: 9px;
            height: 9px;
            border-radius: 50%;
        }

        .owl-dot.active {
            background: #7cbcfc !important;
            width: 9px;
            height: 9px;
            border-radius: 50%;
        }

        /* CSS for loader */
        .loader {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(255, 255, 255, 1);
            z-index: 1000;
        }

        .spinner {
            border: 4px solid rgba(255, 255, 255, 0.3);
            border-top: 4px solid #496C8C;
            border-radius: 50%;
            width: 40px;
            height: 40px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <div class="col-lg-10">
                <div class="row g-0">
                    @include('includes.header')
                    <div class="col-12 position-relative">
                        <span class="position-absolute coachPrevBtn d-none d-md-block">
                            <i class="bi bi-arrow-left-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                        </span>
                        {{-- check for any errors --}}
                        @if ($errors->any())
                            <div class="m-3">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissible fade show " role="alert">
                                        {{ $error }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div class="row g-2 p-md-4 owl-carousel mb-5 mb-md-0" id="coachesCarousel">
                            <!-- New Cards -->
                            @foreach ($counsellors as $counsellor)
                                <div class="col-lg-12 h-max-auto  my-3 coach_card_wrapper">
                                    <div class="p-0 mx-md-2 coach_card">
                                        <!--  image portion -->
                                        <div class="coach_card_header">
                                            <img src="{{ asset($counsellor->profile_photo ?? 'new_design_assets/img/random-coach.svg') }}"
                                                alt="">
                                        </div>
                                        <!--  sub text section -->
                                        <div class="coach_card_body">
                                            <h3 class="special_monstrate_text text-center text-dark fw-bold fs-6 m-0 p-0">
                                                {{ $counsellor->full_name ?? 'N/A' }}</h3>
                                            <p class="special_monstrate_text mb-1 p-0 text-center text-dark"
                                                style="font-size:14px">{{ $counsellor->counsellorProfile?->city ?? 'N/A' }}
                                            </p>
                                            @php
                                                $sessionsCount = 0;
                                                foreach ($counsellor->counsellorSessions as $session) {
                                                    if ($session->canBeBooked()) {
                                                        $sessionsCount++;
                                                    }
                                                }
                                            @endphp
                                            <p class="sessions_history special_monstrate_text text-dark"><i
                                                    class="bi bi-clock"></i>{{ $sessionsCount }}
                                                Sessions</p>
                                        </div>
                                        {{-- button portion --}}
                                        <div class="coach_card_footer scrollmenu">
                                            <a href="javascript:void(0)" class="anchor-button border-0 m-auto profile"
                                                data-counsellor-id="{{ $counsellor->id }}">View Profile</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <span class="position-absolute coachNextBtn d-none d-md-block">
                            <i class="bi bi-arrow-right-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                        </span>

                        {{-- single coach data starts here --}}
                        <div class="counsellors">
                            @forelse ($counsellors as $counsellor)
                                <div class="counsellor" style="display: {{ $loop->first ? 'block' : 'none' }};"
                                    data-profile-id="{{ $counsellor->id ?? 'N/A' }}">
                                    <div class="row g-2 p-md-4 px-md-2">
                                        <div class="m-auto single_coach_data p-4" style="width: 95%;">
                                            <div class="d-flex flex-column flex-md-row gap-3">
                                                <img src="{{ asset($counsellor->profile_photo ?? 'new_design_assets/img/random-coach.svg') }}"
                                                    alt="" height="160" width="160">
                                                <div class="d-flex flex-column justify-content-end align-items-start gap-2">
                                                    <div class="first_data">
                                                        <i class="bi bi-mortarboard-fill"></i>
                                                        <h3
                                                            class="m-0 p-0 special_monstrate_text text-dark fw-bold text-uppercase">
                                                            Name:
                                                        </h3>
                                                        <p class="m-0 p-0 special_monstrate_text text-dark">
                                                            {{ $counsellor->full_name ?? 'N/A' }}</p>
                                                    </div>
                                                    <div class="first_data">
                                                        <i class="bi bi-stars"></i>
                                                        <h3
                                                            class="m-0 p-0 special_monstrate_text text-dark fw-bold text-uppercase">
                                                            Specialized in:
                                                        </h3>
                                                        <p class="m-0 p-0 special_monstrate_text text-dark">
                                                            {{ $counsellor->counsellorProfile?->interested_in ?? 'N/A' }}
                                                        </p>
                                                    </div>
                                                    <div class="first_data">
                                                        <i class="bi bi-linkedin"></i>
                                                        <h3
                                                            class="m-0 p-0 special_monstrate_text text-dark fw-bold text-uppercase">
                                                            Linkedin:
                                                        </h3>
                                                        <p class="m-0 p-0 special_monstrate_text text-dark">
                                                            <a href="{{ $counsellor->counsellorProfile?->linked_in ?? '#' }}"
                                                                class="special_monstrate_text d-block d-md-inline"
                                                                target="_blank" style="color: #729CC4" target="_blank">View
                                                                Profile</a>
                                                        </p>
                                                    </div>
                                                    <div class="first_data">
                                                        <i class="bi bi-map"></i>
                                                        <h3
                                                            class="m-0 p-0 special_monstrate_text text-dark fw-bold text-uppercase">
                                                            Country:
                                                        </h3>
                                                        <p class="m-0 p-0 special_monstrate_text text-dark">
                                                            {{ $counsellor->counsellorProfile?->city ?? 'N/A' }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-3">
                                                <p class="special_monstrate_text text-dark"
                                                    style="font-weight: 500!important;text-align:justify">
                                                    @php
                                                        $maxWords = 100; // Set the maximum number of words you want to show
                                                        $careerCouch = $counsellor->counsellorProfile ? $counsellor->counsellorProfile->career_couch : null;
                                                        $wordCount = preg_match_all('/\S+/', $careerCouch, $matches);
                                                    @endphp

                                                    @if ($wordCount > $maxWords)
                                                        @php
                                                            $shortenedContent = implode(' ', array_slice($matches[0], 0, $maxWords));
                                                            $remainingContent = implode(' ', array_slice($matches[0], $maxWords));
                                                        @endphp

                                                        <span id="shorten-{{ $counsellor->id }}">
                                                            {{ $shortenedContent }}
                                                        </span>

                                                        <span id="more-{{ $counsellor->id }}" style="display: none;">
                                                            {{ $remainingContent }}
                                                        </span>

                                                        <a href="#" {{-- onclick="toggleReadMore('{{ $counsellor->id }}')" --}}
                                                            id="readBtn-{{ $counsellor->id }}" data-bs-toggle="modal"
                                                            data-bs-target="#counsellor{{ $counsellor->id }}DescriptionModal">
                                                            Read more
                                                        </a>
                                                    @else
                                                        {{ $careerCouch ?? 'N/A' }}
                                                    @endif
                                                </p>
                                                <button class="anchor-button border-0" type="button" data-bs-toggle="modal"
                                                    data-bs-target="#sessionModal{{ $counsellor->id }}">Book a
                                                    Session</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                No coaches found
                            @endforelse
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @foreach ($counsellors as $counsellor)
        <div class="modal fade" id="counsellor{{ $counsellor->id }}SessionsModal" tabindex="-1"
            aria-labelledby="counsellor{{ $counsellor->id }}SessionsModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="counsellor{{ $counsellor->id }}SessionsModalLabel">Available Sessions
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                @if (auth()->user()->timezone)
                                    <table class="table payments" style="vertical-align:middle">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Time</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($counsellor->counsellorSessions as $session)
                                                @if ($session->canBeBooked())
                                                    <tr>
                                                        <td class="text-center rounded-start">
                                                            {{ (new DateTime($session->session_timing, new DateTimeZone($session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y') ?? '' }}
                                                        </td>
                                                        <td class="text-center">
                                                            {{ (new DateTime($session->session_timing, new DateTimeZone($session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('h:i A') ?? '' }}
                                                        </td>
                                                        <td class="text-center rounded-end">
                                                            <a href="{{ route('user.payments.info.index', $session->id) }}?session=true"
                                                                class="btn btn-sm btn-primary">Schedule</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @empty
                                                <tr>
                                                    <td colspan="3" class="text-center">No sessions available for
                                                        booking.</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-warning" role="alert">
                                        <h4 class="alert-heading">Warning!</h4>
                                        <p class="mb-0">Please set your timezone first and then try again.</p>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <div>
                            <strong>Timezone: </strong> {{ auth()->user()->timezone ?? 'N/A' }}
                            <a href="#" title="Change Timezone" class="text-decoration-none fs-6"
                                data-bs-toggle="modal" data-bs-target="#changeTimezoneModal">
                                <small>
                                    <i class="fa-regular fa-pen-to-square"></i>
                                </small>
                            </a>
                        </div>
                        <button type="button" class="btn btn-sm fontChanger btn-secondary closeChanger"
                            data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- Coach Description Modal --}}
        <div class="modal fade" id="counsellor{{ $counsellor->id }}DescriptionModal" tabindex="-1"
            aria-labelledby="counsellor{{ $counsellor->id }}DescriptionModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="counsellor{{ $counsellor->id }}DescriptionModalLabel">About Coach
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <p style="text-align: justify">
                                    {{ $counsellor->counsellorProfile ? $counsellor->counsellorProfile->career_couch : 'N/A' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary closeChanger"
                            data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- Counsellor session booking modal --}}
        <div class="modal fade" tabindex="-1" id="sessionModal{{ $counsellor->id }}">
            <div class="modal-dialog modal-xl">
                <div class="modal-content coach_session_info position-relative">
                    <button type="button" class="btn-close position-absolute" data-bs-dismiss="modal"
                        aria-label="Close" style="top: 0;right:0;"></button>
                    <div class="row">
                        <div class="col-12 col-lg-4 first-part px-5 py-5">
                            <div
                                class="d-flex flex-column d-md-block  align-items-center justify-content-center ps-md-3 pt-md-4">
                                <img src="{{ asset($counsellor->profile_photo ?? 'new_design_assets/img/random-coach.svg') }}"
                                    class="rounded-circle" alt="" width="100" height="100">
                                <h3 class="m-0 p-0 special_monstrate_text text-dark fs-5 heading mt-2">
                                    {{ $counsellor->full_name }}</h3>
                            </div>
                            <hr class="line">
                            <div>
                                <p class="special_monstrate_text text-dark" style="text-align: justify">
                                    {{ $counsellor->counsellorProfile ? $counsellor->counsellorProfile->career_couch : 'N/A' }}
                                </p>
                            </div>
                            <hr class="line">
                            <div>
                                <div class="session-icon">
                                    <i class="bi bi-clock session-icon__icon"></i>
                                    <p class="special_monstrate_text text-dark session-icon__time">30 Min</p>
                                </div>
                                <div class="session-icon">
                                    <i class="bi bi-camera-video session-icon__icon"></i>
                                    <p class="special_monstrate_text text-dark session-icon__time">Meeting details will
                                        be provided upon confirmation.</p>
                                </div>
                            </div>
                            <hr class="line">
                            <div>
                                <p class="special_monstrate_text text-dark" style="font-weight: 600">Connect with me
                                    on</p>
                                <div class="session-icon">
                                    <i class="bi bi-linkedin session-icon__icon--small"></i>
                                    <i class="bi bi-twitter session-icon__icon--small"></i>
                                    <i class="bi bi-facebook session-icon__icon--small"></i>
                                    <i class="bi bi-instagram session-icon__icon--small"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 second-part py-5">
                            <div class="d-flex flex-column  align-items-center justify-content-start">
                                <div class="mt-md-5">
                                    <h3 class="m-0 p-0 special_monstrate_text text-dark fs-5 heading mt-2">SELECT A
                                        DATE &
                                        TIME</h3>
                                </div>
                                <div id="color-calendar{{ $counsellor->id }}" data-counselor-id="{{ $counsellor->id }}">
                                </div>
                            </div>
                            <div class="ps-5 ">
                                <form id="timezoneForm{{ $counsellor->id }}" method="post"
                                    action="{{ route('user.timezone.update', auth()->id()) }}">
                                    @csrf
                                    <p class="special_monstrate_text text-dark" style="font-weight: 600">Time Zone</p>
                                    <div class="position-relative">
                                        @php
                                            $timezones = App\Models\Timezone::all();
                                        @endphp

                                        <select class="session-location select2bs4" name="timezone"
                                            id="timezone{{ $counsellor->id }}" required>
                                            <option value="">Select timezone</option>
                                            @foreach ($timezones as $timezone)
                                                <option value="{{ $timezone->timezone }}"
                                                    @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                                    {{ $timezone->timezone }}</option>
                                            @endforeach
                                        </select>
                                        <i class="bi bi-globe position-absolute"
                                            style="top:10px;left:10px;color:white"></i>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div
                            class="col-12 col-lg-4 d-flex flex-column d-md-block  align-items-center justify-content-center ps-md-5">
                            <div class="selected-date" style="margin-top: 165px">
                                <p id="selectedDate{{ $counsellor->id }}"
                                    class="special_monstrate_text text-dark text-uppercase m-0" style="font-weight: 500">
                                    mon, 28 dec</p>
                            </div>
                            <hr class="line" style="margin: 3px 0px!important;width:80%!important;">
                            <div class="position-relative" style="width: 80%; height: auto; min-height: 50%">
                                <div class="my-4" style="width: 100%;" id="availableTime{{ $counsellor->id }}">
                                    <!--  ------  -->
                                </div>
                                <!-- Loader element -->
                                <div class="loader loader2" style="display: none; height: 100%;">
                                    <!-- You can add a loading spinner or any other visual indicator here -->
                                    <div class="spinner"></div>
                                    <p>Loading...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Loader element -->
                <div class="loader loader3" style="display: none; height: 100%;">
                    <!-- You can add a loading spinner or any other visual indicator here -->
                    <div class="spinner"></div>
                    <p>Loading...</p>
                </div>
            </div>
        </div>
    @endforeach
    {{-- modal for the payment starts here --}}
    <div class="modal fade" tabindex="-1" id="paymentModal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content payment-info position-relative p-3 p-md-5">
                <button type="button" class="btn-close position-absolute" data-bs-dismiss="modal" aria-label="Close"
                    style="top: 0;right:0;"></button>
                <h3 class="m-0 p-0 special_monstrate_text text-dark fs-4 heading text-center" style="font-weight: 700">
                    PAYMENT METHOD</h3>
                <p class="special_monstrate_text text-dark text-center mt-1" style="font-weight: 600;font-size:16px">
                    Choose a
                    payment option and fill in the requested information</p>
                <div class="row payment-info-main mt-4">
                    <div class="col-12 col-lg-8 payment-info__left p-2 pt-4 px-3 p-md-4">
                        <div class="d-flex align-items-center justify-content-between">
                            <h3 class="special_monstrate_text text-dark fs-6 m-0" style="font-weight: 600">
                                Payment
                                Options</h3>
                            <div class="d-flex align-items-center gap-2">
                                <svg width="18" height="18" style="margin-top: -4px" viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill="currentColor"
                                        d="M17 9V7c0-2.8-2.2-5-5-5S7 4.2 7 7v2c-1.7 0-3 1.3-3 3v7c0 1.7 1.3 3 3 3h10c1.7 0 3-1.3 3-3v-7c0-1.7-1.3-3-3-3M9 7c0-1.7 1.3-3 3-3s3 1.3 3 3v2H9z" />
                                </svg>
                                <p class="m-0 special_monstrate_text text-dark fs-6" style="font-weight: 600">
                                    Secure Server</p>
                            </div>
                        </div>
                        <div class="p-3 payment-info__left--child payment-info_left--first">
                            <div class="d-flex align-items-center flex-wrap justify-content-between"
                                style="padding-bottom:15px">
                                <div class="">
                                    <div class="d-flex align-items-center gap-2">
                                        <input type="radio" name="creditType" value="credit-card" id="credit-card">
                                        <label class="m-0 special_monstrate_text" style="color: #729CC4;font-weight:600"
                                            for="credit-card">Credit/Debit
                                            Card</label>
                                    </div>
                                    <p class="m-0 special_monstrate_text text-dark ps-4"
                                        style="font-weight:600;font-size:14px">Secure transfer using your bank
                                        account</p>
                                </div>
                                <div>
                                    <img src="{{ asset('new_design_assets/img/bank.png') }}" alt=""
                                        width="300px" height="50px" class="" style="object-fit: cover;">
                                </div>
                            </div>
                            <div id="credit-list1" style="border-top: 2px dashed rgba(128, 128, 128, 0.527);">
                                <form role="form" action="{{ route('user.stripe.payment') }}" method="post"
                                    enctype="multipart/form-data" class="validation needs-validation py-4 row" novalidate
                                    id="stripeForm" data-cc-on-file="false"
                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}">
                                    @csrf
                                    <div class='form-row row'>
                                        <div class='col-md-12 error form-group d-none'>
                                            <div class='alert-danger alert'>
                                                {{-- error message --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="hidden" id="stripeName" name="stripe_name">
                                        <input type="hidden" id="total_amount" name="total_amount">
                                        <input type="hidden" name="user_counsellor_session_id"
                                            value="{{ encrypt('1') }}">
                                        <input type="hidden" id="couponHidden" name="coupon_code">
                                        <input type="hidden" id="coupon_code_applied" name="coupon_code_applied"
                                            value="no">


                                        <div class="col-md-6">
                                            <legend class="legend">First Name</legend>
                                            <input type="text" class="input required" name="first_name"
                                                id="first_name" required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-6">
                                            <legend class="legend">Last Name</legend>
                                            <input type="text" class="input required" name="last_name" id="last_name"
                                                required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-6">
                                            <legend class="legend">Card Number</legend>
                                            <input type="number" max="9999999999999999" min="10000000000000"
                                                id="card-number" name="card_number" class="input required" required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-4">
                                            <legend class="legend">Expiry Date</legend>
                                            <input type="month" class="input required" name="expiryDate"
                                                id="expiryDate" required>
                                            <small class="text-danger"></small>
                                            <input type="hidden" id="expiryMonth" name="expiry_month">
                                            <input type="hidden" id="expiryYear" name="expiry_year">
                                        </div>
                                        <div class="col-md-2">
                                            <legend class="legend">CVC</legend>
                                            <input type="number" max="999" min="001" id="card-ccv"
                                                name="ccv" class="input required" required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-6">
                                            <legend class="legend">Postal Code</legend>
                                            <input type="number" class="input" name="postal_code" id="">
                                        </div>
                                        <div class="col-md-6">
                                            <legend class="legend">Email</legend>
                                            <input type="email" class="input" name="email" id="">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        {{-- <div class="p-3 payment-info__left--child payment-info_left--second">
                                    <div class="d-flex align-items-center flex-wrap justify-content-between"
                                        style="padding-bottom:15px">
                                        <div class="">
                                            <div class="d-flex align-items-center gap-2">
                                                <input type="radio" name="creditType" class="credit">
                                                <p class="m-0 special_monstrate_text"
                                                    style="color: #729CC4;font-weight:600">Paypal</p>
                                            </div>
                                            <p class="m-0 special_monstrate_text text-dark ps-4"
                                                style="font-weight:600;font-size:14px">Secure online payment through
                                                the
                                                PayPal portal</p>
                                        </div>
                                        <div>
                                            <img src="{{ asset('new_design_assets/img/paypal.png') }}" alt=""
                                                width="150px" height="50px" class=""
                                                style="object-fit: contain;">
                                        </div>
                                    </div>
                                </div> --}}
                        <div class="p-3 payment-info__left--child payment-info_left--third">
                            <div class="d-flex align-items-center flex-wrap justify-content-between gap-4 gap-md-0"
                                style="padding-bottom:15px">
                                <div class="">
                                    <div class="d-flex align-items-center gap-2">
                                        <input type="radio" name="creditType" value="jazzcash" id="jazzcash">
                                        <label class="m-0 special_monstrate_text" style="color: #729CC4;font-weight:600"
                                            for="jazzcash">Payment
                                            through JAZZCASH</label>
                                    </div>
                                    <p class="m-0 special_monstrate_text text-dark ps-4"
                                        style="font-weight:600;font-size:14px">Secure transfer using JazzCash
                                        account</p>
                                </div>
                                <div>
                                    <img src="{{ asset('new_design_assets/img/jazzcash.png') }}" alt=""
                                        width="150px" height="50px" class=""
                                        style="object-fit: contain;transform: scale(1.3);">
                                </div>
                            </div>
                            <div id="credit-list2" style="border-top: 2px dashed rgba(128, 128, 128, 0.527);">
                                <div class="my-3"
                                    style="border-bottom: 2px solid rgba(128, 128, 128, 0.527);padding-bottom:15px">
                                    <p class="m-0 special_monstrate_text text-dark"
                                        style="font-weight: 600;font-size:14px">Account title: Baber Jameel</p>
                                    <p class="m-0 special_monstrate_text text-dark"
                                        style="font-weight: 600;font-size:14px">Account number: +92 3250751341
                                    </p>
                                </div>
                                <form action="{{ route('user.payments.store') }}" method="post"
                                    enctype="multipart/form-data" class="needs-validation jazzcash" novalidate
                                    id="jazzCashForm">
                                    @csrf
                                    <div class="row">
                                        <input type="hidden" name="bank" value="Jazz Cash">
                                        <input type="hidden" name="user_counsellor_session_id"
                                            value="{{ encrypt('1') }}">

                                        <div class="col-md-6">
                                            <legend class="legend">Transaction ID</legend>
                                            <input type="text" class="input required" name="transaction_id" required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-6">
                                            <legend class="legend">Amount</legend>
                                            <input type="number" step="0.01" class="input required" name="amount"
                                                required>
                                            <small class="text-danger"></small>
                                        </div>
                                        <div class="col-md-12">
                                            <legend class="legend">Receipt</legend>
                                            <input type="file" class="input form-control required" name="receipt"
                                                accept=".png,.jpeg,.jpg" required>
                                            <small class="text-danger"></small>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-end gap-2">
                            <button type="button" class="button bg-dark text-white" style="width: 160px" type="button"
                                data-bs-dismiss="modal" aria-label="Close">CLOSE</button>
                            <button type="button" class="button text-white" style="width: 160px"
                                onclick="submitForm()">SUBMIT</button>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 payment-info__right p-2 pt-4 px-3 p-md-4">
                        <h3 class="special_monstrate_text text-center fs-6 m-0" style="font-weight: 600">
                            Payment
                            Settings</h3>
                        <div class="mt-5"
                            style="border-bottom: 2px solid rgba(160, 156, 156, 0.993);padding-bottom:15px">
                            <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                Pay for coach</h3>
                            <p class="special_monstrate_text text-white m-0" style="font-weight: 400;font-size:15px">
                                See the details about coach fee and basic editing.</p>
                        </div>
                        <div class="mt-3"
                            style="border-bottom: 2px dashed rgba(160, 156, 156, 0.993);padding-bottom:15px">
                            <div class="d-flex align-items-center justify-content-between">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Transaction Date:</h3>
                                <p class="special_monstrate_text text-white m-0" style="font-weight: 400;font-size:15px">
                                    {{ Carbon\Carbon::now()->format('d M, Y') }}</p>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Time:</h3>
                                <p class="special_monstrate_text text-white m-0" style="font-weight: 400;font-size:15px">
                                    {{ Carbon\Carbon::now()->format('h:i A') }}</p>
                            </div>
                        </div>
                        <div class="mt-3"
                            style="border-bottom: 2px solid rgba(160, 156, 156, 0.993);padding-bottom:15px">
                            <div class="d-flex align-items-center justify-content-between">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Sub Total:</h3>
                                <p id="subTotal" class="special_monstrate_text text-white m-0"
                                    style="font-weight: 400;font-size:15px">
                                    $40.00 USD</p>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Discount:</h3>
                                <p id="discount" class="special_monstrate_text text-white m-0"
                                    style="font-weight: 400;font-size:15px">0</p>
                            </div>
                        </div>
                        <div class="mt-3">
                            <div class="d-flex align-items-center justify-content-between ">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Coupon:</h3>
                                <input type="text" class="coupan" id="coupon_code" name="coupon"
                                    style="width: 50%">
                            </div>
                            <div id="coupon_code_msg" class="text-white text-end mt-1"></div>
                            <div class="d-flex align-items-center justify-content-between mt-2">
                                <h3 class="special_monstrate_text my-2" style="font-weight: 500;font-size:16px">
                                    Total:</h3>
                                <h3 id="total" class="special_monstrate_text my-2"
                                    style="font-weight: 500;font-size:16px">0</h3>
                            </div>
                        </div>
                        <button type="button" id="applyCouponCode"
                            class="button text-white d-block m-auto my-4 text-uppercase" style="width: 85%">Apply
                            Coupon</button>
                    </div>
                </div>

                <!-- Loader element -->
                <div class="loader" style="display: none;">
                    <!-- You can add a loading spinner or any other visual indicator here -->
                    <div class="spinner"></div>
                    <p>Please wait while your transaction is processing...</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('style/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/color-calendar/dist/bundle.js"></script>
    <script>
        // $(function() {
        //     //Initialize Select2 Elements
        //     $('.select2bs4').select2({
        //         theme: 'bootstrap4',
        //         // dropdownParent: $("#sessionModal{{ $counsellor->id }}")
        //     });
        // });

        $('.scrollmenu .profile').on('click', (e) => {
            // hide current profile
            const currentCounsellor = $(`[data-profile-id="${e.currentTarget.dataset.counsellorId}"]`);
            $(".counsellors .counsellor").hide();
            currentCounsellor.show();
        });

        function toggleReadMore(counsellorId) {
            const readBtn = document.getElementById(`readBtn-${counsellorId}`);
            const firstContent = document.getElementById(`shorten-${counsellorId}`).textContent.trim();
            const moreContent = document.getElementById(`more-${counsellorId}`);
            if (!moreContent || !readBtn || !firstContent) {
                console.log("Elements not found!");
                return;
            }

            // Remove ellipsis (three dots) from the first content if present
            if (firstContent.slice(-3) === '...') {
                document.getElementById(`shorten-${counsellorId}`).textContent = firstContent.slice(0, -3).trim();
            }

            moreContent.textContent = moreContent.textContent.trim();

            if (moreContent.style.display === 'none') {
                moreContent.style.display = 'inline';
                readBtn.textContent = 'Read less';
            } else {
                moreContent.style.display = 'none';
                readBtn.textContent = 'Read more';
            }
        }
        // owl -carousel settings starts here
        $(document).ready(function() {

            // ************couch cards carousel settings are here**************
            var couchesCarousel = $("#coachesCarousel").owlCarousel({
                // loop: true,
                margin: 10,
                nav: false,
                smartSpeed: 1000,
                responsive: {
                    0: {
                        items: 1,
                        center: true
                    },
                    550: {
                        items: 2
                    },
                    700: {
                        items: 3
                    },
                    1200: {
                        items: 4
                    },
                    1600: {
                        items: 5
                    }
                }
            });

            $('.coachNextBtn').click(function() {
                couchesCarousel.trigger('next.owl.carousel');
            });

            // Go to the previous item
            $('.coachPrevBtn').click(function() {
                // With optional speed parameter
                // Parameters have to be in square brackets '[]'
                couchesCarousel.trigger('prev.owl.carousel');
            });
        });
    </script>

    <script>
        // restrict numeric input fields length
        document.addEventListener('DOMContentLoaded', () => {
            document.querySelectorAll('input[type="number"]')
                .forEach(input => {
                    input.addEventListener('keyup', () => {
                        input.value = input.value.slice(0, input.dataset.maxlength);
                    });
                });
        });

        // disabling form submissions if there are invalid fields
        (function() {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }
                        form.classList.add('was-validated')
                    }, false)
                })
        })()


        $('#expiryDate').on('change', function() {
            const selectedDate = new Date(this.value);
            $('#expiryMonth').val(selectedDate.getMonth() + 1); // Month is zero-based
            $('#expiryYear').val(selectedDate.getFullYear());
        });

        const days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
        const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        // Define your options object
        const options = {
            // Other options...
            dateChanged: (currentDate, filteredDateEvents, counselorId) => {
                // Handle the date change event
                // console.log('Selected Date:', currentDate);
                // console.log('Counselor ID:', counselorId);
                $('#selectedDate' + counselorId).text(days[currentDate.getDay()] + ", " + currentDate.getDate() +
                    ' ' + months[currentDate.getMonth()]);

                var utcDate = new Date(Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate
                    .getDate(), 0, 0, 0));
                var formatedDate = utcDate.toISOString();
                // Fetch available sessions for the selected date
                fetchSessions(formatedDate, counselorId);
            }
        };

        function fetchSessions(selectedDate, counselorId) {
            let $spinner = $('.loader2');
            // Show the loader
            $spinner.show();
            // Make an AJAX request
            $.ajax({
                url: '/user/counsellor/available-sessions',
                method: 'POST',
                data: {
                    selectedDate: selectedDate,
                    counselorId: counselorId,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    // Handle the response
                    // console.log(response);
                    if (response.success) {
                        // Append the available sessions to the corresponding div
                        $('#availableTime' + response.counselorId).text('');
                        sessions = response.sessions;
                        $spinner.hide();
                        if (sessions.length > 0) {
                            sessions.forEach(session => {
                                $('#availableTime' + response.counselorId).append(`
                                        <div class="w-100 d-flex align-items-center gap-2">
                                            <div class="available-time my-2 special_monstrate_text text-dark text-uppercase" style="font-weight:500; letter-spacing: 1px">${session.time}
                                            </div>
                                            <button class="confirm special_monstrate_text text-uppercase"
                                                data-session-id="${session.session.id}" data-counselor-id="${response.counselorId}" onclick="confirmSession(${session.session.id}, ${response.counselorId})">Confirm</button>
                                        </div>
                                `);
                            });
                        } else {
                            $('#availableTime' + response.counselorId).text('No sessions available');
                        }
                        $('#total_amount').val(response.amount);
                        $('#subTotal').text(response.amount + ' ' + response.currency);
                        $('#discount').text('0.00 ' + response.currency);
                        $('#total').text(response.amount + ' ' + response.currency);
                    }
                },
                error: function(xhr, status, error) {
                    // Handle the error
                    console.log(error);
                }
            });
        }

        function confirmSession(session_id, counselor_id) {
            $('#confirmSession').attr('data-session-id', session_id);
            $('#confirmSession').attr('data-counselor-id', counselor_id);
            const inputs = `<input type="hidden" name="session" value="${session_id}">
                                        <input type="hidden" name="counsellor_id"
                                            value="${counselor_id}">`;


            $('#stripeForm').append(inputs);
            $('#jazzCashForm').append(inputs);

            // close the sessionModal
            $('#sessionModal' + counselor_id).modal('hide');
            $('#paymentModal').modal('show');
        }

        $('#applyCouponCode').click(function() {
            $('#coupon_code_applied').val('no');
            var couponCode = $('#coupon_code').val();
            $('#couponHidden').val(couponCode);
            var amount = $('#total_amount').val();
            if (couponCode == '') {
                $('#coupon_code_msg').html('Please enter a coupon code.');
            } else {
                $.ajax({
                    type: 'post',
                    url: '{{ route('user.stripe.apply_coupon') }}',
                    data: {
                        coupon_code: couponCode,
                        amount: amount,
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(result) {
                        console.log(result);
                        if (result.status == 'success') {
                            $('#coupon_code_msg').html(result.msg);
                            $('#discount').text(result.discount + ' ' + result.currency);
                            $('#total_amount').val(result.totalPrice);
                            $('#total').text(result.totalPrice + ' ' + result.currency);
                            $('#coupon_code_applied').val('yes');

                            $('#applyCouponCode').text('Coupon Applied');
                            $('#applyCouponCode').prop('disabled', true);
                            $('#applyCouponCode').removeClass(
                                    'form-control bg-primary btn btn-sm btn-primary bookChanger py-2')
                                .addClass(
                                    'form-control bg-success btn btn-sm btn-primary bookChanger py-2');
                            // $('#coupon_code').attr('disabled', true);
                        } else {
                            $('#coupon_code_msg').html(result.msg);
                            $('#coupon_code').val('');
                            $('#coupon_code_applied').val('no');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }
        });

        // Define your minimum selectable date
        const myMinDate = new Date("{{ now()->addDays(2)->format('Y-m-d') }}"); // Add 2 days

        @foreach ($counsellors as $counsellor)
            // Immediately Invoke this function for each counselor
            (function(counsellorId) { // Pass the current counsellor ID as an argument
                const calendarOptions = {
                    id: "#color-calendar" + counsellorId,
                    calendarSize: "small",
                    ...options,
                    dateChanged: function(currentDate, filteredDateEvents) {
                        const current = new Date(currentDate);
                        if (current < myMinDate) {
                            console.log("Selected date is before the minimum allowed date.");
                            // Since `this` refers to the calendar instance inside the dateChanged callback, we can use it to call setDate
                            this.setDate(myMinDate.toISOString());
                        } else {
                            if (typeof options.dateChanged === 'function') {
                                options.dateChanged(currentDate, filteredDateEvents, counsellorId);
                            }
                        }
                    }
                };

                // Create and initialize the calendar with the specified options
                let myCalendar = new Calendar(calendarOptions);

                $('#timezone' + counsellorId).on('change', function() {
                    let loader3 = $('.loader3');
                    loader3.show();
                    $('#timezoneForm' + counsellorId).submit();
                });
            })('{{ $counsellor->id }}'); // Immediately invoke the function with the current counsellor ID
        @endforeach


        // Attach event listener to a parent element that exists in the DOM
        document.addEventListener('click', function(event) {
            // Check if the clicked element is an available-time element
            if (event.target.classList.contains('available-time')) {
                // Toggle the 'active' class on the clicked element
                event.target.classList.toggle('active');

                // Toggle the 'active' class on the corresponding confirm button
                const index = Array.from(event.target.parentNode.children).indexOf(event.target);
                const confirms = event.target.parentNode.querySelectorAll('.confirm');
                confirms[index].classList.toggle('active');
            }
        });


        var formToSubmit = '';
        // jquery for the credit card
        $('input[name="creditType"]').on('change', function() {
            if ($(this).val() === 'credit-card') {
                $('#credit-list1').show();
                $('#credit-list2').hide();
                formToSubmit = 'stripeForm';
            } else if ($(this).val() === 'jazzcash') {
                $('#credit-list2').show();
                $('#credit-list1').hide();
                formToSubmit = 'jazzCashForm';
            } else {
                $('#credit-list1').hide();
                $('#credit-list2').hide();
            }
        });

        function submitForm() {
            if (formToSubmit == '') {
                return;
            } else if (formToSubmit == 'stripeForm') {
                $('#stripeName').val($('#first_name').val() + ' ' + $('#last_name').val());
                $('#stripeForm').submit();
            } else if (formToSubmit == 'jazzCashForm') {
                $('#jazzCashForm').submit();
            }
        }

        $('form.jazzcash').submit(function(event) {
            var form = $(this); // Get the current form
            var isValid = true;

            // Validate required fields
            form.find('.required').each(function() {
                if ($.trim($(this).val()) === '') {
                    isValid = false;
                    $(this).next('small').text('This field is required.');
                } else {
                    $(this).next('small').text('');
                }
            });

            if (!isValid) {
                event.preventDefault(); // Prevent form submission if there are validation errors
                return;
            } else {
                form.get(0).submit();
            }
            event.preventDefault();
        });
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // Define the form submit event handler
            $('form.validation').submit(function(event) {
                var $form = $(this); // Get the current form;

                var isValid = true;

                // Validate required fields
                $form.find('.required:input').each(function() {
                    console.log($(this).attr('name'));
                    if ($.trim($(this).val()) === '') {
                        isValid = false;
                        console.log('error');
                        $(this).next('small').text('This field is required.');
                    } else {
                        $(this).next('small').text('');
                        isValid = true;
                    }
                });

                if (!isValid) {
                    event.preventDefault();
                    return; // Stop processing if there are validation errors
                }

                // Get the Stripe publishable key from the data attribute
                var publishableKey = $form.data('stripe-publishable-key');

                if (!publishableKey) {
                    alert('Stripe publishable key is missing.');
                    event.preventDefault();
                    return;
                }

                var $loader = $('.loader');
                // Show the loader
                $loader.show();
                // Initialize Stripe with the publishable key
                Stripe.setPublishableKey(publishableKey);
                // Create a token for the credit card information
                Stripe.createToken({
                    number: $('#card-number').val(),
                    cvc: $('#card-ccv').val(),
                    exp_month: $('#expiryMonth').val(),
                    exp_year: $('#expiryYear').val()
                }, function(status, response) {
                    if (response.error) {

                        $loader.hide();
                        $('.error').removeClass('d-none').find('.alert').text(response.error
                            .message);
                    } else {

                        var token = response.id;
                        var $form = $('form.validation'); // Get the form again
                        $form.find('input[type=number]').val(''); // Clear sensitive data
                        $form.append("<input type='hidden' name='stripeToken' value='" + token +
                            "'/>");
                        $form.get(0).submit();
                        // Hide the loader
                        // $loader.hide();
                    }
                });


                event.preventDefault();
            });

            // Handle the response from Stripe
            function stripeHandleResponse(status, response) {
                if (response.error) {
                    $('.error').removeClass('hide').find('.alert').text(response.error.message);
                } else {
                    var token = response.id;
                    var $form = $('form.validation'); // Get the form again
                    $form.find('input[type=text]').val(''); // Clear sensitive data
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }
        });

        $('form#stripeForm').submit(function() {
            // $("#save_changes").prop('disabled', true);
        });
    </script>
@endpush

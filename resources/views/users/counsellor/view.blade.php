<div class="row g-3">
    <input type="hidden" name="profile_id" id="profile_id" value="{{$profile->id}}">
    <div class="col-lg-3 text-center text-lg-start">
        <img src="{{asset($profile->profile_photo)}}" class="img-fluid">
    </div>
    <div class="col-lg-9">
        <div class="mb-2">
            <span class="fw-bold">
                <img src="{{ asset('assets/icons/graduate.svg') }}" role="image" alt="name">
                Name:
            </span>
            {{$profile->full_name ?? 'N/A'}}
        </div>
        <div class="mb-2">
            <span class="fw-bold">
                <img src="{{ asset('assets/icons/stars.svg') }}" role="image" alt="Interested in">
                Specialized in:
            </span>
            {{ $profile->counsellorProfile?->interested_in ?? 'N/A' }}
        </div>
        {{-- <div class="mb-2">
            <span class="fw-bold">
                <i class="fas fa-graduation-cap"></i>
                Profession:
            </span>
            {{ $profile->counsellorProfile?->certified }}
        </div> --}}
        <div class="mb-2">
            <span class="fw-bold"><i class="fa-brands fa-linkedin"></i> Linkedin:</span> <a href="{{ $profile->counsellorProfile?->linked_in ?? '#' }}" class="text-decoration-none d-inline-block" target="_blank">View Profile</a>
        </div>
        <div class="mb-2">
            <span class="fw-bold"><i class="fa-solid fa-city"></i> Country:</span> {{ $profile->counsellorProfile?->city ?? 'N/A' }}
        </div>
    </div>
    <div class="col-12" style="text-algin: justify;">
        {!! $profile->counsellorProfile?->career_couch ?? 'N/A' !!}
    </div>
</div>
<button type="button" class="btn text-white bg-special btn-sm mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
    Book A Session
</button>

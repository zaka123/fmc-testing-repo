@extends('layouts.user', ['pagename' => 'Session Payment Info'])

@section('css')
    <style>
        @media (min-width:992px) {
            .nav.nav-tabs button img {
                width: 2.5rem;
                height: 2.5rem;
            }

            .border-lg-end {
                border-right: var(--bs-border-width) var(--bs-border-style) var(--bs-border-color) !important;
            }
        }

        @media (max-width:576px) {
            .nav.nav-tabs button img {
                width: 2rem;
                height: 2rem;
            }
        }

        [type="number"]::-webkit-inner-spin-button {
            display: none;
        }

        .bookChanger:hover {
            background: black !important;
        }
    </style>
@endsection

@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10">
                <div class="row g-0">
                    <div class="col-12 bg-white sticky-top">
                        <div class="d-flex px-3 pb-lg-3">
                            <button class="btn mt-3 btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar">
                                <i class="fa-solid fa-bars fs-4"></i>
                            </button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12 main" style="min-height: 83.3vh;">
                        <div class="floating"></div>
                        <div class="row">
                            <div class="col-12 col-lg-10 offset-lg-1">
                                <div class="card border-0 pt-3 bg-transparent">
                                    <div class="card-body">
                                        <div class="row inner">
                                            <div class="col-12">
                                                <div class="p-4">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h3>One to One Session</h3>
                                                        </div>
                                                        <div class="col-12 col-lg-6 pt-lg-4">
                                                            <div class="text-capitalize mb-2">from</div>
                                                            <h1 class="text-special mb-3">PKR {{ $amount }}/-</h1>
                                                            <h4>Duration 45-60 minutes</h4>
                                                            <p class="mb-5" style="text-align: justify">The coaching
                                                                session enables you to identify strengths, interests, and
                                                                values and devise a personalized career plan based on your
                                                                skills, environment, and aptitude. The career coach helps
                                                                you set short-term and long-term goals and sets specific
                                                                measurable milestones to help you reach those goals. At the
                                                                end of each session, your coach will ask you to commit to
                                                                some of the action points. This is so that you can track
                                                                your progress toward your goals.</p>
                                                            {{-- @if (auth()->user()->hasZoomAccount()) --}}
                                                            <button data-bs-toggle="modal" data-bs-target="#exampleModal"
                                                                class="btn btn-sm bg-special px-3 text-white bookChanger">
                                                                Book Now
                                                            </button>
                                                            {{-- @else
                                                        <form action="{{ route("user.zoom.account.connect") }}" method="post" onsubmit="this.querySelector('button').setAttribute('disabled', true);" class="d-inline">
                                                            @csrf
                                                            <button class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-title="Connect zoom account to attend sessions.">
                                                                Connect Zoom
                                                            </button>
                                                        </form>
@endif --}}
                                                        </div>
                                                        <div class="col-12 col-lg-6">
                                                            <img src="{{ asset('assets/img/Remote team-bro.png') }}"
                                                                class="img-fluid" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- counsellor profile modal -->
    <div class="modal fade" id="counsellorProfile" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
@section('modal')
    <div class="modal-body">
        <div class="row">
            <div class="col-12 p-3">
                <nav class="">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="text-dark nav-link active" id="nav-info-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-info" type="button" role="tab" aria-controls="nav-info"
                            aria-selected="true" onclick="$('#save_changes').hide();">
                            Payment Details
                        </button>
                        <button class="text-dark nav-link" id="nav-easypaisa-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-easypaisa" type="button" role="tab" aria-controls="nav-easypaisa"
                            aria-selected="false" onclick="change('easyPaisaForm')">
                            <img src="{{ asset('assets/img/easypaisa.png') }}" alt="easypaisa logo" class="rounded"><br>
                            <span class="d-none d-lg-inline">Easypaisa</span>
                        </button>
                        <button class="text-dark nav-link" id="nav-jazzcash-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-jazzcash" type="button" role="tab" aria-controls="nav-jazzcash"
                            aria-selected="false" onclick="change('jazzCashForm');">
                            <img src="{{ asset('assets/img/jazcash.png') }}" alt="jazzcash logo" class="rounded">
                            <br>
                            <span class="d-none d-lg-inline">Jazzcash</span>
                        </button>
                        <button class="text-dark nav-link" id="nav-bank-tab" data-bs-toggle="tab" data-bs-target="#nav-bank"
                            type="button" role="tab" aria-controls="nav-bank" aria-selected="false"
                            onclick="change('bankForm');">
                            <img src="{{ asset('assets/img/bank.jpg') }}" alt="bank logo" class="rounded">
                            <br>
                            <span class="d-none d-lg-inline">Bank</span>
                        </button>
                        <button class="text-dark nav-link" id="nav-stripe-tab" onclick="change('stripeForm');"
                            data-bs-toggle="tab" data-bs-target="#nav-stripe" type="button" role="tab"
                            aria-controls="nav-stripe" aria-selected="false">
                            <img src="{{ asset('assets/img/debit-credit-card.png') }}" alt="bank logo"
                                class="rounded img-fluid"><br>
                            {{-- <i class="fa fa-credit-card fs-2" aria-hidden="true"></i><br> --}}
                            <span class="d-none d-lg-inline">Credit/Debit Card</span>
                        </button>
                    </div>
                </nav>
                <div class="tab-content mb-3 p-3 bg-white border-start border-bottom border-end rounded-bottom"
                    id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-info" role="tabpanel"
                        aria-labelledby="nav-info-tab">
                        <div class="row">
                            <div class="col-md-6 border-lg-end">
                                <h6>Easypaisa</h6>
                                <div class="text-muted">Account title: Rehan Tariq</div>
                                <div class="text-muted">Account number: +92 300 0000000</div>
                                <hr>
                                <h6>Jazzcash</h6>
                                <div class="text-muted">Account title: Rehan Tariq</div>
                                <div class="text-muted">Account number: +92 300 0000000</div>
                            </div>
                            <div class="col-md-6">
                                <h6>Bank Details</h6>
                                <div class="text-muted">Bank Name : Faisal bank</div>
                                <div class="text-muted">Account Title : The Educatics</div>
                                <div class="text-muted">Account Number : PK21FAYS3423301000000289</div>
                            </div>
                        </div>
                    </div>
                    <!-- easy paisa tab -->
                    <div class="tab-pane fade" id="nav-easypaisa" role="tabpanel" aria-labelledby="nav-easypaisa-tab">
                        <form action="{{ route('user.payments.store') }}" method="post" enctype="multipart/form-data"
                            class="needs-validation" id="easyPaisaForm" novalidate>
                            @csrf
                            <input type="hidden" name="bank" value="Easy Paisa">
                            <input type="hidden" name="session" value="{{ old('session') ?? $session->id }}">
                            <input type="hidden" name="counsellor_id"
                                value="{{ old('counsellor_id') ?? $session->counsellor_id }}">
                            <input type="hidden" name="user_counsellor_session_id" value="{{ encrypt('1') }}">
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Transaction ID</label>
                                        <input type="number" min="1" value="{{ old('transaction_id') }}"
                                            class="@error('transaction_id') is-invalid @enderror form-control shadow-other"
                                            name="transaction_id" required placeholder="Enter transaction id here">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Amount</label>
                                        <input type="number" min="1" value="{{ old('amount') }}"
                                            class="@error('amount') is-invalid @enderror form-control shadow-other"
                                            name="amount" required placeholder="Enter amount here">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="role-name">Receipt</label>
                                        <input type="file"
                                            class="@error('receipt') is-invalid @enderror form-control shadow-other"
                                            name="receipt" accept=".png,.jpg,.jpeg" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- jazz cash tab -->
                    <div class="tab-pane fade" id="nav-jazzcash" role="tabpanel" aria-labelledby="nav-jazzcash-tab">
                        <form action="{{ route('user.payments.store') }}" method="post" enctype="multipart/form-data"
                            class="needs-validation" novalidate id="jazzCashForm">
                            @csrf
                            <input type="hidden" name="bank" value="Jazz Cash">
                            <input type="hidden" name="session" value="{{ old('session') ?? $session->id }}">
                            <input type="hidden" name="counsellor_id"
                                value="{{ old('counsellor_id') ?? $session->counsellor_id }}">
                            <input type="hidden" name="user_counsellor_session_id" value="{{ encrypt('1') }}">
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Transaction ID</label>
                                        <input value="{{ old('transaction_id') }}"
                                            class="@error('transaction_id') is-invalid @enderror form-control shadow-other"
                                            name="transaction_id" required placeholder="Enter transaction id here">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Amount</label>
                                        <input type="number" min="1" value="{{ old('amount') }}"
                                            class="@error('amount') is-invalid @enderror form-control shadow-other"
                                            name="amount" required placeholder="Enter amount here">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="role-name">Receipt</label>
                                        <input type="file"
                                            class="@error('receipt') is-invalid @enderror form-control shadow-other"
                                            name="receipt" accept=".png,.jpeg,.jpg" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- bank tab -->
                    <div class="tab-pane fade" id="nav-bank" role="tabpanel" aria-labelledby="nav-bank-tab">
                        <form action="{{ route('user.payments.store') }}" method="post" enctype="multipart/form-data"
                            class="needs-validation" novalidate id="bankForm">
                            @csrf
                            <input type="hidden" name="session" value="{{ old('session') ?? $session->id }}">
                            <input type="hidden" name="counsellor_id"
                                value="{{ old('counsellor_id') ?? $session->counsellor_id }}">
                            <input type="hidden" name="user_counsellor_session_id" value="{{ encrypt('1') }}">
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="bank">Bank</label>
                                        <select name="bank"
                                            class="@error('bank') is-invalid @enderror form-select shadow-other"
                                            id="bank" required>
                                            <option value="">Please select a bank</option>
                                            <option value="Al Baraka Bank (Pakistan) Limited">Al Baraka Bank (Pakistan)
                                                Limited</option>
                                            <option value="Allied Bank Limited">Allied Bank Limited</option>
                                            <option value="Askari Bank">Askari Bank</option>
                                            <option value="Bank Alfalah Limited">Bank Alfalah Limited</option>
                                            <option value="Bank Al-Habib Limited">Bank Al-Habib Limited</option>
                                            <option value="BankIslami Pakistan Limited">BankIslami Pakistan Limited
                                            </option>
                                            <option value="Citi Bank">Citi Bank</option>
                                            <option value="Deutsche Bank A.G">Deutsche Bank A.G</option>
                                            <option value="The Bank of Tokyo-Mitsubishi UFJ">The Bank of Tokyo-Mitsubishi
                                                UFJ</option>
                                            <option value="Dubai Islamic Bank Pakistan Limited">Dubai Islamic Bank Pakistan
                                                Limited</option>
                                            <option value="Faysal Bank Limited">Faysal Bank Limited</option>
                                            <option value="First Women Bank Limited">First Women Bank Limited</option>
                                            <option value="Habib Bank Limited">Habib Bank Limited</option>
                                            <option value="Standard Chartered Bank (Pakistan) Limited">Standard Chartered
                                                Bank (Pakistan) Limited</option>
                                            <option value="Habib Metropolitan Bank Limited">Habib Metropolitan Bank Limited
                                            </option>
                                            <option value="Industrial and Commercial Bank of China">Industrial and
                                                Commercial Bank of China</option>
                                            <option value="Industrial Development Bank of Pakistan">Industrial Development
                                                Bank of Pakistan</option>
                                            <option value="JS Bank Limited">JS Bank Limited</option>
                                            <option value="MCB Bank Limited">MCB Bank Limited</option>
                                            <option value="MCB Islamic Bank Limited">MCB Islamic Bank Limited</option>
                                            <option value="Meezan Bank Limited">Meezan Bank Limited</option>
                                            <option value="National Bank of Pakistan">National Bank of Pakistan</option>
                                        </select>
                                        <!-- <input type="text" name="bank" value="{{ old('bank') }}" class="@error('bank') is-invalid @enderror form-control"> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="branch">Branch&nbsp;</label><small
                                            class="d-md-none d-lg-inline-block">(This field is optional)</small>
                                        <input type="text"
                                            class="@error('branch') is-invalid @enderror form-control shadow-other"
                                            name="branch" value="{{ old('branch') }}"
                                            placeholder="Enter branch name here">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Transaction ID</label>
                                        <input value="{{ old('transaction_id') }}"
                                            class="@error('transaction_id') is-invalid @enderror form-control shadow-other"
                                            name="transaction_id" required placeholder="Enter transaction id here">
                                        <input type="hidden" value="{{ old('session') }}" name="session"
                                            id="session" class="session">
                                        <input type="hidden" value="{{ old('user_counsellor_session_id') }}"
                                            name="user_counsellor_session_id" id="user_counsellor_session_id"
                                            class="user_counsellor_session_id">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role-name">Amount</label>
                                        <input type="number" min="1" value="{{ old('amount') }}"
                                            class="@error('amount') is-invalid @enderror form-control shadow-other"
                                            name="amount" data-="6" required
                                            placeholder="Enter transaction id here">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="role-name">Receipt</label>
                                        <input type="file"
                                            class="@error('receipt') is-invalid @enderror form-control shadow-other"
                                            name="receipt" accept=".png,.jpeg,.jpg" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- stripe form -->
                    <div class="tab-pane fade" id="nav-stripe" role="tabpanel" aria-labelledby="nav-stripe-tab"
                        tabindex="0">
                        <form role="form" action="{{ route('user.stripe.payment') }}" method="post"
                            enctype="multipart/form-data" class="validation needs-validation py-4 row" novalidate
                            id="stripeForm" data-cc-on-file="false"
                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}" id="stripeForm">
                            <input type="hidden" name="session" value="{{ old('session') ?? $session->id }}">
                            <input type="hidden" name="counsellor_id"
                                value="{{ old('counsellor_id') ?? $session->counsellor_id }}">
                            <input type="hidden" name="user_counsellor_session_id" value="{{ encrypt('1') }}">
                            @csrf
                            <div class='form-row row'>
                                <div class='col-md-12 error form-group d-none'>
                                    <div class='alert-danger alert'></div>
                                </div>
                            </div>
                            <div class="form-group col-md-6 mb-3">
                                <label for="stripe_name">Name on Card</label>
                                <input id="stripe_name" value="{{ old('stripe_name') }}"
                                    class="@error('stripe_name') is-invalid @enderror form-control shadow-other"
                                    name="stripe_name" placeholder="Name" required>
                            </div>
                            <div class="form-group col-md-6 mb-3">
                                <label for="card_number">Card number</label>
                                <input type="number" max="9999999999999999" min="10000000000000" id="card_number"
                                    class="@error('card_number') is-invalid @enderror form-control card-num shadow-other"
                                    name="card_number" placeholder="card number" size="20" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="ccv">CCV</label>
                                <input type="number" max="999" min="001" id="ccv" name="ccv"
                                    class="@error('ccv') is-invalid @enderror form-control shadow-other"
                                    placeholder="ccv" required>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="expiry_month">Expiry Month</label>
                                <select name="expiry_month" id="expiry_month"
                                    class='@error('expiry_month') is-invalid @enderror form-control card-expiry-month shadow-other'
                                    required>
                                    <option value="">------------</option>
                                    <option @selected(old('expiry_month') == '01')>01</option>
                                    <option @selected(old('expiry_month') == '02')>02</option>
                                    <option @selected(old('expiry_month') == '03')>03</option>
                                    <option @selected(old('expiry_month') == '04')>04</option>
                                    <option @selected(old('expiry_month') == '05')>05</option>
                                    <option @selected(old('expiry_month') == '06')>06</option>
                                    <option @selected(old('expiry_month') == '07')>07</option>
                                    <option @selected(old('expiry_month') == '08')>08</option>
                                    <option @selected(old('expiry_month') == '09')>09</option>
                                    <option @selected(old('expiry_month') == '10')>10</option>
                                    <option @selected(old('expiry_month') == '11')>11</option>
                                    <option @selected(old('expiry_month') == '12')>12</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 mb-3">
                                <label for="expiry_year">Expiry Year</label>
                                <input value="{{ old('expiry_year') }}"
                                    class='@error('expiry_year') is-invalid @enderror form-control card-expiry-year shadow-other'
                                    name="expiry_year" id="expiry_year" placeholder='YYYY' min="{{ date('Y') }}"
                                    type='year' required>
                            </div>

                            {{-- Coupon Section --}}

                            <div class="form-group col-md-4 mb-3">
                                <label for="card_number">Amount</label>
                                <input type="number" id="discountedAmount" class="form-control shadow-other"
                                    name="discountedAmount" value="{{ $amount }}" size="20" disabled>
                                <input type="hidden" id="total_amount" class="form-control shadow-other"
                                    name="total_amount" value="{{ $amount }}" size="20">
                            </div>
                            <div id="coupon_box" class="form-group col-md-4 mb-3">
                                <label for="card_number">Coupon Code</label>
                                <input type="text" id="coupon_code" class="form-control shadow-other"
                                    name="coupon_code" placeholder="Code" size="20">
                                <div id="coupon_code_msg" class="text-danger"></div>

                            </div>
                            <input type="hidden" id="coupon_code_applied" class="form-control shadow-other"
                                name="coupon_code_applied" value="no" size="20">

                            <div class="form-group col-md-4 mb-3 mt-4">
                                <button type="button" id="applyCouponCode"
                                    class="form-control bg-primary btn btn-sm btn-primary bookChanger py-2">Apply
                                    Coupon</button>
                            </div>

                            {{-- Coupon Section End --}}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="bg-secondary btn btn-sm btn-secondary bookChanger"
            data-bs-dismiss="modal">Close</button>
        <button type="submit" form="easyPaisaForm" style="display:none" id="save_changes"
            class="bg-primary btn btn-sm btn-primary bookChanger">Submit</button>
    </div>
@endsection
@include('extra.modal', ['modal_title' => 'Add Payment'])
@endsection
@push('script')
<script>
    // restrict numeric input fields length
    document.addEventListener('DOMContentLoaded', () => {
        document.querySelectorAll('input[type="number"]')
            .forEach(input => {
                input.addEventListener('keyup', () => {
                    input.value = input.value.slice(0, input.dataset.maxlength);
                });
            });
    });

    function getProfile(id) {
        var modal = document.querySelector('#counsellorProfile');
        var url = "{{ url('user/getCounsellor') }}";
        $.ajax({
            url: url + "/" + id,
            type: 'GET',
            success: function(res) {
                document.querySelector('#counsellorProfile .modal-body').innerHTML = res;
                document.querySelector('#bookNowButton').style.display = 'none';
            },
            error: function(error) {
                console.log(error);
            }
        })
        modal = new bootstrap.Modal(modal);
        modal.show();
    }

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }
                    form.classList.add('was-validated')
                }, false)
            })
    })()

    // apply coupon code - added by sami

    $('#applyCouponCode').click(function() {
        $('#coupon_code_applied').val('no');
        var couponCode = $('#coupon_code').val();
        var amount = $('#discountedAmount').val();
        if (couponCode == '') {
            $('#coupon_code_msg').html('Please enter a coupon code.');
        } else {
            $.ajax({
                type: 'post',
                url: '{{ route('user.stripe.apply_coupon') }}',
                data: 'coupon_code=' + couponCode + '&amount=' + amount + '&_token=' + $(
                    "[name='_token']").val(),
                success: function(result) {
                    console.log(result.status);
                    if (result.status == 'success') {
                        $('#coupon_code_msg').removeClass('text-danger');
                        $('#coupon_code_msg').addClass('text-success');
                        $('#coupon_code_msg').html(result.msg);
                        $('#discountedAmount').val(result.totalPrice);
                        $('#total_amount').val(result.totalPrice);
                        $('#coupon_code_applied').val('yes');

                        $('#coupon_box').addClass('d-none');
                        $('#applyCouponCode').text('Coupon Code Applied');
                        $('#applyCouponCode').prop('disabled', true);
                        $('#applyCouponCode').removeClass(
                                'form-control bg-primary btn btn-sm btn-primary bookChanger py-2')
                            .addClass(
                                'form-control bg-success btn btn-sm btn-primary bookChanger py-2');

                    } else {
                        $('#coupon_code_msg').html(result.msg);
                        $('#coupon_code').val('');
                        $('#coupon_code_applied').val('no');
                    }
                }
            });
        }
    });
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    try {
        $(function() {
            var $form = $(".validation");
            $('form.validation').bind('submit', function(e) {
                var $form = $(".validation"),
                    inputVal = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'
                    ].join(', '),
                    $inputs = $form.find('.required').find(inputVal),
                    $errorStatus = $form.find('div.error'),
                    valid = true;
                $errorStatus.addClass('hide');

                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorStatus.removeClass('hide');
                        e.preventDefault();
                    }
                });
                // console.log($form.data('cc-on-file'));
                console.log($form.data('stripe-publishable-key'));
                if (!$form.data('cc-on-file')) {
                    // Stripe.setPublishableKey('{{ env('STRIPE_KEY') }}');
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    e.preventDefault();
                    Stripe.createToken({
                        number: $('.card-num').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeHandleResponse);
                }

            });

            function stripeHandleResponse(status, response) {
                if (response.error) {
                    $('.error').removeClass('hide').find('.alert').text(response.error.message);
                } else {
                    var token = response['id'];
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }

        });
    } catch (error) {
        alert('Something went wrong');
        // console.log(error);
    }

    $('form#stripeForm').submit(function() {
        // $("#save_changes").prop('disabled', true);
    });
</script>
@endpush

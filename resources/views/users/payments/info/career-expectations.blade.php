@extends('layouts.user', ['pagename' => $test->test_name])
@section('css')
<style>
    .career-expectations-test {
        color: var(--special) !important;
        font-weight: 500
    }
</style>
@endsection
@section('user-content')
<div class="container-fluid px-0">
    <div class="row g-1">
        <!-- side bar here  -->
        @include('users.side-bar')
        <!--/ side bar -->
        <div class="col-md-10">
            <div class="row g-0">
                <div class="col-12 sticky-top bg-white">
                    <div class="text-end pb-3 pe-3">@include('navbars.user')</div>
                </div>
                <div class="col-12 main" style="min-height: 83.3vh;">
                        <div class="floating"></div>
                    <div class="row">
                        <div class="col-12 col-lg-10 offset-lg-1">
                            <div class="card border-0 pt-3 bg-transparent">
                                <div class="card-body">
                                    <div class="row inner">
                                        <div class="col-12">
                                            <div class="p-4">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h3>{{ $test->test_name }}</h3>
                                                    </div>
                                                    <div class="col-12 col-lg-6 pt-lg-4">
                                                        <div class="text-capitalize mb-2">from</div>
                                                        <h1 class="text-special mb-3">PKR {{ $amount }}/-</h1>
                                                        <p class="mb-5">Lorem ipsum dolor sit amet, consectetur, adipisicing elit. Sed impedit, cumque tenetur, exercitationem sit qui ex, itaque sapiente nesciunt laborum rerum maiores sunt fugiat distinctio at fugit adipisci, voluptate labore.</p>
                                                        <a href="{{ route("user.test", $test->id) }}" class="btn btn-sm bg-special px-3 text-white">Buy Now</a>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <img src="{{ asset("assets/img/career-expectations-test-info.png") }}" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @include('footer.user')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.user', ['pagename' => 'Add Payment'])
@section('css')
    <style>
        .my-img {
            width: 3rem;
        }

        .my-img:focus {
            background-color: #d3d3c8;
        }

        .img-color {
            background-color: #d3d3c8 !important;
        }

        .card-bg {
            background-color: #a7a7a7;
        }

        .bg-input {
            background-color: #F1F4F4;
        }

        input:focus {
            box-shadow: none !important;
        }

        .btn:focus {
            box-shadow: none !important;
        }

        @media (min-width:576px) {
            .nav.nav-tabs button img {
                width: 2.5rem;
                height: 2.5rem;
            }

            .border-lg-end {
                border-right: var(--bs-border-width) var(--bs-border-style) var(--bs-border-color) !important;
            }
        }

        @media (max-width:576px) {
            .nav.nav-tabs button img {
                width: 2rem;
                height: 2rem;
            }
        }

        [type="number"]::-webkit-inner-spin-button {
            display: none;
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            @include('users.side-bar')
            <div class="col-md-10">
                <div class="row g-0">
                    <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="main" style="min-height: 83.2vh">
                            <div class="floating"></div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="inner">
                                                <div class="p-4">
                                                    <h5 class="mb-3 text-capitalize">select your payment method</h5>
                                                    <nav class="">
                                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                            <button class="text-dark nav-link active" id="nav-info-tab"
                                                                data-bs-toggle="tab" data-bs-target="#nav-info"
                                                                type="button" role="tab" aria-controls="nav-info"
                                                                aria-selected="true" onclick="$('#save_changes').hide();">
                                                                Payment Details
                                                            </button>
                                                            <button class="text-dark nav-link" id="nav-home-tab"
                                                                onclick="change('easyPaisaForm');" data-bs-toggle="tab"
                                                                data-bs-target="#nav-home" type="button" role="tab"
                                                                aria-controls="nav-home" aria-selected="true">
                                                                <img src="{{ asset('assets/img/easypaisa.png') }}"
                                                                    alt="easypaisa logo" class="rounded"><br>
                                                                <span class="d-none d-lg-inline">Easypaisa</span>
                                                            </button>
                                                            <button class="text-dark nav-link" id="nav-profile-tab"
                                                                onclick="change('jazzCashForm');" data-bs-toggle="tab"
                                                                data-bs-target="#nav-profile" type="button" role="tab"
                                                                aria-controls="nav-profile" aria-selected="false">
                                                                <img src="{{ asset('assets/img/jazcash.png') }}"
                                                                    alt="jazzcash logo" class="rounded"><br>
                                                                <span class="d-none d-lg-inline">Jazzcash</span>
                                                            </button>
                                                            <button class="text-dark nav-link" id="nav-contact-tab"
                                                                onclick="change('bankForm');" data-bs-toggle="tab"
                                                                data-bs-target="#nav-contact" type="button" role="tab"
                                                                aria-controls="nav-contact" aria-selected="false">
                                                                <img src="{{ asset('assets/img/bank.jpg') }}"
                                                                    alt="bank logo" class="rounded"><br>
                                                                <span class="d-none d-lg-inline">Bank</span>
                                                            </button>
                                                            <button class="text-dark nav-link" id="nav-stripe-tab"
                                                                onclick="change('stripeForm');" data-bs-toggle="tab"
                                                                data-bs-target="#nav-stripe" type="button" role="tab"
                                                                aria-controls="nav-stripe" aria-selected="false">
                                                                {{-- <i class="fa fa-credit-card fs-2" aria-hidden="true"></i><br> --}}
                                                                <img src="{{ asset('assets/img/debit-credit-card.png') }}"
                                                                    alt="bank logo" class="rounded img-fluid"><br>
                                                                <span class="d-none d-lg-inline">Credit/Debit Card</span>
                                                            </button>
                                                        </div>
                                                    </nav>
                                                    <div class="tab-content mb-3 px-3 pb-3 bg-white border-start border-bottom border-end rounded-bottom"
                                                        id="nav-tabContent">
                                                        <div class="tab-pane fade show active" id="nav-info"
                                                            role="tabpanel" aria-labelledby="nav-info-tab">
                                                            <div class="row p-2">
                                                                <div class="col-md-6 border-lg-end">
                                                                    <h6>Easypaisa</h6>
                                                                    <div class="text-muted">Account title: Rehan Tariq</div>
                                                                    <div class="text-muted">Account number: +92 300 0000000
                                                                    </div>
                                                                    <hr>
                                                                    <h6>Jazzcash</h6>
                                                                    <div class="text-muted">Account title: Rehan Tariq</div>
                                                                    <div class="text-muted">Account number: +92 300 0000000
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h6>Bank Details</h6>
                                                                    <div class="text-muted">Bank Name : Faisal bank</div>
                                                                    <div class="text-muted">Account Title : The Educatics
                                                                    </div>
                                                                    <div class="text-muted">Account Number :
                                                                        PK21FAYS3423301000000289</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="nav-home" role="tabpanel"
                                                            aria-labelledby="nav-home-tab" tabindex="0">
                                                            <form action="{{ route('user.payments.store') }}"
                                                                method="post" enctype="multipart/form-data"
                                                                class="needs-validation py-4 row" novalidate
                                                                id="easyPaisaForm">
                                                                @csrf
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Transaction ID</label>
                                                                    <input type="number" min="1"
                                                                        value="{{ old('transaction_id') }}"
                                                                        class="@error('transaction_id') is-invalid @enderror form-control"
                                                                        name="transaction_id" required>
                                                                    <input type="hidden" value="Test Payment"
                                                                        name="session" id="session">
                                                                    <input type="hidden"
                                                                        value="{{ $id ?? 'Test Payment' }}"
                                                                        name="user_counsellor_session_id"
                                                                        id="user_counsellor_session_id">
                                                                    <input type="hidden" name="bank"
                                                                        value="Easy Paisa"
                                                                        class="@error('bank') is-invalid @enderror form-control">
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Amount</label>
                                                                    <input type="number" min="1"
                                                                        value="{{ old('amount') }}"
                                                                        class="@error('amount') is-invalid @enderror form-control"
                                                                        name="amount" required>
                                                                </div>
                                                                <div class="form-group col-12 mb-3">
                                                                    <label for="role-name">Receipt</label>
                                                                    <input type="file"
                                                                        class="@error('receipt') is-invalid @enderror form-control"
                                                                        name="receipt" accept="image/*,application/pdf"
                                                                        required>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                                            aria-labelledby="nav-profile-tab" tabindex="0">
                                                            <form action="{{ route('user.payments.store') }}"
                                                                method="post" enctype="multipart/form-data"
                                                                class="needs-validation py-4 row" novalidate
                                                                id="jazzCashForm">
                                                                @csrf
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Transaction ID</label>
                                                                    <input type="number" min="1"
                                                                        value="{{ old('transaction_id') }}"
                                                                        class="@error('transaction_id') is-invalid @enderror form-control"
                                                                        name="transaction_id" required>
                                                                    <input type="hidden" value="Test Payment"
                                                                        name="session" id="session">
                                                                    <input type="hidden"
                                                                        value="{{ $id ?? 'Test Payment' }}"
                                                                        name="user_counsellor_session_id"
                                                                        id="user_counsellor_session_id">
                                                                    <input type="hidden" name="bank"
                                                                        value="Jazz Cash"
                                                                        class="@error('bank') is-invalid @enderror form-control">
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Amount</label>
                                                                    <input type="number" min="1"
                                                                        value="{{ old('amount') }}"
                                                                        class="@error('amount') is-invalid @enderror form-control"
                                                                        name="amount" required>
                                                                </div>
                                                                <div class="form-group col-12 mb-3">
                                                                    <label for="role-name">Receipt</label>
                                                                    <input type="file"
                                                                        class="@error('receipt') is-invalid @enderror form-control"
                                                                        name="receipt" accept="image/*,application/pdf"
                                                                        required>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                                            aria-labelledby="nav-contact-tab" tabindex="0">
                                                            <form action="{{ route('user.payments.store') }}"
                                                                method="post" enctype="multipart/form-data"
                                                                class="needs-validation py-4 row" novalidate
                                                                id="bankForm">
                                                                @csrf
                                                                <input type="hidden" value="Test Payment" name="session"
                                                                    id="session">
                                                                <input type="hidden" value="{{ $id ?? 'Test Payment' }}"
                                                                    name="user_counsellor_session_id"
                                                                    id="user_counsellor_session_id">
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="bank">Bank</label>
                                                                    <select name="bank"
                                                                        class="@error('bank') is-invalid @enderror form-select"
                                                                        id="bank" required>
                                                                        <option value="{{ old('bank') }}">Please select
                                                                            a bank
                                                                        </option>
                                                                        <option value="Al Baraka Bank (Pakistan) Limited">
                                                                            Al
                                                                            Baraka Bank (Pakistan)
                                                                            Limited</option>
                                                                        <option value="Allied Bank Limited">Allied Bank
                                                                            Limited</option>
                                                                        <option value="Askari Bank">Askari Bank</option>
                                                                        <option value="Bank Alfalah Limited">Bank Alfalah
                                                                            Limited</option>
                                                                        <option value="Bank Al-Habib Limited">Bank Al-Habib
                                                                            Limited</option>
                                                                        <option value="BankIslami Pakistan Limited">
                                                                            BankIslami Pakistan Limited
                                                                        </option>
                                                                        <option value="Citi Bank">Citi Bank</option>
                                                                        <option value="Deutsche Bank A.G">Deutsche Bank A.G
                                                                        </option>
                                                                        <option value="The Bank of Tokyo-Mitsubishi UFJ">
                                                                            The
                                                                            Bank of
                                                                            Tokyo-Mitsubishi
                                                                            UFJ</option>
                                                                        <option
                                                                            value="Dubai Islamic Bank Pakistan Limited">
                                                                            Dubai Islamic Bank
                                                                            Pakistan
                                                                            Limited</option>
                                                                        <option value="Faysal Bank Limited">Faysal Bank
                                                                            Limited</option>
                                                                        <option value="First Women Bank Limited">First
                                                                            Women
                                                                            Bank Limited</option>
                                                                        <option value="Habib Bank Limited">Habib Bank
                                                                            Limited</option>
                                                                        <option
                                                                            value="Standard Chartered Bank (Pakistan) Limited">
                                                                            Standard
                                                                            Chartered
                                                                            Bank (Pakistan) Limited</option>
                                                                        <option value="Habib Metropolitan Bank Limited">
                                                                            Habib Metropolitan Bank
                                                                            Limited
                                                                        </option>
                                                                        <option
                                                                            value="Industrial and Commercial Bank of China">
                                                                            Industrial and
                                                                            Commercial Bank of China</option>
                                                                        <option
                                                                            value="Industrial Development Bank of Pakistan">
                                                                            Industrial
                                                                            Development
                                                                            Bank of Pakistan</option>
                                                                        <option value="JS Bank Limited">JS Bank Limited
                                                                        </option>
                                                                        <option value="MCB Bank Limited">MCB Bank Limited
                                                                        </option>
                                                                        <option value="MCB Islamic Bank Limited">MCB
                                                                            Islamic
                                                                            Bank Limited</option>
                                                                        <option value="Meezan Bank Limited">Meezan Bank
                                                                            Limited</option>
                                                                        <option value="National Bank of Pakistan">National
                                                                            Bank of Pakistan</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="branch">Branch</label><small> (This field
                                                                        is
                                                                        optional)</small>
                                                                    <input type="text"
                                                                        class="@error('branch') is-invalid @enderror form-control"
                                                                        name="branch" value="{{ old('branch') }}">
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Transaction ID</label>
                                                                    <input type="number" min="1"
                                                                        value="{{ old('transaction_id') }}"
                                                                        class="@error('transaction_id') is-invalid @enderror form-control"
                                                                        name="transaction_id" required>
                                                                    <input type="hidden" value="Test Payment"
                                                                        name="session" id="session">
                                                                    <input type="hidden"
                                                                        value="{{ $id ?? 'Test Payment' }}"
                                                                        name="user_counsellor_session_id"
                                                                        id="user_counsellor_session_id">
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="role-name">Amount</label>
                                                                    <input type="hidden" name="test_id"
                                                                        value="{{ $id ?? 'Test Payment' }}">
                                                                    <input type="number" min="1"
                                                                        value="{{ old('amount') }}"
                                                                        class="@error('amount') is-invalid @enderror form-control"
                                                                        name="amount" data-="6" required>
                                                                </div>
                                                                <div class="form-group col-12">
                                                                    <label for="role-name">Receipt</label>
                                                                    <input type="file"
                                                                        class="@error('receipt') is-invalid @enderror form-control"
                                                                        name="receipt" accept="image/*,application/pdf"
                                                                        required>
                                                                </div>
                                                            </form>
                                                        </div>


                                                        <div class="tab-pane fade" id="nav-stripe" role="tabpanel"
                                                            aria-labelledby="nav-stripe-tab" tabindex="0">
                                                            <form role="form"
                                                                action="{{ route('user.stripe.payment') }}"
                                                                method="post" enctype="multipart/form-data"
                                                                class="validation needs-validation py-4 row" novalidate
                                                                id="stripeForm" data-cc-on-file="false"
                                                                data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                                id="stripeForm">
                                                                @csrf
                                                                <div class='form-row row'>
                                                                    <div class='col-md-12 d-none error form-group'>
                                                                        <div class='alert-danger alert'>Fix the errors
                                                                            before you begin.</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="stripe_name">Name on Card</label>
                                                                    <input id="stripe_name"
                                                                        value="{{ old('stripe_name') }}"
                                                                        class="@error('stripe_name') is-invalid @enderror form-control"
                                                                        name="stripe_name" placeholder="Name" required>
                                                                    <input type="hidden" value="Test Payment"
                                                                        name="session" id="session">
                                                                    <input type="hidden"
                                                                        value="{{ $id ?? 'Test Payment' }}"
                                                                        name="user_counsellor_session_id"
                                                                        id="user_counsellor_session_id">
                                                                </div>
                                                                <div class="form-group col-md-6 mb-3">
                                                                    <label for="card_number">Card number</label>
                                                                    <input type="number" max="9999999999999999"
                                                                        min="10000000000000" id="card_number"
                                                                        class="@error('card_number') is-invalid @enderror form-control card-num"
                                                                        name="card_number" placeholder="card number"
                                                                        size="20" required>
                                                                </div>
                                                                <div class="form-group col-md-4 mb-3">
                                                                    <label for="ccv">CCV</label>
                                                                    <input type="number" max="999" min="100"
                                                                        id="ccv" name="ccv"
                                                                        class="@error('ccv') is-invalid @enderror form-control"
                                                                        placeholder="ccv" required>
                                                                </div>
                                                                <div class="form-group col-md-4 mb-3">
                                                                    <label for="expiry_month">Expiry Month</label>
                                                                    <select name="expiry_month" id="expiry_month"
                                                                        class='@error('expiry_month') is-invalid @enderror form-control card-expiry-month'
                                                                        required>
                                                                        <option value="">------------</option>
                                                                        <option @selected(old('expiry_month') == '01')>01</option>
                                                                        <option @selected(old('expiry_month') == '02')>02</option>
                                                                        <option @selected(old('expiry_month') == '03')>03</option>
                                                                        <option @selected(old('expiry_month') == '04')>04</option>
                                                                        <option @selected(old('expiry_month') == '05')>05</option>
                                                                        <option @selected(old('expiry_month') == '06')>06</option>
                                                                        <option @selected(old('expiry_month') == '07')>07</option>
                                                                        <option @selected(old('expiry_month') == '08')>08</option>
                                                                        <option @selected(old('expiry_month') == '09')>09</option>
                                                                        <option @selected(old('expiry_month') == '10')>10</option>
                                                                        <option @selected(old('expiry_month') == '11')>11</option>
                                                                        <option @selected(old('expiry_month') == '12')>12</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4 mb-3">
                                                                    <label for="expiry_year">Expiry Year</label>
                                                                    <input value="{{ old('expiry_year') }}"
                                                                        class='@error('expiry_year') is-invalid @enderror form-control card-expiry-year'
                                                                        name="expiry_year" id="expiry_year"
                                                                        placeholder='YYYY' min="{{ date('Y') }}"
                                                                        type='number' required>
                                                                </div>


                                                            </form>
                                                        </div>
                                                    </div> <!-- ./ tab container -->
                                                    <div class="text-end">
                                                        <button type="submit" style="display:none" form="easyPaisaForm"
                                                            id="save_changes"
                                                            class="btn btn-sm text-white bg-special">Confirm
                                                            Payment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ./ tab column -->
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">
        try {
            $(function() {
                var $form = $(".validation");
                $('form.validation').bind('submit', function(e) {
                    var $form = $(".validation"),
                        inputVal = ['input[type=email]', 'input[type=password]',
                            'input[type=text]', 'input[type=file]',
                            'textarea'
                        ].join(', '),
                        $inputs = $form.find('.required').find(inputVal),
                        $errorStatus = $form.find('div.error'),
                        valid = true;
                    $errorStatus.addClass('hide');

                    $('.has-error').removeClass('has-error');
                    $inputs.each(function(i, el) {
                        var $input = $(el);
                        if ($input.val() === '') {
                            $input.parent().addClass('has-error');
                            $errorStatus.removeClass('hide');
                            e.preventDefault();
                        }
                    });

                    if (!$form.data('cc-on-file')) {
                        e.preventDefault();
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                        Stripe.createToken({
                            number: $('.card-num').val(),
                            cvc: $('.card-cvc').val(),
                            exp_month: $('.card-expiry-month').val(),
                            exp_year: $('.card-expiry-year').val()
                        }, stripeHandleResponse);
                    }

                });

                function stripeHandleResponse(status, response) {
                    if (response.error) {
                        $('.error').removeClass('hide').find('.alert').text(response.error.message);
                    } else {
                        var token = response['id'];
                        $form.find('input[type=text]').empty();
                        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                        $form.get(0).submit();
                    }
                }

            });
        } catch (error) {
            alert('Something went wrong');
            // console.log(error);
        }
        $('form#stripeForm').submit(function() {
            $("#save_changes").prop('disabled', true);
        });
    </script>
@endpush

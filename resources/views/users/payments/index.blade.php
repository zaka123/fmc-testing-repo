@extends('layouts.user', ['pagename' => 'Payments'])
@section('css')
    <style>
        .inner::before {
            top: 60%;
        }

        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .payment-history {
            color: var(--special) !important;
            font-weight: 500
        }

        .title::before {
            content: "Payments";
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    {{-- <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div> --}}
                    @include('includes.header')

                    <div class="col-12">
                        <div class="main px-3 px-lg-0" style="min-height: 83.3vh;">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row inner">
                                                <div class="col-12">
                                                    <div class="p-2 p-lg-4">
                                                        <table style="vertical-align: middle;"
                                                            class="table table-borderless payments">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">#</th>
                                                                    <th class="text-center">Type</th>
                                                                    <th class="text-center">Bank</th>
                                                                    <th class="text-center">Branch</th>
                                                                    <th class="text-center">Transaction ID</th>
                                                                    <th class="text-center">Status</th>
                                                                    <th class="text-center">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($payments as $key => $payment)
                                                                    <tr>
                                                                        <td class="text-center rounded-start"
                                                                            >
                                                                            {{ ++$key }}</td>
                                                                        <td class="text-center">
                                                                            {{ $payment->session_id == 'Test Payment'
                                                                                ? "Test
                                                                                                                                                                                                                            Payment"
                                                                                : 'Session Payment' }}
                                                                        </td>
                                                                        <td class="text-center">{{ $payment->bank }} </td>
                                                                        <td class="text-center">
                                                                            {{ $payment->branch ?? '-----' }} </td>
                                                                        <td class="text-center">
                                                                            {{ $payment->transaction_id }} </td>
                                                                        <td class="text-center">
                                                                            {!! $payment->payment_status !!}
                                                                        </td>
                                                                        <td class="text-center rounded-end">
                                                                            <div class="text-center">
                                                                                @if ($payment->status != 'Confirm')
                                                                                    <form
                                                                                        action="{{ route('user.payments.destroy', $payment->id) }}"
                                                                                        method="post">
                                                                                        @csrf @method('DELETE')
                                                                                        <button type="submit"
                                                                                            class="btn btn-sm btn-outline-danger"><i
                                                                                                class="fas fa-trash"></i></button>
                                                                                    </form>
                                                                                @else
                                                                                    <span class="badge text-bg-dark"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="confirmed payments can't be deleted">
                                                                                        <i class="fa fa-info"></i>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function initializeDataTable() {
            return $('.payments')
                .DataTable({
                    "responsive": true,
                    "lengthChange": false,
                    "autoWidth": false,
                    "ordering": false,
                    "pagingType": "simple",
                    "pageLength": 5,
                    dom: '<"title pt-3"<"filter"f>>rt<"row"<"col-12 col-md-6"i><"col-12 col-md-6"p>>',
                    "language": {
                        "emptyTable": "You don't have any payment yet."
                    }
                });
        }
    </script>
@endsection

@extends('layouts.user',['pagename'=>'Payment Confirmation'])
@section('user-content')
<div class="container-fluid px-0">
    <div class="row g-1">
        <!-- side bar -->
        @include('users.side-bar')
        <div class="col-md-10">
            <div class="row g-0">
                <div class="col-12 sticky-top bg-white">
                    <div class="text-end pb-3 px-3 d-flex">
                        <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas" data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                        <div class="ms-auto">@include('navbars.user')</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="main px-3 px-lg-0" style="min-height: 83.2vh">
                        <div class="floating"></div>
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="card border-0 pt-3 bg-transparent">
                                    <div class="card-body">
                                        <div class="row inner">
                                            <div class="col-12">
                                                <div class="p-4">
                                                    <div class="row">
                                                        <div class="col-lg-10 offset-lg-1">
                                                            <div class="text-center mb-4">
                                                                <img src="{{ asset('assets/icons/check-circle.png') }}"
                                                                    alt="congratulations" width="100"
                                                                    class="img-fluid mb-3">
                                                                <h5>
                                                                    Your Payment is completed Successfully!
                                                                </h5>
                                                            </div>
                                                                <p>
                                                                    You will get notification of payment approval within 24 hour., consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita.
                                                                </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @include('footer.user')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
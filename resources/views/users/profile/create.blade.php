@extends('layouts.navbar', ['pagename' => 'Create Profile - '])

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/users.profile.create.css') }}">

    <style>
        input[type="radio"]:checked+label {
            background-color: var(--special) !important;
            color: white !important;
        }

        .form-control,
        .form-select {
            box-shadow: none !important;
        }

        input[type="radio"].invalid:not(:valid)+label {
            border: 2px solid var(--bs-danger) !important;
        }

        .fs-7 {
            font-size: 0.75rem !important
        }

        .w-fit-content {
            width: fit-content !important
        }

        .mb-6 {
            margin-bottom: 6rem !important
        }

        @media (min-height: 650px) {
            footer {
                position: fixed !important;
                bottom: 0 !important;
                width: 100%;
            }
        }

        .swiper-slide-active .scroll {
            overflow-x: auto;
        }

        .swiper-slide-prev * {
            display: none;
        }

        .hoverChangerLook:hover {
            background-color:black!important;
            color: white!important;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="swiper">
            <form action="{{ route('user.profile.store') }}" id="profileForm" class="swiper-wrapper" method="post">
                @csrf
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <p class="fw-500 text-center fs-5">

                                Congratulations <span
                                    class="70cf9771 fw-bold text-special">{{ auth()->user()->first_name }}</span>, your
                                account has been
                                created successfully. It is now time for you to start your journey with FindMeCareer and
                                take your professional development to the NEXT LEVEL!
                            </p>
                        </div>
                        <div class="col-lg-8">
                            <p class="fw-500 text-center fs-5">Let’s get some more information to know more about your
                                study habits and career ambitions!</p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 text-center mb-6">
                            <button type="button" disabled style="background-color:#6CBBDE"
                                class="btn btn-sm text-white previous" onclick="changeQuestion(0)">Previous</button>
                            <button type="button" title="Press enter to show next question"
                                style="background-color:#6CBBDE" onclick="changeQuestion(1)"
                                class="btn btn-sm text-white next hoverChangerLook">Next</button>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question px-0">
                                <div class="row">
                                    <div class="col-md-8 col-lg-5 offset-md-2 offset-lg-4">
                                        <label for="average_grade_point" class="fs-5 fw-500">How would you describe
                                            yourself
                                            as a student? Ranging from A to D</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="average_grade_point" placeholder="GPA: 3.4" required
                                            id="average_grade_point" value="{{ old('average_grade_point') }}"
                                            class="@error('average_grade_point') is-invalid @enderror form-select">
                                            <option value="">Please select your answer</option>
                                            <option>A-Grade Student (80% and above)</option>
                                            <option>B-Grade Student (70-80%)</option>
                                            <option>C-Grade Student (60-70%)</option>
                                            <option>D-Grade Student (50-60%)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="question px-0">
                                <div class="fw-500 text-center fs-5">Perfect, we now have the basic information to choose
                                    the right mentor for you. Let’s get some more information for the final touches.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question px-0">
                                <div class="row">
                                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                                        <label for="father_education" class="fs-5 fw-500">What level of education did your
                                            father get?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="father_education" required id="father_education"
                                            class="@error('father_education') is-invalid @enderror form-select">
                                            <option value="">Select an option</option>
                                            <option>Not qualified</option>
                                            <option>Primary Education</option>
                                            <option>Secondary Education</option>
                                            <option>Vocational Training</option>
                                            <option>Bachelors</option>
                                            <option>Masters</option>
                                            <option>PhD</option>
                                            <option>Prefer not to say</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question">
                                <div class="row">
                                    <div class="col-md-8 col-lg-7 offset-lg-4">
                                        <label for="father_occupation" class="fs-5 fw-500">What occupation or field does
                                            your
                                            father work in?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="father_occupation" required id="father_occupation"
                                            class="@error('father_occupation') is-invalid @enderror form-select">
                                            <option value="">Select an option</option>
                                            <option>Unemployed</option>
                                            <option>Unskilled</option>
                                            <option>Semi-skilled job</option>
                                            <option>High skilled job</option>
                                            <option>Business</option>
                                            <option>Prefer not to say</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question">
                                <div class="row">
                                    <div class="col-md-8 col-lg-6 offset-lg-4">
                                        <label for="mother_education" class="fs-5 fw-500">What level of education did your
                                            mother get?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="mother_education" required id="mother_education"
                                            class="@error('mother_education') is-invalid @enderror form-select">
                                            <option value="">Select an option</option>
                                            <option>Not qualified</option>
                                            <option>Primary Education</option>
                                            <option>Secondary Education</option>
                                            <option>Vocational Training</option>
                                            <option>Bachelors</option>
                                            <option>Masters</option>
                                            <option>PhD</option>
                                            <option>Prefer not to say</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question">
                                <div class="row">
                                    <div class="col-lg-7 offset-lg-4">
                                        <label for="mother_occupation" class="fs-5 fw-500">What occupation or field does
                                            your
                                            mother work in?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="mother_occupation" required id="mother_occupation"
                                            class="@error('mother_occupation') is-invalid @enderror form-select">
                                            <option value="">Select an option</option>
                                            <option>Unemployed</option>
                                            <option>Unskilled</option>
                                            <option>Semi-skilled job</option>
                                            <option>High skilled job</option>
                                            <option>Business</option>
                                            <option>Prefer not to say</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="question">
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <label class="fs-5 fw-500" for="parents_retired">Are your parents retired?</label>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8 col-lg-4">
                                        <select name="parents_retired" required id="parents_retired"
                                            class="@error('mother_occupation') is-invalid @enderror form-select">
                                            <option value="">Select an option</option>
                                            <option>Father</option>
                                            <option>Mother</option>
                                            <option>Both</option>
                                            <option>Neither</option>
                                            <option>Prefer not to say</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 mb-3 mb-lg-0 scroll">
                            <div class="fs-4 fw-500">How often do the following people work with you on your
                                schoolwork?
                                <span class="text-muted">
                                    <small>(Please&nbsp;select&nbsp;one&nbsp;response&nbsp;in&nbsp;each)</small>
                                </span>
                            </div>
                            <table class="table table-borderless" id="conflicting">
                                <thead>
                                    <tr class="text-center">
                                        <th></th>
                                        <th>Never&nbsp;or almost&nbsp;never</th>
                                        <th>Few&nbsp;times a&nbsp;year</th>
                                        <th>About&nbsp;once a&nbsp;month</th>
                                        <th>Several&nbsp;times a&nbsp;month</th>
                                        <th>Several&nbsp;times a&nbsp;week</th>
                                        <th>Not&nbsp;applicable</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mother</td>
                                        <td colspan="6">
                                            <input type="range" value="0" class="form-range" step="25"
                                                max="125" name="personal[6][mother]">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Father</td>
                                        <td colspan="6">
                                            <input type="range" value="0" class="form-range" step="25"
                                                max="125" name="personal[6][father]">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Siblings</td>
                                        <td colspan="6">
                                            <input type="range" value="0" class="form-range" step="25"
                                                max="125" name="personal[6][siblings]">
                                        </td>
                                    <tr>
                                        <td>Relatives</td>
                                        <td colspan="6">
                                            <input type="range" value="0" class="form-range" step="25"
                                                max="125" name="personal[6][relatives]">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Other&nbsp;persons (Tuition,&nbsp;etc)</td>
                                        <td colspan="6">
                                            <input type="range" value="0" class="form-range" step="25"
                                                max="125" name="personal[6][other_persons]">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook" id="buttonNext">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                            <label class="fs-4 fw-500" for="personal[1]">Usually, how long you study in the
                                morning
                                before going to school/college or university?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-4">
                            <div class="form-group position-relative">
                                <input type="number" min="0" max="24" step="0.1"
                                    name="study_habits_and_routine[1]" id="personal[1]" class="form-control"
                                    placeholder="Write your answer here ..." required>
                                <span class="input-group-text position-absolute"
                                    style="top: 0px;right: 0px;border-top-left-radius: 0;border-bottom-left-radius: 0;">hour<sub>(s)</sub></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)" id="buttonPrevious">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                            <label class="fs-4 fw-500" for="study_habits_and_routine[2]">Usually, how long you
                                study in
                                the evening after the school/college or university?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-4">
                            <div class="form-group position-relative">
                                <input type="number" min="0" max="24" step="0.1"
                                    name="study_habits_and_routine[2]" id="personal[2]" class="form-control"
                                    placeholder="Write your answer here ..." required>
                                <span class="input-group-text position-absolute"
                                    style="top: 0px;right: 0px;border-top-left-radius: 0;border-bottom-left-radius: 0;">hour<sub>(s)</sub></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide study_survey_question">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 scroll" style="overflow-x: auto;">
                            <div class="fs-4 fw-500">Why did you study before or after school?</div>
                            <div class="text-muted">
                                (Please&nbsp;select&nbsp;one&nbsp;response&nbsp;in&nbsp;each)
                            </div>
                            <table class="table table-borderless align-middle">
                                <tbody>
                                    <tr>
                                        <td>Interesting&nbsp;Content</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="296f202c"
                                                name="study_habits_and_routine[3][interesting_content]" value="Yes">
                                            <label for="296f202c"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="c044a487"
                                                name="study_habits_and_routine[3][interesting_content]" value="No">
                                            <label for="c044a487"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="ce55a54f"
                                                name="study_habits_and_routine[3][interesting_content]"
                                                value="Prefer not to say">
                                            <label for="ce55a54f"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Upcoming&nbsp;Exams/Test</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="c933edad"
                                                name="study_habits_and_routine[3][upcoming_exam_or_test]" value="Yes">
                                            <label for="c933edad"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="fd0aa769"
                                                name="study_habits_and_routine[3][upcoming_exam_or_test]" value="No">
                                            <label for="fd0aa769"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="37a204ed"
                                                name="study_habits_and_routine[3][upcoming_exam_or_test]"
                                                value="Prefer not to say">
                                            <label for="37a204ed"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Parents&nbsp;Pressure</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="d14faaa0"
                                                name="study_habits_and_routine[3][parents_pressure]" value="Yes">
                                            <label for="d14faaa0"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="d858ceff"
                                                name="study_habits_and_routine[3][parents_pressure]" value="No">
                                            <label for="d858ceff"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="e9e89be9"
                                                name="study_habits_and_routine[3][parents_pressure]"
                                                value="Prefer not to say">
                                            <label for="e9e89be9"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Homework/Assignment</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="cb2af379"
                                                name="study_habits_and_routine[3][homework_or_assignment]" value="Yes">
                                            <label for="cb2af379"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="a9274b14"
                                                name="study_habits_and_routine[3][homework_or_assignment]" value="No">
                                            <label for="a9274b14"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="50040645"
                                                name="study_habits_and_routine[3][homework_or_assignment]"
                                                value="Prefer not to say">
                                            <label for="50040645"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Classmate/Peer&nbsp;pressure</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="96b7b6b6"
                                                name="study_habits_and_routine[3][classmate_or_peer_pressure]"
                                                value="Yes">
                                            <label for="96b7b6b6"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="a129153a"
                                                name="study_habits_and_routine[3][classmate_or_peer_pressure]"
                                                value="No">
                                            <label for="a129153a"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="bb0322bf"
                                                name="study_habits_and_routine[3][classmate_or_peer_pressure]"
                                                value="Prefer not to say">
                                            <label for="bb0322bf"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>My&nbsp;Daily&nbsp;routine</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="77cf1f29"
                                                name="study_habits_and_routine[3][my_daily_routine]" value="Yes">
                                            <label for="77cf1f29"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="981b9a01"
                                                name="study_habits_and_routine[3][my_daily_routine]" value="No">
                                            <label for="981b9a01"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="a9620b26"
                                                name="study_habits_and_routine[3][my_daily_routine]"
                                                value="Prefer not to say">
                                            <label for="a9620b26"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Other&nbsp;reasons</td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="90a7bafc"
                                                name="study_habits_and_routine[3][other_reasons]" value="Yes">
                                            <label for="90a7bafc"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">yes</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="2500d9bf"
                                                name="study_habits_and_routine[3][other_reasons]" value="No">
                                            <label for="2500d9bf"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7 text-capitalize">no</label>
                                        </td>
                                        <td>
                                            <input hidden required type="radio" class="form-check-input" id="34c3c54b"
                                                name="study_habits_and_routine[3][other_reasons]"
                                                value="Prefer not to say">
                                            <label for="34c3c54b"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide no_study_survey_question">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 scroll" style="overflow-x: auto;">
                            <div class="fs-4 fw-500">Why didn't you study before or after school?</div>
                            <div class="text-muted">
                                (Please&nbsp;select&nbsp;one&nbsp;response&nbsp;in&nbsp;each)
                            </div>
                            <table class="table table-borderless align-middle">
                                <tbody>
                                    <tr>
                                        <td>No&nbsp;time to&nbsp;study</td>
                                        <td>
                                            <input hidden id="976e74e5" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_time_to_study]" value="Yes">
                                            <label for="976e74e5"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="bd70119e" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_time_to_study]" value="No">
                                            <label for="bd70119e"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="c0bdaf94" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_time_to_study]"
                                                value="Prefer not to say">
                                            <label for="c0bdaf94"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No&nbsp;Interesting content</td>
                                        <td>
                                            <input hidden id="72247587" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_interesting_content]" value="Yes">
                                            <label for="72247587"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="572ff741" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_interesting_content]" value="No">
                                            <label for="572ff741"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="7013d4e8" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_interesting_content]"
                                                value="Prefer not to say">
                                            <label for="7013d4e8"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No&nbsp;upcoming exam/test</td>
                                        <td>
                                            <input hidden id="a1f906ee" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_upcoming_exam_or_test]"
                                                value="Yes">
                                            <label for="a1f906ee"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="5f38ccc3" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_upcoming_exam_or_test]"
                                                value="No">
                                            <label for="5f38ccc3"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="1ca5c13f" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_upcoming_exam_or_test]"
                                                value="Prefer not to say">
                                            <label for="1ca5c13f"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No&nbsp;pressure from&nbsp;anyone</td>
                                        <td>
                                            <input hidden id="ddddefdb" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_pressure_from_anyone]"
                                                value="Yes">
                                            <label for="ddddefdb"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="a8c3abc8" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_pressure_from_anyone]"
                                                value="No">
                                            <label for="a8c3abc8"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="e104a481" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_pressure_from_anyone]"
                                                value="Prefer not to say">
                                            <label for="e104a481"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No&nbsp;homework/assignment</td>
                                        <td>
                                            <input hidden id="58d745b7" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_homework_or_assignment]"
                                                value="Yes">
                                            <label for="58d745b7"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="431ff6b5" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_homework_or_assignment]"
                                                value="No">
                                            <label for="431ff6b5"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="b2c994a2" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][no_homework_or_assignment]"
                                                value="Prefer not to say">
                                            <label for="b2c994a2"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>None&nbsp;of&nbsp;my classmate&nbsp;study</td>
                                        <td>
                                            <input hidden id="1fcdc4dc" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][none_of_my_classmate_study]"
                                                value="Yes">
                                            <label for="1fcdc4dc"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="19731479" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][none_of_my_classmate_study]"
                                                value="No">
                                            <label for="19731479"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="6cb3cd4a" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][none_of_my_classmate_study]"
                                                value="Prefer not to say">
                                            <label for="6cb3cd4a"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>I&nbsp;never&nbsp;study</td>
                                        <td>
                                            <input hidden id="a16a733d" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][i_never_study]" value="Yes">
                                            <label for="a16a733d"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="8acc7d42" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][i_never_study]" value="No">
                                            <label for="8acc7d42"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>

                                        <td>
                                            <input hidden id="6aa73694" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][i_never_study]"
                                                value="Prefer not to say">
                                            <label for="6aa73694"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Other&nbsp;reasons</td>
                                        <td>
                                            <input hidden id="e7c79264" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][other_reasons]" value="Yes">
                                            <label for="e7c79264"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes</label>
                                        </td>
                                        <td>
                                            <input hidden id="e86e9518" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][other_reasons]" value="No">
                                            <label for="e86e9518"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No</label>
                                        </td>
                                        <td>
                                            <input hidden id="ef407665" required type="radio" class="form-check-input"
                                                name="study_habits_and_routine[4][other_reasons]"
                                                value="Prefer not to say">
                                            <label for="ef407665"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Prefer
                                                not to
                                                say</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide future_self_reflection">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-4">
                            <label class="fs-4 fw-500" for="future_self_reflection[1]">What do you think you
                                will be
                                doing 5 years from Now?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="future_self_reflection[1]" id="future_self_reflection[1]"
                                class="form-select with-modal" required>
                                <option value="">Select an option</option>
                                <option>Working</option>
                                <option>Studying</option>
                                <option>Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-11 col-xxl-10 scroll">
                            <div class="fs-4 fw-500">How important are the following things in the decisions you
                                make about your future education/occupation?
                                <span
                                    class="text-muted"><small>(Please&nbsp;select&nbsp;one&nbsp;response&nbsp;in&nbsp;each)</small></span>
                            </div>
                            <table class="table table-borderless" id="secondConflicting">
                                <thead>
                                    <tr class="text-center">
                                        <th></th>
                                        <th>Not&nbsp;important</th>
                                        <th>Somewhat&nbsp;important</th>
                                        <th>Important</th>
                                        <th>Very&nbsp;important</th>
                                        <th>Prefer&nbsp;Not to say</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Parents&nbsp;expectation</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][parents_expectations]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Friends&nbsp;Influence</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][friends_influence]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>School/college&nbsp;or university&nbsp;grades</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][school_college_university]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Favorite&nbsp;subject</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][favorite_subject_importance]"
                                                step="33.33" max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Talent/Skills</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][talent_skills]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hobbies</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][hobbies]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Scope/Market&nbsp;Demand of&nbsp;Occupation</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][scope_market_demand]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Financial&nbsp;support&nbsp;for education&nbsp;or&nbsp;training</td>
                                        <td colspan="5">
                                            <input type="range" value="0" class="form-range"
                                                name="future_self_reflection[2][financial_support_for_edu]" step="33.33"
                                                max="133.33333333333334">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook" id="secondButtonNext">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[1]">Once you finished school/ university/
                                internship, what do you think you will do?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-4">
                            <select name="ambitions[1]" id="ambitions[1]" class="form-select" required>
                                <option value="">Select an option</option>
                                <option>Training Course</option>
                                <option>University Studies</option>
                                <option>Job</option>
                                <option>Undecided</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)" id="secondButtonPrevious">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[2]">Who has influenced your previous
                                decision?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="ambitions[2]" id="ambitions[2]" class="ambitions_2 form-select" required>
                                <option value="">Select an option</option>
                                <option>Parents</option>
                                <option>Siblings</option>
                                <option>Relatives</option>
                                <option>Friends</option>
                                <option>Counsellor</option>
                                <option>Nobody</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[4]">Are you looking to adopt same
                                professions
                                as</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="ambitions[4]" id="ambitions[4]" class="form-select" required>
                                <option value="">Select an option</option>
                                <option>Father</option>
                                <option>Mother</option>
                                <option>Siblings</option>
                                <option>Friends</option>
                                <option>Relatives</option>
                                <option>Other Person</option>
                                <option>No one you know</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[5]">Does the choice of your degree
                                coincide
                                with what your parents want you to do?</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="ambitions[5]" id="ambitions[5]" class="form-select" required>
                                <option value="">Select an option</option>
                                <option>Yes</option>
                                <option>No</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[6]">Mark the reason why you would choose
                                your profession</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="ambitions[6]" id="ambitions[6]" class="form-select" required>
                                <option value="">Select an option</option>
                                <option>Acquire leadership/responsibility</option>
                                <option>High salary</option>
                                <option>Work-life Balance</option>
                                <option>Better Job Market</option>
                                <option>Society and welfare</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-4">
                            <label class="fs-4 fw-500" for="ambitions[7]">Mark the reason why you are going to
                                study
                                your university degree</label>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <select name="ambitions[7]" id="ambitions[7]" class="form-select" required>
                                <option value="">Select an option</option>
                                <option>Match Interest and aptitude</option>
                                <option>Match skill set</option>
                                <option>Increase job market</option>
                                <option>Parent influence</option>
                                <option>Friend/peer influence</option>
                                <option>Other reason</option>
                                <option>Prefer not to say</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="job_question swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <label for="ambitions[8]" class="fs-4 fw-500">
                                With which of these statements do you agree with most. My choice will provide me
                                a
                                job
                                <span class="text-muted">
                                    <small>(you&nbsp;may&nbsp;choose&nbsp;multiple)</small>
                                </span>
                            </label>
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" role="switch" id="ambitions[8][1]"
                                    name="ambitions[8][1]">
                                <label class="form-check-label" for="ambitions[8][1]">Which is dynamic,
                                    interesting,
                                    varied</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" role="switch" id="ambitions[8][2]"
                                    name="ambitions[8][2]">
                                <label class="form-check-label" for="ambitions[8][2]">With social prestige and
                                    admired by others</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" role="switch" id="ambitions[8][3]"
                                    name="ambitions[8][3]">
                                <label class="form-check-label" for="ambitions[8][3]">With a stable
                                    salary</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" role="switch" id="ambitions[8][4]"
                                    name="ambitions[8][4]">
                                <label class="form-check-label" for="ambitions[8][4]">Where you can learn
                                    continuously</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" role="switch" id="ambitions[8][5]"
                                    name="ambitions[8][5]">
                                <label class="form-check-label" for="ambitions[8][5]">Which allows me to be
                                    original
                                    and creative</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" role="switch" id="ambitions[8][6]"
                                    name="ambitions[8][6]">
                                <label class="form-check-label" for="ambitions[8][6]">Prefer not to say</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            onclick="changeQuestion(1)" class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide skills_aquired_survey">
                    <div class="row justify-content-center">
                        <div class="col-lg-9 scroll">
                            <div class="fs-4 fw-500">Which of the following skills have you acquired?</div>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Job&nbsp;or&nbsp;education&nbsp;program&nbsp;search</td>
                                        <td>
                                            <input id="f32ff463" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][job_or_edu]" value="Yes at institute">
                                            <label for="f32ff463"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                at
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="2006109f" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][job_or_edu]" value="Yes out of institute">
                                            <label for="2006109f"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                out
                                                of
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="d6ca1d42" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][job_or_edu]" value="No never">
                                            <label for="d6ca1d42"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No
                                                never</label>
                                        </td>
                                        <td>
                                            <input id="27cb0e44" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][job_or_edu]" value="Not applicable">
                                            <label for="27cb0e44"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Not
                                                applicable</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Resume&nbsp;Creation</td>
                                        <td>
                                            <input id="c972a7a0" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][resume_creation]" value="Yes at institute">
                                            <label for="c972a7a0"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                at
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="37e345fa" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][resume_creation]" value="Yes out of institute">
                                            <label for="37e345fa"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                out
                                                of
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="35835f41" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][resume_creation]" value="No never">
                                            <label for="35835f41"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No
                                                never</label>
                                        </td>
                                        <td>
                                            <input id="59fbf8dd" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][resume_creation]" value="Not applicable">
                                            <label for="59fbf8dd"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Not
                                                applicable</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cover&nbsp;letter&nbsp;Creation</td>
                                        <td>
                                            <input id="2ed2aaa6" hidden required type="radio" class="form-check-input"
                                                name="ambitions[9][cover_letter_creation]" value="Yes at institute">
                                            <label for="2ed2aaa6"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                at
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="567d2f6f" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][cover_letter_creation]"
                                                value="Yes out of institute">
                                            <label for="567d2f6f"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                out
                                                of
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="c7a538c0" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][cover_letter_creation]"
                                                value="No never">
                                            <label for="c7a538c0"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No
                                                never</label>
                                        </td>
                                        <td>
                                            <input id="56828a21" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][cover_letter_creation]"
                                                value="Not applicable">
                                            <label for="56828a21"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Not
                                                applicable</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Interview&nbsp;preparation</td>
                                        <td>
                                            <input id="f2e53d5d" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][interview_prep]"
                                                value="Yes at institute">
                                            <label for="f2e53d5d"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                at
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="fe1c934d" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][interview_prep]"
                                                value="Yes out of institute">
                                            <label for="fe1c934d"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                out
                                                of
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="9c169d6a" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][interview_prep]"
                                                value="No never">
                                            <label for="9c169d6a"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No
                                                never</label>
                                        </td>
                                        <td>
                                            <input id="21e0a3a1" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][interview_prep]"
                                                value="Not applicable">
                                            <label for="21e0a3a1"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Not
                                                applicable</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Presentation</td>
                                        <td>
                                            <input id="bf3619dd" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][presentation]"
                                                value="Yes at institute">
                                            <label for="bf3619dd"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                at
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="097f70b7" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][presentation]"
                                                value="Yes out of institute">
                                            <label for="097f70b7"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Yes
                                                out
                                                of
                                                institute</label>
                                        </td>
                                        <td>
                                            <input id="6de5745f" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][presentation]"
                                                value="No never">
                                            <label for="6de5745f"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">No
                                                never</label>
                                        </td>
                                        <td>
                                            <input id="8967da25" hidden required type="radio"
                                                class="form-check-input" name="ambitions[9][presentation]"
                                                value="Not applicable">
                                            <label for="8967da25"
                                                class="py-2 px-3 rounded cursor-pointer bg-white fs-7">Not
                                                applicable</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6 mt-3">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="button" title="Press enter to show next question"
                            style="background-color:#6CBBDE" onclick="changeQuestion(1)"
                            class="btn btn-sm text-white next hoverChangerLook">Next</button>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="fs-4 fw-500">Please Rate your employability skills: (out of 5)</div>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>Communication Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="communication_ratings" name="influencer[2]" required
                                                step="20">
                                            <datalist id="communication_ratings" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Teamwork Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="teamwork_ratings" name="influencer[3]" required step="20">
                                            <datalist id="teamwork_ratings" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Problem Solving Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="problem_solving_ratings" name="influencer[4]" required
                                                step="20">
                                            <datalist id="problem_solving_ratings"
                                                class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Initiative and Enterprise Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="initiative_ratings" name="influencer[5]" required
                                                step="20">
                                            <datalist id="initiative_ratings" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Planning and Organizing Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="planning_ratings" name="influencer[6]" required step="20">
                                            <datalist id="planning_ratings" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Self-management Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="self_management_ratings" name="influencer[7]" required
                                                step="20">
                                            <datalist id="self_management_ratings"
                                                class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Learning Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="learning_rating" name="influencer[8]" required step="20">
                                            <datalist id="learning_rating" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Technology Skills:</p>
                                            <input type="range" value="0" class="form-range"
                                                list="techNology_rating" name="influencer[9]" required step="20">
                                            <datalist id="techNology_rating" class="d-flex justify-content-between">
                                                <option value="0" label="0"></option>
                                                <option value="20" label="1"></option>
                                                <option value="40" label="2"></option>
                                                <option value="60" label="3"></option>
                                                <option value="80" label="4"></option>
                                                <option value="100" label="5"></option>
                                            </datalist>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 text-center mb-6">
                        <button type="button" style="background-color:#6CBBDE" class="btn btn-sm text-white previous hoverChangerLook"
                            onclick="changeQuestion(0)">Previous</button>
                        <button type="submit" title="Press enter to show next question"
                            style="background-color:#6CBBDE" class="btn btn-sm text-white next hoverChangerLook">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="degreeOfInfluencerStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
        tabindex="-1" aria-labelledby="degreeOfInfluencerStaticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="fs-4 mb-3 fw-500" for="ambitions[3]">State the degree to which
                            that person has influenced you</label>
                        <input type="range" value="0" form="profileForm" class="form-range"
                            id="ambitions[3]" name="ambitions[3]" list="ambitions[3]_ratings" step="20">
                        <datalist id="ambitions[3]_ratings" class="d-flex justify-content-between">
                            <option value="0" label="0"></option>
                            <option value="20" label="1"></option>
                            <option value="40" label="2"></option>
                            <option value="60" label="3"></option>
                            <option value="80" label="4"></option>
                            <option value="100" label="5"></option>
                        </datalist>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="background-color:#6CBBDE"
                        class="btn btn-sm text-white hoverChangerLook">Next</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" style="background-color:#6CBBDE"
                        class="btn btn-sm text-white hoverChangerLook">Next</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('assets/plugins/jQuery/jQuery.min.js') }}"></script>
@endpush
@push('script')
    <script>
        var slider;
        $(document).ready(function() {
            slider = new Swiper('.swiper', {
                allowTouchMove: false,
                autoHeight: true,
                notify: {
                    enabled: true
                },
            });
        });
    </script>
@endpush
@push('script')
    <script>
        $(document).on('keypress', detectReturnKey);
        $(document).on('show.bs.modal', () => {
            $(document).off('keypress');
        });

        $(document).on('hide.bs.modal', () => {
            $(document).on('keypress', detectReturnKey);
        });

        // ask another question in student profile completion
        try {
            document.querySelector('.with-modal').oninput = askAnotherQuestion;
        } catch {
            // do nothing
        }

        function detectReturnKey(e) {
            if (e.key == 'Enter') {
                e.preventDefault();
                changeQuestion(1);
            }
        }

        /*
            - flag = 0 means previous
            - falg = 1 means next
        */
        function changeQuestion(flag) {
            validated = false;
            if (flag) {
                if (validateQuestion(slider.slides[slider.activeIndex])) {
                    slider.slideNext();
                }
                return;
            } else {
                slider.slidePrev();
            }
        }

        function validateQuestion(question) {
            if (question.classList.contains("study_survey_question")) {
                // Get all rows in the swiper-slide
                const rows = document.querySelectorAll(".study_survey_question .table tbody tr");

                // Check if at least one option is selected in each row
                let isValid = true;
                for (const row of rows) {
                    const radioButtons = row.querySelectorAll('input[type="radio"]');
                    let isOptionSelected = false;

                    for (const radioButton of radioButtons) {
                        if (radioButton.checked) {
                            isOptionSelected = true;
                            break;
                        }
                    }

                    if (!isOptionSelected) {
                        // Add the 'invalid' class to all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.add('invalid');
                        });

                        isValid = false; // Validation failed, one row doesn't have an option selected.
                    } else {
                        // Remove the 'invalid' class from all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.remove('invalid');
                        });
                    }
                }
                if (!isValid) {
                    // toastr.error("Please answer all questions");
                    return false;
                }

                return isValid; // All rows have at least one option selected.
            }

            if (question.classList.contains("no_study_survey_question")) {
                // Get all rows in the swiper-slide
                const rows = document.querySelectorAll(".no_study_survey_question .table tbody tr");

                // Check if at least one option is selected in each row
                let isValid = true;
                for (const row of rows) {
                    const radioButtons = row.querySelectorAll('input[type="radio"]');
                    let isOptionSelected = false;

                    for (const radioButton of radioButtons) {
                        if (radioButton.checked) {
                            isOptionSelected = true;
                            break;
                        }
                    }

                    if (!isOptionSelected) {
                        // Add the 'invalid' class to all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.add('invalid');
                        });

                        isValid = false; // Validation failed, one row doesn't have an option selected.
                    } else {
                        // Remove the 'invalid' class from all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.remove('invalid');
                        });
                    }
                }
                if (!isValid) {
                    // toastr.error("Please answer all questions");
                    return false;
                }

                return isValid; // All rows have at least one option selected.
            }

            if (question.classList.contains("skills_aquired_survey")) {
                // Get all rows in the swiper-slide
                const rows = document.querySelectorAll(".skills_aquired_survey .table tbody tr");

                // Check if at least one option is selected in each row
                let isValid = true;
                for (const row of rows) {
                    const radioButtons = row.querySelectorAll('input[type="radio"]');
                    let isOptionSelected = false;

                    for (const radioButton of radioButtons) {
                        if (radioButton.checked) {
                            isOptionSelected = true;
                            break;
                        }
                    }

                    if (!isOptionSelected) {
                        // Add the 'invalid' class to all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.add('invalid');
                        });

                        isValid = false; // Validation failed, one row doesn't have an option selected.
                    } else {
                        // Remove the 'invalid' class from all radio buttons in the current row
                        radioButtons.forEach(radioButton => {
                            radioButton.classList.remove('invalid');
                        });
                    }
                }
                if (!isValid) {
                    // toastr.error("Please answer all questions");
                    return false;
                }

                return isValid; // All rows have at least one option selected.
            }

            if (question.classList.contains("job_question")) {
                var checkboxes = question.querySelectorAll('input[type="checkbox"]');
                var checkedOne = false;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        checkedOne = true;
                        break;
                    }
                }

                if (!checkedOne) {
                    toastr.error("Please select at least one option");
                    return false;
                }
            }

            const inputs = question.querySelectorAll('input');
            const selects = question.querySelectorAll('select');
            const textareas = question.querySelectorAll('textarea');

            var flag = false;
            inputs.forEach(input => {
                if (input.checkValidity()) {
                    flag = true;
                    input.classList.remove('invalid');
                } else {
                    flag = false;
                    input.classList.add('invalid');
                }
            });
            selects.forEach(select => {
                if (select.checkValidity()) {
                    flag = true;
                    select.classList.remove('invalid');
                } else {
                    flag = false;
                    select.classList.add('invalid');
                }
            });
            textareas.forEach(textarea => {
                if (textarea.checkValidity()) {
                    flag = true;
                    textarea.classList.remove('invalid');
                } else {
                    flag = false;
                    textarea.classList.add('invalid');
                }
            });

            // validate if the slide was a question
            if (inputs.length || selects.length || textareas.length) {
                return flag;
            } else {
                return true;
            }
        }

        function askAnotherQuestion(e) {
            const working = 1;
            const studying = 2;
            const other = 3;
            const selected = e.target.selectedIndex;
            var modal;
            try {
                modal = document.querySelector('#staticBackdrop');
            } catch (e) {
                console.log(e);
            }

            switch (selected) {
                case working:
                    question = `<label class="fs-4 fw-500">
            Why do you think you will be working 5 years from now?
                </label>
                <span class="text-muted">
                    (you&nbsp;may&nbsp;choose&nbsp;multiple)
                </span>
                <div class="form-check form-switch">
                    <input class="form-check-input" form="profileForm" type="checkbox" role="switch" id="future_self_reflection[1][working][1]" name="future_self_reflection[1][working][1]">
                    <label class="form-check-label" for="future_self_reflection[1][working][1]">Financial Independence</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][2]" name="future_self_reflection[1][working][2]">
                    <label class="form-check-label" for="future_self_reflection[1][working][2]">Education Requirements Fulfilled</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][3]" name="future_self_reflection[1][working][3]">
                    <label class="form-check-label" for="future_self_reflection[1][working][3]">Family Support</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][4]" name="future_self_reflection[1][working][4]">
                    <label class="form-check-label" for="future_self_reflection[1][working][4]">Societal Pressure</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][5]" name="future_self_reflection[1][working][5]">
                    <label class="form-check-label" for="future_self_reflection[1][working][5]">Lack of Vision</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][6]" name="future_self_reflection[1][working][6]">
                    <label class="form-check-label" for="future_self_reflection[1][working][6]">Other Reason</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][working][7]" name="future_self_reflection[1][working][7]">
                    <label class="form-check-label" for="future_self_reflection[1][working][7]">Prefer not to say</label>
                </div>`;
                    break;
                case studying:
                    question = `<label class="fs-4 fw-500">
            Why do you think you will be studying 5 years from now?
                </label>
                <span class="text-muted">
                    (you&nbsp;may&nbsp;choose&nbsp;multiple)
                </span>
                <div class="form-check form-switch">
                    <input class="form-check-input" form="profileForm" type="checkbox" role="switch" id="future_self_reflection[1][studying][1]" name="future_self_reflection[1][studying][1]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][1]">Lack of Vision</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][2]" name="future_self_reflection[1][studying][2]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][2]">Lack of Skills</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][3]" name="future_self_reflection[1][studying][3]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][3]">Lack of opportunities</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][4]" name="future_self_reflection[1][studying][4]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][4]">Societal Pressure</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][5]" name="future_self_reflection[1][studying][5]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][5]">Acquire Higher Education</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][6]" name="future_self_reflection[1][studying][6]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][6]">Other Reason</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" form="profileForm" role="switch" id="future_self_reflection[1][studying][7]" name="future_self_reflection[1][studying][7]">
                    <label class="form-check-label" for="future_self_reflection[1][studying][7]">Prefer not to say</label>
                </div>`;
                    break;
                case other:
                    changeQuestion(1);
                    return;
                default:
                    changeQuestion(1);
                    return;
            }

            function validateModal() {
                if (true) {
                    var checkboxes = document.querySelectorAll('#staticBackdrop input[type="checkbox"]');
                    var checkedOne = false;
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkedOne = true;
                            break;
                        }
                    }

                    if (!checkedOne) {
                        toastr.error("Please select at least one option");
                        return false;
                    }
                    modal.hide();
                    changeQuestion(1);
                } else {
                    document.querySelector('#staticBackdrop .modal-body').querySelector('select').classList.add('invalid');
                }
            }
            modal.querySelector('#staticBackdrop .modal-body').innerHTML = question;
            try {
                modal.querySelector('#staticBackdrop .modal-header').remove(); // remove header
            } catch (e) {
                console.log(e);
            }

            try {
                modal.querySelector('#staticBackdrop .modal-footer button').addEventListener('click', validateModal);
            } catch (e) {
                console.log(e);
            }
            // instanctiate modal to show
            modal = new bootstrap.Modal(modal);
            modal.show();

        }

        // if the user is influenced by none, then don't ask for degree of influence
        document.getElementsByClassName("ambitions_2")[0].onchange = (e) => {
            const next = 1;
            const selectedOption = e.target.selectedOptions[0].value;
            if (selectedOption == "Nobody" || selectedOption == "Prefer not to say") {
                // show next question
                changeQuestion(next);
            } else {
                const degreeOfInfluencerStaticBackdrop = new bootstrap.Modal(document.getElementById(
                    "degreeOfInfluencerStaticBackdrop"));
                degreeOfInfluencerStaticBackdrop.show();
                document.querySelector("#degreeOfInfluencerStaticBackdrop .modal-footer button").onclick = (e) => {
                    degreeOfInfluencerStaticBackdrop.hide();
                    changeQuestion(next);
                }
            }
        }
        var conflictingTable = document.getElementById('conflicting');
        var secondConflicting=document.getElementById('secondConflicting');
        var nextButton = document.getElementById('buttonNext');
        var secondButtonNext=document.getElementById('secondButtonNext');
        var previousButton = document.getElementById('buttonPrevious');
        var secondButtonPrevious=document.getElementById('secondButtonPrevious')
        nextButton.addEventListener('click', function() {
            conflictingTable.style.visibility = "hidden";
        });
        secondButtonNext.addEventListener('click', function() {
            secondConflicting.style.visibility = "hidden";
        });
        previousButton.addEventListener('click', function() {
            conflictingTable.style.visibility = "visible";
        });
        secondButtonPrevious.addEventListener('click',function(){
            secondConflicting.style.visibility="visible";
        })
    </script>
@endpush

@extends('layouts.user', ['pagename' => 'Edit Profile'])
@section('css')
    <style>
        .form-control,
        .form-select {
            transition-property: shadow;
            transition-duration: 250ms;
            box-shadow: 0px 3px 3px 1px var(--special) !important;
        }

        .others {
            box-shadow: 0px 3px 3px 1px var(--bs-secondary) !important;
        }

        .form-control:focus {
            box-shadow: 0px 3px 3px 1px var(--special) !important;
        }

        .swal2-popup .swal2-styled.swal2-confirm {
            background-color: rgb(238, 21, 21) !important;
        }

        .swal2-popup .swal2-styled.swal2-cancel {
            background-color: rgb(78, 115, 235) !important;
        }
        .adjustedWidth {
            width: 120px !important;
        }

        @media(max-width:480px) {
            .buttonRow button {
                margin-top: 5px !important;
            }
        }

        @media(max-width:355px) {
            .buttonRow a {
                margin-top:5px!important;
            }
        }
        .saveChanges:hover{
            background-color:#02618a!important;
        }
        .cancelChanges:hover{
            background-color:black!important;
        }
        .deleteChanges:hover{
            background-color:red!important;
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->

            <!-- update profile -->
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 bg-white sticky-top">
                        <div class="d-flex px-3 pb-lg-3">
                            <button class="btn mt-3 btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar">
                                <i class="fa-solid fa-bars fs-4"></i>
                            </button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="main">
                            <div class="floating"></div>
                            <div class="p-0 p-lg-4">
                                <div class="card border-0 bg-transparent">
                                    <div class="card-body">
                                        <div class="inner p-3">
                                            <form action="{{ route('user.profile.update', $userProfile->id) }}"
                                                id="editProfile" method="POST" class="row g-3 align-items-baseline">
                                                @csrf @method('PUT')
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">First Name</label>
                                                        <input type="text" name="name" id="name"
                                                            value="{{ $user->name ?? '' }}"
                                                            class="@error('name') is-invalid @enderror form-control">
                                                            @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="last_name">Last Name</label>
                                                        <input type="text" name="last_name" id="last_name"
                                                            value="{{ $user->last_name ?? '' }}"
                                                            class="@error('last_name') is-invalid @enderror form-control">
                                                            @error('last_name')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" name="email" id="email"
                                                            value="{{ $user->email ?? '' }}"
                                                            class="@error('email') is-invalid @enderror form-control">
                                                            @error('email')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="date_of_birth">Date of Birth </label>
                                                        <input type="date" name="date_of_birth" id="date_of_birth"
                                                            value="{{ date('Y-m-d', strtotime($userProfile->date_of_birth)) }}"
                                                            class="@error('date_of_birth') is-invalid @enderror form-control">
                                                            @error('date_of_birth')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="city">Country</label>
                                                        <select name="city" id="city"
                                                            class="@error('city') is-invalid @enderror form-select">
                                                            @foreach ($countryNames as $country)
                                                                <option @selected($userProfile->city == $country)>{{ $country }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @error('city')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="favorite_subject">Favorite Subject</label>
                                                        <input type="text" name="favorite_subject" id="favorite_subject"
                                                            value="{{ $userProfile->favorite_subject ?? '' }}"
                                                            class="@error('favorite_subject') is-invalid @enderror form-control">
                                                            @error('favorite_subject')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="easiest_subject">Easiest Subject</label>
                                                        <input type="text" name="easiest_subject" id="easiest_subject"
                                                            value="{{ $userProfile->easiest_subject ?? '' }}"
                                                            class="@error('easiest_subject') is-invalid @enderror form-control">
                                                            @error('easiest_subject')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="difficult_subject">Difficult Subject</label>
                                                        <input type="text" name="difficult_subject"
                                                            id="difficult_subject"
                                                            value="{{ $userProfile->difficult_subject ?? '' }}"
                                                            class="@error('difficult_subject') is-invalid @enderror form-control">
                                                            @error('difficult_subject')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="average_grade_point">Average Grade Point</label>
                                                        <select name="average_grade_point" id="average_grade_point"
                                                            class="@error('average_grade_point') is-invalid @enderror form-select">
                                                            <option @selected($userProfile->average_grade_point == 'A-Grade Student (80% and above)')>A-Grade Student (80% and
                                                                above)</option>
                                                            <option @selected($userProfile->average_grade_point == 'B-Grade Student (70-80%)')>B-Grade Student (70-80%)
                                                            </option>
                                                            <option @selected($userProfile->average_grade_point == 'C-Grade Student (60-70%)')>C-Grade Student (60-70%)
                                                            </option>
                                                            <option @selected($userProfile->average_grade_point == 'D-Grade Student (50-60%)')>D-Grade Student (50-60%)
                                                            </option>
                                                        </select>
                                                        @error('average_grade_point')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="father_education">Father Education</label>
                                                        <select name="father_education" id="father_education"
                                                            class="@error('father_education') is-invalid @enderror form-control">
                                                            <option value="">Select an option</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Not qualified' ? 'selected' : '' }}
                                                                value="Not qualified">Not qualified</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Primary Education' ? 'selected' : '' }}
                                                                value="Primary Education">Primary Education</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Secondary Education' ? 'selected' : '' }}
                                                                value="Secondary Education">Secondary Education</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Vocational Training' ? 'selected' : '' }}
                                                                value="Vocational Training">Vocational Training</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Bachelors' ? 'selected' : '' }}
                                                                value="Bachelors">Bachelors</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Masters' ? 'selected' : '' }}
                                                                value="Masters">Masters</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'PhD' ? 'selected' : '' }}
                                                                value="PhD">PhD</option>
                                                            <option
                                                                {{ $userProfile->father_education == 'Prefer not to say' ? 'selected' : '' }}
                                                                value="Prefer not to say">Prefer not to say</option>
                                                        </select>
                                                        @error('father_education')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="father_occupation">Father Occupation</label>
                                                        <select name="father_occupation" id="father_occupation"
                                                            class="@error('father_occupation') is-invalid @enderror form-control">
                                                            <option value="">Select an option</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'Unemployed' ? 'selected' : '' }}
                                                                value="Unemployed">Unemployed</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'Unskilled' ? 'selected' : '' }}
                                                                value="Unskilled">Unskilled</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'Semi-skilled job' ? 'selected' : '' }}
                                                                value="Semi-skilled job">Semi-skilled job</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'High skilled job' ? 'selected' : '' }}
                                                                value="High skilled job">High skilled job</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'Business' ? 'selected' : '' }}
                                                                value="Business">Business</option>
                                                            <option
                                                                {{ $userProfile->father_occupation == 'Prefer not to say' ? 'selected' : '' }}
                                                                value="Prefer not to say">Prefer not to say</option>
                                                        </select>
                                                        @error('father_occupation')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mother_education">Mother Education</label>
                                                        <select name="mother_education" id="mother_education"
                                                            class="@error('mother_education') is-invalid @enderror form-control">
                                                            <option value="">Select an option</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Not qualified' ? 'selected' : '' }}
                                                                value="Not qualified">Not qualified</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Primary Education' ? 'selected' : '' }}
                                                                value="Primary Education">Primary Education</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Secondary Education' ? 'selected' : '' }}
                                                                value="Secondary Education">Secondary Education</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Vocational Training' ? 'selected' : '' }}
                                                                value="Vocational Training">Vocational Training</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Bachelors' ? 'selected' : '' }}
                                                                value="Bachelors">Bachelors</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Masters' ? 'selected' : '' }}
                                                                value="Masters">Masters</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'PhD' ? 'selected' : '' }}
                                                                value="PhD">PhD</option>
                                                            <option
                                                                {{ $userProfile->mother_education == 'Prefer not to say' ? 'selected' : '' }}
                                                                value="Prefer not to say">Prefer not to say</option>
                                                        </select>
                                                        @error('mother_education')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mother_occupation">Mother Occupation</label>
                                                        <select name="mother_occupation" id="mother_occupation"
                                                            class="@error('mother_occupation') is-invalid @enderror form-control">
                                                            <option value="">Select an option</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'Unemployed' ? 'selected' : '' }}
                                                                value="Unemployed">Unemployed</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'Unskilled' ? 'selected' : '' }}
                                                                value="Unskilled">Unskilled</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'Semi-skilled job' ? 'selected' : '' }}
                                                                value="Semi-skilled job">Semi-skilled job</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'High skilled job' ? 'selected' : '' }}
                                                                value="High skilled job">High skilled job</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'Business' ? 'selected' : '' }}
                                                                value="Business">Business</option>
                                                            <option
                                                                {{ $userProfile->mother_occupation == 'Prefer not to say' ? 'selected' : '' }}
                                                                value="Prefer not to say">Prefer not to say</option>
                                                        </select>
                                                        @error('mother_occupation')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="parents_retired">Parents Retired</label><br>
                                                        <div class="form-check form-check-inline">
                                                            <input id="father" class="form-check-input" type="radio"
                                                                id="parents_retired"
                                                                @if ($userProfile->parents_retired == 'Father') checked @endif
                                                                name="parents_retired" value="Father"
                                                                class="btn-check
                                                        @error('parents_retired') is-invalid @enderror">
                                                            <label for="father" class="form-check-label">Father</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input id="mother" class="form-check-input" type="radio"
                                                                id="parents_retired"
                                                                @if ($userProfile->parents_retired == 'Mother') checked @endif
                                                                name="parents_retired" value="Mother"
                                                                class="btn-check
                                                        @error('parents_retired') is-invalid @enderror">
                                                            <label for="mother" class="form-check-label">Mother</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input id="both" class="form-check-input" type="radio"
                                                                @if ($userProfile->parents_retired == 'Both') checked @endif
                                                                id="parents_retired" name="parents_retired"
                                                                value="Both"
                                                                class="btn-check @error('parents_retired') is-invalid
                                                        @enderror">
                                                            <label for="both" class="form-check-label">Both</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input id="neither" class="form-check-input" type="radio"
                                                                @if ($userProfile->parents_retired == 'Neither') checked @endif
                                                                id="parents_retired" name="parents_retired"
                                                                value="Neither"
                                                                class="btn-check @error('parents_retired') is-invalid
                                                        @enderror">
                                                            <label for="neither" class="form-check-label">Neither</label>
                                                        </div>
                                                        @error('parents_retired')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="gender">Gender</label><br>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Male') checked @endif
                                                                name="gender" type="radio" id="Male"
                                                                value="Male">
                                                            <label class="form-check-label" for="Male">Male</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Female') checked @endif
                                                                name="gender" type="radio" id="Female"
                                                                value="Female">
                                                            <label class="form-check-label" for="Female">Female</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Other') checked @endif
                                                                name="gender" type="radio" id="Other"
                                                                value="Other">
                                                            <label class="form-check-label" for="Other">Other</label>
                                                        </div>
                                                        @error('gender')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <input type="hidden" name="profession" value="{{ $user->profession }}">
                                                    {{-- <div class="col-md-12 mb-3">
                                                        <div class="form-group">
                                                            <label for="profession">Profession</label><br>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" @if ($user->profession == 'Student') checked @endif
                                                                name="profession" type="radio" id="Student" value="Student">
                                                                <label class="form-check-label" for="Student">Student</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" @if ($user->profession == 'University Student') checked @endif
                                                                name="profession" type="radio" id="University Student"
                                                                value="University Student">
                                                                <label class="form-check-label" for="University Student">
                                                                    University Student
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" @if ($user->profession == 'Professional') checked @endif
                                                                name="profession" type="radio" id="Professional"
                                                                value="Professional">
                                                                <label class="form-check-label" for="Professional">
                                                                    Professional
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" @if ($user->profession == 'Parent')
                                                                checked @endif name="profession" type="radio" id="Parent"
                                                                value="Parent">
                                                                <label class="form-check-label" for="Parent">
                                                                    Parent Looking for Child
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="row buttonRow">
                                                        <div class="diButton mt-3 ">
                                                            <input type="submit" value="Save Changes"
                                                                class="btn bg-special text-white saveChanges btn-sm adjustedWidth">
                                                            <a href="{{ route('dashboard') }}"
                                                                class="btn cancelChanges btn-secondary btn-sm adjustedWidth">Cancel Changes</a>
                                                            <!-- Delete Account Button -->
                                                            <button type="submit" form="deletRecordForm"
                                                                class="btn btn-danger btn-sm text-white delete-confirm adjustedWidth deleteChanges"
                                                                data-name="{{ $user->name }}"
                                                                data-original-title="Delete Account">
                                                                Delete Account
                                                            </button>
                                                        </div>
                                                    </div>

                                            </form>
                                            <!-- Delete Account Form -->
                                            <form id="deletRecordForm"
                                                action="{{ route('user.profile.destroy', $user->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>
            <!--/ update profile -->
        </div>
    </div>
    {{-- Sweet Alert JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        // for delete confirmation method
        $('.delete-confirm').click(function(event) {
            // var form = $(this).closest("form");
            var form = $('#deletRecordForm');
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Are you sure to delete your account permanantly?`,
                    text: "You won't be able to recover your account!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection

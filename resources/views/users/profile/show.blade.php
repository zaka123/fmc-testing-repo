@extends('layouts.user', ['pagename' => 'User Profile'])

@section('user-content')
    <style>
        .downloadProfile:hover {
            background-color: black !important;
        }
    </style>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->

            <!-- show profile -->
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 bg-white sticky-top">
                        <div class="d-flex px-3 pb-lg-3">
                            <button class="btn mt-3 btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar">
                                <i class="fa-solid fa-bars fs-4"></i>
                            </button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between gy-3">
                        <div class="mx-3" style="padding-top:6px">
                            <h4>My Profile</h4>
                        </div>
                        <div style="margin-right:35px">
                            {{-- <a href="{{ route('user.profile.edit', $user->userProfile->id) }}" class="btn btn-primary text-white downloadProfile">Edit Profile</a> --}}
                            <button id="downloadPdf" class="btn btn-success text-white downloadProfile">Download
                                Profile</button>
                        </div>
                    </div>
                    <div id="pdf-content" class="row gy-3 mb-5">
                        <div class="col-12 col-lg-4">
                            <div class="card h-auto">
                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="rounded-circle border" src="{{ asset($user->profilePhoto) }}"
                                            alt="User profile picture" width="150" height="150">
                                    </div>

                                    <h3 class="text-center headingFont">{{ $user->fullName }}</h3>

                                    <p class="text-muted text-center"><b>{{ $user->email }}</b></p>

                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <b>D.O.B:</b> <span
                                                class="float-right">{{ $user->userProfile->date_of_birth }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Country:</b> <span class="float-right">{{ $user->userProfile->city }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Favorite Subject:</b> <span
                                                class="float-right">{{ $user->userProfile->favorite_subject }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Easiest Subject:</b> <span
                                                class="float-right">{{ $user->userProfile->easiest_subject }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Difficult Subject:</b> <span
                                                class="float-right">{{ $user->userProfile->difficult_subject }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Average Grade Point:</b> <span
                                                class="float-right">{{ $user->userProfile->average_grade_point }}</span>
                                        </li>
                                    </ul>

                                    <div class="mt-3">
                                        <div class="card-header text-bg-dark text-center">Family Information</div>
                                        <div class="card-body">
                                            @if ($familyInfo && count($familyInfo) > 0)
                                                <div class="row gy-2" style="font-weight: 500;">
                                                    <div class="col-6">Father Education</div>
                                                    <div class="col-6">{{ $userProfile->father_education }}</div>
                                                    <div class="col-6">Father Occupation</div>
                                                    <div class="col-6">{{ $userProfile->father_occupation }}</div>
                                                    <div class="col-6">Mother Education</div>
                                                    <div class="col-6">{{ $userProfile->mother_education }}</div>
                                                    <div class="col-6">Mother Occupation</div>
                                                    <div class="col-6">{{ $userProfile->mother_occupation }}</div>
                                                    <div class="col-6">Retired Parents</div>
                                                    <div class="col-6">{{ $userProfile->parents_retired }}</div>
                                                    <div class="col-12 fw-bold fs-6">Family Assistance</div>
                                                    <div class="col-3 col-md-6">Father</div>
                                                    <div class="col-9 col-md-6">
                                                        @if ($familyInfo['father'] > 100)
                                                            Not Applicable
                                                        @else
                                                            <div class="progress bg-light rounded-0">
                                                                <div class="progress-bar text-warning bg-warning"
                                                                    style="width: {{ $familyInfo['father'] }}%"></div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-3 col-md-6">Mother</div>
                                                    <div class="col-9 col-md-6">
                                                        @if ($familyInfo['mother'] > 100)
                                                            Not Applicable
                                                        @else
                                                            <div class="progress bg-light rounded-0">
                                                                <div class="progress-bar text-warning bg-warning"
                                                                    style="width: {{ $familyInfo['mother'] }}%"></div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-3 col-md-6">Siblings</div>
                                                    <div class="col-9 col-md-6">
                                                        @if ($familyInfo['siblings'] > 100)
                                                            Not Applicable
                                                        @else
                                                            <div class="progress bg-light rounded-0">
                                                                <div class="progress-bar text-warning bg-warning"
                                                                    style="width: {{ $familyInfo['siblings'] }}%"></div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-3 col-md-6">Relatives</div>
                                                    <div class="col-9 col-md-6">
                                                        @if ($familyInfo['relatives'] > 100)
                                                            Not Applicable
                                                        @else
                                                            <div class="progress bg-light rounded-0">
                                                                <div class="progress-bar text-warning bg-warning"
                                                                    style="width: {{ $familyInfo['relatives'] }}%"></div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-3 col-md-6">Others</div>
                                                    <div class="col-9 col-md-6">
                                                        @if ($familyInfo['other_persons'] > 100)
                                                            Not Applicable
                                                        @else
                                                            <div class="progress bg-light rounded-0">
                                                                <div class="progress-bar text-warning bg-warning"
                                                                    style="width: {{ $familyInfo['other_persons'] }}%">
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @else
                                                <div class="alert alert-info" role="alert">No data found</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{-- <div class="card shadow mt-3" style="height: 49%;">
                                <h4 class="card-header text-bg-dark">Family Information</h4>
                                <div class="card-body">
                                    @if ($familyInfo && count($familyInfo) > 0)
                                        <div class="row gy-2" style="font-weight: 500;">
                                            <div class="col-6">Father Education</div>
                                            <div class="col-6">{{ $userProfile->father_education }}</div>
                                            <div class="col-6">Father Occupation</div>
                                            <div class="col-6">{{ $userProfile->father_occupation }}</div>
                                            <div class="col-6">Mother Education</div>
                                            <div class="col-6">{{ $userProfile->mother_education }}</div>
                                            <div class="col-6">Mother Occupation</div>
                                            <div class="col-6">{{ $userProfile->mother_occupation }}</div>
                                            <div class="col-6">Retired Parents</div>
                                            <div class="col-6">{{ $userProfile->parents_retired }}</div>
                                            <div class="col-12 fw-bold fs-6">Family Assistance</div>
                                            <div class="col-3 col-md-6">Father</div>
                                            <div class="col-9 col-md-6">
                                                @if ($familyInfo['father'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-warning bg-warning"
                                                            style="width: {{ $familyInfo['father'] }}%"></div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-3 col-md-6">Mother</div>
                                            <div class="col-9 col-md-6">
                                                @if ($familyInfo['mother'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-warning bg-warning"
                                                            style="width: {{ $familyInfo['mother'] }}%"></div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-3 col-md-6">Siblings</div>
                                            <div class="col-9 col-md-6">
                                                @if ($familyInfo['siblings'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-warning bg-warning"
                                                            style="width: {{ $familyInfo['siblings'] }}%"></div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-3 col-md-6">Relatives</div>
                                            <div class="col-9 col-md-6">
                                                @if ($familyInfo['relatives'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-warning bg-warning"
                                                            style="width: {{ $familyInfo['relatives'] }}%"></div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-3 col-md-6">Others</div>
                                            <div class="col-9 col-md-6">
                                                @if ($familyInfo['other_persons'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-warning bg-warning"
                                                            style="width: {{ $familyInfo['other_persons'] }}%"></div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-info" role="alert">No data found</div>
                                    @endif
                                </div>
                            </div> --}}
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="card h-auto ">
                                <div class="card-header text-bg-dark text-center">Study Habits And Routine</div>
                                <div class="card-body">
                                    @if ($studyHabitsAndRoutine->count() > 0)
                                        <div class="row gy-2" style="font-weight: 500;">
                                            <div class="col-7 col-lg-8">Morning study hours</div>
                                            <div class="col-5 col-lg-4 text-end">
                                                {{ $studyHabitsAndRoutine[0]['answer'] }}
                                                {{ Str::plural('hour', $studyHabitsAndRoutine[0]['answer']) }}</div>
                                            <div class="col-7 col-lg-8">Evening study hours</div>
                                            <div class="col-5 col-lg-4 text-end">
                                                {{ $studyHabitsAndRoutine[1]['answer'] }}
                                                {{ Str::plural('hour', $studyHabitsAndRoutine[1]['answer']) }}</div>
                                            <div class="col-12 fw-bold mt-3">Reason for home study</div>
                                            <div class="col-12">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <td>Interesting content</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['interesting_content'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Upcoming exam or test</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['upcoming_exam_or_test'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Parents pressure</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['parents_pressure'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Homework or assignment</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['homework_or_assignment'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Classmate or peer pressure</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['classmate_or_peer_pressure'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>My daily routine</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['my_daily_routine'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Other reasons</td>
                                                        <td>{{ $studyHabitsAndRoutine[2]->answer['other_reasons'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-12 fw-bold">Reason for no home study</div>
                                            <div class="col-12">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <td>No time to study</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['no_time_to_study'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>No interesting content</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['no_interesting_content'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>No upcoming exam or test</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['no_upcoming_exam_or_test'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>No pressure from anyone</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['no_pressure_from_anyone'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>No homework or assignment</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['no_homework_or_assignment'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>None of my classmate study</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['none_of_my_classmate_study'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>I never study</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['i_never_study'] }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Other reasons</td>
                                                        <td>{{ $studyHabitsAndRoutine[3]->answer['other_reasons'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-info" role="alert">No data found</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="card h-auto ">
                                <div class="card-header text-bg-dark text-center">Career Ambitions</div>
                                <div class="card-body">
                                    @if ($careerAmbitions->count() > 0)
                                        <div class="row gy-2">
                                            <div class="col-md-7 fw-bold">Immediate next step:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 1)->first()->answer }}</div>
                                            <div class="col-md-7 fw-bold">Most influence personality:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 2)->first()->answer }}</div>
                                            <div class="col-md-7 fw-bold">Same profession as:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 4)->first()->answer }}</div>
                                            <div class="col-md-7 fw-bold">Parents expectation are same:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 5)->first()->answer }}</div>
                                            @if (
                                                $careerAmbitions->where('question_no', 2)->first()->answer != 'Nobody' &&
                                                    $careerAmbitions->where('question_no', 2)->first()->answer != 'Prefer not to say')
                                                <div class="col-md-7 fw-bold">Degree of person who influenced you:</div>
                                                <div class="col-md-5">
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar" role="progressbar"
                                                            style="width: {{ $careerAmbitions[2]['answer'] }}%"></div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-7 fw-bold">Reason for career choice:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 6)->first()->answer }}</div>
                                            <div class="col-md-7 fw-bold">Reason for university degree choice:</div>
                                            <div class="col-md-5">
                                                {{ $careerAmbitions->where('question_no', 7)->first()->answer }}</div>
                                            <div class="col-md-7 fw-bold">Reason for getting a job:</div>
                                            <div class="col-md-5">
                                                <ul>
                                                    @foreach ($careerAmbitions->where('question_no', 8)->first()->answer as $index => $value)
                                                        <li>{{ $reasonsForGettingJob[$index] }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="col-12 fw-bold">Acquired skills:</div>
                                            <div class="col-12">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td>Job or education</td>
                                                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer['job_or_edu'] }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Resume creation</td>
                                                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer['resume_creation'] }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cover letter creation</td>
                                                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer['cover_letter_creation'] }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Interview preparation</td>
                                                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer['interview_prep'] }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Presentation</td>
                                                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer['presentation'] }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="col-md-7 fw-bold fs-6">Employability skills</div>

                                            <div class="col-6 col-md-7">Communication skill:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[0]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Teamwork skill:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[1]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Problem Solving:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[2]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Initiative and Enterprise Skills:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[3]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7"> Planning and Organizing Skill:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[4]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Self-management Skills:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[5]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Learning Skills:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[6]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                            <div class="col-6 col-md-7">Technology Skills:</div>
                                            <div class="col-6 col-md-5">
                                                <div class="progress bg-light rounded-0">
                                                    <div class="progress-bar text-special bg-special"
                                                        style="width: {{ $decision[7]['answer'] }}%"></div>
                                                </div>
                                            </div>

                                        </div>
                                    @else
                                        <div class="alert alert-info" role="alert">No data found</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="card h-auto">
                                <div class="card-header text-bg-dark text-center">Future Self Reflection</div>
                                <div class="card-body">
                                    @if ($futureSelfReflection && $futureSelfReflection->count() > 0)
                                        <div class="row gy-2" style="font-weight: 500;">
                                            <div class="col-md-7">5-years
                                                Forecast:&nbsp;&nbsp;&nbsp;{{ $forcast }}</div>
                                            @if ($forcast != 'Other')
                                                <div class="col-12 fw-bold">Reason:</div>
                                                <div class="col-12">
                                                    @foreach ($futureSelfReflection[0]->answer as $reasonObject)
                                                        <ul>
                                                            @foreach ($reasonObject as $index => $value)
                                                                <li>{{ $reasons[$index] }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                            @endif

                                            <div class="col-12 fw-bold fs-6">Decision Factors</div>
                                            <div class="col-md-7">Parents expectation:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['parents_expectations'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['parents_expectations'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Friends Influence:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['friends_influence'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['friends_influence'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">School/University Grades:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['school_college_university'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['school_college_university'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Favorite Subject:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['favorite_subject_importance'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['favorite_subject_importance'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Talent/skill:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['talent_skills'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['talent_skills'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Hobbies:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['hobbies'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['hobbies'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Scope/Market demand of occupation:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['scope_market_demand'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['scope_market_demand'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-7">Financial Support for education or training:</div>
                                            <div class="col-md-5">
                                                @if ($futureSelfReflection[1]->answer['financial_support_for_edu'] > 100)
                                                    Not Applicable
                                                @else
                                                    <div class="progress bg-light rounded-0">
                                                        <div class="progress-bar text-special bg-special"
                                                            style="width: {{ $futureSelfReflection[1]->answer['financial_support_for_edu'] }}%">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-info" role="alert">No data found</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card shadow h-100">
                                <div class="h4 rounded-top headingFont text-center text-white py-3"
                                    style="background-color: #212529 !important;">
                                    PERSONALITY TEST RESULTS
                                </div>
                                <div class="card-body px-lg-5 text-center">
                                    @if ($personalityTestResults > 99)
                                        @include('admin.users.results.personality')
                                    @elseif($personalityTestResults < 1)
                                        You have not attempted the personality test yet
                                    @else
                                        You have not attempted the personality test yet
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>
            <!--/ show profile -->
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jspdf@2.5.1/dist/jspdf.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>

    <script>
        // $(document).ready(function() {
        //     $('#downloadPdf').click(function() {
        //         window.jsPDF = window.jspdf.jsPDF;

        //         // const pdf = new jsPDF();
        //         // const pdfContent = document.getElementById("pdf-content");
        //         // const pdfWidth = pdf.internal.pageSize.getWidth();
        //         // const pdfHeight = pdf.internal.pageSize.getHeight();

        //         // // Use the html2canvas library to render the div content into images
        //         // html2canvas(pdfContent, {
        //         //     scale: 2
        //         // }).then(function(canvas) {
        //         //     const imgData = canvas.toDataURL("image/png");

        //         //     const imgWidth = pdfWidth;
        //         //     const imgHeight = (canvas.height * imgWidth) / canvas.width;

        //         //     let position = 0;

        //         //     // Add images as separate pages
        //         //     pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
        //         //     position -= pdfHeight;

        //         //     while (position > -canvas.height) {
        //         //         pdf.addPage();
        //         //         pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
        //         //         position -= pdfHeight;
        //         //     }

        //         //     // Save the PDF
        //         //     pdf.save("document.pdf");
        //         // });

        //         const pdf = new jsPDF();
        //         const pdfContent = document.getElementById("pdf-content");

        //         // Use the html2canvas library to render the div content into an image
        //         html2canvas(pdfContent).then(function(canvas) {
        //             const imgData = canvas.toDataURL("image/png");

        //             // Add the image as a PDF page
        //             pdf.addImage(imgData, "PNG", 10, 10, 190, 0);

        //             // Save the PDF
        //             pdf.save("Profile.pdf");
        //         });

        //     })
        // })

        $(document).ready(function() {

            $('#downloadPdf').click(function() {
                window.jsPDF = window.jspdf.jsPDF;

                const pdf = new jsPDF();
                const pdfContent = document.getElementById("pdf-content");
                const pdfWidth = pdf.internal.pageSize.getWidth();
                const pdfHeight = pdf.internal.pageSize.getHeight();

                // Use the html2canvas library to render the div content into images
                html2canvas(pdfContent, {
                    scale: 2
                }).then(function(canvas) {
                    const imgData = canvas.toDataURL("image/png");

                    const imgWidth = pdfWidth;
                    const imgHeight = (canvas.height * imgWidth) / canvas.width;

                    let position = 0;

                    // Add images as separate pages
                    pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
                    position -= pdfHeight;
                    pdf.addPage();
                    pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);

                    // Save the PDF
                    pdf.save("profile.pdf");
                });
            });
        });
    </script>
@endpush

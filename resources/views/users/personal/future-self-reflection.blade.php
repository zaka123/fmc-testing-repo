<div class="row gy-2" style="font-weight: 500;">
    <div class="col-12">5-years Forecast:&nbsp;&nbsp;&nbsp;{{ $forcast }}</div>
    @if ($forcast != "Other")
    <div class="col-12">Reason:</div>
    <div class="col-12">
        @foreach($info[0]->answer as $key => $reasonObject)
            <ul class="row">
                @foreach($reasonObject as $index => $value)
                    <li class="col-md-4">{{ $reasons[$index] }}</li>
                @endforeach
            </ul>
        @endforeach
    </div>
    @endif

    @php
        $future_self_reflection = $info[1]['answer'];
    @endphp

    <div class="col-md-7 fw-bold fs-6">Decision Factors</div>
    <div class="col-md-5"></div>
    <div class="col-md-7">Parents expectation:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["parents_expectations"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["parents_expectations"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Friends Influence:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["friends_influence"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["friends_influence"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">School/University Grades:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["school_college_university"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["school_college_university"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Favorite Subject:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["favorite_subject_importance"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["favorite_subject_importance"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Talent/skill:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["talent_skills"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["talent_skills"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Hobbies:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["hobbies"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["hobbies"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Scope/Market demand of occupation:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["scope_market_demand"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["scope_market_demand"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-md-7">Financial Support for education or training:</div>
    <div class="col-md-5">
        @if ($future_self_reflection["financial_support_for_edu"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $future_self_reflection["financial_support_for_edu"] }}%"></div>
        </div>
        @endif
    </div>
</div>

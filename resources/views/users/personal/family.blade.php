<div class="row gy-2" style="font-weight: 500;">
    <div class="col-6">Father Education</div>
    <div class="col-6">{{ $profile->father_education }}</div>
    <div class="col-6">Father Occupation</div>
    <div class="col-6">{{ $profile->father_occupation }}</div>
    <div class="col-6">Mother Education</div>
    <div class="col-6">{{ $profile->mother_education }}</div>
    <div class="col-6">Mother Occupation</div>
    <div class="col-6">{{ $profile->mother_occupation }}</div>
    <div class="col-6">Retired Parents</div>
    <div class="col-6">{{ $profile->parents_retired }}</div>
    
    <div class="col-12 fw-bold fs-6">Family Assistance</div>
    <div class="col-3 col-md-6">Father</div>
    <div class="col-9 col-md-6">
        @if ($info["father"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-warning bg-warning" style="width: {{ $info["father"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-3 col-md-6">Mother</div>
    <div class="col-9 col-md-6">
        @if ($info["mother"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-warning bg-warning" style="width: {{ $info["mother"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-3 col-md-6">Siblings</div>
    <div class="col-9 col-md-6">
        @if ($info["siblings"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-warning bg-warning" style="width: {{ $info["siblings"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-3 col-md-6">Relatives</div>
    <div class="col-9 col-md-6">
        @if ($info["relatives"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-warning bg-warning" style="width: {{ $info["relatives"] }}%"></div>
        </div>
        @endif
    </div>
    <div class="col-3 col-md-6">Others</div>
    <div class="col-9 col-md-6">
        @if ($info["other_persons"] > 100)
        Not Applicable
        @else
        <div class="progress rounded-0">
            <div class="progress-bar text-warning bg-warning" style="width: {{ $info["other_persons"] }}%"></div>
        </div>
        @endif
    </div>
</div>

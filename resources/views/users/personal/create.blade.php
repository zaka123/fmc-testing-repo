@extends('layouts.user',['backgroundColor'=>"background: #f5f5f5;"])
@section('user-content')

<header class="mt-4 mb-5">
    <div class="d-flex flex-column  ">
        <div class="d-flex align-items-center">
            <div class="container">
                <form action="{{url('user/personal-habits')}}" method="post">
                    @csrf
                    <div class="row bg-white p-3 rounded">
                        @foreach($personal_habits as $key=>$personal)
                        <div class="col-md-12 mt-3">
                            <div class="form-group">
                                <label>{{$personal->question }}</label>
                                <textarea name="answer[{{$personal->id}}]" class="form-control summernote">{{$personal->userPersonalInformation?->answer ?? '' }}</textarea>
                            </div>
                        </div>
                        <br>
                        @endforeach
                        <input type="hidden" name="test_name" value="{{$test_name ?? ''}}">
                        <div class="col-md-12 mt-3 d-flex justify-content-center">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
@endsection
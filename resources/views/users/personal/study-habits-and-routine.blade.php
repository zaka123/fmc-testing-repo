<div class="row gy-2" style="font-weight: 500;">
    <div class="col-7 col-lg-6">Morning study hours</div>
    <div class="col-5 col-lg-6" id="study_habits_and_routine1">{{ $info[0]->answer }} {{ Str::plural("hour", $info[0]->answer) }}</div>
    <div class="col-7 col-lg-6">Evening study hours</div>
    <div class="col-5 col-lg-6" id="study_habits_and_routine2">{{ $info[1]->answer }} {{ Str::plural("hour", $info[1]->answer) }}</div>
    <div class="col-12 fw-bold">Reason for home study:</div>
    @php $reasonForHomeStudy = $info[2]->answer; @endphp
    <div class="col-12">
        <table class="table table-hover">
            <tr>
                <td>Interesting content</td>
                <td>{{ $reasonForHomeStudy["interesting_content"] }}</td>
            </tr>
            <tr>
                <td>Upcoming exam or test</td>
                <td>{{ $reasonForHomeStudy["upcoming_exam_or_test"] }}</td>
            </tr>
            <tr>
                <td>Parents pressure</td>
                <td>{{ $reasonForHomeStudy["parents_pressure"] }}</td>
            </tr>
            <tr>
                <td>Homework or assignment</td>
                <td>{{ $reasonForHomeStudy["homework_or_assignment"] }}</td>
            </tr>
            <tr>
                <td>Classmate or peer pressure</td>
                <td>{{ $reasonForHomeStudy["classmate_or_peer_pressure"] }}</td>
            </tr>
            <tr>
                <td>My daily routine</td>
                <td>{{ $reasonForHomeStudy["my_daily_routine"] }}</td>
            </tr>
            <tr>
                <td>Other reasons</td>
                <td>{{ $reasonForHomeStudy["other_reasons"] }}</td>
            </tr>
        </table>
    </div>
    <div class="col-12">Reason for no home study:</div>
    @php $reasonForNoHomeStudy = $info[3]->answer; @endphp
    <div class="col-12" id="study_habits_and_routine4">
        <table class="table table-hover">
            <tr>
                <td>No time to study</td>
                <td>{{ $reasonForNoHomeStudy["no_time_to_study"] }}</td>
            </tr>
            <tr>
                <td>No interesting content</td>
                <td>{{ $reasonForNoHomeStudy["no_interesting_content"] }}</td>
            </tr>
            <tr>
                <td>No upcoming exam or test</td>
                <td>{{ $reasonForNoHomeStudy["no_upcoming_exam_or_test"] }}</td>
            </tr>
            <tr>
                <td>No pressure from anyone</td>
                <td>{{ $reasonForNoHomeStudy["no_pressure_from_anyone"] }}</td>
            </tr>
            <tr>
                <td>No homework or assignment</td>
                <td>{{ $reasonForNoHomeStudy["no_homework_or_assignment"] }}</td>
            </tr>
            <tr>
                <td>None of my classmate study</td>
                <td>{{ $reasonForNoHomeStudy["none_of_my_classmate_study"] }}</td>
            </tr>
            <tr>
                <td>I never study</td>
                <td>{{ $reasonForNoHomeStudy["i_never_study"] }}</td>
                </tr>
            <tr>
                <td>Other reasons</td>
                <td>{{ $reasonForNoHomeStudy["other_reasons"] }}</td>
            </tr>
        </table>
    </div>
</div>

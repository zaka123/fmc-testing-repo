<div class="row gy-2" style="font-weight: 500;">
    <div class="col-md-7">Immediate next step:</div>
    <div class="col-md-5">{{ $info->where('question_no', 1)->first()->answer }}</div>
    <div class="col-md-7">Most influence personality:</div>
    <div class="col-md-5">{{ $info->where('question_no', 2)->first()->answer }}</div>
    <div class="col-md-7">Same profession as:</div>
    <div class="col-md-5">{{ $info->where('question_no', 4)->first()->answer }}</div>
    <div class="col-md-7">Parents expectation are same:</div>
    <div class="col-md-5">{{ $info->where('question_no', 5)->first()->answer }}</div>
    @if ($info[1]->answer != "Nobody" && $info[1]->answer != "Prefer not to say")
    <div class="col-md-7">Degree of person who influenced you:</div>
    <div class="col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $info->where('question_no', 3)->first()->answer }}%"></div>
        </div>
    </div>
    @endif
    <div class="col-md-7">Reason for career choice:</div>
    <div class="col-md-5">
        <span>{{ $info->where('question_no', 6)->first()->answer }}</span>
    </div>
    <div class="col-md-7">Reason for university degree choice:</div>
    <div class="col-md-5">
        <span>{{ $info->where('question_no', 7)->first()->answer }}</span>
    </div>
    @php $reasonsForGettingJob = $info->where('question_no', 8)->first()->answer; @endphp
    <div class="col-12 fw-bold">Reason for getting a job:</div>
    <div class="col-12">
        <ul class="ps-lg-5">
        @foreach($reasonsForGettingJob as $key => $reason)
        <li>{{ $reasons[$key] }}</li>
        @endforeach
        </ul>
    </div>
    <div class="col-12 fw-bold">Acquired skills:</div>
    <div class="col-12">
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td>Job or education</td>
                    <td>{{ $info->where('question_no', 9)->first()->answer["job_or_edu"] }}</td>
                </tr>
                <tr>
                    <td>Resume creation</td>
                    <td>{{ $info->where('question_no', 9)->first()->answer["resume_creation"] }}</td>
                </tr>
                <tr>
                    <td>Cover letter creation</td>
                    <td>{{ $info->where('question_no', 9)->first()->answer["cover_letter_creation"] }}</td>
                </tr>
                <tr>
                    <td>Interview preparation</td>
                    <td>{{ $info->where('question_no', 9)->first()->answer["interview_prep"] }}</td>
                </tr>
                <tr>
                    <td>Presentation</td>
                    <td>{{ $info->where('question_no', 9)->first()->answer["presentation"] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-7 fw-bold fs-6">Employability skills</div>
    <div class="col-6 col-md-7">Communication skill:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[0]->answer }}%"></div>
        </div>
    </div>
    <div class="col-6 col-md-7">Teamwork skill:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[1]->answer }}%"></div>
        </div>
    </div>
    <div class="col-6 col-md-7">Problem Solving:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[2]->answer }}%"></div>
        </div>
    </div>
    <div class="col-6 col-md-7">Initiative and Enterprise Skills:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[3]->answer }}%"></div>
        </div>
    </div>

    <div class="col-6 col-md-7"> Planning and Organizing Skill:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[4]->answer }}%"></div>
        </div>
    </div>

    <div class="col-6 col-md-7">Self-management Skills:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[5]->answer }}%"></div>
        </div>
    </div>

    <div class="col-6 col-md-7">Learning Skills:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[6]->answer }}%"></div>
        </div>
    </div>

    <div class="col-6 col-md-7">Technology Skills:</div>
    <div class="col-6 col-md-5">
        <div class="progress rounded-0">
            <div class="progress-bar text-special bg-special" style="width: {{ $decision[7]->answer }}%"></div>
        </div>
    </div>

</div>

@if (count($reports))
<div class="row g-2">
    <div class="col-12">
        <h4 class="pt-4"><strong>Latest Reviews</strong></h4>
    </div>
</div>
@endif
<div class="row g-2" style="flex-wrap: nowrap;overflow-x: auto;padding-bottom: 1rem;">
    @foreach($reports as $report)
    <div class="col-sm-6 col-lg-3">
        <div class="text-light p-2 shadow-other rounded bg-special" style="--shadow-other-b:6px">
            <div class="text-end">
                <small style="font-size: .8em">{{ $report->date_uploaded }}</small>
            </div>
            <div class="fw-bold text-truncate">{{ $report->counsellor->full_name }}</div>
            {{-- <div class="text-end"><small>({{ $report->date_uploaded }})</small></div> --}}
            <div class="text-center">
                <a href="#" data-session-id="{{ $report->session->id }}" data-url="{{ route("api.user.session.report.show", ['session' => $report->session->id, 'report' => $report->id]) }}" data-report-id="{{ $report->id }}" class="text-decoration-none text-white" data-bs-toggle="modal" data-bs-target="#latestReviewStaticBackdrop">
                    <small style="font-weight:500">View report ></small>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="modal fade" id="latestReviewStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}' data-bs-backdrop="static" tabindex="-1" aria-labelledby="latestReviewStaticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title ms-auto fs-5" id="latestReviewStaticBackdropLabel"></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="font-size:.9rem;">
                
                <div class="text-center">
                    <img id="counsellor_profile_picture" style="width:6rem;height:6rem;" src="" class="rounded-circle" alt="counsellor">
                    <br>
                    <strong id="counsellor_name"></strong>
                    <br>
                    <small id="date_uploaded"></small>
                </div>

                <h3 class="mb-3">Review</h3>

                <hgroup>
                    <h6>Reflections from previous session</h6>
                    <p id="reflections" style="text-align: justify"></p>
                </hgroup>
                <hgroup>
                    <h6>Warm up - What's in your mind</h6>
                    <p id="warm_up" style="text-align: justify"></p>
                </hgroup>
                <hgroup>
                    <h6>Challenges</h6>
                    <p id="challenges" style="text-align: justify"></p>
                </hgroup>
                <hgroup>
                    <h6>Goal setting</h6>
                    <p id="goal_setting" style="text-align: justify"></p>
                </hgroup>
                <hgroup>
                    <h6>Commitments</h6>
                    <p id="commitments" style="text-align: justify"></p>
                </hgroup>
                <hgroup>
                    <h6>Additional notes</h6>
                    <p id="additional_notes" style="text-align: justify"></p>
                </hgroup>

            </div>
            <div class="modal-footer sticky-bottom"></div>
        </div>
    </div>
</div>

@push('script')
<script>
    $("#latestReviewStaticBackdrop").on('show.bs.modal', (e) => {
        const sessionId = e.relatedTarget.dataset.sessionId;
        const reportId = e.relatedTarget.dataset.reportId;
        const url = e.relatedTarget.dataset.url;
        
        $.ajax({
            url: url,
            type: 'get',
            success: function (response) {
                for (let key in response.data) {
                    if (key == "counsellor_profile_picture") {
                        $("#" + key).attr('src', response.data[key]);
                    } else {
                        $("#" + key).html(response.data[key]);
                    }
                }
            },
            error: function (error) {
                console.log(error.statusText);
            }
        });
    });
</script>
@endpush

@extends('layouts.user', ['pagename' => 'Sessions Reports'])
@section('css')
    <style>
        table.sessionReport tbody td {
            background-color: var(--bs-white) !important;
        }

        table.sessionReport tbody td:first-of-type {
            border-top-left-radius: var(--bs-border-radius);
            border-bottom-left-radius: var(--bs-border-radius)
        }

        table.sessionReport tbody td:last-of-type {
            border-top-right-radius: var(--bs-border-radius);
            border-bottom-right-radius: var(--bs-border-radius)
        }

        table.sessionReport {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .session-report {
            color: var(--special) !important;
            font-weight: 500
        }

        .title::before {
            content: "Sessions Report"
        }
   @media(max-width:991px){
    .smallResponsive{
        width: 100%!important;
    }
   }

    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="main px-3 px-lg-0" style="min-height: 83.2vh;">
                            <div class="floating"></div>
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row inner">
                                                <div class="col-lg-10 offset-lg-1">
                                                    <div class="p-2 p-lg-4">
                                                        <table style="vertical-align: middle;"
                                                            class="table table-borderless sessionReport">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">#</th>
                                                                    <th class="text-center">Coach</th>
                                                                    <th class="text-center">Date</th>
                                                                    <th class="text-center">Time</th>
                                                                    <th class="text-center">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($reports as $key => $report)
                                                                    <tr>
                                                                        <td class="text-center">{{ $loop->iteration }}</td>
                                                                        <td class="text-center">
                                                                            {{ $report->counsellor->full_name }}</td>
                                                                        <td class="text-center">
                                                                            {{ date('d-m-Y', strtotime($report->session->session->session_timing)) }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ date('h:i A', strtotime($report->session->session->session_timing)) }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <button class="btn btn-sm btn-success"
                                                                                data-session-id="{{ $report->session->id }}"
                                                                                data-url="{{ route('api.user.session.report.show', ['session' => $report->session->id, 'report' => $report->id]) }}"
                                                                                data-report-id="{{ $report->id }}"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#latestReviewStaticBackdrop">
                                                                                <i class="fa-solid fa-eye"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="latestReviewStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" tabindex="-1" aria-labelledby="latestReviewStaticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title ms-auto fs-5" id="latestReviewStaticBackdropLabel"></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="font-size:.9rem;">

                    <div class="text-center">
                        <img id="counsellor_profile_picture" style="width:6rem;height:6rem;" src=""
                            class="rounded-circle" alt="counsellor">
                        <br>
                        <strong id="counsellor_name"></strong>
                        <br>
                        <small id="date_uploaded"></small>
                    </div>

                    <h3 class="mb-3">Review</h3>

                    <hgroup>
                        <h6>Reflections from previous session</h6>
                        <p id="reflections"></p>
                    </hgroup>
                    <hgroup>
                        <h6>Warm up - What's in your mind</h6>
                        <p id="warm_up"></p>
                    </hgroup>
                    <hgroup>
                        <h6>Challenges</h6>
                        <p id="challenges"></p>
                    </hgroup>
                    <hgroup>
                        <h6>Goal setting</h6>
                        <p id="goal_setting"></p>
                    </hgroup>
                    <hgroup>
                        <h6>Commitments</h6>
                        <p id="commitments"></p>
                    </hgroup>
                    <hgroup>
                        <h6>Additional notes</h6>
                        <p id="additional_notes"></p>
                    </hgroup>

                </div>
                <div class="modal-footer sticky-bottom"></div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $("#latestReviewStaticBackdrop").on('show.bs.modal', (e) => {
            const sessionId = e.relatedTarget.dataset.sessionId;
            const reportId = e.relatedTarget.dataset.reportId;
            const url = e.relatedTarget.dataset.url;

            $.ajax({
                url: url,
                type: 'get',
                success: function(response) {
                    for (let key in response.data) {
                        if (key == "counsellor_profile_picture") {
                            $("#" + key).attr('src', "/" + response.data[key]);
                        } else {
                            $("#" + key).html(response.data[key]);
                        }
                    }
                },
                error: function(error) {
                    console.log(error.statusText);
                }
            });
        });

        function initializeDataTable() {
            return $('.sessionReport')
                .DataTable({
                    "responsive": true,
                    "lengthChange": false,
                    "autoWidth": false,
                    "ordering": false,
                    "pagingType": "simple",
                    "pageLength": 5,
                    dom: '<"title pt-3"<"filter"f>>rt<"row"<"col-12 col-md-6"i><"col-12 col-md-6"p>>',
                    "language": {
                        "emptyTable": "You don't have any session report yet."
                    }
                });
        }
    </script>
@endpush

    <div class="col-12 col-lg-5">
    <h4><strong>Upcoming Sessions</strong></h4>
    <div class="card overflow-auto upcoming-sessions py-0 border-0 rounded-3 shadow-other">
        <div class="card-body py-0">
            <div class="bg-white sticky-top mb-1" style="height:20px"></div>
            @forelse($upcomingSessions as $coming)
            <div class="d-flex justify-content-start mb-3">
                <div class="px-3"><i class="fa-solid fa-circle-notch"></i></div>
                <div>
                    One meeting a head,<br>
                    <span class="text-truncate">Coach name: {{ $coming->counsellor->fullName }}</span><br>
                    {{-- Date: {{ date('m-d-Y',strtotime($coming->session->session_timing)) }}  --}}
                    Date: {{ (new DateTime($coming->session->session_timing, new DateTimeZone($coming->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y') ?? '' }}
                </div>
            </div>
            @empty
            <div class="d-flex justify-content-start mb-3">
                <div class="px-3"><i class="fa-solid fa-circle-notch"></i></div>
                <div>
                    You don't have any upcoming session.<br>
                </div>
            </div>
            @endforelse
            <div class="bg-white sticky-bottom mb-1" style="height:20px"></div>
        </div>
    </div>
</div>

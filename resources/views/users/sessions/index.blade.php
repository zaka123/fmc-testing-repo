@extends('layouts.user', ['pagename' => 'Sessions'])

@section('css')
    <style>
        table.sessionsTable tbody td {
            background-color: var(--bs-white) !important;
        }

        table.sessionsTable {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .title::before {
            content: "Session History"
        }

        @media (min-width:992px) {
            .nav.nav-tabs button img {
                width: 2.5rem;
                height: 2.5rem;
            }
        }

        @media (max-width:576px) {
            .nav.nav-tabs button img {
                width: 2rem;
                height: 2rem;
            }
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }

        .font-seven {
            font-weight: 600 !important;
        }

        .accordion-button:Active,
        .accordion-button:focus {
            box-shadow: none !important;
            outline: none !important;
        }

        .accordion-button:not(.collapsed) {
            background-color: rgba(114, 156, 196, 0.4);
        }
    </style>
@endsection

@section('user-content')
    <!-- session report modal -->
    <div class="modal fade" id="latestReviewStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}' data-bs-backdrop="static"
        tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md modal-dialog-scrollable">
            <div class="modal-content bg-white border-0">
                <div class="modal-body position-relative" style="font-size:.9rem;">
                    <button type="button" class="btn-close position-fixed end-0 top-0 m-1" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                    <div class="text-center">
                        <img id="counsellor_profile_picture" src="" class="rounded-circle" alt="counsellor"
                            width="120" height="120">
                        <br>
                        <strong id="counsellor_name" class="special_monstrate_text text-dark"></strong>
                        <br>
                        <small id="date_uploaded"></small>
                    </div>

                    <h3 class="spacial_bold_monstrate_text text-dark fs-4">Review</h3>
                    <div class="accordion" id="reviewAccordian">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                    Reflections from previous session
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show"
                                data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="reflections" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    Warm up - What's in your mind
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="warm_up" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                    Challenges
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="challenges" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                    aria-expanded="false" aria-controls="collapseFour">
                                    Goal setting
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="goal_setting" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                    aria-expanded="false" aria-controls="collapseFive">
                                    Commitments
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="commitments" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                    aria-expanded="false" aria-controls="collapseSix">
                                    Additional notes
                                </button>
                            </h2>
                            <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="additional_notes" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- counsellor profile modal -->
    <div class="modal fade" id="counsellorProfile" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    @include('includes.header')
                    <div class="col-12">
                        <div class="main" style="min-height: 83.3vh;">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row inner">
                                                <div class="col-lg-10 offset-lg-1">
                                                    <div class="p-2 p-lg-4">
                                                        <table class="table table-borderless sessionsTable"
                                                            style="vertical-align:middle">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">#</th>
                                                                    {{-- <th class="text-center">Picture</th> --}}
                                                                    <th class="text-center">Coach</th>
                                                                    <th class="text-center">Date </th>
                                                                    <th class="text-center">Time</th>
                                                                    <th class="text-center">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($sessions as $key => $session)
                                                                    <tr>
                                                                        <td class="text-center rounded-start"
                                                                            scope="row">{{ ++$key }}</td>
                                                                        {{-- <td class="text-center">
                                                                            <img src="{{ asset($session->counsellor->profile_photo) }}"
                                                                                class="rounded-circle" width="50"
                                                                                height="50" style="cursor: pointer;"
                                                                                onclick="getProfile('{{ $session->counsellor->id }}')">
                                                                        </td> --}}
                                                                        <td class="text-center cursor-pointer"
                                                                            onclick="getProfile('{{ $session->counsellor->id }}')">
                                                                            {{ $session->counsellor->fullname ?? '' }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y') ?? '' }}
                                                                            {{-- {{ date('d-m-Y', strtotime($session->session->session_timing)) }} --}}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('h:i A') ?? '' }}
                                                                            {{-- {{ date('h:i A', strtotime($session->session->session_timing)) }} --}}
                                                                        </td>
                                                                        <td class="text-center rounded-end">
                                                                            <div class="btn-group btn-group-sm">
                                                                                @if (!$session->payment->is_approved)
                                                                                    <button class="btn btn-dark"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="wait for admin to approve your payment request">
                                                                                        <i class="fa fa-info"></i>
                                                                                    </button>
                                                                                @elseif ($session->is_starting)
                                                                                    <button class="btn btn-dark"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="wait for session time to arrive">
                                                                                        <i class="fa fa-info"></i>
                                                                                    </button>
                                                                                @elseif($session->has_started)
                                                                                    @if ($session->videoCall->id ?? '')
                                                                                        <a href="{{ route('user.video-call.show', $session->videoCall->id) }}"
                                                                                            data-bs-toggle="tooltip"
                                                                                            title="join now"
                                                                                            class="btn btn-success">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </a>
                                                                                    @else
                                                                                        <button class="btn btn-dark"
                                                                                            data-bs-toggle="tooltip"
                                                                                            title="the session was attempted with a fake email. Please try again with a valid email.">
                                                                                            <i class="fa fa-info"></i>
                                                                                        </button>
                                                                                    @endif
                                                                                @elseif($session->has_ended)
                                                                                    {{-- <button class="btn btn-dark"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="the session ended">
                                                                                        <i class="fa fa-info"></i>
                                                                                    </button> --}}
                                                                                    @isset($session->report)
                                                                                        <button
                                                                                            class="btn btn-sm text-white"
                                                                                            title="View Report"
                                                                                            data-session-id="{{ $session->id }}"
                                                                                            data-url="{{ route('api.user.session.report.show', ['session' => $session->id, 'report' => $session->report->id]) }}"
                                                                                            data-report-id="{{ $session->report->id }}"
                                                                                            data-bs-toggle="modal"
                                                                                            data-bs-target="#latestReviewStaticBackdrop"  style="background-color:#729cc4">
                                                                                            <i class="fa-solid fa-eye"></i>
                                                                                        </button>
                                                                                    @endisset
                                                                                @endif

                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        // // Check if the interval has already been created in localStorage
        // var intervalExists = sessionStorage.getItem('intervalExists');
        // console.log(intervalExists);
        // if (!intervalExists) {
        //     console.log(intervalExists);

        //     // Check and reload the page every 5 seconds
        //     var intervalId = setInterval(function() {
        //         checkAndReloadPage();
        //     }, 5000);

        //     // Set a flag in localStorage to indicate that the interval has been created
        //     sessionStorage.setItem('intervalExists', 'true');
        // }

        setInterval(function() {
            checkAndReloadPage();
        }, 60000);


        // Function to reload the page if the current time is equal to or greater than the target datetime
        function checkAndReloadPage() {
            var inputTime = '';
            var targetDatetime = '';
            @if ($sessions)
                @foreach ($sessions as $session)
                    @if ($session->session->session_timing)
                        // inputTime = '{{ $session->session->session_timing }}';
                        inputTime =
                            '{{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone))->format('m-d-Y H:i') }}';
                        // console.log(inputTime);

                        // Convert to javascript date
                        targetDatetime = new Date(inputTime);
                        var currentDatetime = new Date();
                        var oneHourLater = new Date(targetDatetime.getTime() + 60 * 60 *
                            1000); // Add 1 hour to the current time
                        if (currentDatetime >= targetDatetime && currentDatetime < oneHourLater) {
                            // clearInterval(intervalId); // Clear the interval
                            location.reload();
                        }
                    @endif
                @endforeach
            @endif
        }

        $("#latestReviewStaticBackdrop").on('show.bs.modal', (e) => {
            const sessionId = e.relatedTarget.dataset.sessionId;
            const reportId = e.relatedTarget.dataset.reportId;
            const url = e.relatedTarget.dataset.url;

            $.ajax({
                url: url,
                type: 'get',
                success: function(response) {
                    for (let key in response.data) {
                        if (key == "counsellor_profile_picture") {
                            $("#" + key).attr('src', "/" + response.data[key]);
                        } else {
                            $("#" + key).html(response.data[key]);
                        }
                    }
                },
                error: function(error) {
                    console.log(error.statusText);
                }
            });
        });

        function getProfile(id) {
            var modal = document.querySelector('#counsellorProfile');
            var url = "{{ url('user/getCounsellor') }}";
            $.ajax({
                url: url + "/" + id,
                type: 'GET',
                success: function(res) {
                    document.querySelector('#counsellorProfile .modal-body').innerHTML = res;
                    document.querySelector('#bookNowButton').style.display = 'none';
                },
                error: function(error) {
                    console.log(error);
                }
            })
            modal = new bootstrap.Modal(modal);
            modal.show();
        }

        function initializeDataTable() {
            return $('.sessionsTable')
                .DataTable({
                    "responsive": true,
                    "lengthChange": false,
                    "autoWidth": false,
                    "ordering": false,
                    "pagingType": "simple",
                    "pageLength": 5,
                    dom: '<"title pt-3"<"filter"f>>rt<"row"<"col-12 col-md-6"i><"col-12 col-md-6"p>>',
                    "language": {
                        "emptyTable": "You don't have any session yet."
                    }
                });
        }
    </script>
@endpush

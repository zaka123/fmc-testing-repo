@forelse($sessions as $session)
<tr>
    <td class="rounded-start">{{date('d-M-Y',strtotime($session->session_date)) ?? ''}}</td>
    <td>{{date('h:i A',strtotime($session->session_timing)) ?? ''}}</td>
    <td class="rounded-end"> <a onclick="booking('{{$session->id}}','{{$session->counsellor_id}}')" class="btn btn-sm btn-primary">
            Schedule</a>
    </td>
</tr>
@empty
<tr class="text-center">
    <td colspan="3" class="rounded">No available session on this date.</td>
</tr>
@endforelse

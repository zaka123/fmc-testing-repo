<style>
    .changerFont:hover{
        color: black!important;
    }
</style>
<div class="col-12 col-lg-7">
    <h4><strong>Latest Sessions</strong></h4>
    <div class="card latest-sessions border-0 rounded-3 position-relative mb-3 mb-lg-0 shadow-other">
        <div class="card-body py-4">
            <table class="table" style="vertical-align:middle;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th class="d-none d-lg-table-cell">Coach Name</th>
                        <th class="d-lg-none">Counselor<br>Name</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    @forelse($sessions as $key => $session)
                        <tr>
                            <th>{{ ++$key }}</th>
                            <td>{{ $session->counsellor->fullname }}</td>
                            <td>{{ date('d-M-Y',strtotime($session->session->session_timing)) }}</td>
                        </tr>
                    @empty
                        <tr class="text-center">
                            <td colspan="3">You don't have any session yet.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <a href="{{url('user/session')}}" class="btn card-link btn-sm shadow-other changerFont bg-special text-white px-3 position-absolute" style="top:100%;left: 50%;transform: translate(-50%, -50%);">View All </a>
    </div>
</div> <!-- / sessions column -->

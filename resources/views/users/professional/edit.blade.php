@extends('layouts.user', ['pagename' => 'Edit Profile'])
@section('css')
    <style>
        :root {
            --shadow-other-c: var(--special) !important;
        }

        .inner {
            min-height: 72vh !important
        }

        .adjustedWidth {
            width: 120px !important;
        }

        @media(max-width:480px) {
            .buttonRow button {
                margin-top: 5px !important;
            }
        }

        @media(max-width:355px) {
            .buttonRow a {
                margin-top:5px!important;
            }
        }
        .saveChanges:hover{
            background-color:#02618a!important;
        }
        .cancelChanges:hover{
            background-color:black!important;
        }
        .deleteChanges:hover{
            background-color:red!important;
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            @include('users.side-bar')
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="p-4 main">
                            <div class="card border-0 bg-transparent">
                                <div class="card-body">
                                    <div class="inner p-3">
                                        <form action="{{ route('user.professional.update', auth()->id()) }}" method="post">
                                            @csrf @method('PUT')
                                            <div class="row g-3">
                                                <div class="col-12">
                                                    <div class="text-end">
                                                        <!-- @include('navbars.user') -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="name">Name</label>
                                                        <input type="text" name="name" id="name"
                                                            value="{{ old('name') ?? ($user->name ?? '') }}"
                                                            class="@error('name') is-invalid @enderror form-control shadow-other">
                                                            @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="last_name">Last Name</label>
                                                        <input type="text" name="last_name" id="last_name"
                                                            value="{{ old('last_name') ?? ($user->last_name ?? '') }}"
                                                            class="@error('last_name') is-invalid @enderror form-control shadow-other">
                                                            @error('last_name')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" name="email" id="email"
                                                            value="{{ old('email') ?? ($user->email ?? '') }}"
                                                            class="@error('email') is-invalid @enderror form-control shadow-other">
                                                            @error('email')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <label for="gender">Gender</label><br>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Male') checked @endif
                                                                name="gender" type="radio" id="Male" value="Male">
                                                            <label class="form-check-label" for="Male">Male</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Female') checked @endif
                                                                name="gender" type="radio" id="Female" value="Female">
                                                            <label class="form-check-label" for="Female">Female</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"
                                                                @if ($user->gender == 'Other') checked @endif
                                                                name="gender" type="radio" id="Other" value="Other">
                                                            <label class="form-check-label" for="Other">Other</label>
                                                        </div>
                                                            @error('gender')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <input type="hidden" name="profession" value="{{ $user->profession }}">
                                                {{-- <div class="col-md-8 mb-3">
                                                <div class="form-group">
                                                    <label for="profession">Profession</label><br>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input"
                                                            @if ($user->profession == 'Student') checked @endif
                                                        name="profession" type="radio" id="Student" value="Student">
                                                        <label class="form-check-label" for="Student">Student</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input"
                                                            @if ($user->profession == 'University Student') checked @endif
                                                        name="profession" type="radio" id="University Student"
                                                        value="University Student">
                                                        <label class="form-check-label"
                                                            for="University Student">University Student</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input"
                                                            @if ($user->profession == 'Professional') checked @endif
                                                        name="profession" type="radio" id="Professional"
                                                        value="Professional">
                                                        <label class="form-check-label"
                                                            for="Professional">Professional</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" @if ($user->profession == 'Parent')
                                                        checked @endif name="profession" type="radio" id="Parent"
                                                        value="Parent">
                                                        <label class="form-check-label" for="Parent">Parent Looking for
                                                            Child</label>
                                                    </div>
                                                </div>
                                            </div> --}}
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="date_of_birth">Date of Birth</label>
                                                        <input type="date" name="date_of_birth" id="date_of_birth"
                                                            value="{{ old('date_of_birth') ?? ($userProfessional->date_of_birth ?? '') }}"
                                                            class="@error('date_of_birth') is-invalid @enderror form-control shadow-other">
                                                            @error('date_of_birth')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="country">Country</label>
                                                        <select type="text" name="city" id="country"
                                                            class="@error('city') is-invalid @enderror form-select shadow-other">
                                                            <option value="">Please select your country</option>
                                                            @foreach ($countryNames as $countryName)
                                                                <option @selected($userProfessional?->city == $countryName)>{{ $countryName }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @error('city')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="experience">Experience</label>
                                                        <input type="text" name="experience" id="experience"
                                                            value="{{ old('experience') ?? ($userProfessional->experience ?? '') }}"
                                                            class="@error('experience') is-invalid @enderror form-control shadow-other">
                                                            @error('experience')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="education_level">Education Level</label>
                                                        <select
                                                            class="@error('education_level') is-invalid @enderror form-control shadow-other"
                                                            name="education_level" id="education_level" required>
                                                            <option value="">Please select your education level
                                                            </option>
                                                            <option @selected((old('education_level') ?? $userProfessional?->education_level) == 'Undergraduate')>Undergraduate</option>
                                                            <option @selected((old('education_level') ?? $userProfessional?->education_level) == 'Graduate')>Graduate</option>
                                                            <option @selected((old('education_level') ?? $userProfessional?->education_level) == 'Postgraduate')>Postgraduate</option>
                                                            <option @selected((old('education_level') ?? $userProfessional?->education_level) == 'Postdoc')>Postdoc</option>
                                                        </select>
                                                        @error('education_level')<span class="text-danger">{{ $message }}</span>@enderror
                                                        {{-- <input type="text" name="education_level" id="education_level"
                                                        value="{{old('education_level') ?? $userProfessional->education_level ?? '' }}"
                                                        class="@error('education_level') is-invalid @enderror form-control shadow-other">
                                                    --}}
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="organization_name">Organization Name</label>
                                                        <input type="text" name="organization_name"
                                                            id="organization_name"
                                                            value="{{ old('organization_name') ?? ($userProfessional->organization_name ?? '') }}"
                                                            class="@error('organization_name') is-invalid @enderror form-control shadow-other">
                                                            @error('organization_name')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="role">Role</label>
                                                        <input type="text" name="role" id="role"
                                                            value="{{ old('role') ?? ($userProfessional->role ?? '') }}"
                                                            class="@error('role') is-invalid @enderror form-control shadow-other">
                                                            @error('role')<span class="text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row buttonRow">
                                                <div class="diButton mt-3 ">
                                                    <input type="submit" value="Save Changes"
                                                        class="btn bg-special saveChanges text-white btn-sm adjustedWidth">
                                                    <a href="{{ route('dashboard') }}"
                                                        class="btn btn-secondary cancelChanges btn-sm adjustedWidth">Cancel Changes</a>
                                                    <!-- Delete Account Button -->
                                                    <button type="submit" form="deletRecordForm"
                                                        class="btn btn-danger btn-sm text-white delete-confirm adjustedWidth deleteChanges"
                                                        data-name="{{ $user->name }}"
                                                        data-original-title="Delete Account">
                                                        Delete Account
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Delete Account Form -->
                                        <form id="deletRecordForm"
                                            action="{{ route('user.professional.destroy', $user->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>
        </div>
    </div>

    {{-- Sweet Alert JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        // for delete confirmation method
        $('.delete-confirm').click(function(event) {
            // var form = $(this).closest("form");
            var form = $('#deletRecordForm');
            // var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Are you sure to delete your account permanantly?`,
                    text: "You won't be able to recover your account!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection

@extends('layouts.user', ['pagename' => 'User Profile'])
@section('user-content')
<style>
    .downloadChanger:hover{
       background-color:black!important;
    }
</style>
<div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->

            <!-- show profile -->
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 bg-white sticky-top">
                        <div class="d-flex px-3 pb-lg-3">
                            <button class="btn mt-3 btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar">
                                <i class="fa-solid fa-bars fs-4"></i>
                            </button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between gy-3">
                        <div class="mx-3" style="padding-top:6px">
                            <h4>My Profile</h4>
                        </div>
                        <div style="margin-right:35px">
                            {{-- <button id="downloadPdf" class="btn btn-success text-white downloadChanger">Download
                                Profile</button> --}}
                        </div>
                    </div>
                    <div id="pdf-content" class="row gy-3 mb-5">
                        <div class="col-12 col-lg-3">
                            <div class="card h-100 shadow">
                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="rounded-circle border" src="{{ asset($user->profilePhoto) }}"
                                            alt="User profile picture" width="150" height="150">
                                    </div>

                                    <h3 class="text-center headingFont">{{ $user->fullName }}</h3>

                                    <p class="text-muted text-center"><b>{{ $user->email }}</b></p>

                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <b>D.O.B:</b> <span class="float-right">{{ $userProfile?->date_of_birth }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Country:</b> <span class="float-right">{{ $userProfile?->city }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Experience:</b> <span
                                                class="float-right">{{ $userProfile?->experience }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Education level:</b> <span
                                                class="float-right">{{ $userProfile?->education_level }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Organization working in:</b> <span
                                                class="float-right">{{ $userProfile?->organization_name }}</span>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Role:</b> <span class="float-right">{{ $userProfile?->role }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-12 col-lg-3">
                            <div class="card h-100 shadow">
                                <div class="card-body">
                                    <table class="table table-striped" id="example1">
                                        <thead> <strong> User's Sessions </strong>
                                            <tr>
                                                <th>#</th>
                                                <th>Counsellor</th>
                                                <th>Date </th>
                                                <th>Time</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($sessions as $key => $session)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>
                                                    <a href="{{ route("admin.counsellors.show", $session->counsellor->id) }}">{{ $session->counsellor->fullname }}</a>
                                                </td>
                                                <td>{{ date("d-m-Y", strtotime($session->session->session_timing)) }}</td>
                                                <td>{{ date("h:i A", strtotime($session->session->session_timing)) }}</td>
                                                <td>{!! $session->session_status !!}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-12 col-lg-9">
                            <div class="card shadow h-100">
                                <div class="h4 rounded-top headingFont text-center text-white py-3"
                                    style="background-color: #212529 !important;">
                                    PERSONALITY TEST RESULTS
                                </div>
                                <div class="card-body px-lg-5 text-center">
                                    @if ($personalityTestResults > 99)
                                        @include('admin.users.results.personality')
                                    @elseif($personalityTestResults < 1)
                                        You have not attempted the personality test yet
                                    @else
                                    You have not attempted the personality test yet
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card h-100 shadow">
                                <div class="h4 rounded-top headingFont text-white text-center py-3"
                                    style="background-color: #212529 !important">
                                    CAREER EXPECTATIONS TEST RESULTS
                                </div>
                                <div class="card-body px-lg-5 text-center">
                                    @if ($careerExpectationsTestResults > 99)
                                        @include('admin.users.results.career-expectations')
                                    @elseif($careerExpectationsTestResults < 1)
                                        You have not attempted the career expectations test yet
                                    @else
                                    You have not attempted the career expectations test yet
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card h-100 shadow">
                                <div class="h4 rounded-top headingFont text-center text-white py-3"
                                    style="background-color: #212529 !important;">SKILL ASSESSMENT TEST RESULTS</div>
                                <div class="card-body px-lg-5 text-center">
                                    @if ($skillAssessmentTestResults > 99)
                                        @include('admin.users.results.skill-assessment')
                                    @elseif($skillAssessmentTestResults < 1)
                                        You have not attempted the skill assessment test yet
                                    @else
                                        You have not attempted the skill assessment test yet
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>
            <!--/ show profile -->
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jspdf@2.5.1/dist/jspdf.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>

    <script>
        $(document).ready(function() {

            $('#downloadPdf').click(function() {
                window.jsPDF = window.jspdf.jsPDF;

                const pdf = new jsPDF();
                const pdfContent = document.getElementById("pdf-content");
                const pdfWidth = pdf.internal.pageSize.getWidth();
                const pdfHeight = pdf.internal.pageSize.getHeight();

                // Use the html2canvas library to render the div content into images
                html2canvas(pdfContent, {
                    scale: 2
                }).then(function(canvas) {
                    const imgData = canvas.toDataURL("image/png");

                    const imgWidth = pdfWidth;
                    const imgHeight = (canvas.height * imgWidth) / canvas.width;

                    let position = 0;

                    // Add images as separate pages
                    pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
                    position -= pdfHeight;
                    pdf.addPage();
                    pdf.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);

                    // Save the PDF
                    pdf.save("profile.pdf");
                });
            });
        });
    </script>
@endpush

@extends('layouts.user', ['pagename' => 'Dashboard'])
@section('css')
    {{-- our css --}}
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style>
        .btn-disabled {
            background-color: rgba(128, 128, 128, 0.424) !important;
            cursor: not-allowed;
        }

        @media (min-width:2000px) {
            .onTheFloor {
                position: static !important;
                bottom: 0 !important;
                width: 100% !important;
            }
        }

        @media (min-width:2200px) {
            .onTheFloor {
                position: fixed !important;
                bottom: 0 !important;
                width: 100% !important;
            }
        }

        .font-seven {
            font-weight: 600 !important;
        }

        .accordion-button:Active,
        .accordion-button:focus {
            box-shadow: none !important;
            outline: none !important;
        }

        .accordion-button:not(.collapsed) {
            background-color: rgba(114, 156, 196, 0.4);
        }

        .skeleton {
            position: relative;
        }

        .skeleton::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 999;
            background-color: #fff;
        }
    </style>
@endsection
@section('user-content')
    <div class="modal fade" id="latestReviewStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}' data-bs-backdrop="static"
        tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md modal-dialog-scrollable">
            <div class="modal-content bg-white border-0">
                <div class="modal-body position-relative" style="font-size:.9rem;">
                    <button type="button" class="btn-close position-fixed end-0 top-0 m-1" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                    <div class="text-center">
                        <img id="counsellor_profile_picture" src="" class="rounded-circle" alt="counsellor"
                            width="120" height="120">
                        <br>
                        <strong id="counsellor_name" class="special_monstrate_text text-dark"></strong>
                        <br>
                        <small id="date_uploaded"></small>
                    </div>

                    <h3 class="spacial_bold_monstrate_text text-dark fs-4">Review</h3>
                    <div class="accordion" id="reviewAccordian">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                    Reflections from previous session
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show"
                                data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="reflections" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    Warm up - What's in your mind
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="warm_up" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                    Challenges
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="challenges" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                    aria-expanded="false" aria-controls="collapseFour">
                                    Goal setting
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="goal_setting" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                    aria-expanded="false" aria-controls="collapseFive">
                                    Commitments
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="commitments" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button
                                    class="accordion-button collapsed spacial_bold_monstrate_text text-dark fs-6 font-seven"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                    aria-expanded="false" aria-controls="collapseSix">
                                    Additional notes
                                </button>
                            </h2>
                            <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#reviewAccordian">
                                <div class="accordion-body">
                                    <p id="additional_notes" style="text-align: justify"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar -->
            @include('users.side-bar')
            <!-- main central contents column  -->
            <div class="spinner-border spinner-border-sm position-absolute top-50" role="status"
                style="z-index: 9999;left:60%">
                <span class="visually-hidden">Loading...</span>
            </div>
            <div class="col-lg-10 skeleton">
                @include('includes.header')
                <div class="pt-2 pb-4 px-4 px-lg-2 position-relative">
                    <!-- new showcase cards for the services -->
                    {{-- left button --}}
                    <span class="position-absolute customPrevBtn d-none d-md-block">
                        <i class="bi bi-arrow-left-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                    </span>
                    <div class="g-2 p-md-4 owl-carousel mb-5 mb-md-0" id="showcaseCardsCarousel"
                        style="width:100%;height:auto">
                        <!-- New Cards -->
                        <div class="my-3 showcase_card_wrapper" style="width: 100%">
                            <div class="p-0 mx-md-2 showcase_card">
                                <!-- card main text and image portion -->
                                <div class="showcase_card_header">
                                    <div class="h-100 p-3">
                                        <a href="{{ url('user/career/coaching') }}" class="showcase_card_title">Career
                                            Coaching</a>
                                    </div>
                                    <div class="h-100 flex-all-center gaint_icon_wrapper">
                                        <i
                                            class="fa-solid fa-arrows-down-to-people  flex-all-center gaint_circular-icon"></i>
                                    </div>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="showcase_card_body">
                                    <div class="px-3">
                                        <p class="p-0 m-0 special_monstrate_text">
                                            Gain insights, set goals, and develop strategies to thrive in your chosen field.
                                        </p>
                                    </div>
                                    <div class="px-3 pt-1 learn_more">
                                        <!-- if you need button here you are free to use that  -->
                                        <a href="{{ url('user/career/coaching') }}" class="gap-1">Learn more<i
                                                class="bi bi-chevron-right" style="font-size:12px;"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3 showcase_card_wrapper" style="width: 100%">
                            <div class="p-0 mx-md-2 showcase_card">
                                <!-- card main text and image portion -->
                                <div class="showcase_card_header">
                                    <div class="h-100 p-3">
                                        <a href="{{ route('user.test.report') }}"
                                            class="showcase_card_title">Assessments</a>
                                    </div>
                                    <div class="h-100 flex-all-center gaint_icon_wrapper">
                                        <i class="fa-solid fa-file-invoice flex-all-center gaint_circular-icon"></i>
                                    </div>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="showcase_card_body pt-1">
                                    <div class="px-3">
                                        <p class="p-0 m-0 special_monstrate_text">
                                            Unlock a deeper understanding of your skills and career aspirations through our
                                            comprehensive assessment tools. </p>
                                    </div>
                                    <div class="px-3 learn_more">
                                        <!-- if you need button here you are free to use that  -->
                                        <a href="{{ route('user.test.report') }}" class="gap-1">Learn more<i
                                                class="bi bi-chevron-right" style="font-size: 12px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3 showcase_card_wrapper" style="width: 100%">
                            <div class="p-0 mx-md-2 showcase_card">
                                <!-- card main text and image portion -->
                                <div class="showcase_card_header">
                                    <div class="h-100 p-3">
                                        <a href="{{ route('user.test.instructions', 4) }}"
                                            class="showcase_card_title">Career Explorer</a>
                                    </div>
                                    <div class="h-100 flex-all-center gaint_icon_wrapper">
                                        <i class="fa-solid fa-graduation-cap flex-all-center  gaint_circular-icon"></i>
                                    </div>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="showcase_card_body">
                                    <div class="px-3">
                                        <p class="p-0 m-0 special_monstrate_text">
                                            Embark on a journey of self-discovery with our career explorer tool, designed to
                                            uncover your unique traits and preferences. </p>
                                    </div>
                                    <div class="px-3  learn_more">
                                        <!-- if you need button here you are free to use that  -->
                                        <a href="{{ route('user.test.instructions', 4) }}" class="gap-1">Learn more<i
                                                class="bi bi-chevron-right" style="font-size: 12px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- right button --}}
                    <span class="position-absolute customNextBtn d-none d-md-block">
                        <i class="bi bi-arrow-right-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                    </span>
                    <!-- end new cards for the services-->
                    <!-- videos portion starts here -->
                    {{-- left button --}}
                    <span class="position-absolute customPrevBtnVideo d-none d-md-block">
                        <i class="bi bi-arrow-left-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                    </span>
                    <div class="g-2 p-md-4 owl-carousel  mb-md-0" id="videoCarousel" style="width:100%">
                        <!-- New Cards -->
                        <div class="my-1 video_card_wrapper">
                            <div class="p-0 mx-md-2 video_card">
                                <!-- card main text and image portion -->
                                <div class="video_card_header">
                                    <iframe width="100%" height="100%"
                                        src="https://www.youtube.com/embed/bRtBHF-WPpM?si=tdk9kvDVKtFi4a8q"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                        allowfullscreen></iframe>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="video_card_body">
                                    <h3 class="video_card_title p-2 pb-0 m-0">How to figure out what you
                                        really want | Ashley Stahl | TEDxLeidenUniversity
                                    </h3>
                                    <div class="px-2 d-flex align-items-center gap-3 p-1">
                                        <span class="d-flex gap-1" style="font-size: 12px;opacity:0.7"><i
                                                class="bi bi-play-circle"></i>Video</span>
                                        <span style="font-size:12px;opacity:0.7">19 min watch</span>
                                    </div>
                                    <p class="p-2 py-0 m-0 special_monstrate_text text-dark video_description">
                                        Have you ever wondered what you actually want? Then join Ashley Stahl -career coach,
                                        author, former counter-terrorism and podcast host - as she shares her three key
                                        steps to help you connect to your life's purpose, discover your ideal career path,
                                        and make what she likes to call a "you-turn," the decision to get out of fear and
                                        tap into to what you actually want out of life. She hosts inspirational guests each
                                        week on her show, "The You Turn Podcast," with the intention of helping her
                                        listeners upgrade their mindset both in work and love, and land a new job they love.
                                        Ashley’s been named a “Top 99 Foreign Policy Leader Under 33” by Diplomatic Courier
                                        Magazine and Young Professionals in Foreign Policy. She’s a columnist for Forbes,
                                        and her work has been featured on the Wall Street Journal, CBS, SELF, Washington
                                        Post, Chicago Tribune and more. Ashley earned her Master’s degree in IR from King’s
                                        College London, and another Master’s in Spiritual Psychology at the University of
                                        Santa Monica. She holds a BA from University of Redlands in government, history and
                                        French. This talk was given at a TEDx event using the TED conference format but
                                        independently organized by a local community. Learn more at https://www.ted.com/tedx
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="my-1 video_card_wrapper">
                            <div class="p-0 mx-md-2 video_card">
                                <!-- card main text and image portion -->
                                <div class="video_card_header">
                                    <iframe width="100%" height="100%"
                                        src="https://www.youtube.com/embed/mX2Ezd268MQ?si=FRC7b2cBjD-VaExs"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                        allowfullscreen></iframe>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="video_card_body">
                                    <h3 class="video_card_title  p-2 pb-0 m-0">What is Career Coach | Explained in 2 min
                                        ||<br>
                                        video by Productivity Guy
                                    </h3>
                                    <div class="px-2 d-flex align-items-center gap-3 p-1">
                                        <span class="d-flex gap-1" style="font-size: 12px;opacity:0.7"><i
                                                class="bi bi-play-circle"></i>Video</span>
                                        <span style="font-size:12px;opacity:0.7">2 min watch</span>
                                    </div>
                                    <p class="p-2 py-0 m-0 special_monstrate_text text-dark video_description">
                                        In this video, we will explore What is a Career Coach.
                                        Career Coaches use a solution-oriented approach to assist and inform any person who
                                        is in need of career advice.Career coaches play a pivotal role in guiding
                                        individuals through the complexities of the professional world. </p>
                                </div>
                            </div>
                        </div>
                        <div class="my-1 video_card_wrapper">
                            <div class="p-0 mx-md-2 video_card">
                                <!-- card main text and image portion -->
                                <div class="video_card_header">
                                    <iframe width="100%" height="100%"
                                        src="https://www.youtube.com/embed/t5IEv8qIvDE?si=CVT5t4QnXF9JEy1e"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                        allowfullscreen></iframe>
                                </div>
                                <!-- icons and sub text section -->
                                <div class="video_card_body">
                                    <h3 class="video_card_title  p-2 pb-0 m-0">What Does a Careers Coach Do? - Job Overview
                                        ||<br>
                                        video by Career Insights
                                    </h3>
                                    <div class="px-2 d-flex align-items-center gap-3 p-1">
                                        <span class="d-flex gap-1" style="font-size: 12px;opacity:0.7"><i
                                                class="bi bi-play-circle"></i>Video</span>
                                        <span style="font-size:12px;opacity:0.7">8 min watch</span>
                                    </div>
                                    <p class="p-2 py-0 m-0 special_monstrate_text text-dark video_description">

                                        Career coaches help people with their professional development at any stage of their
                                        careers.

                                        By listening to their clients they’re able to extract and help them determine career
                                        paths and goals which can reinvigorate their passion for work.

                                        This video explores what a careers coach is as well as what it’s like day to day and
                                        how to become a career coach. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- right button --}}
                    <span class="position-absolute customNextBtnVideo  d-none d-md-block">
                        <i class="bi bi-arrow-right-circle-fill" style="font-size: 24px;color:#496C8C"></i>
                    </span>
                    <!-- video portion ends here-->
                    {{-- sessions starts here --}}
                    <div class="row px-md-4" style="height: auto!important">
                        <!-- New Cards -->
                        <div class="col-md-6 my-3 p-0 p-md-2 h-auto my-5">
                            <h3 class="heading-now">Latest Sessions</h3>
                            <div class="w-100">
                                <div class="sessions w-100 my-4">
                                    <ul class="sessions_list  w-100 m-0 p-0">
                                        <li>SR.</li>
                                        <li>Career Coaches</li>
                                        <li>Date</li>
                                        <li>Report</li>
                                    </ul>
                                </div>
                                @forelse($sessions as $key => $session)
                                    <div class="dynamic-sessions w-100 mt-2">
                                        <ul class="dynamic_sessions_list  w-100 m-0 my-1 p-0">
                                            <li>{{ $loop->iteration }}</li>
                                            <li class="">
                                                <div class="session-wrapper">
                                                    <div class="coach-info">
                                                        <img src="{{ asset($session->counsellor->profile_photo ?? 'assets/img/3.jpg') }}"
                                                            alt="">
                                                        <p class="p-0 m-0 special_monstrate_text text-dark">
                                                            {{ $session->counsellor->fullname ?? '' }}</p>
                                                    </div>
                                                    <div class="session-date">
                                                        <p class="special_monstrate_text text-dark p-0 m-0">
                                                            {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d M, Y') ?? '' }}
                                                        </p>
                                                    </div>
                                                    <div class="session-button">
                                                        @if ($session->report)
                                                            <button class="special_monstrate_text view"
                                                                data-session-id="{{ $session->id }}"
                                                                data-url="{{ route('api.user.session.report.show', ['session' => $session->id, 'report' => $session->report->id]) }}"
                                                                data-report-id="{{ $session->report->id }}"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#latestReviewStaticBackdrop"><i
                                                                    class="bi bi-eye-fill"></i>View</button>
                                                        @else
                                                            <button class="special_monstrate_text view btn-disabled"
                                                                title="No report" disabled><i
                                                                    class="bi bi-eye-fill"></i>View</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @empty
                                    <div class="mt-3 text-center">
                                        <p>You don't have any session yet.</p>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                        <div class="col-md-6 my-3 p-2 h-auto my-5">
                            <div class="w-100">
                                <h3 class="heading-now">Upcoming Sessions</h3>
                                <div class="sessions w-100 my-4">
                                    <ul class="sessions_list  w-100 m-0 p-0">
                                        <li>SR.</li>
                                        <li>Career Coaches</li>
                                        <li>Date</li>
                                        <li>Session</li>
                                    </ul>
                                </div>
                                @forelse($upcomingSessions as $coming)
                                    <div class="dynamic-sessions w-100 mt-2">
                                        <ul class="dynamic_sessions_list  w-100 m-0 my-1 p-0">
                                            <li>{{ $loop->iteration }}</li>
                                            <li class="">
                                                <div class="session-wrapper">
                                                    <div class="coach-info">
                                                        <img src="{{ asset($coming->counsellor->profile_photo ?? 'assets/img/3.jpg') }}"
                                                            alt="">
                                                        <p class="p-0 m-0 special_monstrate_text text-dark">
                                                            {{ $coming->counsellor->fullname }}</p>
                                                    </div>
                                                    <div class="bg-successs session-date">
                                                        <p class="special_monstrate_text text-dark p-0 m-0">
                                                            {{ (new DateTime($coming->session->session_timing, new DateTimeZone($coming->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d M, Y') ?? '' }}
                                                        </p>
                                                    </div>
                                                    <div class="bg-primsary session-button">
                                                        @php

                                                        @endphp

                                                        @if (!$coming->payment->is_approved)
                                                            <button class="special_monstrate_text start btn-disabled"
                                                                disabled
                                                                title="Wait for admin to approve your payment.">Start</button>
                                                        @elseif ($coming->has_started)
                                                            @if ($coming->videoCall)
                                                                @if ($coming->videoCall->id ?? '')
                                                                    <a href="{{ route('user.video-call.show', $coming->videoCall->id) }}"
                                                                        title="Join now"
                                                                        class="special_monstrate_text start">
                                                                        Start
                                                                    </a>
                                                                @else
                                                                    <button
                                                                        class="special_monstrate_text start btn-disabled"
                                                                        disabled>Start</button>
                                                                @endif
                                                            @else
                                                                <button class="special_monstrate_text start btn-disabled"
                                                                    disabled>Start</button>
                                                            @endif
                                                            <button class="special_monstrate_text start btn-disabled"
                                                                disabled>Start</button>
                                                        @else
                                                            <button class="special_monstrate_text start btn-disabled"
                                                                disabled>Start</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @empty
                                    <div class="mt-3 text-center">
                                        You don't have any upcoming session.<br>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    {{-- sessions starts here --}}
                </div>
                {{-- footer goes here --}}
                <footer class="mt-5 text-bg-dark px-4 py-3 onTheFloor">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                                    class="d-inline d-md-none"><br></span>All rights reserved</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $("#latestReviewStaticBackdrop").on('show.bs.modal', (e) => {
            const sessionId = e.relatedTarget.dataset.sessionId;
            const reportId = e.relatedTarget.dataset.reportId;
            const url = e.relatedTarget.dataset.url;

            $.ajax({
                url: url,
                type: 'get',
                success: function(response) {
                    for (let key in response.data) {
                        if (key == "counsellor_profile_picture") {
                            $("#" + key).attr('src', response.data[key]);
                        } else {
                            $("#" + key).html(response.data[key]);
                        }
                    }
                },
                error: function(error) {
                    console.log(error.statusText);
                }
            });
        });

        $(document).ready(function() {

            // ************showcase cards carousel settings are here**************

            var showCaseOwl = $("#showcaseCardsCarousel").owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                smartSpeed: 1000,
                responsive: {
                    0: {
                        items: 1,
                        center: true
                    },
                    1000: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    },
                    1600: {
                        items: 4
                    }
                }
            });

            $('.customNextBtn').click(function() {
                showCaseOwl.trigger('next.owl.carousel');
            });

            // Go to the previous item
            $('.customPrevBtn').click(function() {
                // With optional speed parameter
                // Parameters have to be in square brackets '[]'
                showCaseOwl.trigger('prev.owl.carousel');
            });

            // ***************video carousel settings are here*********

            var owlVideo = $("#videoCarousel").owlCarousel({
                loop: true,
                margin: 10,
                smartSpeed: 1000,
                nav: false,
                responsive: {
                    0: {
                        items: 1,
                        center: true
                    },
                    1000: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    },
                    1600: {
                        items: 4
                    }
                }
            });

            $('.customNextBtnVideo').click(function() {
                owlVideo.trigger('next.owl.carousel');
            });

            // Go to the previous item
            $('.customPrevBtnVideo').click(function() {
                // With optional speed parameter
                // Parameters have to be in square brackets '[]'
                owlVideo.trigger('prev.owl.carousel');
            });
            var spinner = $('.spinner-border');
            var skeleton = $('.skeleton');
            $(window).on('load', function() {
                spinner.hide();
                skeleton.removeClass('skeleton');
            });
        });
    </script>
@endpush

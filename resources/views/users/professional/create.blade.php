@extends('layouts.navbar', ['pagename' => 'Create Profile'])
@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/users.profile.create.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row g-0 align-items-center" style="min-height: 74.5vh;">
        <div class="col-12">
            <div class="row gx-0 gy-3">
                <div class="col-12">
                    <form action="{{ route('user.professional.store') }}" id="profileForm" class="px-4 mb-3"
                        method="post">
                        @csrf
                        <div class="question">
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <label for="dateOfBirth">
                                        <strong>
                                            <h4 class="fw-500">What is your Date of Birth?</h4>
                                        </strong>
                                    </label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <input id="dateOfBirth" value="{{ old('date_of_birth') }}" max="{{ date('Y-m-d') }}"
                                        class="form-control ps-4 shadow" type="date" name="date_of_birth" required>
                                </div>
                            </div>
                        </div>
                        <div class="question d-none">
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <label class="fw-500 fs-4" for="city">Which country are you from?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <select type="text" name="city" id="city"
                                        class="@error('city') is-invalid @enderror form-control shadow-other" required>
                                        <option value="">Please select your country</option>
                                        @foreach ($countryNames as $country)
                                        <option>{{ $country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-none question">
                            <div class="row">
                                <div class="col-lg-6 offset-lg-4">
                                    <label class="fw-500 fs-4" for="experience">Can you tell us how many years of
                                        working
                                        experience do you have altogether?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <input type="text" name="experience" id="experience"
                                        value="{{ old('experience') ?? '' }}"
                                        class="@error('experience') is-invalid @enderror form-control shadow-other"
                                        required placeholder="experience ...">
                                </div>
                            </div>
                        </div>
                        <div class="d-none question">
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <label class="fw-500 fs-4" for="education_level">What is the highest level of
                                        education
                                        you have received?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <select
                                        class="@error('education_level') is-invalid @enderror form-control shadow-other"
                                        name="education_level" id="education_level" required>
                                        <option value="">Please select your education level</option>
                                        <option @selected(old('education_level')=='Undergraduate' )>Undergraduate
                                        </option>
                                        <option @selected(old('education_level')=='Graduate' )>Graduate</option>
                                        <option @selected(old('education_level')=='Postgraduate' )>Postgraduate</option>
                                        <option @selected(old('education_level')=='Postdoc' )>Postdoc</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-none question">
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <label class="fw-500 fs-4" for="organization_name">Which organisation are you
                                        currently
                                        working for?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <input type="text" name="organization_name" id="organization_name"
                                        value="{{ old('organization_name') ?? '' }}"
                                        class="@error('organization_name') is-invalid @enderror form-control shadow-other"
                                        required placeholder="organization name ...">
                                </div>
                            </div>
                        </div>
                        <div class="d-none question">
                            <div class="row offset-lg-4">
                                <div class="col-lg-7">
                                    <label class="fw-500 fs-4" for="role">Could you tell us your designation?</label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                        <input type="text" name="role" id="role" value="{{ old('role') ?? '' }}"
                                            class="@error('role') is-invalid @enderror form-control shadow-other"
                                            required placeholder="role ...">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-12 text-center mb-5">
                        <button type="button" title="Press shift+enter to show previous question" disabled
                            style="background-color:#6CBBDE"
                            class="btn shadow-other btn-sm text-white previous">Previous</button>
                        <button type="button" title="Press enter to show next question" style="background-color:#6CBBDE"
                            class="btn shadow-other btn-sm text-white next">Next</button>
                        <button type="submit" style="background-color:#6CBBDE;display: none;"
                            class="btn shadow-other btn-sm text-white submit" form="profileForm">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer.user')
@endsection
@push('script')
<script src="{{ asset('assets/plugins/jQuery/jQuery.min.js') }}"></script>
<script src="{{ asset('assets/js/users.profile.create.js') }}"></script>
@endpush
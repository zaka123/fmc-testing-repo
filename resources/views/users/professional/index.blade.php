@extends('layouts.user', ['pagename' => 'Dashboard'])
@section('css')
    <style>
        .dashboard {
            color: var(--special) !important;
        }

        i[class*="person"] {
            font-size: 1.3em !important;
        }

        .latest-sessions,
        .upcoming-sessions {
            --shadow-other-b: 6px;
            --shadow-other-y: 0;
            --shadow-other-c: #5A6063
        }

        .progress-bar {
            transition-property: width;
            transition-duration: 1000ms;
            transition-timing-function: linear;
        }
        .colorChangerBack:hover{
            color: black!important;
        }
        .closeChanger:hover{
            background-color:black!important;
        }
        .blueChanger:hover{
            background-color:blue!important;
        }
        .onTheBottom{
            position:fixed!important;
            bottom: 0!important;
         }

         .onTheBottom {
                width: 66.5% !important;
            }
            @media(max-width:1300px){
                .onTheBottom{
                position: relative!important;
                width:auto!important;
                }
            }
        @media (min-width: 1200px) and (min-height: 900px) {
            .onTheBottom {
                width: 66.5% !important;
            }
        }
    </style>
@endsection
@section('user-content')
    @php
        $timezones = App\Models\Timezone::all();
    @endphp
    <!-- timezone update modal -->
    <div class="modal fade" id="changeTimezoneModal" tabindex="-1" role="dialog" aria-labelledby="changeTimezoneModalTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-black" id="changeTimezoneModalTitle">Change Timezone</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('user.timezone.update', auth()->id()) }}" class="needs-validation"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body border-0 ">
                        <select class="form-select" name="timezone" id="timezone" required>
                            <option value="">Select timezone</option>
                            @foreach ($timezones as $timezone)
                                <option value="{{ $timezone->timezone }}" @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                    {{ $timezone->timezone }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary closeChanger" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary blueChanger">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar -->
            @include('users.side-bar')

            <!-- main central contents column  -->
            <div class="col-lg-8" style="background-color: rgba(112,112,112,7%);">
                <h3 class="bg-white text-special p-3 sticky-top d-flex">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <span class="d-none d-lg-inline"><strong>Welcome {{ $user->name }}</strong></span>
                    <span class="mx-auto text-center d-inline d-lg-none"><strong>Welcome
                            <br>{{ $user->name }}</strong></span>
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                        data-bs-target="#infobar">
                        <i class="fa-solid fa-ellipsis-vertical fs-4" style="rotate:90deg"></i>
                    </button>
                </h3>
                <div class="pt-2 pb-4 px-4 px-lg-2" style="min-height: 80.6vh;">
                    <div class="row g-2">
                        <div class="col-12">
                            <h4 class="pt-4">
                                <strong>Assessments</strong>
                            </h4>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="text-light pt-3 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">{{ $personalityTest->test_name }}</div>
                                <div class="text-center">
                                    {{-- @if (!$user->hasPaidFor(4))
                                unlock
                                <a href="{{ route(" user.payments.info.index", 4) }}"
                                    class="text-decoration-none text-white">
                                    <small style="font-weight:500">Unlock test ></small>
                                </a> --}}
                                    @if ($personalityTestResults == 0)
                                        {{-- start test --}}
                                        <a href="{{ route('user.test.instructions', 4) }}"
                                            class="text-decoration-none text-white ">
                                            <small style="font-weight:500" class="colorChangerBack">Start assessment ></small>
                                        </a>
                                    @elseif($personalityTestResults > 0 && $personalityTestResults < 100)
                                        {{-- resume --}}
                                        <a href="{{ route('user.test.instructions', 4) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="colorChangerBack">Resume assessment ></small>
                                        </a>
                                    @else
                                        {{-- view score --}}
                                        <a href="#" class="text-decoration-none text-white" data-bs-toggle="modal"
                                            data-bs-target="#personalityTestScoreStaticBackdrop">
                                            <small style="font-weight:500" class="colorChangerBack">View report ></small>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="text-light pt-3 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">{{ $skillAssessmentTest->test_name }}</div>
                                <div class="text-center">
                                    {{-- @if (!$user->hasPaidFor(6))
                                <a href="{{ route(" user.payments.info.index", 6) }}"
                                    class="text-decoration-none text-white">
                                    <small style="font-weight:500">Unlock test ></small>
                                </a> --}}
                                    @if ($skillAssessmentTestResults == 0)
                                        <a href="{{ route('user.test.instructions', 6) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="colorChangerBack">Start assessment ></small>
                                        </a>
                                    @elseif($skillAssessmentTestResults > 0 && $skillAssessmentTestResults < 100)
                                        <a href="{{ route('user.test.instructions', 6) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="colorChangerBack">Resume assessment ></small>
                                        </a>
                                    @else
                                        <a href="#" class="text-decoration-none text-white" data-bs-toggle="modal"
                                            data-bs-target="#skillAssessmentTestScoreStaticBackdrop">
                                            <small style="font-weight:500" class="colorChangerBack">View report ></small>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="text-light pt-3 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">{{ $careerExpectationTest->test_name }}</div>
                                <div class="text-center">
                                    {{-- @if (!$user->hasPaidFor(5))
                                <a href="{{ route(" user.payments.info.index", 5) }}"
                                    class="text-decoration-none text-white">
                                    <small style="font-weight:500">Unlock test ></small>
                                </a> --}}
                                    @if ($careerExpectationsTestResults == 0)
                                        <a href="{{ route('user.test.instructions', 5) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="colorChangerBack">Start assessment ></small>
                                        </a>
                                    @elseif($careerExpectationsTestResults > 0 && $careerExpectationsTestResults < 100)
                                        <a href="{{ route('user.test.instructions', 5) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="colorChangerBack">Resume assessment ></small>
                                        </a>
                                    @else
                                        <a href="#" class="text-decoration-none text-white" data-bs-toggle="modal"
                                            data-bs-target="#careerExpectationsTestScoreStaticBackdrop">
                                            <small style="font-weight:500" class="colorChangerBack">View report ></small>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- latest reviews --}}
                    @if ($reports)
                        @include('users.sessions.reports.latest')
                    @endif

                    <div class="row g-2 my-2">
                        <!-- latest sessions -->
                        @include('users.sessions.latest')

                        <!-- upcoming sessions -->
                        @include('users.sessions.upcoming')
                    </div>
                </div>
                {{-- @include('footer.user') --}}
                <section class="text-bg-dark px-4 py-3 onTheBottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                                    class="d-inline d-md-none"><br></span>All rights reserved</div>
                            <div class="col-md-6 text-center text-md-end">Powered by <a href="https://www.theeducatics.com/"
                                    class="text-decoration-none lookBack text-white">The Educatics</a> | Made with <i
                                    class="fa-solid fa-heart text-danger"></i> by <a href="https://lastwavetechnology.com"
                                    class="text-decoration-none lookBack text-white" target="_blank">LWT</a>
                            </div>
                        </div>
                    </div>
                </section> 
            </div>
            <div class="col-2 offcanvas-lg offcanvas-end" id="infobar">
                <div class="px-3 text-center vh-100 sticky-top overflow-auto">
                    @include('navbars.user')
                    <div class="mt-4 mb-2 position-relative">
                        <img src="{{ asset($user->profile_photo) }}" alt="profile pic" class="rounded shadow-sm"
                            width="120" height="120">
                        <span class="position-absolute bottom-0 me-2 text-secondary" style="right:28px;cursor:pointer"
                            data-bs-toggle="modal" data-bs-target="#profilePictureModal" alt="camera">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-camera-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                <path
                                    d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2zm.5 2a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0z" />
                            </svg>
                        </span>
                    </div>
                    <div class="d-flex justify-content-center" style="gap:10px">
                        <span>
                            <b>{{ $user->fullname ?? '' }}</b>
                        </span>
                        <a href="{{ route('user.professional.edit', auth()->id()) }}" class="text-info fs-6">
                            <small>
                                <i class="fa-regular fa-pen-to-square" title="Edit Profile"></i>
                            </small>
                        </a>
                    </div>
                    <div class="ms-2 d-flex text-start justify-content-between">
                        <div>
                            <small>{{ $userProfile->age < 1 ? '< 1' : $userProfile->age }} Years</small><br>
                            <small>{{ $user->gender ?? 'N/A' }} {!! $user->gender == 'Male'
                                ? '<i
                                                                                                                                                                                                        class="fa-solid fa-person"></i>'
                                : ($user->gender == 'Female'
                                    ? '<i
                                                                                                                                                                                                        class="fa-solid fa-person-dress"></i>'
                                    : '') !!}</small>
                        </div>
                        <div class="fw-bold">|<br>|</div>
                        <div>
                            <small class="text-capitalize">{{ Str::limit($userProfile->city, 12, '...') }}</small><br>
                            <small>{{ $user->profession }}</small>
                        </div>
                    </div>
                    <hr>
                    <div class="text-start">
                        <p>
                            <b>Experience: </b>
                            {{ $userProfile->experience ?? '' }}
                        </p>
                        <p>
                            <b>Education Level: </b>
                            {{ $userProfile->education_level ?? '' }}
                        </p>
                        <p>
                            <b>Working in:</b>
                            {{ $userProfile->organization_name ?? '' }}
                        </p>
                        <p>
                            <b>Role: </b>
                            {{ $userProfile->role ?? '' }}
                        </p>
                    </div>
                    <hr>
                    <div class="text-start">
                        <span>
                            <b>Timezone: </b><a href="#" title="Change Timezone" class="text-info fs-6"
                                data-bs-toggle="modal" data-bs-target="#changeTimezoneModal">
                                <small>
                                    <i class="fa-regular fa-pen-to-square"></i>
                                </small>
                            </a><br>
                            {{ $user->timezone ?? 'N/A' }}
                        </span>
                    </div>
                    {{-- @include("users.zoom.connect") --}}
                </div>
            </div>
        </div>
    </div>



    {{-- @if (!$user->hasPaidFor(4))
<div class="modal fade" id="unlockPersonalityTestStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
    tabindex="-1" aria-hidden="true">
    @include('users.results.unlock', ['href' => route('user.payments.info.index', 4)])
</div> --}}
    @if ($personalityTestResults >= 99)
        <div class="modal fade" id="personalityTestScoreStaticBackdrop" data-bs-backdrop="static" tabindex="-1"
            aria-labelledby="personalityTestScoreStaticBackdropLabel" aria-hidden="true">
            @include('users.results.personality')
        </div>
    @endif

    {{-- @if (!$user->hasPaidFor(6))
<div class="modal fade" id="unlockSkillAssessmentTestStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
    tabindex="-1" aria-hidden="true">
    @include('users.results.unlock', ['href' => route('user.payments.info.index', 6)])
</div> --}}
    @if ($skillAssessmentTestResults >= 99)
        <div class="modal fade" id="skillAssessmentTestScoreStaticBackdrop" data-bs-backdrop="static" tabindex="-1"
            aria-labelledby="skillAssessmentScoreStaticBackdropLabel" aria-hidden="true">
            @include('users.results.skill-assessments')
        </div>
    @endif

    {{-- @if (!$user->hasPaidFor(5))
<div class="modal fade" id="unlockCareerExpectationsTestStaticBackdrop" data-bs-backdrop="static"
    data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    @include('users.results.unlock', ['href' => route('user.payments.info.index', 5)])
</div> --}}
    @if ($careerExpectationsTestResults >= 99)
        <div class="modal fade" id="careerExpectationsTestScoreStaticBackdrop" data-bs-backdrop="static" tabindex="-1"
            aria-labelledby="careerExpectationsTestScoreStaticBackdropLabel" aria-hidden="true">
            <!-- resutls dialog -->
            @include('users.results.career')
        </div>
    @endif

    <!-- Profile Picture -->
    @include('extra.profile-picture-modal')
@endsection
{{-- @push('script')
    <script>
        // Get the user's timezone offset in minutes
        var timezoneOffset = new Date().getTimezoneOffset();
        console.log(timezoneOffset);
        // Get the CSRF token from the meta tag in your HTML
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        // Send the timezone offset to the server using an AJAX request or form submission
        $.ajax({
            url: "{{ route('user.timezone.update') }}",
            type: "POST",
            data: {
                timezoneOffset: timezoneOffset,
                _token: '{!!csrf_token() !!}',
            },
            success: function(response) {
                console.log(response.message);
                console.log(response.userTimezone);
            }
        });
    </script>
@endpush --}}

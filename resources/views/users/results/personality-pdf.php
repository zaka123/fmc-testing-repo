<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Personality Test Results</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body style="font-family: sans-serif">
    <section>
        <hgroup>
            <h2 style="text-align: center;">Congratulations</h2>
            <p style="font-size: 1.25rem;">
                We're all so proud of your results, massive congratulations and well done! Look at you completely acing your exams, congrats! You're an absolute superstar, massive well done on your epic exam results. Congrats on absolutely smashing your exams!
            </p>
        </hgroup>
        <div style="font-size: 1.25rem;padding-left: 7rem;padding-right: 7rem;">
            <div>Extroversion:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#80610C;width:<?= (isset($result['extroversion'])) ? $result['extroversion'] : 0; ?>%">
                        <?= (isset($result['extroversion'])) ? $result['extroversion'] : 0; ?>%</div>
                </div>
            </div>
            <div>Neuroticism:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#ffc107;width:<?= (isset($result['neuroticism'])) ? $result['neuroticism'] : 0; ?>%">
                        <?= (isset($result['neuroticism'])) ? $result['neuroticism'] : 0; ?>%</div>
                </div>
            </div>
            <div>Conscientiousness:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#0dcaf0;width:<?= (isset($result['conscientiousness'])) ? $result['conscientiousness'] : 0; ?>%">
                        <?= (isset($result['conscientiousness'])) ? $result['conscientiousness'] : 0; ?>%</div>
                </div>
            </div>
            <div>Agreeableness:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#6c757d;width:<?= (isset($result['agreeableness'])) ? $result['agreeableness'] : 0; ?>%">
                        <?= (isset($result['agreeableness'])) ? $result['agreeableness'] : 0; ?>%</div>
                </div>
            </div>
            <div>Openness&nbsp;To&nbsp;Experience:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#ffc107;width:<?= (isset($result['opennessToExperience'])) ? $result['opennessToExperience'] : 0; ?>%">
                        <?= (isset($result['opennessToExperience'])) ? $result['opennessToExperience'] : 0; ?>%</div>
                </div>
            </div>
        </div>
        <p style="font-size: 1.25rem;">
            <strong>So the combination is:</strong>
            <a style="text-decoration: none;" href="#">
                <?php if (isset($wordcombination->title)) {
                                                            echo $wordcombination->title;
                                                        }  ?></a>
        </p>
        <div>
            <strong>Description</strong>
            <p>
                <?php if (isset($wordcombination->description)) {
                    echo $wordcombination->description;
                }  ?>
            </p>
        </div>
        <div>
            <strong>Favoured Careers</strong>
            <p>
                <?php if (isset($wordcombination->favoured_careers)) {
                    echo $wordcombination->favoured_careers;
                }  ?>
            </p>
        </div>
        <div>
            <strong>Dis Favourite Careers</strong>
            <p>
                <?php if (isset($wordcombination->disFavoured_careers)) {
                    echo $wordcombination->disFavoured_careers;
                }  ?>
            </p>
        </div>
    </section>
</body>

</html>

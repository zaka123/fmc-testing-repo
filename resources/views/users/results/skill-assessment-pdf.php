<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Skill Assessment Test Results</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body style="font-family: sans-serif">
    <section>
        <hgroup>
            <h2 style="text-align: center;">Congratulations</h2>
            <p style="font-size: 1.25rem;">
                We're all so proud of your results, massive congratulations and well done! Look at you completely acing your exams, congrats! You're an absolute superstar, massive well done on your epic exam results. Congrats on absolutely smashing your exams!
            </p>
        </hgroup>
        <div style="font-size: 1.25rem;padding-left: 7rem;padding-right: 7rem;">
            <div>In Meeting:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color: #198754;width:<?= (isset($result['inMeetings'])) ? $result['inMeetings'] : 0; ?>%">
                        <?= (isset($result['inMeetings'])) ? round($result['inMeetings'], 2) : 0; ?>%</div>
                </div>
            </div>
            <div>Outside Meeting:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color: #0d6efd;width:<?= (isset($result['outsideMeetings'])) ? $result['outsideMeetings'] : 0; ?>%">
                        <?= (isset($result['outsideMeetings'])) ? round($result['outsideMeetings'], 2) : 0; ?>%</div>
                </div>
            </div>
            <div>Speaking:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color: #6c757d;width:<?= (isset($result['speaking'])) ? $result['speaking'] : 0; ?>%">
                        <?= (isset($result['speaking'])) ? round($result['speaking'], 2) : 0; ?>%</div>
                </div>
            </div>
            <div>Writing:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#0dcaf0;width:<?= (isset($result['writing'])) ? $result['writing'] : 0; ?>%">
                        <?= (isset($result['writing'])) ? round($result['writing'], 2) : 0; ?>%</div>
                </div>
            </div>
            <div>Organizing:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#ffc107;width:<?= (isset($result['organizing'])) ? $result['organizing'] : 0; ?>%">
                        <?= (isset($result['organizing'])) ? round($result['organizing'], 2) : 0; ?>%</div>
                </div>
            </div>
            <div>Creativity:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#212529;width:<?= (isset($result['creativity'])) ? $result['creativity'] : 0; ?>%">
                        <?= (isset($result['creativity'])) ? round($result['creativity'], 2) : 0; ?>%</div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>

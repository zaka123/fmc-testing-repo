<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header shadow-sm border-0 align-items-start">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body px-5" style="font-size:.9rem;">
            <div class="py-3">
                <div class="row gy-2" style="font-weight: 500;">
                    <div class="col-12 fs-6">
                        <b>Teamwork Skills</b>
                    </div>
                    <div class="col-md-5">
                        In Meeting:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$skillAssessmentTestResults['inMeetings'] ?? '0'}}%" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['inMeetings'] ,0) }}%
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        Outside Meetings:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-primary" role="progressbar" style="width: {{$skillAssessmentTestResults['outsideMeetings'] ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['outsideMeetings'] ,0) }}%
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-12 fs-6">
                        <b>Communication Skills</b>
                    </div>
                    <div class="col-md-5">
                        Speaking:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-secondary" role="progressbar" style="width: {{$skillAssessmentTestResults['speaking'] ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['speaking'] ,0) }}%
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        Writing:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" style="width: {{$skillAssessmentTestResults['writing'] ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['writing'] ,0) }}%
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-12 fs-6">
                        <b>Organization Skills</b>
                    </div>
                    <div class="col-md-5">
                        Organizing:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{$skillAssessmentTestResults['organizing'] ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['organizing'] ,0) }}%
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-12 fs-6">
                        <b>Creativity Skills</b>
                    </div>
                    <div class="col-md-5">
                        Creativity:
                    </div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar bg-dark" role="progressbar" style="width: {{$skillAssessmentTestResults['creativity'] ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($skillAssessmentTestResults['creativity'] ,0) }}%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer border-0 justify-content-center shadow"></div>
    </div>
</div>

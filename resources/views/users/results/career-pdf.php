<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Career Expectations Questionaire Results</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body style="font-family: sans-serif">
    <section>
        <hgroup>
            <h2 style="text-align: center;">Congratulations</h2>
            <p style="font-size: 1.25rem;">
                We're all so proud of your results, massive congratulations and well done! Look at you completely acing your exams, congrats! You're an absolute superstar, massive well done on your epic exam results. Congrats on absolutely smashing your exams!
            </p>
        </hgroup>
        <div style="font-size: 1.25rem;padding-left: 7rem;padding-right: 7rem;">
            <div>Competition:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#80610C;width:<?= (isset($result['competition'])) ? $result['competition'] : 0; ?>%">
                        <?= (isset($result['competition'])) ? $result['competition'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Freedom:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#0d6efd;width:<?= (isset($result['freedom'])) ? $result['freedom'] : 0; ?>%">
                        <?= (isset($result['freedom'])) ? $result['freedom'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Management:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#0dcaf0;width:<?= (isset($result['management'])) ? $result['management'] : 0; ?>%">
                        <?= (isset($result['management'])) ? $result['management'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Life Balance:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#6c757d;width:<?= (isset($result['lifeBalance'])) ? $result['lifeBalance'] : 0; ?>%">
                        <?= (isset($result['lifeBalance'])) ? $result['lifeBalance'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Organization Membership:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#ffc107;width:<?= (isset($result['organisationMembership'])) ? $result['organisationMembership'] : 0; ?>%">
                        <?= (isset($result['organisationMembership'])) ? $result['organisationMembership'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Expertise:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#212529;width:<?= (isset($result['expertise'])) ? $result['expertise'] : 0; ?>%">
                        <?= (isset($result['expertise'])) ? $result['expertise'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Learning:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#0d6efd;width:<?= (isset($result['learning'])) ? $result['learning'] : 0; ?>%">
                        <?= (isset($result['learning'])) ? $result['learning'] : 0; ?>%
                    </div>
                </div>
            </div>
            <div>Entrepreneurship:</div>
            <div style="margin-bottom: 1rem;">
                <div style="width:100%;display: flex;height: 1.1rem;overflow: hidden;font-size: 0.75rem;background-color: #e9ecef;border-radius: 0.375rem">
                    <div style="padding:0 2px;display: flex;flex-direction: column;justify-content: center;overflow: hidden;color: #fff;text-align: center;white-space: nowrap;transition: width 0.6s ease;background-color:#ffc107;width:<?= (isset($result['entrepreneurship'])) ? $result['entrepreneurship'] : 0; ?>%">
                        <?= (isset($result['entrepreneurship'])) ? $result['entrepreneurship'] : 0; ?>%
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <hgroup>
            <h6 style="font-weight:bold;">Competition</h6>
            <p>The idea of a career as a contest competing with others and with definite success
                indicators is important to you. You need to have recognition of your achievements – if not
                you will feel dissatisfied and frustrated.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Freedom</h6>
            <p>You expect to have considerable autonomy over your day to day work, in how you
                approach it and setting priorities. You expect to be evaluated on ultimate achievements
                rather than detailed methods. You would not take kindly to someone watching closely
                over your shoulder.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Management</h6>
            <p>It is important for you to be able to use a range of generalist skills to achieve results
                through and with others. Position, title and status (along with the rewards) are important
                to you and you need to achieve a position of responsibility quickly.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Life Balance</h6>
            <p>For you the right balance between work and the rest of your life is important. You will
                seek opportunities that allow you to develop this balance and will feel resentment if (in
                your eyes) unacceptable demands are placed upon you that intrude into your non-work
                space. You expect give and take, and flexible working practices.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Organisation Membership</h6>
            <p>You will identify strongly with organisational goals and values. You are a person who will
                enjoy being seen as an “organisational man or woman” and your needs and values will fit
                very closely with the organisation. You are likely to put company needs before your own.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Expertise</h6>
            <p>You need to have the opportunity to develop your expertise and to become more self-
                confident about your personal value. Your security comes from the opportunity to
                specialise and you would find any attempt to make you a generalist rather unsettling,
                feeling vulnerable and exposed.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Learning</h6>
            <p>You will thrive on being challenged and learning to overcome difficulties through
                acquisition of new skills and expertise. Your positive attitude will allow you to tackle issues
                5and problems that may seem daunting to others. If you are not constantly challenged with
                new learning opportunities you will feel a lack of direction.</p>
        </hgroup>
        <hgroup>
            <h6 style="font-weight:bold;">Entrepreneurship</h6>
            <p>Risk taking is your lifeblood and source of stimulation rather than being frightened and
                resistant. Self-conviction, self-determination, self-control are essential to you whether they
                exist through self-employment or an organisation that allows you this scope.</p>
        </hgroup>
    </section>
</body>

</html>

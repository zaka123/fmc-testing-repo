@extends('layouts.user', ['pagename' => 'Tests Reports'])
@section('user-content')
    <style>
        .text-justify {
            text-align: justify !important;
        }

        @media(max-width:992px) {
            .full-width {
                width: 95% !important;
            }
        }
    </style>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->

            <!-- show profile -->
            <div class="col-lg-10">
                <div class="row g-0">
                    <div class="col-12 sticky-top">
                        <div class="d-flex px-3 pb-lg-3">
                            <button class="btn mt-3 btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar">
                                <i class="fa-solid fa-bars fs-4"></i>
                            </button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-center"
                        style="height: calc(100vh - 156px);overflow:auto">
                        <div class="floating"></div>
                        @php
                            $personalityTestResults = auth()
                                ->user()
                                ->getTestResults(4);

                            $wordCombination = '';
                            $personalityTestResults['extroversion'] > 20 ? ($wordCombination = 'S') : ($wordCombination = 'R');
                            $personalityTestResults['neuroticism'] > 20 ? ($wordCombination .= 'L') : ($wordCombination .= 'C');
                            $personalityTestResults['conscientiousness'] > 20 ? ($wordCombination .= 'O') : ($wordCombination .= 'U');
                            $personalityTestResults['agreeableness'] > 20 ? ($wordCombination .= 'A') : ($wordCombination .= 'E');
                            $personalityTestResults['opennessToExperience'] > 20 ? ($wordCombination .= 'I') : ($wordCombination .= 'N');
                        @endphp
                        <div class="px-md-5 w-75 py-3 inner full-width h-auto d-flex align-items-center" style="font-size:.9rem;overflow:auto">
                            <section class="mb-2 h-50 w-100">
                                {{-- <h6>Description</h6> --}}
                                <h6 class="px-4">Career You should Choose</h6>
                                <p id="description" class="text-justify  p-0 m-0" style="display: flex!important"></p>
                            </section>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>
            <!--/ show profile -->
        </div>
    </div>
@endsection

@push('script')
    <script>
        var url = "{{ url('user/benefit') }}" + "/{{ $wordCombination }}"
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {

                $('#description').append(data.description);
                $('#favoured_careers').text(data.favoured_careers);
                $('#disFavoured_careers').text(data.disFavoured_careers);
            }, //end of success
            error: function(data) {
                $(window).scrollTop(0);
            }
        });
    </script>
@endpush

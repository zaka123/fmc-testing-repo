@extends('layouts.user', ['pagename' => 'Tests Reports'])
@section('css')
    <style>
        .career_choose--img {
            width: 160px;
            height: 200px;
        }

        .career_choose--img img {
            width: 100%;
            height: 80%;
            object-fit: cover;
            object-position: top center;
        }

        .career_choose--img h2 {
            background: #005291 0% 0% no-repeat padding-box;
            opacity: 1;
            font-weight: 600;
            color: white;
            text-transform: uppercase;
            font-size: 14px;
            padding: 9px 0;
            text-align: center;
            line-height: 1.5;
        }

        .career__choose--option {
            width: fit-content;
            padding: 5px 15px;
            background: #729CC4 0% 0% no-repeat padding-box;
            border-radius: 10px;
            opacity: 1;
            color: white;
            transition: background 0.3s ease-in-out;
        }

        .career__choose--option:hover {
            background: #415B74 0% 0% no-repeat padding-box;
        }

        .career__choose--row {
            border-bottom: 2px solid rgba(128, 128, 128, 0.534);
            padding-bottom: 30px;
            margin-top: 30px;
        }
    </style>
@endsection
@section('user-content')
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    <div class="col-12 bg-white">
                        @include('includes.header')
                    </div>
                    <div class="col-12">
                        <div class="main px-3 px-lg-0" style="min-height:88vh">
                            <div class="row justify-content-center" style="--bs-gutter-x:0">
                                <div class="col-lg-10">
                                    <div class="mt-4">
                                        <span class="spacial_bold_monstrate_text fs-2 fw-bolder report d-block"
                                            style="border-bottom: 2px solid rgba(128, 128, 128, 0.658)">
                                            <i>
                                                CAREER YOU SHOULD CHOOSE
                                                {{-- ({{ $wordCombination }}) --}}
                                            </i>
                                        </span>
                                    </div>
                                    <div class="mt-5">
                                        @foreach ($careersToChoose as $mainCareer => $subCareers)
                                            <div class="row career__choose--row" style="--bs-gutter-x:0">
                                                <div class="col-md-3 career_choose--img p-0">
                                                    {{-- <img src="https://source.unsplash.com/random/?{{ $mainCareer }}"
                                                        alt=""> --}}
                                                    <img src="{{ asset('assets/courses_pics/' . $mainCareer . '.webp') }}"
                                                        alt="">
                                                    <h2 class="spacial_bold_monstrate_text">{{ $mainCareer }}</h2>
                                                </div>
                                                <div class="col-md-9  ps-5 pt-4">
                                                    <div class=" d-flex align-items-center flex-wrap gap-3 ">
                                                        @foreach ($subCareers as $career)
                                                            <div class="career__choose--option">
                                                                {{ $career->sub_subject }}
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        const numb = document.querySelectorAll(".numb");
        let counter = 0;
        numb.forEach((single) => {
            setInterval(() => {
                if (counter == 100) {
                    clearInterval();
                } else {
                    counter += 1;
                    single.textContent = counter + "%";
                }
            }, 80);
        })
    </script>
@endpush


{{-- @push('script')
    <script>
        var url = "{{ url('user/benefit') }}" + "/{{ $wordCombination }}"
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {

                $('#description').append(data.description);
                $('#favoured_careers').text(data.favoured_careers);
                $('#disFavoured_careers').text(data.disFavoured_careers);
            }, //end of success
            error: function(data) {
                $(window).scrollTop(0);
            }
        });
    </script>
@endpush --}}

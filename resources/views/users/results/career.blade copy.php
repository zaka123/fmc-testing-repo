<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" style="--bs-modal-width: 620px;">
    <div class="modal-content">
        <div class="modal-header shadow-sm border-0 align-items-start">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body px-5" style="font-size:.9rem;">
            <div class="px-5 py-3">
                @php
                    $colors = ["bg-success", "bg-primary", "bg-secondary", "bg-info", "bg-warning", "bg-dark", "bg-blue", "bg-yellow"];
                @endphp
                <div class="row gy-2" style="font-weight: 500;">
                    {{-- @foreach($careerExpectationsTestResults as $key=>$value) --}}
                    @foreach($careerExpectationsTestResults as $key=>$value)
                    <div class="col-md-5">{!! $key !!}:</div>
                    <div class="col-md-7">
                        <div class="progress">
                            <div class="progress-bar {{ $colors[$loop->index] }}" role="progressbar" style="width: {{ $value ?? '0'}}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                {{ round($value, 0) }}%
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <section>
                <hgroup>
                    <h6>Competition</h6>
                    <p>The idea of a career as a contest competing with others and with definite success
                        indicators is important to you. You need to have recognition of your achievements – if not
                        you will feel dissatisfied and frustrated.</p>
                </hgroup>
                <hgroup>
                    <h6>Freedom</h6>
                    <p>You expect to have considerable autonomy over your day to day work, in how you
                        approach it and setting priorities. You expect to be evaluated on ultimate achievements
                        rather than detailed methods. You would not take kindly to someone watching closely
                        over your shoulder.</p>
                </hgroup>
                <hgroup>
                    <h6>Management</h6>
                    <p>It is important for you to be able to use a range of generalist skills to achieve results
                        through and with others. Position, title and status (along with the rewards) are important
                        to you and you need to achieve a position of responsibility quickly.</p>
                </hgroup>
                <hgroup>
                    <h6>Life Balance</h6>
                    <p>For you the right balance between work and the rest of your life is important. You will
                        seek opportunities that allow you to develop this balance and will feel resentment if (in
                        your eyes) unacceptable demands are placed upon you that intrude into your non-work
                        space. You expect give and take, and flexible working practices.</p>
                </hgroup>
                <hgroup>
                    <h6>Organisation Membership</h6>
                    <p>You will identify strongly with organisational goals and values. You are a person who will
                        enjoy being seen as an “organisational man or woman” and your needs and values will fit
                        very closely with the organisation. You are likely to put company needs before your own.</p>
                </hgroup>
                <hgroup>
                    <h6>Expertise</h6>
                    <p>You need to have the opportunity to develop your expertise and to become more self-
                        confident about your personal value. Your security comes from the opportunity to
                        specialise and you would find any attempt to make you a generalist rather unsettling,
                        feeling vulnerable and exposed.</p>
                </hgroup>
                <hgroup>
                    <h6>Learning</h6>
                    <p>You will thrive on being challenged and learning to overcome difficulties through
                        acquisition of new skills and expertise. Your positive attitude will allow you to tackle issues
                        5and problems that may seem daunting to others. If you are not constantly challenged with
                        new learning opportunities you will feel a lack of direction.</p>
                </hgroup>
                <hgroup>
                    <h6>Entrepreneurship</h6>
                    <p>Risk taking is your lifeblood and source of stimulation rather than being frightened and
                        resistant. Self-conviction, self-determination, self-control are essential to you whether they
                        exist through self-employment or an organisation that allows you this scope.</p>
                </hgroup>
            </section>
        </div>
        <div class="modal-footer border-0 justify-content-center shadow"></div>
    </div>
</div>

<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header border-0 align-items-start">
            <button type="button" class="btn-close" style="opacity:1" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body px-5">
            <div class="px-5" style="font-weight:500">
                <h4 class="text-danger text-center">We are sorry!</h4>
                <p>Please give your test to view score.</p>
                <p>You did not attempt the test.</p>
                <p>Thanks.</p>
            </div>
        </div>
        <div class="modal-footer justify-content-center border-0">
            <a href="{{ $href ?? '#' }}" class="btn btn-sm btn-warning">Unlock</a>
        </div>
    </div>
</div>

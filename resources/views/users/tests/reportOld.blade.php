@extends('layouts.user', ['pagename' => 'Tests Reports'])
@section('css')
    <style>
        .test-report {
            color: var(--special) !important;
            font-weight: 500
        }

        .restart-test {
            bottom: -55px !important;
            left: 50% !important;
            box-shadow: 0px 3px 6px #707070 !important;
            backdrop-filter: blur(6px) !important;
            border-radius: 4px !important;
            translate: -50%;
        }

        @media(max-width:384px) {
            .restart-test {
                width: 70%;
            }
        }

        .yellowishChanger:hover {
            background-color: rgb(216, 216, 36) !important;
        }

        .blackishChanger:hover {
            background-color: black !important;
        }

        .redChanger:hover {
            background-color: red !important;
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }

        /* new page styles */

        .skill-box {
            width: 100%;
            margin: 10px 0;
        }

        .skill-box .title {
            font-weight: 600 !important;
        }

        .skill-bar-wrapper {
            width: 70%;
            height: 35px;
            background: #FFFFFF 0% 0% no-repeat padding-box;
            box-shadow: 0px 3px 6px #00000029;
            border: 1px solid #D1CCCC;
            border-radius: 25px;
            opacity: 1;
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 5px 10px;
            margin-left: auto;
        }

        .skill-box .skill-bar {
            height: 15px;
            width: 100%;
            background: transparent linear-gradient(0deg, #FFFFFF 0%, #D7D7D8 38%, #B0AFB2 100%) 0% 0% no-repeat padding-box;
            border-radius: 3px;
            display: flex;
            align-items: center;
            padding-left: 3px;

        }

        .skill-bar .skill-per {
            position: relative;
            display: block;
            height: 60%;
            width: 70%;
            border-radius: 3px;
            /* background: #06A512; */
            animation: progress 0.4s ease-in-out forwards;
            opacity: 0;
        }

        .freedom {
            background-color: #06A512;
        }

        .Entrepreneurship {
            background-color: #06A512;
        }

        .Management {
            background-color: #EF8D0C;
        }

        .Organization {
            background-color: #06A5A5;
        }

        .Expertise {
            background-color: #EF8D0C;
        }

        .Learning {
            background-color: #06A5A5;
        }

        .Competition {
            background-color: #FF4343;
        }

        .Life {
            background-color: #FF4343;
        }

        @keyframes progress {
            0% {
                width: 0;
                opacity: 1;
            }

            100% {
                opacity: 1;
            }
        }

        .skill-per .tooltip {
            position: absolute;
            right: -21px;
            top: -54px;
            padding: 10px !important;
            font-size: 17px;
            font-weight: bold;
            font-family: 'Montserrat', sans-serif !important;
            letter-spacing: 0px;
            color: #496C8C;
            opacity: 1;
            color: #496C8C;
            padding: 2px 6px;
            border-radius: 7px;
            background: transparent linear-gradient(0deg, #DCDCDC 0%, #FFFFFF 100%) 0% 0% no-repeat padding-box;
            box-shadow: 0px 4px 1px #2727271e;
            opacity: 1;
        }

        .tooltip::before {
            content: '';
            position: absolute;
            left: 45%;
            bottom: -10px;
            height: 15px;
            width: 5px;
            z-index: -1;
            background: transparent linear-gradient(0deg, #FFFFFF 0%, #999999 100%) 0% 0% no-repeat padding-box;
            opacity: 1;
        }

        .tooltip::after {
            content: '';
            position: absolute;
            left: 50%;
            bottom: -25px;
            height: 20px;
            width: 20px;
            border: 4px solid #F0F0F1;
            border-radius: 50%;
            z-index: -1;
            background-color: #729CC4;
            transform: translateX(-50%) rotate(45deg);
        }

        .report {
            border-bottom: 3px solid rgba(128, 128, 128, 0.253);
            display: block;
        }

        .circular {
            height: 100px;
            width: 100px;
            position: relative;
        }

        .circular .inner-bar,
        .circular .outer,
        .circular .circle {
            position: absolute;
            z-index: 6;
            height: 100%;
            width: 100%;
            border-radius: 100%;
            box-shadow: inset 0 1px 0 rgba(0, 0, 0, 0.2);
        }

        .circular .outer {
            box-shadow: inset 2px 2px 5px #000;
        }

        .circular .inner-bar {
            top: 50%;
            left: 50%;
            height: 80px;
            width: 80px;
            margin: -40px 0 0 -40px;
            background: #F2F2F2 0% 0% no-repeat padding-box;
            box-shadow: -1px 1px 2px #36353585;
            border-radius: 100%;
            opacity: 1;
        }

        .circular .circle {
            z-index: 1;
            box-shadow: none;
        }

        .circular .numb {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 10;
            font-size: 25px;
            font-weight: 500;
            color: #7100A7;
        }

        .circular .bar {
            position: absolute;
            height: 100%;
            width: 100%;
            background: #F2F2F2 0% 0% no-repeat padding-box;
            -webkit-border-radius: 100%;
            clip: rect(0px, 100px, 100px, 50px);
        }

        .circle .bar .progress {
            position: absolute;
            height: 100%;
            width: 100%;
            -webkit-border-radius: 100%;
            clip: rect(0px, 50px, 100px, 0px);
        }

        .circle .bar .progress,
        .dot span {
            background: transparent linear-gradient(30deg, #7100A7 0%, #DE007F 100%) 0% 0% no-repeat padding-box;
        }

        .circle .left .progress {
            z-index: 1;
            animation: left 4s linear both;
        }

        @keyframes left {
            100% {
                transform: rotate(180deg);
            }
        }

        .circle .right {
            z-index: 3;
            transform: rotate(180deg);
        }

        .circle .right .progress {
            animation: right 4s linear both;
            animation-delay: 4s;
        }

        @keyframes right {
            100% {
                transform: rotate(180deg);
            }
        }

        .circle .dot {
            z-index: 2;
            position: absolute;
            left: 50%;
            top: 50%;
            width: 50%;
            height: 10px;
            margin-top: -5px;
            animation: dot 8s linear both;
            transform-origin: 0% 50%;
        }

        .circle .dot span {
            position: absolute;
            right: 0;
            width: 10px;
            height: 10px;
            border-radius: 100%;
        }

        @keyframes dot {
            0% {
                transform: rotate(-90deg);
            }

            50% {
                transform: rotate(90deg);
                z-index: 4;
            }

            100% {
                transform: rotate(270deg);
                z-index: 4;
            }
        }

        .circular_bar_results {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: start;
            flex-wrap: wrap;
        }

        .category {
            border-bottom: 3px solid rgba(128, 128, 128, 0.253);
        }

        .single_circular_bar {
            width: 25%;
            display: flex;
            flex-direction: column;
            align-items: start;
            justify-content: center !important;
            gap: 20px;
            margin: 20px;
        }

        @media(max-width:568px) {
            .single_circular_bar {
                width: 50%;
                align-items: center;
                margin: 10px 0px;
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="modal fade" id="restartTestModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
        aria-labelledby="restartTestModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="restartTestModalLabel">Attention!</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>If you restart the assessment, you wouldn't be able to see your previous results.</p>
                    <p>Are you sure you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary blackishChanger"
                        data-bs-dismiss="modal">No</button>
                    <a id="restartLink" {{-- href="{{ url('user/test/4/restart') }}" --}} class="btn btn-sm btn-danger redChanger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    <div class="col-12 bg-white">
                        @include('includes.header')
                    </div>
                    <div class="col-12">
                        <div class="main px-3 px-lg-0" style="min-height:88vh">
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <div class="mt-4">
                                        <span class="spacial_bold_monstrate_text fs-2 fw-bolder report"><i>ASSESMENT
                                                REPORT</i></span>
                                        <!-- for freedom start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Freedom:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per freedom">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for freedom ends here -->
                                        <!-- for Entrepreneurship: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span
                                                class="special_monstrate_text text-dark title">Entrepreneurship:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Entrepreneurship">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Entrepreneurship: ends here -->
                                        <!-- for Management: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Management:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Management">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Management: ends here -->
                                        <!-- for Organization Membership:: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Organization
                                                Membership:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Organization">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Organization Membership:: ends here -->
                                        <!-- for Expertise: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Expertise:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Expertise">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Expertise: ends here -->
                                        <!-- for Learning:: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Learning:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Learning">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Learning: ends here -->
                                        <!-- for Competition:: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Competition:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Competition">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Competition: ends here -->
                                        <!-- for Life Balance: start here -->
                                        <div class="skill-box mt-5 mb-3">
                                            <span class="special_monstrate_text text-dark title">Life Balance:</span><br>
                                            <div class="skill-bar-wrapper">
                                                <div class="skill-bar">
                                                    <span class="skill-per Life">
                                                        <span class="tooltip">70%</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- for Life Balance: ends here -->
                                    </div>
                                    <div class="mt-5">
                                        <span class="spacial_bold_monstrate_text fs-2 fw-bolder report"><i>SKILL ASSESMENT
                                                REPORT</i></span>
                                        <!-- category for teamwork skills starts here -->
                                        <div class="category">
                                            <span class="spacial_bold_monstrate_text fs-5 fw-bold mt-3 d-block">TEAMWORK
                                                SKILLS</span>
                                            <div class="circular_bar_results">
                                                <!-- for in meeting  starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">In
                                                        Meeting</span>
                                                </div>
                                                <!-- for in meeting  ends here-->
                                                <!-- for in meeting  starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">Outside
                                                        Meeting</span>
                                                </div>
                                                <!-- for in meeting  ends here-->

                                            </div>
                                        </div>
                                        <!-- category for teamwork skills ends here -->
                                        <!-- category for Communication skills starts here -->
                                        <div class="category">
                                            <span
                                                class="spacial_bold_monstrate_text fs-5 fw-bolder mt-3 d-block">COMMUNICATION
                                                SKILLS
                                                SKILLS</span>
                                            <div class="circular_bar_results">
                                                <!-- for speaking starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span
                                                        class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">Speaking</span>
                                                </div>
                                                <!-- for speaking ends here-->
                                                <!-- for listening starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span
                                                        class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">Listening</span>
                                                </div>
                                                <!-- for listening ends here-->
                                            </div>
                                        </div>
                                        <!-- category for communication skills ends here -->
                                        <!-- category for organizition skills starts here -->
                                        <div class="category">
                                            <span
                                                class="spacial_bold_monstrate_text fs-5 fw-bolder mt-3 d-block">ORGANIZATIONAL
                                                SKILLS</span>
                                            <div class="circular_bar_results">
                                                <!-- for organinzing starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span
                                                        class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">Organizing</span>
                                                </div>
                                                <!-- for organizing ends here-->
                                            </div>
                                        </div>
                                        <!-- category for organization skills ends here -->
                                        <!-- category for creativity skills starts here -->
                                        <div class="category">
                                            <span
                                                class="spacial_bold_monstrate_text fs-5 fw-bolder mt-3 d-block">CREATIVITY
                                                SKILLS</span>
                                            <div class="circular_bar_results">
                                                <!-- for organinzing starts here-->
                                                <div class="single_circular_bar">
                                                    <div class="circular">
                                                        <div class="inner-bar"></div>
                                                        <div class="outer"></div>
                                                        <div class="numb spacial_bold_monstrate_text fs-3 fw-bolder">
                                                            0%
                                                        </div>
                                                        <div class="circle" style="background-color: blue;">
                                                            <div class="dot">
                                                                <span></span>
                                                            </div>
                                                            <div class="bar left">
                                                                <div class="progress"></div>
                                                            </div>
                                                            <div class="bar right">
                                                                <div class="progress"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span
                                                        class="spacial_bold_monstrate_text fs-6 fw-medium ps-3">Creativity</span>
                                                </div>
                                                <!-- for organizing ends here-->
                                            </div>
                                        </div>
                                        <!-- category for creativity ends here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        const numb = document.querySelectorAll(".numb");
        let counter = 0;
        numb.forEach((single) => {
            setInterval(() => {
                if (counter == 100) {
                    clearInterval();
                } else {
                    counter += 1;
                    single.textContent = counter + "%";
                }
            }, 80);
        })
    </script>
@endpush

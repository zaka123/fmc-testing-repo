@extends('layouts.user', ['pagename' => 'Assessments'])
@section('css')
    <style>
        .counsellorz {
            color: var(--special) !important;
            font-weight: 500;
        }

        #more {
            display: none;
        }

        #readBtn {
            text-decoration: none;
        }

        @media (max-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        @media (min-width: 576px) {
            .my-image {
                width: 50px !important;
            }
        }

        /* Medium devices (tablets, 768px and up) */
        @media (min-width: 768px) {
            .my-image {
                width: 100px !important;
            }
        }

        /* Large devices (desktops, 992px and up) */
        @media (min-width: 992px) {
            .my-image {
                width: 150px !important;
            }
        }

        .profile {
            cursor: pointer !important;
        }

        .inner::before {
            top: 69%;
        }

        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        .scrollmenu {
            white-space: nowrap;
            overflow-x: auto;
        }

        .scrollmenu::-webkit-scrollbar {
            width: 1px !important;
        }

        .scrollmenu::-webkit-scrollbar-track {
            background: lightgrey;
            width: 1px !important;
            border-radius: 1rem;

        }

        .scrollmenu::-webkit-scrollbar-thumb {
            background: #888;
            border-radius: 10px;
        }

        .scrollmenu::-webkit-scrollbar-thumb:hover {

            background: #555;
        }

        .fontChanger:hover {
            background-color: black !important;
        }

        .closeChanger:hover {
            background-color: black !important;
        }

        .saveChanger:hover {
            background-color: rgb(53, 53, 175) !important;
        }

        .helping_list_item {
            list-style: none;
            font: normal normal 600 17px/19px;
            font-family: 'Montserrat', sans-serif !important;
            letter-spacing: 0px;
            color: black;
            opacity: 1;
            position: relative;
            margin: 20px 0px
        }

        .helping_list_item::before {
            content: "\f058";
            font-family: 'Font Awesome 5 Free';
            font-weight: 100;
            color: #496C8C;
            position: absolute;
            left: -30px;
        }

        .career-image {
            object-fit: fill;
            margin: auto;
            display: block
        }

        @media(max-width:992px) {
            .career-image {
                width: 90%;
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="modal fade" id="restartTestModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
        aria-labelledby="restartTestModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="restartTestModalLabel">Attention!</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>If you restart the assessment, you wouldn't be able to see your previous results.</p>
                    <p>Are you sure you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary blackishChanger"
                        data-bs-dismiss="modal">No</button>
                    <a id="restartLink" {{-- href="{{ url('user/test/4/restart') }}" --}} class="btn btn-sm btn-danger redChanger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <div class="col-lg-10">
                <div class="row g-0">
                    @include('includes.header')
                    <div class="col-12">
                        <div class="main" style="min-height:91vh">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="m-auto" style="width: 90%">
                                        <img src="{{ asset('new_design_assets/img/Group 445.png') }}" alt=""
                                            width="400" height="300" class="career-image">
                                        <h2 class="text-center spacial_bold_monstrate_text my-3" style="font-style: italic">
                                            ASSESSMENTS</h2>
                                        <p class="m-auto special_monstrate_text text-dark" style="text-align:justify">
                                            Unleash your potential with our Skill Assessment tool, meticulously crafted to
                                            uncover your distinctive strengths and growth opportunities. Dive into the
                                            skills test to unveil insights into your proficiencies across diverse domains,
                                            encompassing both technical prowess and soft skills. Furthermore, delve into our
                                            Career Expectation Assessment to illuminate your professional ambitions and
                                            preferences, providing invaluable clarity on your career trajectory. Armed with
                                            these assessments, you'll cultivate a profound understanding of your
                                            capabilities and aspirations, empowering you to navigate your career journey
                                            with confidence and purpose. </p>
                                        @if ($skillAssessmentTestResults)
                                            @if ($skillAssessmentTestResults == 0)
                                                <a id="startLink6" href="{{ url('user/test/6') }}"
                                                    class="anchor-button my-3 m-auto">Access your skills</a>
                                            @elseif ($skillAssessmentTestResults > 0 && $skillAssessmentTestResults < 100)
                                                <a href="{{ url('user/test/6') }}" class="anchor-button my-3 m-auto">Resume
                                                    Assessments</a>
                                            @else
                                                <a href="{{ route('user.getTestReport', 'skill-assessment') }}"
                                                    class="anchor-button my-3 m-auto">View Report</a>

                                                <a class="anchor-button my-3 m-auto" href="javascript:void(0)"
                                                    onclick="openRestartModal(6)">
                                                    Restart Assessment</a>
                                            @endif
                                        @else
                                            <a id="startLink6" href="{{ url('user/test/6') }}"
                                                class="anchor-button my-3 m-auto">Access your skills</a>
                                        @endif

                                        <hr style="background-color: rgb(59, 58, 58);height:3px">
                                        <h4 class="special_monstrate_text text-dark fw-bolder fs-6">We can help you if:</h4>
                                        <ul class="helping_list">
                                            <li class="helping_list_item">Personalized skill assessment to identify
                                                strengths and areas for development.</li>
                                            <li class="helping_list_item">Insights into your career expectations and goals
                                                through our career expectation assessment.</li>
                                            <li class="helping_list_item">Tailored guidance and recommendations based on
                                                your assessment results.</li>
                                            <li class="helping_list_item">Resources and tools to help you enhance and
                                                leverage your existing skills.</li>
                                            <li class="helping_list_item">Support in setting realistic career goals aligned
                                                with your expectations and aspirations.</li>
                                            <li class="helping_list_item">Mentorship and advice from industry experts to
                                                navigate your career development journey effectively.</li>
                                            <li class="helping_list_item">Ongoing assistance and follow-up to track your
                                                progress and adapt your career strategy as needed.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('footer.user')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        function openRestartModal(testNumber) {
            const restartLink = document.getElementById('restartLink');
            restartLink.href = `/user/test/${testNumber}/restart`;

            const modal = new bootstrap.Modal(document.getElementById('restartTestModal'));
            modal.show();
        }

        // Update the button text on page load
        btnTextChange(6);

        function btnTextChange(testNumber) {
            // Update the button text to "Resume Assessment" if the test is in progress
            var taskName = 'tasks-' + testNumber;
            let tasks = JSON.parse(localStorage.getItem(taskName));
            if (tasks !== null) {
                /* Update the button text to "Resume Assessment" */
                $(`#startLink${testNumber}`).html('');
                $(`#startLink${testNumber}`).html('Resume Assessment');
            }
        }
    </script>
@endpush

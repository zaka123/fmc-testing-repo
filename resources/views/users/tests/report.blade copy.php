@extends('layouts.user', ['pagename' => 'Tests Reports'])
@section('css')
    <style>
        .test-report {
            color: var(--special) !important;
            font-weight: 500
        }

        .restart-test {
            bottom: -55px !important;
            left: 50% !important;
            box-shadow: 0px 3px 6px #707070 !important;
            backdrop-filter: blur(6px) !important;
            border-radius: 4px !important;
            translate: -50%;
        }

        @media(max-width:384px) {
            .restart-test {
                width: 70%;
            }
        }

        .yellowishChanger:hover {
            background-color: rgb(216, 216, 36) !important;
        }

        .blackishChanger:hover {
            background-color: black !important;
        }

        .redChanger:hover {
            background-color: red !important;
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
    </style>
@endsection
@section('user-content')
    <div class="modal fade" id="restartTestModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
        aria-labelledby="restartTestModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="restartTestModalLabel">Attention!</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>If you restart the assessment, you wouldn't be able to see your previous results.</p>
                    <p>Are you sure you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary blackishChanger"
                        data-bs-dismiss="modal">No</button>
                    <a id="restartLink" {{-- href="{{ url('user/test/4/restart') }}" --}} class="btn btn-sm btn-danger redChanger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar here  -->
            @include('users.side-bar')
            <!--/ side bar -->
            <div class="col-md-10 smallResponsive">
                <div class="row g-0">
                    <div class="col-12 sticky-top bg-white">
                        <div class="text-end pb-3 px-3 d-flex">
                            <button class="btn btn-sm d-inline-block d-lg-none pt-3" data-bs-toggle="offcanvas"
                                data-bs-target="#sidebar"><i class="fa-solid fa-bars fs-4"></i></button>
                            <div class="ms-auto">@include('navbars.user')</div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="main px-3 px-lg-0" style="min-height:88vh">
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row justify-content-center inner py-5 px-3 px-lg-0">
                                                @if (auth()->user()->profession == 'Professional')
                                                    <div class="col-lg-10">
                                                        <div class="position-relative">
                                                            <div class="h5 fw-bold mt-5">
                                                                {{ $skillAssessmentTest->test_name }}</div>
                                                            @if ($skillAssessmentTestResults == 0)
                                                                <p class="text-light pb-4 rounded-special p-3 bg-green">
                                                                    {{ $skillAssessmentTest->start_text }}
                                                                </p>
                                                                <a id="startLink6"
                                                                    class="btn view-report shadow text-light btn-sm position-absolute blackishChanger"
                                                                    href="{{ url('user/test/6') }}">
                                                                    Start assessment
                                                                </a>
                                                            @elseif($skillAssessmentTestResults > 0 && $skillAssessmentTestResults < 100)
                                                                <p class="text-light pb-4 rounded-special p-3 bg-green">
                                                                    {{ $skillAssessmentTest->resume_text }}
                                                                </p>
                                                                <a class="btn view-report shadow text-light btn-sm position-absolute blackishChanger"
                                                                    href="{{ url('user/test/6') }}">
                                                                    Resume assessment
                                                                </a>
                                                            @else
                                                                <p class="text-light pb-4 rounded-special p-3 bg-green">
                                                                    {{ $skillAssessmentTest->finish_text }}
                                                                </p>
                    

                                                                <a href="{{ route('user.getTestReport', 'skill-assessment') }}" class="btn view-report shadow bg-green text-light btn-sm position-absolute yellowishChanger">View Report</a>

                                                                <a class="btn restart-test shadow bg-secondary text-light btn-sm position-absolute blackishChanger"
                                                                    href="#"
                                                                    onclick="openRestartModal(6)">
                                                                    Restart Assessment
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-10">
                                                        <div class="position-relative">
                                                            <div class="h5 fw-bold mt-5">
                                                                {{ $careerExpectationTest->test_name }}</div>
                                                            @if ($careerExpectationsTestResults == 0)
                                                                <p class="text-light text-light pb-4 rounded-special p-3"
                                                                    style="background-color:#729CC4">
                                                                    {{ $careerExpectationTest->start_text }}
                                                                </p>
                                                                <a id="startLink5"
                                                                    class="btn view-report shadow text-light btn-sm position-absolute blackishChanger"
                                                                    href="{{ url('user/test/5') }}">
                                                                    Start assessment
                                                                </a>
                                                            @elseif($careerExpectationsTestResults > 0 && $careerExpectationsTestResults < 100)
                                                                <p class="text-light text-light pb-4 rounded-special p-3"
                                                                    style="background-color:#729CC4">
                                                                    {{ $careerExpectationTest->resume_text }}
                                                                </p>
                                                                <a class="btn view-report shadow text-light btn-sm position-absolute blackishChanger"
                                                                    href="{{ url('user/test/5') }}">
                                                                    Resume assessment
                                                                </a>
                                                            @else
                                                                <p class="text-light text-light pb-4 rounded-special p-3"
                                                                    style="background-color:#729CC4">
                                                                    {{ $careerExpectationTest->finish_text }}
                                                                </p>
                                                                <a href="{{ route('user.getTestReport', 'career-expectations') }}" class="btn view-report shadow text-light btn-sm position-absolute yellowishChanger">
                                                                    View Report
                                                                </a>
                                                                <a class="btn restart-test shadow bg-secondary text-light btn-sm position-absolute blackishChanger"
                                                                    href="#"
                                                                    onclick="openRestartModal(5)">
                                                                    Restart Assessment
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        // Update the button text on page load
        btnTextChange(5);
        btnTextChange(6);

        function openRestartModal(testNumber) {
            const restartLink = document.getElementById('restartLink');
            restartLink.href = `/user/test/${testNumber}/restart`;

            const modal = new bootstrap.Modal(document.getElementById('restartTestModal'));
            modal.show();
        }

        function btnTextChange(testNumber) {
            // Update the button text to "Resume Assessment" if the test is in progress
            var taskName = 'tasks-' + testNumber;
            let tasks = JSON.parse(localStorage.getItem(taskName));
            if (tasks !== null) {
                /* Update the button text to "Resume Assessment" */
                $(`#startLink${testNumber}`).html('');
                $(`#startLink${testNumber}`).html('Resume Assessment');
            }
        }
    </script>
@endpush

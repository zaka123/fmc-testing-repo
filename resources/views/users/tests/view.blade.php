@extends('layouts.navbar', ['pagename' => $test->test_name ?? 'Personality Test' . ' | '])
@section('css')
    <style>
        .container {
            background-color: #FFF !important;
        }

        #navbarNav,
        footer {
            display: none !important;
        }

        main {
            min-height: 82.8vh;
        }

        #da3f429 {
            overflow-x: hidden;
        }

        @media (min-width: 768px) {
            .col-md-8 {
                width: 66.66666667% !important
            }
        }

        .swiper-slide:not(.swiper-slide-active) .card {
            pointer-events: none;
            user-select: none;
        }


        .previous {
            display: none;
            width: 80px;
        }

        fieldset {
            user-select: none;
        }

        label {
            cursor: pointer;
            user-select: none;
        }

        input:checked+label {
            background-color: #749BC2 !important;
            color: white !important;
            border: none !important;
        }

        .justify-items-center {
            justify-items: center;
        }

        .changerBody:hover {
            background-color: black !important;
            color: white !important;
        }

        .title__text {
            font-family: rockwell !important;
            font-weight: 700;
            color: #749BC2;
            letter-spacing: 2px;
        }

        .title__small__text {
            font-family: rockwell !important;
            font-weight: 400;
            font-size: 25px;
            color: rgba(0, 0, 0, 0.5) !important;
            border-bottom: 5px solid #9F86C0;
            width: fit-content;
            margin: auto;

        }

        label {
            font-size: 15px;
            font-family: segoe, sans-serif;
            font-weight: 400;
            color: gray
        }

        button {
            width: 100px;
            height: 29px;
            border: none;
            background-color: #749BC2;
            color: white;
            border-radius: 3px
        }

        .bg-special {
            background-color: #749BC2 !important;
        }

        /* CSS for loader */
        .loader {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(255, 255, 255, 0.8);
            z-index: 1000;
        }

        .spinner {
            border: 4px solid rgba(255, 255, 255, 0.3);
            border-top: 4px solid #007bff;
            border-radius: 50%;
            width: 40px;
            height: 40px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('content')
    <div class="container py-4">
        <div class="row">
            <div class="col-12 ">
                <div class="row justify-content-center mb-3">
                    <div class="col-md-7">
                        <div class="ps-md-3">
                            <div class="text-end">
                                <span id="current_question">1</span>/<span id="total_questions">{{ count($questions) }}</span>
                            </div>
                            <div class="progress" style="height:26px;">
                                <div class="progress-bar bg-special rounded-2" style="width:0%" role="progressbar">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="da3f429">
                    <div class="swiper-wrapper">
                        @foreach ($questions as $question)
                            <div class="swiper-slide col-md-12 ">
                                <div class="card me-4 me-md-0 border-0 rounded-0">
                                    <div class="card-body mt-3">
                                        <fieldset id="{{ $question->question_no }}">
                                            <h2 class="text-center title__text">
                                                {{ $question->category ?? '' }}</h2>
                                            <h4 class="text-center title__small__text text-dark mb-3">
                                                {{ $question->sub_category ?? '' }}</h4>
                                            <div>
                                                <h5 class="text-center title__small__text border-0 text-dark"
                                                    style="font-family: rockwell !important;
                                                font-size: 25px!important;">
                                                    {{ $question->question }}
                                                    {{-- {!! $question->question !!} --}}
                                                </h5>
                                                <div class="d-md-flex gap-4 justify-content-center mt-4"
                                                    style="flex-wrap: wrap;">
                                                    <div class="d-grid text-center justify-items-center mb-3">
                                                        <input type="radio"
                                                            @if (!is_null($question->userResult) && $question->userResult->answer == 1) checked @endif
                                                            id="question_{{ $question->question_no }}_option_first"
                                                            name="answer{{ $question->question_no }}"
                                                            class="d-none form-check-input
                                                m-3"
                                                            value="1">
                                                        <label class="px-4 rounded-2 bg-white"
                                                            style="border:1px solid gray;height:30px;line-height:30px"
                                                            for="question_{{ $question->question_no }}_option_first">
                                                            {!! $question->option !!}
                                                        </label>
                                                    </div>
                                                    <div class="d-grid text-center justify-items-center mb-3">
                                                        <input type="radio"
                                                            @if (!is_null($question->userResult) && $question->userResult->answer == 2) checked @endif
                                                            id="question_{{ $question->question_no }}_option_second"
                                                            name="answer{{ $question->question_no }}"
                                                            class="d-none form-check-input
                                                m-3"
                                                            value="2">
                                                        <label class="px-4 rounded-2 bg-white"
                                                            style="border:1px solid gray;height:30px;line-height:30px"
                                                            for="question_{{ $question->question_no }}_option_second">
                                                            {{ $question->option_second }}
                                                        </label>
                                                    </div>
                                                    <div class="d-grid text-center justify-items-center mb-3">
                                                        <input type="radio"
                                                            @if (!is_null($question->userResult) && $question->userResult->answer == 3) checked @endif
                                                            id="question_{{ $question->question_no }}_option_third"
                                                            name="answer{{ $question->question_no }}"
                                                            class="d-none form-check-input
                                                m-3"
                                                            value="3">
                                                        <label class="px-4 rounded-2 bg-white"
                                                            style="border:1px solid gray;height:30px;line-height:30px"
                                                            for="question_{{ $question->question_no }}_option_third">
                                                            {{ $question->option_third }}
                                                        </label>
                                                    </div>
                                                    <div class="d-grid text-center justify-items-center mb-3">
                                                        <input type="radio"
                                                            @if (!is_null($question->userResult) && $question->userResult->answer == 4) checked @endif
                                                            id="question_{{ $question->question_no }}_option_fourth"
                                                            name="answer{{ $question->question_no }}"
                                                            class="d-none form-check-input
                                                m-3"
                                                            value="4">
                                                        <label class="px-4 rounded-2 bg-white"
                                                            style="border:1px solid gray;height:30px;line-height:30px"
                                                            for="question_{{ $question->question_no }}_option_fourth">
                                                            {{ $question->option_fourth }}
                                                        </label>
                                                    </div>
                                                    @if (!is_null($question->option_five))
                                                        <div class="d-grid text-center justify-items-center mb-3">
                                                            <input type="radio"
                                                                @if (!is_null($question->userResult) && $question->userResult->answer == 5) checked @endif
                                                                id="question_{{ $question->question_no }}_option_fifth"
                                                                name="answer{{ $question->question_no }}"
                                                                class="d-none form-check-input
                                                m-3"
                                                                value="5">
                                                            <label class="px-4 rounded-2 bg-white"
                                                                style="border:1px solid gray;height:30px;line-height:30px"
                                                                for="question_{{ $question->question_no }}_option_fifth">
                                                                {{ $question->option_five }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{-- <div class="swiper-slide col-md-7">
                        <div class="card mx-lg-5">
                            <div class="card-body">
                                <h4 class="text-center mt-2 text-secondary archive">Thank you for attending test.</h4>
                                <a href="{{ route('user.test.report') }}" type="button"
                                    class="float-end text-white mt-5 btn-sm btn bg-special" style="width: 80px">Next</a>
                            </div>
                        </div>
                    </div> --}}

                        {{-- My added button for storing data in db --}}
                        <div class="swiper-slide col-md-7">
                            <div class="card mx-lg-5">
                                <div class="card-body"
                                    style="display: flex;flex-direction: column;justify-content:center;align-items:center">
                                    <h4 class="text-center mt-2 text-secondary archive">Thank you for attending the test.
                                        Please submit your answers to see results.</h4>
                                    <button id="saveToDb" class="text-white mt-2 btn-sm btn bg-special changerBody"
                                        style="width: 80px">Submit</button>
                                </div>
                            </div>
                        </div>
                        {{-- End of "My added button for storing data in db" --}}
                    </div>
                </div>
                <div class="row justify-content-center mb-3">
                    <div class="col-md-8">
                        <button class="previous btn btn-sm changerBody btn-secondary mt-5">Previous</button>
                    </div>
                </div>
            </div>
            <!-- Loader element -->
            <div class="loader" style="display: none;">
                <!-- You can add a loading spinner or any other visual indicator here -->
                <div class="spinner"></div>
                {{-- <p>Please wait...</p> --}}
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        var redirectUrl = "{{ route('user.getTestReport', 'personality') }}";
        var swiper, progress, currentQuestion, step, test, question;
        test = parseInt("{{ $test->id }}");
        if(test == 6){
            redirectUrl = "{{ route('user.getTestReport', 'skill-assessment') }}";
        }
        // progress = parseFloat("{{ $total }}");
        // currentQuestion = parseInt("{{ $lastAnswered }}");
        var taskName = 'tasks-' + test;
        var totalTasks = JSON.parse(localStorage.getItem(taskName));
        if (totalTasks && Array.isArray(totalTasks) && totalTasks.length > 0) {

            // Iterate through each task in the tasks array
            totalTasks.forEach(function(task) {
                var questionNo = task.question;
                var answer = task.answer;

                // Set the radio input as checked based on the question number and the given answer
                $(`input[name="answer${questionNo}"][value="${answer}"]`).prop('checked', true);
            });
            progress = totalTasks.length;
            currentQuestion = parseInt(totalTasks[progress - 1].question);
        } else {
            progress = 0;
            currentQuestion = 0;
        }


        $(document).ready(function() {
            swiper = new Swiper("#da3f429", {
                slidesPerView: "auto",
                centeredSlides: true,
                spaceBetween: 25,
                allowTouchMove: false,
                notify: {
                    enabled: true
                },
                preventInteractionOnTransition: true
            });

            step = 100 / (swiper.slides.length - 1);

            // when the student re-visits the test
            if (currentQuestion > 0) {
                swiper.slideTo(currentQuestion);
                $(".previous").show();
                setQuestionsAttempted();
                setProgressBar();
            }

            $(".previous").click(function() {
                swiper.slidePrev();
                setProgressBar();
            });

            $("input").on("change", function() {
                question = $(this).closest("fieldset").attr("id");
                submitAnswer(question, test);
            });

            $("label").on("click", function() {
                // if ($(this).css('background-color') === 'rgb(108, 187, 222)') { // old code
                if ($(this).css('background-color') === 'rgb(116, 155, 194)') {
                    setProgressBar();
                    swiper.slideNext();
                }
            });

            swiper.on("slideChange", function() {
                if (this.activeIndex === 0) {
                    $(".previous").hide();
                } else if (this.activeIndex === this.slides.length - 1) {
                    // this is the last slide
                } else {
                    if ($('input[name="answer' + this.activeIndex + '"]:checked').get().length < 1) {
                        swiper.slidePrev();
                    }
                    $(".previous").show();
                }
                setQuestionsAttempted();
                setProgressBar();
            });


            function setProgressBar() {
                var arg;
                if ($('input[name="answer' + swiper.activeIndex + '"]:checked').get().length < 1 && swiper
                    .activeIndex != 0) {
                    arg = 1;
                } else {
                    arg = 0;
                }

                var percent = (swiper.activeIndex + arg) * step;
                $(".progress-bar")
                    .css("width", percent + "%")
                    .html(percent.toFixed() + "%");
            }

            function setQuestionsAttempted() {
                var arg;
                if (swiper.activeIndex === swiper.slides.length - 1) {
                    arg = 0;
                } else {
                    arg = 1;
                }
                $('#current_question').delay(250).html(swiper.activeIndex + arg);
            }

            function submitAnswer(question_no, test_id) {
                var answer = $('input[name="answer' + question_no + '"]:checked').val();
                if ($('input[name="answer' + question_no + '"]:checked').get().length < 1) {
                    toastr.error("Please select an option to continue.");
                    return;
                } else {

                    const storedTasks = JSON.parse(localStorage.getItem(taskName)) || [];
                    const taskIdToFind = question;
                    const foundTask = storedTasks.find((task) => task.question === taskIdToFind);
                    if (foundTask) {
                        // Update the task object with the new data
                        const newTaskData = {
                            answer: answer,
                            test_id: test_id,
                            question: taskIdToFind,
                        };
                        // Merge the newTaskData with the existing foundTask
                        Object.assign(foundTask, newTaskData);
                        // Save the updated array back to localStorage
                        localStorage.setItem(taskName, JSON.stringify(storedTasks));
                    } else {
                        localStorage.setItem(taskName, JSON.stringify([...JSON.parse(localStorage.getItem(
                            taskName) || "[]"), {
                            answer: answer,
                            question: question,
                            test_id: test_id
                        }]));
                    }

                    // let tasks = JSON.parse(localStorage.getItem(taskName));
                    // console.log(tasks);
                    // localStorage.clear();

                    progress += step;
                    setProgressBar(progress);
                    swiper.slideNext();
                }
            }



            $('#saveToDb').click(function() {
                // Show the loader
                var loader = $('.loader');
                loader.show();
                // make the saveToDb button disabled
                $('#saveToDb').prop('disabled', true);
                var tasks = JSON.parse(localStorage.getItem(taskName));
                var token = '{!! csrf_token() !!}';
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': token
                    }
                });
                $.ajax({
                    url: "{{ route('user.results.store') }}",
                    method: 'POST',
                    data: {
                        tasks: tasks
                    },
                    dataType: 'json',
                    success: function(response) {
                        toastr.success(response.msg);
                        // localStorage.clear();
                        localStorage.removeItem(taskName);
                        // Redirect to a new route after successful AJAX call
                        window.location.href = redirectUrl;
                    },
                    error: function(response) {
                        // make the saveToDb button enabled
                        $('#saveToDb').prop('disabled', false);
                        toastr.error(
                            "Something went wrong. Please refresh the page and try again");
                    }
                });
            })

        });
    </script>
@endpush

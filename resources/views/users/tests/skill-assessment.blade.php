@extends('layouts.navbar', ['pagename' => $test->test_name . ' | '])
@section('css')
<style>
    body {
        background-image: url('/assets/icons/index.svg');
        background-size: cover;
        background-position: 100% 10%;
        background-color: #6cbbde80 !important
    }

    #navbarNav,
    footer {
        display: none !important;
    }

    main {
        min-height: 82.8vh;
    }

    .floating {
        top: 40% !important;
    }

    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }

    #regiration_form {
        animation-name: fadeIn;
        animation-duration: 250ms;
        animation-iteration-count: 1;
    }

    .progress {
        --bs-progress-bar-transition: width 1000ms linear;
    }

    #current_question {
        animation-name: fadeIn;
        animation-duration: 1000ms;
        animation-iteration-count: 1;
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }
    }

    .justify-items-center {
        justify-items: center;
    }

    label {
        cursor: pointer;
    }

    input:checked + label {
        background-color: var(--special) !important;
        color: white !important;
    }
</style>
@endsection
@section('content')
<div class="page-height">
    <div class="floating"></div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="w3-light-grey">
                    <div class="w3-grey" style="height:24px;width:{{$total ?? 0}}%"></div>
                </div>
                <div class="text-end">
                    <span id="current_question">1</span>/<span id="total_questions">{{ count($questions) }}</span>
                </div>
                <div class="progress" style="height:24px;">
                    <div class="progress-bar bg-special" style="width:0%" role="progressbar">{{ round($total ?? 0,0) }}%</div>
                </div>
                <form id="regiration_form" class="mt-3" novalidate>
                    @foreach($questions as $question)
                    <fieldset id="{{$question->question_no}}">
                        <h3>{{$question->category ?? ''}}</h3>
                        <h4>{{$question->sub_category ?? ''}}</h4>
                        <div>
                            <h5 class="text-bold d-inline-block">
                                {!! $question->question !!}
                            </h5>
                            <div class="mt-3 d-flex gap-4 justify-content-center">
                                <div class="d-grid text-center justify-items-center">
                                    <input type="radio" @if(!is_null($question->userResult) &&
                                    $question->userResult->answer==1) checked @endif id="question_{{ $question->question_no }}_option{{ $question->option }}"
                                    name="answer{{$question->question_no}}" class="d-none form-check-input m-3" value="1">
                                    <label class="py-2 px-3 rounded-4 bg-white" for="question_{{ $question->question_no }}_option{{$question->option}}">{!!
                                        $question->option !!}</label>
                                </div>
                                <div class="d-grid text-center justify-items-center">
                                    <input type="radio" @if(!is_null($question->userResult) &&
                                    $question->userResult->answer==2) checked @endif
                                    id="question_{{ $question->question_no }}_option{{$question->option_second}}" name="answer{{$question->question_no}}"
                                    class="d-none form-check-input m-3" value="2">
                                    <label class="py-2 px-3 rounded-4 bg-white" for="question_{{ $question->question_no }}_option{{$question->option_second}}">{{$question->option_second}}</label>
                                </div>
                                <div class="d-grid text-center justify-items-center">
                                    <input type="radio" @if(!is_null($question->userResult) &&
                                    $question->userResult->answer==3) checked @endif
                                    id="question_{{ $question->question_no }}_option{{$question->option_third}}" name="answer{{$question->question_no}}"
                                    class="d-none form-check-input m-3" value="3">
                                    <label class="py-2 px-3 rounded-4 bg-white" for="question_{{ $question->question_no }}_option{{$question->option_third}}">{{$question->option_third}}</label>
                                </div>
                                <div class="d-grid text-center justify-items-center">
                                    <input type="radio" @if(!is_null($question->userResult) &&
                                    $question->userResult->answer==4) checked @endif
                                    id="question_{{ $question->question_no }}_option{{$question->option_fourth}}" name="answer{{$question->question_no}}"
                                    class="d-none form-check-input m-3" value="4">
                                    <label class="py-2 px-3 rounded-4 bg-white" for="question_{{ $question->question_no }}_option{{$question->option_fourth}}">{{$question->option_fourth}}</label>
                                </div>
                                @if(!is_null($question->option_five))
                                <div class="d-grid text-center justify-items-center">
                                    <input type="radio" @if(!is_null($question->userResult) &&
                                    $question->userResult->answer==5) checked @endif
                                    id="question_{{ $question->question_no }}_option{{$question->option_five}}" name="answer{{$question->question_no}}"
                                    class="d-none form-check-input m-3" value="5">
                                    <label class="py-2 px-3 rounded-4 bg-white" for="question_{{ $question->question_no }}_option{{$question->option_five}}">{{$question->option_five}}</label>
                                </div>
                                @endif
                            </div>
                        </div>
                        @php $answered = $question->userResult?->question; @endphp
                        @if(!$loop->first)
                        <input type="button" class="previous btn btn-sm btn-secondary mt-5 btn-default" value="Previous" style="width: 80px">
                        @endif
                        <input type="button" id="next" onclick="submitAnswer('{{$question->question_no}}','{{$question->test_id}}');" class="next d-none float-end mt-5 btn bg-special text-white btn-sm" value="Next" style="width: 80px">
                    </fieldset>
                    @endforeach
                    <fieldset class="success">
                        <h4 class="text-center mt-2 text-secondary archive">Thank you for attending tests.</h4>
                        <a href="{{ route('user.test.report') }}" type="button" class="float-end text-white mt-5 btn-sm btn bg-special" style="width: 80px">Next</a>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    var current_step, next_step, steps, step, currentQuestion, progress;
    currentQuestion = parseInt("{{ ($lastAnswered ?? '1') ? $lastAnswered : 1 }}"); // last attended or current question
    progress = parseFloat("{{ $total ?? 0 }}"); // amount of test completed

    $(document).ready(function() {

        $("#navbarNav, footer").remove();

        // show next question onselecting an option
        $('.justify-items-center input').click(function () {
            $(this).closest("fieldset").find(".next").delay( 5000 ).click();
        });

        steps = $('fieldset').length - 1; // total number of questions, exclude thank you
        step = 100 / steps; // weight of a question
        // when next btn is clicked
        $(".next").click(function() {
            current_step = $(this).parent();
            var currentQuestionId = parseInt(current_step.attr('id'));
            if ($('input[name="answer' + currentQuestionId + '"]:checked').get().length !== 1 && steps + 1 > currentQuestion) {
                toastr.error("Please select an answer to continue");
                return false;
            } else {
                // console.log($($(this).parent().attr('id')));
                // next_step = $(this).parent().next();
                if (currentQuestionId < steps) {
                    next_step = $(`#${currentQuestionId + 1}`);
                    current_step.hide();
                    next_step.show();

                    // update progress untill thank you message arrives
                } else {
                    $('.success').show(); // show succes message
                    current_step.hide(); // hide current question
                }
                progress += step
                setProgressBar(progress)
                if (steps > currentQuestion)
                    $('#current_question').html(++currentQuestion);
            }
        });
        // when previous btn is clicked
        $(".previous").click(function() {
            current_step = $(this).parent();
            var currentQuestionId = parseInt(current_step.attr('id'));
            next_step = $(`#${currentQuestionId - 1}`);
            next_step.show();
            current_step.hide();
            progress -= step;
            setProgressBar(progress);
            $('#current_question').delay(250).html(--currentQuestion);
        });
    });

    // Change progress bar action
    function setProgressBar(percent) {
        $(".progress-bar")
            .css("width", percent + "%")
            .html(percent.toFixed() + "%");
    }

    // show next question after last attempted question
    if (progress != 0) {
        $('fieldset').hide();
        setProgressBar(progress);
        currentQuestion++;
        $(`#${currentQuestion}`).show();
        $('#current_question').html(`${currentQuestion}`);
    }

    function submitAnswer(question_no, test_id) {
        var answer = $('input[name="answer' + question_no + '"]:checked').val();
        if ($('input[name="answer' + question_no + '"]:checked').get().length < 1) {
            return false;
        } else {
            var question = question_no;
            var test_id = test_id;
            var url = "{{url('user/results/store')}}"
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'answer': answer,
                    'question': question,
                    'test_id': test_id,
                    _token: '{!!csrf_token() !!}',
                },
                success: function(data) {
                    return true;
                    //    here the success message should display but currently we don't need it
                }, //end of success
                error: function(data) {
                    $(window)
                        .scrollTop(0);
                    //    here the failure message should display but currently we don't need it;
                }
            });
        }
    }
</script>
@endpush

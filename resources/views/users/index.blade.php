@extends('layouts.user', ['pagename' => 'Dashboard'])
@section('css')
    <style>
        .dashboard {
            color: var(--special) !important;
        }

        i[class*="person"] {
            font-size: 1.3em !important;
        }

        .latest-sessions,
        .upcoming-sessions {
            --shadow-other-b: 6px;
            --shadow-other-y: 0;
            --shadow-other-c: #5A6063
        }

        .progress-bar {
            transition-property: width;
            transition-duration: 1000ms;
            transition-timing-function: linear;
        }

        .spinner-border {
            --bs-spinner-width: 1rem;
            --bs-spinner-height: 1rem;
            --bs-spinner-border-width: 0.15rem;
        }
         .onTheBottom{
            position:fixed!important;
            bottom: 0!important;
         }

         .onTheBottom {
                width: 66.5% !important;
            }
            @media(max-width:1300px){
                .onTheBottom{
                position: relative!important;
                width:auto!important;
                }
            }
        @media (min-width: 1200px) and (min-height: 900px) {
            .onTheBottom {
                width: 66.5% !important;
            }
        }

        .textChanger:hover {
            color: black !important;
        }

        .googleChanges:hover {
            background-color: black !important;

        }

        .chromeChanges:hover {
            background-color: blue !important;
        }
        .lookBack:hover{
                        color: #6CBBDE!important;
                    }
    </style>
@endsection
@section('user-content')
    @php
        $timezones = App\Models\Timezone::all();
    @endphp
    <!-- timezone update modal -->
    <div class="modal fade" id="changeTimezoneModal" tabindex="-1" role="dialog" aria-labelledby="changeTimezoneModalTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-black" id="changeTimezoneModalTitle">Change Timezone</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('user.timezone.update', auth()->id()) }}" class="needs-validation"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body border-0 ">
                        <select class="form-select" name="timezone" id="timezone" required>
                            <option value="">Select timezone</option>
                            @foreach ($timezones as $timezone)
                                <option value="{{ $timezone->timezone }}" @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                    {{ $timezone->timezone }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary googleChanges"
                            data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn chromeChanges btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-1">
            <!-- side bar -->
            @include('users.side-bar')

            <!-- main central contents column -->
            <div class="col-lg-8" style="background-color: rgba(112,112,112,7%);">
                <h3 class="bg-white text-special p-3 sticky-top d-flex">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                        data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <span class="d-none d-lg-inline"><strong>Welcome {{ $user->name }}</strong></span>
                    <span class="mx-auto text-center d-inline d-lg-none"><strong>Welcome
                            <br>{{ $user->name }}</strong></span>
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                        data-bs-target="#infobar">
                        <i class="fa-solid fa-ellipsis-vertical fs-4" style="rotate:90deg"></i>
                    </button>
                </h3>
                <div class="pt-2 pb-4 px-4 px-lg-2" style="min-height: 80.6vh;">
                    <div class="row g-2">
                        <div class="col-12">
                            <h4><strong>Information</strong></h4>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="text-light pt-4 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">Family Information</div>
                                <div class="text-end">
                                    <a href="#" class="text-decoration-none text-white" data-test-name="Personal"
                                        data-bs-toggle="modal" data-bs-target="#familyInformationStaticBackdrop">
                                        <small class="textChanger">View all ></small>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="text-light pt-4 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">Study habits and Routine</div>
                                <div class="text-end">
                                    <a href="#" class="text-decoration-none text-white"
                                        data-test-name="study_habits_and_routine" data-bs-toggle="modal"
                                        data-bs-target="#studyHabitsAndRoutineStaticBackdrop">
                                        <small class="textChanger">View all ></small>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="text-light pt-4 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">Future Self Reflection</div>
                                <div class="text-end">
                                    <a href="#" class="text-decoration-none text-white"
                                        data-test-name="future_self_reflection" data-bs-toggle="modal"
                                        data-bs-target="#futureSelfReflectionStaticBackdrop">
                                        <small class="textChanger">View all ></small>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="text-light pt-4 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">Career Ambitions</div>
                                <div class="text-end">
                                    <a href="#" class="text-decoration-none text-white" data-test-name="career"
                                        data-bs-toggle="modal" data-bs-target="#careerAmbitionsStaticBackdrop">
                                        <small class="textChanger">View all ></small>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col-12">
                            <h4 class="pt-4"><strong>Assessments</strong></h4>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="text-light pt-4 pb-2 shadow-other px-2 rounded bg-special"
                                style="--shadow-other-b:6px">
                                <div class="fw-bold text-truncate">{{ $personalityTest->test_name }}</div>
                                <div class="text-center">
                                    {{-- @if (!$user->hasPaidFor(4)) --}}
                                    {{-- unlock
                                <a href="{{ route('user.payments.info.index', 4) }}"
                                    class="text-decoration-none text-white">
                                    <small style="font-weight:500">Unlock test ></small>
                                </a> --}}
                                    @if ($personalityTestResults == 0)
                                        {{-- start test --}}
                                        <a href="{{ route('user.test.instructions', 4) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500" class="textChanger">Start assessment ></small>
                                        </a>
                                    @elseif($personalityTestResults > 0 && $personalityTestResults < 100)
                                        {{-- resume --}}
                                        <a href="{{ route('user.test.instructions', 4) }}"
                                            class="text-decoration-none text-white">
                                            <small style="font-weight:500">Resume assessment ></small>
                                        </a>
                                    @else
                                        {{-- view score --}}
                                        <a href="#" class="text-decoration-none text-white" data-bs-toggle="modal"
                                            data-bs-target="#personalityTestScoreStaticBackdrop">
                                            <small style="font-weight:500" class="textChanger">view report ></small>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- latest reviews --}}
                    @if ($reports)
                        @include('users.sessions.reports.latest')
                    @endif

                    <!--/ internal main row -->
                    <div class="row g-2 my-2">
                        <!-- latest sessions -->
                        @include('users.sessions.latest')

                        <!-- upcoming sessions -->
                        @include('users.sessions.upcoming')

                    </div> <!-- internal sub row -->
                </div>
                <!--/ internal padding -->
                {{-- @include('footer.user') --}}
                <section class="text-bg-dark px-4 py-3 onTheBottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                                    class="d-inline d-md-none"><br></span>All rights reserved</div>
                            <div class="col-md-6 text-center text-md-end">Powered by <a href="https://www.theeducatics.com/"
                                    class="text-decoration-none lookBack text-white">The Educatics</a> | Made with <i
                                    class="fa-solid fa-heart text-danger"></i> by <a href="https://lastwavetechnology.com"
                                    class="text-decoration-none lookBack text-white" target="_blank">LWT</a>
                            </div>
                        </div>
                    </div>
                </section>    
            </div>
            <!--/ main central contents column -->
            <div class="col-2 offcanvas-lg offcanvas-end" id="infobar"
                style="box-shadow: -1px 0px 0px 1px #00000026;--bs-offcanvas-width:auto">
                <div class="px-3 text-center vh-100 sticky-top overflow-auto">
                    @include('navbars.user')
                    <div class="mt-4 mb-2 position-relative">
                        <img src="{{ asset($user->profile_photo) }}" alt="profile pic" class="rounded shadow-sm"
                            width="120" height="120">
                        <span class="position-absolute bottom-0 me-2 text-secondary" style="right:28px;cursor:pointer"
                            data-bs-toggle="modal" data-bs-target="#profilePictureModal" alt="camera">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-camera-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                <path
                                    d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2zm.5 2a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0z" />
                            </svg>
                        </span>
                    </div>
                        <div class="d-flex justify-content-center align-items-center" style="gap: 10px">
                            <span>
                                <b>{{ $user->full_name ?? 'N/A' }}</b>
                            </span>
                            <a href="{{ route('user.profile.edit', $userProfile->id) }}" class="text-info fs-6" title="Edit Profile">
                                <small>
                                    <i class="fa-regular fa-pen-to-square"></i>
                                </small>
                            </a>
                        </div>
                    <div class="ms-2 d-flex text-start justify-content-between">
                        <div>
                            <small>{{ $userProfile->age > 0 ? $userProfile->age : '< 1' }} Years</small><br>
                            <small>{{ $user->gender }} {!! $user->gender == 'Male'
                                ? '<i class="fa-solid fa-person"></i>'
                                : ($user->gender == 'Female'
                                    ? '<i class="fa-solid fa-person-dress"></i>'
                                    : '') !!}</small>
                        </div>
                        <div class="fw-bold">|<br>|</div>
                        <div>
                            <small
                                class="text-capitalize">{{ Str::limit($userProfile->city ?? 'N/A', 11, '...') }}</small><br>
                            <small>{{ $user->profession }}</small>
                        </div>
                    </div>
                    <hr>
                    <div class="fs-6 text-start">
                        <div class="mb-2">
                            <small>
                                <b>Most Favorite Subject:</b>
                                <br>
                                <span class="text-muted">{{ $userProfile->favorite_subject ?? '' }}</span>
                            </small>
                        </div>
                        <div class="mb-2">
                            <small>
                                <b>Least Favorite Subject:</b>
                                <br>
                                <span class="text-muted">{{ $userProfile->difficult_subject ?? '' }}</span>
                            </small>
                        </div>
                        <div class="mb-2">
                            <small>
                                <b>Easiest Subject:</b>
                                <br>
                                <span class="text-muted">{{ $userProfile->easiest_subject ?? '' }}</span>
                            </small>
                        </div>
                        <div class="mb-2">
                            <small>
                                <b>Average Grade Point:</b>
                                <br>
                                <span class="text-muted">{{ $userProfile->average_grade_point ?? '' }}</span>
                            </small>
                        </div>
                    </div>
                    <hr>
                    <div class="text-start">
                        <span>
                            <b>Timezone: </b><a href="#" title="Change Timezone" class="text-info fs-6"
                                data-bs-toggle="modal" data-bs-target="#changeTimezoneModal">
                                <small>
                                    <i class="fa-regular fa-pen-to-square"></i>
                                </small>
                            </a><br>
                            {{ $user->timezone ?? 'N/A' }}
                        </span>
                    </div>

                    {{-- @include("users.zoom.connect") --}}
                </div>
            </div>
        </div>
    </div>
    <!-- all modals are here -->

    <!-- family info -->
    <div class="modal fade" id="familyInformationStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="familyInformationStaticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title ms-auto fs-5" id="familyInformationStaticBackdropLabel">Family Information</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 px-lg-5">
                    <div class="d-flex align-items-center">
                        <strong>Loading...</strong>
                        <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                    </div>
                </div>
                <div class="modal-footer sticky-bottom"></div>
            </div>
        </div>
    </div>

    <!-- study habits and routine -->
    <div class="modal fade" id="studyHabitsAndRoutineStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="studyHabitsAndRoutineStaticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title ms-auto fs-5" id="studyHabitsAndRoutineStaticBackdropLabel">Study Habits And
                        Routine</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-3 px-lg-5">
                    <div class="d-flex align-items-center">
                        <strong>Loading...</strong>
                        <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                    </div>
                </div>
                <div class="modal-footer sticky-bottom"></div>
            </div>
        </div>
    </div>
    <!-- future self reflection -->
    <div class="modal fade" id="futureSelfReflectionStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" tabindex="-1" aria-labelledby="futureSelfReflectionStaticBackdropLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="--bs-modal-width: 620px;">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title ms-auto fs-5" id="futureSelfReflectionStaticBackdropLabel">Future Self
                        Reflection
                    </h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="font-size:.9rem;">
                    <div class="d-flex align-items-center">
                        <strong>Loading...</strong>
                        <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                    </div>
                </div>
                <div class="modal-footer sticky-bottom"></div>
            </div>
        </div>
    </div>
    <!-- career ambitions -->
    <div class="modal fade" id="careerAmbitionsStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" tabindex="-1" aria-labelledby="careerAmbitionsStaticBackdropLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title ms-auto fs-5" id="careerAmbitionsStaticBackdropLabel">Career Ambitions</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="font-size:.9rem;">
                    <div class="d-flex align-items-center">
                        <strong>Loading...</strong>
                        <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                    </div>
                </div>
                <div class="modal-footer sticky-bottom"></div>
            </div>
        </div>
    </div>
    {{-- @if (!$user->hasPaidFor(4)) --}}
    <div class="modal fade" id="unlockPersonalityTestStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
        data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
        @include('users.results.unlock', ['href' => route('user.payments.info.index', 4)])
    </div>
    @if ($personalityTestResults >= 99)
        <div class="modal fade" id="personalityTestScoreStaticBackdrop" data-bs-delay='{"show": 600, "hide": 10}'
            data-bs-backdrop="static" tabindex="-1" aria-labelledby="personalityTestScoreStaticBackdropLabel"
            aria-hidden="true">
            <!-- results dialog -->
            @include('users.results.personality')
        </div>
    @endif

    <!-- profile picture modal -->
    @include('extra.profile-picture-modal')

    @push('script')
        <script>
            $("#familyInformationStaticBackdrop").on('show.bs.modal', (e) => {
                personalInformation(e)
            });
            $("#studyHabitsAndRoutineStaticBackdrop").on('show.bs.modal', (e) => {
                personalInformation(e)
            })
            $("#futureSelfReflectionStaticBackdrop").on('show.bs.modal', (e) => {
                personalInformation(e)
            })
            $("#careerAmbitionsStaticBackdrop").on('show.bs.modal', (e) => {
                personalInformation(e)
            })

            const notAuthorized =
                'Something went wrong. Please <a class="text-decoration-none" href="{{ route('login') }}">login</a> to continue';

            function personalInformation(e) {

                const waiting =
                    '<span class="spinner-border text-white" role="status"><span class="visually-hidden">Loading...</span></span>'
                const test = e.relatedTarget.dataset.testName;
                const modal = `${e.relatedTarget.dataset.bsTarget} .modal-body`;
                const user = "{{ auth()->user()->id }}";

                const btnText = e.relatedTarget.innerHTML;
                e.relatedTarget.innerHTML = waiting;

                var url = "{{ url('user/personal-information/') }}";
                url = `${url}/${user}/${test}`;
                $.ajax({
                    url: url,
                    type: 'get',
                    success: function(data) {
                        $(modal).empty();
                        $(modal).append(data);
                        e.relatedTarget.innerHTML = btnText;
                        return;
                    },
                    error: function(error) {
                        if (error.status == 401) {
                            $(modal).empty();
                            $(modal).append(notAuthorized);
                            return;
                        }
                        console.log(error);
                    }
                });
            }
        </script>
    @endpush

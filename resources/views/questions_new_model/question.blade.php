@extends('layouts.new_app')
@section('css')
    <style>
        /* hamburger menu displayed none */
        .sidebarAncestor,
        footer {
            display:
                none !important;
        }

        .question__main {
            height: calc(100vh - 115px);
            width: calc(100vw - 80px);
        }

        @media(max-width:768px) {
            .question__main {
                width: 90vw;
            }
        }

        .bg-special {
            background-color: #749BC2;
        }

        @media(min-width:992px) {
            .w-lg-75 {
                width: 75% !important;
            }
        }

        @media(max-width:768px) {
            .w-max-100 {
                width: 100% !important;
            }
        }

        .title__text {
            font-family: rockwell;
            font-weight: 700;
            color: #749BC2;
        }

        .title__small__text {
            font-family: rockwell;
            font-weight: 400;
            font-size: 25px;
            color: rgba(0, 0, 0, 0.5) !important;
            border-bottom: 5px solid #9F86C0;
            width: fit-content;
            margin: auto;
        }

        label {
            font-size: 15px;
            font-family: segoe, sans-serif;
            font-weight: 400;
            color: gray
        }

        button {
            width: 100px;
            height: 29px;
            border: none;
            background-color: #749BC2;
            color: white;
            border-radius: 3px
        }
    </style>
@endsection
@section('content')
    <div class="question__main">
        <div class="w-100 w-lg-75 h-100 m-auto">
            <div class="h-100 w-max-100 m-auto  pt-2 pt-md-5" style="width:80%">
                {{-- progress bar  --}}
                <div class="px-md-3">
                    <div class="text-end">
                        <span id="current_question">1</span>/<span id="total_questions">50</span>
                    </div>
                    <div class="progress" style="height:26px;">
                        <div class="progress-bar bg-special rounded-2" style="width:25%" role="progressbar">0%</div>
                    </div>
                </div>
                {{-- First Heading --}}
                <div class="mt-3">
                    <h2 class="text-center title__text">Teamwork Skills</h2>
                </div>
                {{-- this div will be showed at the end end of the test --}}
                <div class="d-none mt-5">
                    <h2 class="text-center title__text text-dark" style="font-size:22px;font-weight:400">Thank you for
                        attending the test. Please submit your answers to see
                        results.</h2>
                </div>
                {{-- second Heading --}}
                <div class="">
                    <p class="text-center title__small__text text-dark">In
                        Meetings</p>
                </div>
                {{-- main questions --}}
                <div class="mt-5">
                    <p class="text-center title__small__text border-0 text-dark">Demolish others's ideas at first hearing
                    </p>

                    {{-- for answers --}}
                    <div class="d-md-flex gap-4 justify-content-center align-items-center mt-5" style="flex-wrap: wrap;">
                        <div class="d-grid text-center justify-items-center mb-3">
                            <input type="radio" class="d-none form-check-input m-3" value="1">
                            <label class="px-4 py-md-0 py-1 rounded-2 bg-white" style="border:1px solid gray">
                                Highly Unlikely
                            </label>
                        </div>
                        <div class="d-grid text-center justify-items-center mb-3">
                            <input type="radio" class="d-none form-check-inputm-3" value="2">
                            <label class="px-4 py-md-0 py-1 rounded-2 bg-white" style="border:1px solid gray">
                                Unlikely
                            </label>
                        </div>
                        <div class="d-grid text-center justify-items-center mb-3">
                            <input type="radio" class="d-none form-check-input  m-3" value="3">
                            <label class="px-4 py-md-0 py-1 rounded-2 bg-white" style="border:1px solid gray">
                                Neutral
                            </label>
                        </div>
                        <div class="d-grid text-center justify-items-center mb-3">
                            <input type="radio" class="d-none form-check-input m-3" value="4">
                            <label class="px-4  py-md-0 py-1 rounded-2 bg-white" style="border:1px solid gray">
                                Likely
                            </label>
                        </div>
                        <div class="d-grid text-center justify-items-center mb-3">
                            <input type="radio" class="d-none form-check-input m-3" value="4">
                            <label class="px-4 py-md-0 py-1 rounded-2 bg-white" style="border:1px solid gray">
                                Highly Likely
                            </label>
                        </div>
                    </div>
                    {{-- for answers --}}
                </div>
                {{-- buttons --}}
                <div class="mt-5">
                    <button>Previous</button>
                </div>
                {{-- this button will be showed at the end end of the test --}}
                <div class="d-none mt-5">
                    <button class="m-auto d-block">submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin',['pagename'=>'Edit Instruction'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route("admin.instructions.update", $instruction->id) }}" method="post">
                @csrf
                @method("put")
                <div class="form-group">
                    <label for="test_id" class="form-label">Test</label>
                    <select id="test_id" class="form-control select2 @error('test_id') is-invalid @enderror" name="test_id" style="width: 100%;">
                        @foreach( $tests as $key => $test )
                        <option value="{{ $test->id }}" @selected($instruction->test_id == $test->id)>{{ $test->test_name ?? '' }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="instruction" class="form-label">Instruction</label>
                    <textarea id="instruction" class="form-control summernote @error('instruction') is-invalid @enderror" name="instruction">
                        {{ old('instruction') ?? $instruction->instruction }}
                    </textarea>
                </div>
                <div class="form-group">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary">Cancel</a>
                    <input type="submit" value="Update" class="btn btn-sm btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

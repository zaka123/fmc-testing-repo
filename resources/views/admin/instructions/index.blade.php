@extends('layouts.admin',['pagename'=>'Instructions'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12 justify-content-end">
            <button type="button" class="bg-primary btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-solid fa-plus"></i>
            </button>
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Test</td>
                        <td>Instruction</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($instructions as $key => $group)
                        @foreach($group as $instruction)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{ $instruction->test->test_name ?? '' }}</td>
                            <td>{!! $instruction->instruction !!}</td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('admin.instructions.show', $instruction->id) }}" class="btn btn-outline-success" type="button" title="confirm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route("admin.instructions.edit", $instruction->id) }}" class="btn btn-outline-secondary" title="edit">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    <button class="btn btn-outline-danger" form="delete{{ $instruction->id }}" type="submit" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <form action="{{ route('admin.instructions.destroy',$instruction->id) }}" method="post" hidden id="delete{{ $instruction->id }}">
                                    @csrf @method('DELETE')
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('modal')
<form action="{{ route('admin.instructions.store') }}" method="post" style="overflow-y: scroll;">
    @csrf
    <div class="modal-body text-sm">
        <div class="form-group">
            <div class="row g-3">
                <div class="col-md-6 offset-md-3">
                    <label for="test_id" class="form-label">Test</label>
                    <select id="test_id" class="form-control select2 @error('test_id') is-invalid @enderror" name="test_id" style="width: 100%;">
                        @foreach($tests as $key=>$test)
                        <option value="{{$test->id}}">{{$test->test_name ?? ''}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 offset-md-3">
                    <label for="instruction">Instruction</label>
                    <textarea id="instruction" name="instruction" class="form-control @error('instruction') is-invalid @enderror">{{ old('instruction') }}</textarea>
                </div>
                <div class="col-12 text-center">
                    <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="bg-primary btn btn-primary">Add</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@include('extra.modal',['modal_title'=>'Add Instruction'])
@endsection

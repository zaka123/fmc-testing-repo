@extends('layouts.admin',['pagename'=>'Instruction Details'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-light" role="alert">
                <h4 class="alert-heading">{{ $instruction->test->test_name }}</h4>
                <p>{{ $instruction->instruction }}</p>
                <hr>
                <p class="mb-0">Last updated: {{ $instruction->updated_at }}</p>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.admin', ['pagename' => 'Coaches'])

@section('css')
    <style>
        input+i {
            top: 41px;
            left: 18px;
        }
    </style>
@endsection

@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Coaches') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow p-4 rounded mb-3">
        <form action="{{ route('admin.counsellors.store') }}" method="post" class="row g-3 justify-content-center">
            @csrf
            <input type="hidden" name="role" value="Counsellor">
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="first_name">First Name<span class="text-danger">*</span></label>
                    <input id="first_name" name="first_name" value="{{ old('first_name') }}"
                        class="form-control ps-5 @error('first_name') is-invalid @enderror" required>
                    <i class="fa fa-user fs-5 position-absolute"></i>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="last_name" class="form-label">Last Name<span class="text-danger">*</span></label>
                    <input id="last_name" name="last_name" value="{{ old('last_name') }}"
                        class="form-control ps-5 @error('last_name') is-invalid @enderror" required>
                    <i class="fa fa-user fs-5 position-absolute"></i>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="form-group">
                    <label for="dial_code" class="form-label">Phone</label>
                    <input name="dial_code" list="dial_code_list" id="dial_code" placeholder="+92"
                        class="form-control @error('dial_code') is-invalid @enderror" style="width: 5rem"
                        value="{{ old('dial_code') }}" autocomplete="country-code">
                    <datalist id="dial_code_list"></datalist>
                    </select>
                </div>
                <div class="position-relative ps-2 w-100">
                    <label for="phone_number" class="form-label fw-500 text-white user-select-none">Phone</label>
                    <input id="phone_number" name="phone_number" value="{{ old('phone_number') }}" type="tel"
                        class="form-control ps-5 @error('phone_number') is-invalid @enderror">
                    <i class="fa fa-phone fs-5 position-absolute"></i>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                    <input id="email" name="email" value="{{ old('email') }}" type="email"
                        class="form-control ps-5 @error('email') is-invalid @enderror" required>
                    <i class="fa fa-envelope fs-5 position-absolute"></i>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="linked_in" class="form-label">Linkedin</label>
                    <input id="linked_in" name="linked_in" value="{{ old('linked_in') }}" type="text"
                        class="form-control ps-5 @error('linked_in') is-invalid @enderror">
                    <i class="fa fa-linkedin fs-5 position-absolute"></i>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="interested_in" class="form-label">What are you interested in?<span
                            class="text-danger">*</span></label>
                    <select name="interested_in" id="interested_in"
                        class="form-select @error('interested_in') is-invalid @enderror" required>
                        <option value="">Please select your answer</option>
                        <option @if (old('interested_in') == 'Coaching') selected @endif>Coaching</option>
                        <option @if (old('interested_in') == 'Mentoring') selected @endif>Mentoring</option>
                        <option value="Both Coaching and Mentoring" @if (old('interested_in') == 'Both Coaching and Mentoring') selected @endif>Both
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="country" class="form-label">Country</label>
                    <select id="country" name="country" class="form-select @error('country') is-invalid @enderror">
                        <option value="">Please select your country</option>

                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="gender" class="form-label">Gender</label>
                    <select id="gender" name="gender" class="form-select @error('gender') is-invalid @enderror">
                        <option value="">Please select your gender</option>
                        <option @if (old('gender') == 'Male') selected @endif>Male</option>
                        <option @if (old('gender') == 'Female') selected @endif>Female</option>
                        <option @if (old('gender') == 'Other') selected @endif>Other</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password" class="form-label">Password<span class="text-danger">*</span></label>
                    <input id="password" name="password" type="password"
                        class="form-control @error('password') is-invalid @enderror" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password_confirmation" class="form-label">Confirm Password<span
                            class="text-danger">*</span></label>
                    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control"
                        required>
                </div>
            </div>
            <div class="col-12">
                <button class="btn btn-secondary">Create</button>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            // fetch country names
            $.ajax({
                url: "{{ route('api.country_info', 'name') }}",
                type: "GET",
                success: countries => {
                    countries.forEach(country => {
                        $("#country").append(
                            `<option ${country === "{{ old('country') }}" ? 'selected' : ''}>${country}</option>`
                            );
                    });
                },
                error: function(error) {
                    console.log(error);
                }
            });

            //
            $.ajax({
                url: "{{ route('api.country_info', 'dial_code') }}",
                type: "GET",
                success: dial_codes => {
                    Object.values(dial_codes).forEach(dial_code => {
                        $("#dial_code_list").append("<option>" + dial_code + "</option>"
                        );
                    });
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    </script>
@endpush

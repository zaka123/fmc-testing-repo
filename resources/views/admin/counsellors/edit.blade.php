@extends('layouts.admin', ['pagename' => 'User Profile'])
@section('content')
    <style>
        .saveChanger:hover {
            background-color: #06acf4 !important;
        }

        .cancelChanger:hover {
            background-color: black !important;
        }

        .deleteChanger:hover {
            background-color: red !important;
        }
    </style>
    <div class="container-fluid px-0">
        <div class="row g-0">
            <div class="col-12">
                <div class="p-3 main h-100">
                    <div class="floating"></div>
                    <form action="{{ route('admin.counsellors.update', $counsellor) }}" class="needs-validation" novalidate
                        method="post">
                        @csrf
                        @method('PUT')
                        <div class="inner card">
                            <div class="card-body">
                                <div class="row g-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" name="name"
                                                value="{{ old('name') ?? $counsellor->first_name }}"
                                                class="form-control @error('name') is-invalid @enderror" required>
                                            <div class="invalid-feedback">Please fill in this field.</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" name="last_name"
                                                value="{{ old('last_name') ?? $counsellor->last_name }}"
                                                class="form-control @error('last_name') is-invalid @enderror"
                                                required>
                                            <div class="invalid-feedback">Please fill in this field.</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email"
                                                value="{{ old('email') ?? $counsellor?->email }}"
                                                class="form-control @error('email') is-invalid @enderror" required>
                                            <div class="invalid-feedback">Please fill in this field.</div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select name="country"
                                                class="form-select @error('country') is-invalid @enderror" required>
                                                <option value="">Please select your country</option>
                                                @foreach ($countries as $country)
                                                    <option @selected($country == (old('country') ?? $counsellor->profile->country))>{{ $country }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Please fill in this field.</div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 d-flex justify-content-start">
                                        <a class="btn btn-secondary text-white me-2 cancelChanger"
                                            href="{{ url()->previous() }}">Cancel Changes</a>
                                        <button class="btn btn-primary">Save Changes</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin', ['pagename' => 'Coach Profile'])
@section('content')
    <div class="container-fluid my-2">
        <div class="row gy-3">
            <div class="col-md-3">
                <div class="card h-100 shadow py-2">
                    <div class="text-center">
                        <img class="rounded-circle border" src="{{ asset($counsellor->profilePhoto) }}"
                            alt="User profile picture" width="150" height="150">
                    </div>
                    <div class="card-body">
                        <h3 class="text-center text-uppercase">{{ $counsellor->fullname }}</h3>
                        <p class="text-muted text-center">
                            <b>{{ $counsellor->email }}</b>
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Gender:</b>
                                <span class="float-right">{{ $counsellor->gender }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Country:</b>
                                <span class="float-right">{{ $counsellor->counsellorProfile->country }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Phone number:</b>
                                <span class="float-right">{{ $counsellor->counsellorProfile->i_phone_number }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Linkedin:</b>
                                <span class="float-right">
                                    <a href="{{ $counsellor->counsellorProfile->linked_in }}" target="_blank"
                                        class="text-decoration-none">
                                        View Profile
                                    </a>
                                </span>
                            </li>
                            <li class="list-group-item">
                                <b>Status:</b>
                                <span class="float-right">{!! $counsellor->verified !!}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Specialized in:</b>
                                <span class="float-right">{!! $counsellor->counsellorProfile->interested_in !!}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card shadow h-100">
                    <h4 class="card-header text-uppercase text-white" style="background-color:var(--bs-blue);">Connected
                        Students</h4>
                    <div class="card-body">
                        <div class="row">
                            @forelse($connectedUsers as $session)
                                @if ($session->user->id != null)
                                    <div class="col-12 col-sm-6 col-md-4 mb-3">
                                        <div class="shadow card h-100">
                                            <div class="bg-blue card-header border-0">
                                                <span
                                                    class="mx-auto text-capitalize text-white">{{ $session->user->profession }}</span>
                                            </div>
                                            <div class="card-body text-center text-capitalize">
                                                <div class="mb-2">
                                                    <img src="{{ asset($session->user->profilePhoto) }}" height="100"
                                                        width="100" class="rounded-circle border" alt="...">
                                                </div>
                                                <a href="{{ $session->user->profession == 'Professional' ? route('admin.users.professional.show', [$session->user->id, $session->user->userProfessional->id]) : route('admin.users.show', $session->user->id) }}"
                                                    class="text-decoration-none text-truncate fw-bold">
                                                    {{ $session->user->fullname }}
                                                </a>
                                                <div class="text-start">
                                                    <b>Gender: </b>{{ $session->user->gender }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @empty
                                <div class="col-12">
                                    No students connected
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card shadow h-100">
                    <h4 class="card-header text-uppercase text-white" style="background-color:var(--bs-green);">Latest
                        Sessions</h4>
                    <div class="card-body">
                        <table class="table table-striped" id="example1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Student</th>
                                    <th>Date </th>
                                    <th>Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sessions as $session)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>
                                            {{-- <a href="{{ $session->user->profession == "Professional" ? route("admin.users.professional.show", ['user' => $session->user->id, 'professional' => $session->user->userProfessional->id]) : route("admin.users.show", $session->user->id) }}">{{ $session->user->fullname }}</a> --}}
                                        </td>
                                        <td>{{ date('d-m-Y', strtotime($session->session->session_timing)) }}</td>
                                        <td>{{ date('h:i A', strtotime($session->session->session_timing)) }}</td>
                                        <td>{!! $session->session_status !!}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">No data available in the table.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

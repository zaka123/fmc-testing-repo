@extends('layouts.admin',['pagename'=>'Coaches'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Coaches') }}
    </h2>
</x-slot>
<div class="container bg-white shadow px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12 justify-content-end">
            <a href="{{ route("admin.counsellors.create") }}" class="btn btn-primary" data-bs-toggle="tooltip" data-bs-title="Create Coach Profile">
                <i class="fa fa-solid fa-plus"></i>
            </a>
            <a href="{{ route("admin.counsellors.trashed") }}" class="btn btn-danger">
                {{-- <i class="fa fa-solid fa-plus"></i> --}}
                Deleted Records
            </a>
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($counsellors as $key => $counsellor)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $counsellor->fullname }}</td>
                        <td>{{ $counsellor->email }}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Actions">
                                <a href="{{ route("admin.counsellors.show", $counsellor->id) }}" title="view profile" data-bs-toggle="tooltip" class="btn btn-outline-success">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a type="button" class="btn btn-outline-secondary" title="edit profile" data-bs-toggle="tooltip" href="{{ route("admin.counsellors.edit", $counsellor->id) }}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {{-- <a type="button" class="btn btn-outline-primary" href="{{ route('admin.user.password',$counsellor->id) }}" title="change password" data-bs-toggle="tooltip">
                                    <i class="fa fa-key"></i>
                                </a> --}}
                                <button type="submit" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteUser{{ $counsellor->id }}Modal">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="modal fade" id="deleteUser{{ $counsellor->id }}Modal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="deleteUser{{ $counsellor->id }}ModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="deleteUser{{ $counsellor->id }}ModalLabel">Attention!</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <form action="{{ route('admin.users.destroy', $counsellor->id) }}" method="post">
                                            @csrf @method('DELETE')
                                            <div class="modal-body">
                                                Are you sure you want to delete this coach?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">No</button>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Add new counsellor modal -->
@section('modal')
<form action="" class="needs-validation" id="add_counsellor" class="needs-validation" novalidate method="post">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="first_name" id="first_name" required placeholder="first name">
                    <label for="first_name">First name</label>
                    <div class="invalid-feedback">First name can't be empty.</div>
                    <div class="valid-feedback">Looks good.</div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="first_name" id="last_name" required placeholder="last name">
                    <label for="last_name">Last name</label>
                    <div class="invalid-feedback">Last name can't be empty.</div>
                    <div class="valid-feedback">Looks good!</div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="email" id="email" required placeholder="email address">
                    <label for="email">Email address</label>
                    <div class="invalid-feedback">Email address can't be empty.</div>
                    <div class="valid-feedback">Looks good!</div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="date" class="form-control" name="date_of_birth" id="date_of_birth" required placeholder="date of birth">
                    <label for="date_of_birth">Date of Birth</label>
                    <div class="invalid-feedback">Date of Birth can't be empty.</div>
                    <div class="valid-feedback">Looks good!</div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" name="password" id="password" required placeholder="Password">
                    <label for="password">Password</label>
                    <div class="invalid-feedback">Password can't be empty.</div>
                    <div class="valid-feedback">Looks good!</div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" required placeholder="Confirm Password">
                    <label for="confirm_password">Confirm password</label>
                    <div class="invalid-feedback">Please confirm password.</div>
                    <div class="valid-feedback">Looks good!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="add_counsellor">Add Counsellor</button>
    </div>
</form>
@endsection
@include('extra.modal',['modal_title'=>'Add New Counsellor'])
@endsection

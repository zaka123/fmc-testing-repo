@extends('layouts.admin', ['pagename' => 'Videos'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Videos') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow px-4 py-4 rounded">
        <div class="row">
            <div class="col-md-12 justify-content-end">
                <a href="{{ route('admin.videos.create') }}" class="btn btn-outline-primary" data-bs-toggle="tooltip"
                    data-bs-title="Add Video">
                    <i class="fa fa-solid fa-plus"></i>
                </a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="example1">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Duration</th>
                            <th>Link</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {

        })
    </script>
@endpush

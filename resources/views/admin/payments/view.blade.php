@extends('layouts.admin', ['pagename' => 'Payments'])
@section('content')
    <div class="container shadow bg-white px-4 py-4 rounded">
        <div class="row g-3">
            <div class="col-12">
                <div class="row align-items-center border py-2 rounded mb-3">
                    <div class="col-md-2">
                        <img src="{{ asset($payment->user->profile_photo) }}" width="150" height="150"
                            class="border rounded-circle" alt="profile picture">
                    </div>
                    <div class="col-md-4 ps-md-4">
                        <p><b>Name : </b>{{ $payment->user?->full_name }}</p>
                        <p><b>Email : </b>{{ $payment->user?->email }}</p>
                        {{-- <p><b>Gender : </b>{{ $payment->user?->gender }}</p> --}}
                        <p><b>Status : </b>{!! $payment->user?->verified !!}</p>
                    </div>
                    <div class="col-md-4">
                        <p><b>Profession : </b>{{ $payment->user?->profession }}</p>
                        <p><b>Member since : </b>{{ date('d-m-Y', strtotime($payment->user?->created_at)) }}</p>
                    </div>
                    <div class="col-md-2">
                        @if ($payment->status == 'Pending')
                            <button type="submit" form="approvePayment" class="btn btn-outline-success">
                                Approve
                            </button>
                        @else
                            <button class="btn px-4 btn-secondary" data-bs-toggle="tooltip"
                                title="confirmed payments can't be deleted">
                                <i class="fa fa-info"></i>
                            </button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row g-3 py-2 border rounded">
                    <div class="col-md-4">
                        <div class="card h-100">
                            <div class="card-header text-center bg-success">Payment Details</div>
                            <div class="card-body">
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Payment Method:
                                    </div>
                                    <div class="text-end">
                                        {{ $payment->bank }}
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Branch Name:
                                    </div>
                                    <div class="text-end">
                                        {{ $payment->branch ?? 'N/A' }}
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Transaction ID:
                                    </div>
                                    <div class="text-end">
                                        {{ $payment->transaction_id }}
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Dated:
                                    </div>
                                    <div class="text-end">
                                        {{ $payment->created_at }}
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Payment purpose:
                                    </div>
                                    <div class="text-end">
                                        {!! $payment->payment_purpose !!}
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="fw-bold">
                                        <i class="fa fa-diamond"></i>
                                        Amount:
                                    </div>
                                    <div class="text-end">
                                        {{ strtoupper($payment->currency) }} {{ $payment->amount }}
                                    </div>
                                </div>
                                @if (!empty($payment->coupon))
                                    <div class="mb-2">
                                        <div class="fw-bold">
                                            <i class="fa fa-diamond"></i>
                                            Applied Coupon Code
                                        </div>
                                        <div class="text-end">
                                            {{ $payment->coupon->coupon_code }}
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="fw-bold">
                                            <i class="fa fa-diamond"></i>
                                            Amount Off
                                        </div>
                                        <div class="text-end">
                                            @if ($payment->coupon->discount_type == 'Percent')
                                                {{ $payment->coupon->discount . ' %' }}
                                            @else
                                                {{ 'PKR ' . $payment->coupon->discount }}
                                            @endif
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <div class="card-footer bg-success text-center">
                                @if ($payment->status == 'Pending')
                                    <button class="text-decoration-none btn btn-sm btn-outline-dark" type="submit"
                                        form="approvePayment">
                                        Approve
                                    </button>
                                @else
                                    <button class="btn px-4 btn-dark" data-bs-toggle="tooltip"
                                        title="confirmed payments can't be deleted">
                                        <i class="fa fa-info"></i>
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <img src="{{ asset($payment->receipt ?? 'assets/img/default-credit-cards.jpg') }}"
                            class="img-fluid rounded img-thumbnail">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="{{ route('admin.payments.update', $payment->id) }}" method="post" id="approvePayment">
        @csrf
        @method('PUT')
    </form>
@endsection

@extends('layouts.admin',['pagename'=>'Payments'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>User</td>
                        <td>Type</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $key=>$payment)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$payment->user->full_name ?? '' }}</td>
                        <td>{!! $payment->payment_type !!}</td>
                        <td>{!! $payment->payment_status !!} </td>
                        <td>
                            <div class="btn-group btn-group-sm">
                                <a href="{{route('admin.payments.show', $payment->id)}}" class="btn btn-outline-success" type="button" data-bs-toggle="tooltip" title="view">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <button class="btn btn-outline-danger" form="delete{{ $payment->id }}" type="submit" data-bs-toggle="tooltip" title="delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <form action="{{route('admin.payments.destroy',$payment->id)}}" class="d-none" method="post" hidden id="delete{{ $payment->id }}">
                                @csrf @method('DELETE')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

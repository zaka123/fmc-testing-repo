@extends('layouts.admin',['pagename'=>'Tests'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Tests') }}
    </h2>
</x-slot>

<div class="container bg-white shadow px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12 justify-content-end">
            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-solid fa-plus"></i>
            </button>
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tests as $key=>$test)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$test->test_name ?? 'N/A' }}</td>
                        <td>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#edit_test_{{ $test->id }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </button>
                                <a href="{{ route("admin.tests.show", $test->id) }}" class="btn btn-outline-success">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#delete_test_{{ $test->id }}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <!-- test delete form -->
                            <div class="modal fade" id="delete_test_{{ $test->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="staticBackdropLabel">Attention!</b></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete the test?
                                            <form action="{{route('admin.tests.destroy',$test->id)}}" method="post" hidden id="delete{{ $test->id }}">@csrf @method('DELETE')</form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <button type="submit" class="btn btn-primary" form="delete{{ $test->id }}">Yes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- edit test --}}
                            <div class="modal fade" id="edit_test_{{ $test->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="staticBackdropLabel">Edit <b>{{ $test->test_name }}</b></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route("admin.tests.update", $test->id) }}" method="post" id="test_{{ $test->id }}_form" class="needs-validation" novalidate>
                                                @csrf
                                                @method("PUT")
                                                <input type="text" class="form-control" name="test_name" id="test_name" required value="{{ $test->test_name }}">
                                                <div class="valid-feedback">Looks good!</div>
                                                <div class="invalid-feedback">Test name can't be empty.</div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-primary" form="test_{{ $test->id }}_form">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('modal')
<form action="{{route('admin.tests.store')}}" class="needs-validation" novalidate method="post">
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label for="role-name">Name</label>
            <input type="text" class="form-control" required name="test_name">
            <div class="valid-feedback">Looks good!</div>
            <div class="invalid-feedback">Test name can't be empty.</div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="bg-primary btn btn-primary">Add Test</button>
    </div>
</form>
@endsection
@include('extra.modal',['modal_title'=>'Add Test'])
@endsection

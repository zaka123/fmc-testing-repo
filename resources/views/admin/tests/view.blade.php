@extends('layouts.admin', ['pagename' => $test->test_name])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __($test->test_name) }}
    </h2>
</x-slot>

<div class="container bg-white shadow px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Question</td>
                        <td>Option&nbsp;1</td>
                        <td>Option&nbsp;2</td>
                        <td>Option&nbsp;3</td>
                        <td>Option&nbsp;4</td>
                        <td>Option&nbsp;5</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($questions as $key => $qObject)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $qObject->question }}</td>
                        <td>{{ $qObject->option }}</td>
                        <td>{{ $qObject->option_second }}</td>
                        <td>{{ $qObject->option_third }}</td>
                        <td>{{ $qObject->option_fourth }}</td>
                        <td>{{ $qObject->option_five ?? "N/A" }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@extends('layouts.admin',['pagename'=>'Dashboard'])
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <!-- Counsellors LIST -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Latest Coaches</h3>

                    <div class="card-tools">
                        <span class="badge badge-primary">{{ count($counsellors) }} new {{ Str::plural("coach", count($counsellors)) }}</span>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="users-list clearfix">
                        @forelse($counsellors as $counsellor)
                        <li>
                            <img src="{{ asset($counsellor->profilePhoto) }}" style="width:100px !important; height: 100px !important;" alt="User Image">
                            <a class="users-list-name" href="{{ route('admin.counsellors.show', $counsellor->id) }}">{{ $counsellor->full_name }}</a>
                            <span class="users-list-date">{{ $counsellor->humanReadableDate }}</span>
                        </li>
                        @empty
                        <div class="p-3">No coaches found.</div>
                        @endforelse
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="{{ route('admin.counsellors.index') }}">View All Coaches</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!--/.card -->
        </div>

        <div class="col-md-6">
            <!-- Counsellors LIST -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Latest Students</h3>

                    <div class="card-tools">
                        <span class="badge badge-primary">{{ count($students) }} new {{ Str::plural("student", count($students)) }}</span>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="users-list clearfix">
                        @forelse($students as $student)
                        <li>
                            <img src="{{ asset($student->profilePhoto) }}" style="width:100px !important;height:100px !important" alt="User Image">
                            <a class="users-list-name" href="{{ route("admin.users.show", $student->id) }}">
                                {{ $student->fullName }}
                            </a>
                            <span class="users-list-date">{{ $student->humanReadableDate }}</span>
                        </li>
                        @empty
                        <div class="p-3">No students found.</div>
                        @endforelse
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="{{ route('admin.users.index') }}">View All Students</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!--/.card -->
        </div>
    </div>
</div>
@endsection

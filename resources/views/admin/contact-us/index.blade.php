@extends('layouts.admin',['pagename'=>'Issues'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Benefits') }}
    </h2>
</x-slot>
<div class="container bg-white shadow px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Phone</td>
                        <td>Message</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($issues as $issue)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $issue->name }}</td>
                        <td>{{ $issue->email }}</td>
                        <td>{{ $issue->phone_number }}</td>
                        <td>{{ $issue->message }}</td>
                        <td>
                            <button class="btn btn-sm btn-success">Mark as Solved</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

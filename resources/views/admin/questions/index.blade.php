@extends('layouts.admin', ['pagename' => 'Questions'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Questions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="bg-white px-4 py-4 rounded">
            <div class="row">
                <div class="col-md-12 justify-content-end">
                    <button type="button" class="bg-primary btn btn-sm btn-primary" data-bs-toggle="modal"
                        data-bs-target="#exampleModal">
                        <i class="fa fa-solid fa-plus"></i>
                    </button>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped" id="example1">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Question</td>
                                <td>Number</td>
                                <td>Test</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($questions as $key => $question)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{!! $question->question ?? '' !!}</td>
                                    <td>{!! $question->question_no ?? '' !!}</td>
                                    <td>{{ $question->tests->test_name ?? '' }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{ route('admin.questions.edit', $question->id) }}" type="button"
                                                class="btn btn-outline-secondary">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal"
                                                data-bs-target="#deleteQuestion{{ $question->id }}Modal">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            {{-- <button type="submit" class="btn btn-outline-danger" form="delete_{{ $question->id }}"><i class="fa fa-trash"></i></button> --}}
                                        </div>
                                        {{-- <form action="{{ route('admin.questions.destroy', $question->id) }}" hidden
                                            id="delete_{{ $question->id }}" method="post">@csrf @method('DELETE')</form> --}}

                                        {{-- delete question modal --}}
                                        <div class="modal fade" id="deleteQuestion{{ $question->id }}Modal"
                                            data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
                                            aria-labelledby="deleteQuestion{{ $question->id }}ModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5"
                                                            id="deleteQuestion{{ $question->id }}ModalLabel">Attention!</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('admin.questions.destroy', $question->id) }}" method="post">
                                                        @csrf @method('DELETE')
                                                        <div class="modal-body">
                                                            Are you sure you want to delete this question?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary"
                                                                data-bs-dismiss="modal">No</button>
                                                            <button type="submit"
                                                                class="btn btn-sm btn-danger">Yes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@section('modal')
    <form action="{{ route('admin.questions.store') }}" method="post" style="overflow-y: scroll;">
        @csrf
        <div class="modal-body text-sm">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="tests" class="form-label">Test</label>
                        <select class="form-control select2 @error('tests') is-invalid @enderror" name="tests"
                            style="width: 100%;">
                            @foreach ($tests as $key => $test)
                                <option value="{{ $test->id }}">{{ $test->test_name ?? '' }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="QuestionNumber">Question No</label>
                        <input type="number" name="question_no" value="{{ old('question_no') }}"
                            class="form-control @error('question_no') is-invalid @enderror">
                    </div>
                    <div class="col-md-6">
                        <label for="category">Category <small>(optional only used for skill assessment)</small> </label>
                        <input type="text" maxlength="255" name="category" value="{{ old('category') }}"
                            class="form-control @error('category') is-invalid @enderror">
                    </div>
                    <div class="col-md-6">
                        <label for="sub_category">Sub Category <small>(optional only used for skill assessment)</small>
                        </label>
                        <input type="text" maxlength="255" name="sub_category" value="{{ old('sub_category') }}"
                            class="form-control @error('sub_category') is-invalid @enderror">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="question">Question</label>
                <textarea name="question" id="summernote" cols="30" rows="10" class="@error('question') is-invalid @enderror">{{ old('question') }}</textarea>
            </div>
            <div class="form-group">
                <label for="question_sub">Question Sub <small>(Optional only for Skill assessment)</small></label>
                <textarea name="question_sub" cols="30" rows="10"
                    class="summernote @error('question_sub') is-invalid @enderror">{{ old('question_sub') }}</textarea>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="option">Option</label>
                        <input type="text" class="form-control @error('option') is-invalid @enderror"
                            value="{{ old('option') }}" name="option">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="option_second">Second Option</label>
                        <input type="text" class="form-control @error('option_second') is-invalid @enderror"
                            name="option_second" value="{{ old('option_second') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="option_third">Third Option</label>
                        <input type="text" class="form-control @error('option_third') is-invalid @enderror"
                            name="option_third" value="{{ old('option_third') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="option_fourth">Fourth Option</label>
                        <input type="text" class="form-control @error('option_fourth') is-invalid @enderror"
                            name="option_fourth" value="{{ old('option_fourth') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="option_five">Fifth Option</label>
                        <input type="text" class="form-control @error('option_five') is-invalid @enderror"
                            name="option_five" value="{{ old('option_five') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="bg-primary btn btn-primary">Add Question</button>
        </div>
    </form>
@endsection
@include('extra.modal', ['modal_title' => 'Add Question'])
@endsection

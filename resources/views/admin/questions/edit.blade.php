@extends('layouts.admin', ['pagename' => 'Edit Questions'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Questions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="bg-white px-4 py-4 rounded">
            <div class="row justify-content-center">
                <form action="{{ route('admin.questions.update', $question->id) }}" method="post">
                    @csrf @method('PUT')
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="tests" class="form-label">Test</label>
                                <select class="form-control @error('tests') is-invalid @enderror" name="tests">
                                    @foreach ($tests as $key => $test)
                                        <option @if ($test->id == $question->test_id) selected @endif
                                            value="{{ $test->id }}">{{ $test->test_name ?? '' }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="QuestionNumber">Question No</label>
                                <input type="number" name="question_no"
                                    value="{{ $question->question_no ?? old('question_no') }}"
                                    class="form-control @error('question_no') is-invalid @enderror">
                            </div>
                            <div class="col-md-6">
                                <label for="category">Category <small>(optional only used for skill assessment)</small>
                                </label>
                                <input type="text" maxlength="255" name="category"
                                    value="{{ $question->category ?? old('category') }}"
                                    class="form-control @error('category') is-invalid @enderror">
                            </div>
                            <div class="col-md-6">
                                <label for="sub_category">Sub Category <small>(optional only used for skill
                                        assessment)</small> </label>
                                <input type="text" maxlength="255" name="sub_category"
                                    value="{{ $question->sub_category ?? old('sub_category') }}"
                                    class="form-control @error('sub_category') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="question">Question</label>
                        <textarea name="question" id="" cols="30" rows="5" class="form-control @error('question') is-invalid @enderror">{!! $question->question ?? old('question') !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="question_sub">Question Sub <small>(Optional only for Skill assessment)</small></label>
                        <textarea name="question_sub" cols="30" rows="5"
                            class="form-control @error('question_sub') is-invalid @enderror">{!! $question->question_sub ?? old('question_sub') !!}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="option">Option</label>
                                <input type="text" class="form-control @error('option') is-invalid @enderror"
                                    value="{{ $question->option ?? old('option') }}" name="option">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="option_second">Second Option</label>
                                <input type="text" class="form-control @error('option_second') is-invalid @enderror"
                                    name="option_second" value="{{ $question->option_second ?? old('option_second') }}">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="question_id" value="{{ $question->id ?? '' }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="option_third">Third Option</label>
                                <input type="text" class="form-control @error('option_third') is-invalid @enderror"
                                    name="option_third" value="{{ $question->option_third ?? old('option_third') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="option_fourth">Fourth Option</label>
                                <input type="text" class="form-control @error('option_fourth') is-invalid @enderror"
                                    name="option_fourth" value="{{ $question->option_fourth ?? old('option_fourth') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="option_five">Fifth Option</label>
                                <input type="text" class="form-control @error('option_five') is-invalid @enderror"
                                    name="option_five" value="{{ $question->option_five ?? old('option_five') }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="bg-primary btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>
@endsection

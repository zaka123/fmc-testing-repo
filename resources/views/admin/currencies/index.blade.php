@extends('layouts.admin', ['pagename' => 'Currency Based Charges'])
@section('content')
    {{-- add new currency modal --}}
    <div class="modal fade" id="addCurrencyModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
        aria-labelledby="addCurrencyModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5 text-bold" id="addCurrencyModalLabel">Add New Currency
                    </h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('admin.currencies.store') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="name">Currency Title<span class="text-danger">*</span></label>
                                    <input name="name" placeholder="United States Dollar"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="code">Currency Code<span class="text-danger">*</span></label>
                                    <input name="code" maxlength="30" placeholder="USD"
                                        class="form-control @error('code') is-invalid @enderror">
                                    @error('code')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="charges">Charges<span class="text-danger">*</span></label>
                                    <input name="charges" placeholder="10.00"
                                        class="form-control @error('charges') is-invalid @enderror">
                                    @error('charges')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="charged_for">Charged For<span class="text-danger">*</span></label>
                                    {{-- <input id="charged_for" name="charged_for" placeholder="Session"
                                class="form-control @error('charged_for') is-invalid @enderror" required> --}}
                                    <select name="charged_for" class="form-select">
                                        <option value="Session" selected>Session</option>
                                        {{-- <option value="Test" selected>Test</option> --}}
                                    </select>
                                    @error('charged_for')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- edit currency modal --}}
    <div class="modal fade" id="editCurrencyModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
        aria-labelledby="editCurrencyModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="editCurrencyModalLabel">Edit Currency Charges!
                    </h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('admin.currencies.update') }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <input id="currency_id" type="hidden" name="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="name">Currency Title<span class="text-danger">*</span></label>
                                    <input id="name" name="name" placeholder="United States Dollar"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="code">Currency Code<span class="text-danger">*</span></label>
                                    <input id="code" name="code" maxlength="30" placeholder="USD"
                                        class="form-control @error('code') is-invalid @enderror">
                                    @error('code')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="charges">Charges<span class="text-danger">*</span></label>
                                    <input id="charges" name="charges" placeholder="10.00"
                                        class="form-control @error('charges') is-invalid @enderror">
                                    @error('charges')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label for="charged_for">Charged For<span class="text-danger">*</span></label>
                                    {{-- <input id="charged_for" name="charged_for" placeholder="Session"
                                class="form-control @error('charged_for') is-invalid @enderror" required> --}}
                                    <select id="charged_for" name="charged_for" class="form-select">
                                        <option value="Session" selected>Session</option>
                                        {{-- <option value="Test" selected>Test</option> --}}
                                    </select>
                                    @error('charged_for')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Currency Based Charges') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow px-4 py-4 rounded">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#addCurrencyModal"
                    title="Add New Currency">
                    <i class="fa fa-solid fa-plus"></i>
                </button>

            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="example1">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Code</th>
                            <th>Charges</th>
                            <th>Charged For</th>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($currencies as $key => $currency)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $currency->name }}</td>
                                <td>{{ $currency->code }}</td>
                                <td>{{ $currency->charges }}</td>
                                <td>{{ $currency->charged_for }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Actions">

                                        <button class="btn btn-outline-secondary" title="edit currency charges"
                                            data-bs-toggle="modal" data-bs-target="#editCurrencyModal"
                                            data-id="{{ $currency->id }}">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="submit" form="delete{{ $currency->id }}"
                                            class="btn btn-outline-danger" title="delete currency" data-bs-toggle="tooltip">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>

                                    <!-- delete currency with id ? -->
                                    <form action="{{ route('admin.currencies.destroy', $currency->id) }}" method="post" hidden
                                        id="delete{{ $currency->id }}">
                                        @csrf @method('DELETE')
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        // create ajax request to populate the edit form
        $(document).ready(function() {
            $('#editCurrencyModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var dataId = button.data('id');
                $.ajax({
                    url: '/admin/currencies/' + dataId + '/edit',
                    type: 'get',
                    success: function(data) {
                        // Populate the form fields with the retrieved data
                        $('#currency_id').val(data.id);
                        $('#name').val(data.name);
                        $('#code').val(data.code);
                        $('#charges').val(data.charges);
                        // console.log(data);
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
            })
        })
    </script>
@endpush

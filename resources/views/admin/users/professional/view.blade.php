@extends('layouts.admin',['pagename'=>'User Profile'])
@section('content')
<style>
    .progress {
        height: 1.5rem;
    }
</style>
<div class="container mb-5">
    <div class="row gy-3">
        <div class="col-12 col-lg-4">
            <div class="card h-100 shadow">
                <div class="card-body">
                    <div class="text-center">
                        <img class="rounded-circle border" src="{{ asset($user->profilePhoto) }}" alt="User profile picture" width="150" height="150">
                    </div>

                    <h3 class="text-center headingFont">{{ $user->fullName }}</h3>

                    <p class="text-muted text-center"><b>{{ $user->email }}</b></p>

                    {{-- <ul class="list-group">
                        <li class="list-group-item">
                            <b>D.O.B:</b> <span class="float-right">{{ $userProfile?->date_of_birth }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Country:</b> <span class="float-right">{{ $userProfile?->city }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Experience:</b> <span class="float-right">{{ $userProfile?->experience }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Education level:</b> <span class="float-right">{{ $userProfile?->education_level }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Organization working in:</b> <span class="float-right">{{ $userProfile?->organization_name }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Role:</b> <span class="float-right">{{ $userProfile?->role }}</span>
                        </li>
                    </ul> --}}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8">
            <div class="card h-100 shadow">
                <div class="card-body">
                    <table class="table table-striped" id="example1">
                        <thead> <strong> User's Sessions </strong>
                            <tr>
                                <th>#</th>
                                <th>Counsellor</th>
                                <th>Date </th>
                                <th>Time</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sessions as $key=>$session)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    <a href="{{ route("admin.counsellors.show", $session->counsellor->id) }}">{{ $session->counsellor->fullname }}</a>
                                </td>
                                <td>{{ date("d-m-Y", strtotime($session->session->session_timing)) }}</td>
                                <td>{{ date("h:i A", strtotime($session->session->session_timing)) }}</td>
                                <td>{!! $session->session_status !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card shadow h-100">
                <div class="h4 rounded-top headingFont text-center text-white py-3" style="background-color: #3ab3f8 !important;">
                    PERSONALITY TEST RESULTS
                </div>
                <div class="card-body px-lg-5">
                    @if($personalityTestResults > 99)
                        @include("admin.users.results.personality")
                    @elseif($personalityTestResults < 1)
                        Not attempted yet
                    @else
                        Not completed yet
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card h-100 shadow">
                <div class="h4 rounded-top headingFont text-white text-center py-3" style="background-color: #ffc854 !important">
                    CAREER EXPECTATIONS QUESTIONNAIRE
                </div>
                <div class="card-body px-lg-5">
                    @if($careerExpectationsTestResults > 99)
                        @include("admin.users.results.career-expectations")
                    @elseif($careerExpectationsTestResults < 1)
                        Not attempted yet
                    @else
                        Not completed yet
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card h-100 shadow">
                <div class="h4 rounded-top headingFont text-center text-white py-3" style="background-color: #fe7852 !important;">SKILL ASSESSMENT TEST RESULTS</div>
                <div class="card-body px-lg-5">
                    @if($skillAssessmentTestResults > 99)
                        @include("admin.users.results.skill-assessment")
                    @elseif($skillAssessmentTestResults < 1)
                        Not attempted yet
                    @else
                        Not completed yet
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

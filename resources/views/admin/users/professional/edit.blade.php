@extends('layouts.admin',['pagename'=>'Edit Profile'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Users') }}
    </h2>
</x-slot>

<div class="container bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-12">
            <div class="main">
                <div class="floating"></div>
                <div class="p-0 p-lg-4">
                    <div class="card border-0 bg-transparent">
                        <div class="card-body">
                            <div class="inner p-3">
                                <form action="{{ route('admin.users.professional.update', $user->id) }}" id="editProfile" method="POST" class="row g-3 align-items-baseline">
                                    @csrf @method('PUT')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" name="name" id="name" value="{{ $user->name }}" class="@error('name') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" class="@error('last_name') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" id="email" value="{{ $user->email }}" class="@error('email') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_of_birth">Date of Birth </label>
                                            <input type="date" name="date_of_birth" id="date_of_birth" value="{{ date('Y-m-d', strtotime($userProfile->date_of_birth)) }}" class="@error('date_of_birth') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input name="city" id="city" value="{{ $userProfile->city }}" class="@error('city') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="experience">Experience</label>
                                            <input name="experience" id="experience" value="{{ $userProfile->experience }}" class="@error('experience') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="education_level">Education Level</label>
                                            <select name="education_level" id="education_level" class="@error('education_level') is-invalid @enderror form-select">
                                                <option value="">Please select your education level</option>
                                                <option {{ $userProfile->education_level == "Lower secondary" ? 'selected' : '' }}>Lower secondary</option>
                                                <option {{ $userProfile->education_level == "Upper secondary" ? 'selected' : '' }}>Upper secondary</option>
                                                <option {{ $userProfile->education_level == "Post-secondary" ? 'selected' : '' }}>Post-secondary</option>
                                                <option {{ $userProfile->education_level == "Bachelor's" ? 'selected' : '' }}>Bachelor's</option>
                                                <option {{ $userProfile->education_level == "Master's" ? 'selected' : '' }}>Master's</option>
                                                <option {{ $userProfile->education_level == "Doctorate" ? 'selected' : '' }}>Doctorate</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="organization_name">Organization Name</label>
                                            <input name="organization_name" id="organization_name" value="{{ $userProfile->organization_name }}" class="@error('organization_name') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role">Role</label>
                                            <input name="role" id="role" value="{{ $userProfile->role }}" class="@error('role') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="gender">Gender</label><br>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @checked($user->gender=="Male") name="gender" type="radio" id="Male" value="Male">
                                                <label class="form-check-label" for="Male">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @checked($user->gender=="Female") name="gender" type="radio" id="Female" value="Female">
                                                <label class="form-check-label" for="Female">Female</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @checked($user->gender=="Other") name="gender" type="radio" id="Other" value="Other">
                                                <label class="form-check-label" for="Other">Other</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mt-3">
                                        <a href="{{ url()->previous() }}" class="btn btn-secondary btn-sm">Cancel changes</a>
                                        <input type="submit" value="Save changes" class="btn btn-success btn-sm">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ update profile -->
</div>
@endsection

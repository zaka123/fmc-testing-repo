@extends('layouts.admin', ['pagename' => 'Change User Password'])

@section('content')
<div class="container pb-5 d-flex align-items-center justify-content-center" style="min-height:82vh">
    <div class="card" style="width:35rem;--bs-card-border-color:var(--special)">
        <h5 class="card-header bg-special">Change User Password</h5>
        <div class="card-body">
            {{-- <div class="card-text mb-3 text-muted">
                Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Repellat omnis, nulla harum corporis illum nobis fuga alias delectus repellendus animi deserunt molestiae at est modi corrupti. Earum sed explicabo expedita.
            </div> --}}
            <form method="POST" action="{{ route('admin.users.updateUserPassword', $user->id) }}" id="changePasswordForm">
                @csrf
                <div class="form-group mb-3">
                    <label for="user" class="form-label">User Email: </label>
                    <input class="form-control" type="text" name="user" value="{{ $user->email }}" @readonly(true)>
                </div>
                <div class="form-group mb-3">
                    <label for="password" class="form-label">Password:</label>
                    <input id="password" class="form-control" type="password" name="password" required autofocus>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm Password:</label>
                    <input id="password_confirmation" class="form-control" type="password" name="password_confirmation" required>
                </div>
            </form>
        </div>
        <button class="card-footer btn btn-sm text-white" form="changePasswordForm" style="background-color: #2f90f8">Change User Password</button>
    </div>
</div>
@endsection


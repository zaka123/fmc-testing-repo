@extends('layouts.admin', ['pagename' => $role . ' - Deleted Records'])
@section('content')
    <div class="container bg-white px-4 py-4 rounded">
        <div class="row">
            {{-- <div class="col-md-12 justify-content-end">
                <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"
                    title="add user">
                    <i class="fa fa-solid fa-plus"></i>
                </button>
            </div> --}}
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped" id="admin_users_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>

                                <th class="no-sort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $user->name ?? '' }}</td>
                                    <td>{{ $user->email ?? '' }}</td>

                                    <td>
                                        <div class="btn-group btn-group-sm gap-1">
                                            <a href="{{ route('admin.users.restore', $user->id) }}"
                                                class="btn btn-outline-primary" title="Restore">
                                                <i class="fas fa-trash-restore"></i>
                                            </a>
                                            @if ($user->hasRole('User'))
                                                <button type="submit" class="btn btn-outline-danger" data-bs-toggle="modal"
                                                    data-bs-target="#deleteUser{{ $user->id }}Modal"
                                                    title="Permanently Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>

                                                {{-- delete user modal --}}
                                                <div class="modal fade" id="deleteUser{{ $user->id }}Modal"
                                                    data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
                                                    aria-labelledby="deleteUser{{ $user->id }}ModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h1 class="modal-title fs-5 text-danger"
                                                                    id="deleteUser{{ $user->id }}ModalLabel">Warning!
                                                                </h1>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <form
                                                                action="{{ route('admin.trashedUsers.delete', $user->id) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('DELETE')
                                                                <div class="modal-body">
                                                                    Are you sure to permanantly delete this user? All user
                                                                    related data like sessions, video calls etc will be
                                                                    permanantly deleted.
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-sm btn-secondary"
                                                                        data-bs-dismiss="modal">No</button>
                                                                    <button type="submit"
                                                                        class="btn btn-sm btn-danger">Yes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {

            $("#admin_users_table").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "ordering": true,
            });

        });
    </script>
@endpush

@extends('layouts.admin',['pagename'=>'Edit Profile'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Users') }}
    </h2>
</x-slot>

<div class="container bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-12">
            <div class="main">
                <div class="floating"></div>
                <div class="p-0 p-lg-4">
                    <div class="card border-0 bg-transparent">
                        <div class="card-body">
                            <div class="inner p-3">
                                <form action="{{ route('admin.users.update', $user->id) }}" id="editProfile" method="POST" class="row g-3 align-items-baseline">
                                    @csrf @method('PUT')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" name="name" id="name" value="{{ $user->name }}" class="@error('name') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" class="@error('last_name') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" id="email" value="{{ $user->email }}" class="@error('email') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_of_birth">Date of Birth </label>
                                            <input type="date" name="date_of_birth" id="date_of_birth" value="{{ date('Y-m-d', strtotime($userProfile->date_of_birth)) }}" class="@error('date_of_birth') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" name="city" id="city" value="{{ $userProfile->city }}" class="@error('city') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="favorite_subject">Favorite Subject</label>
                                            <input type="text" name="favorite_subject" id="favorite_subject" value="{{ $userProfile->favorite_subject }}" class="@error('favorite_subject') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="easiest_subject">Easiest Subject</label>
                                            <input type="text" name="easiest_subject" id="easiest_subject" value="{{$userProfile->easiest_subject ?? ''}}" class="@error('easiest_subject') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="difficult_subject">Difficult Subject</label>
                                            <input type="text" name="difficult_subject" id="difficult_subject" value="{{$userProfile->difficult_subject ?? ''}}" class="@error('difficult_subject') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="average_grade_point">Average Grade Point</label>
                                            <input type="text" name="average_grade_point" id="average_grade_point" value="{{$userProfile->average_grade_point ?? ''}}" class="@error('average_grade_point') is-invalid @enderror form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="father_education">Father Education</label>
                                            <select name="father_education" id="father_education" class="@error('father_education') is-invalid @enderror form-control">
                                                <option value="">Select an option</option>
                                                <option {{ $userProfile->father_education == "Not qualified" ? 'selected' : ''  }} value="Not qualified">Not qualified</option>
                                                <option {{ $userProfile->father_education == "Primary Education" ? 'selected' : ''  }} value="Primary Education">Primary Education</option>
                                                <option {{ $userProfile->father_education == "Secondary Education" ? 'selected' : ''  }} value="Secondary Education">Secondary Education</option>
                                                <option {{ $userProfile->father_education == "Vocational Training" ? 'selected' : ''  }} value="Vocational Training">Vocational Training</option>
                                                <option {{ $userProfile->father_education == "Bachelors" ? 'selected' : ''  }} value="Bachelors">Bachelors</option>
                                                <option {{ $userProfile->father_education == "Masters" ? 'selected' : ''  }} value="Masters">Masters</option>
                                                <option {{ $userProfile->father_education == "PhD" ? 'selected' : ''  }} value="PhD">PhD</option>
                                                <option {{ $userProfile->father_education == "Prefer not to say" ? 'selected': ''  }} value="Prefer not to say">Prefer not to say</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="father_occupation">Father Occupation</label>
                                            <select name="father_occupation" id="father_occupation" class="@error('father_occupation') is-invalid @enderror form-control">
                                                <option value="">Select an option</option>
                                                <option {{ $userProfile->father_occupation == "Unemployed" ? 'selected' : '' }} value="Unemployed">Unemployed</option>
                                                <option {{ $userProfile->father_occupation == "Unskilled" ? 'selected' : '' }} value="Unskilled">Unskilled</option>
                                                <option {{ $userProfile->father_occupation == "Semi-skilled job" ? 'selected' : '' }} value="Semi-skilled job">Semi-skilled job</option>
                                                <option {{ $userProfile->father_occupation == "High skilled job" ? 'selected' : '' }} value="High skilled job">High skilled job</option>
                                                <option {{ $userProfile->father_occupation == "Business" ? 'selected' : '' }} value="Business">Business</option>
                                                <option {{ $userProfile->father_occupation == "Prefer not to say" ? 'selected' : '' }} value="Prefer not to say">Prefer not to say</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mother_education">Mother Education</label>
                                            <select name="mother_education" id="mother_education" class="@error('mother_education') is-invalid @enderror form-control">
                                                <option value="">Select an option</option>
                                                <option {{ $userProfile->mother_education == "Not qualified" ? 'selected' : ''  }} value="Not qualified">Not qualified</option>
                                                <option {{ $userProfile->mother_education == "Primary Education" ? 'selected' : ''  }} value="Primary Education">Primary Education</option>
                                                <option {{ $userProfile->mother_education == "Secondary Education" ? 'selected' : ''  }} value="Secondary Education">Secondary Education</option>
                                                <option {{ $userProfile->mother_education == "Vocational Training" ? 'selected' : ''  }} value="Vocational Training">Vocational Training</option>
                                                <option {{ $userProfile->mother_education == "Bachelors" ? 'selected' : ''  }} value="Bachelors">Bachelors</option>
                                                <option {{ $userProfile->mother_education == "Masters" ? 'selected' : ''  }} value="Masters">Masters</option>
                                                <option {{ $userProfile->mother_education == "PhD" ? 'selected' : ''  }} value="PhD">PhD</option>
                                                <option {{ $userProfile->mother_education == "Prefer not to say" ? 'selected': ''  }} value="Prefer not to say">Prefer not to say</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mother_occupation">Mother Occupation</label>
                                            <select name="mother_occupation" id="mother_occupation" class="@error('mother_occupation') is-invalid @enderror form-control">
                                                <option value="">Select an option</option>
                                                <option {{ $userProfile->mother_occupation == "Unemployed" ? 'selected' : '' }} value="Unemployed">Unemployed</option>
                                                <option {{ $userProfile->mother_occupation == "Unskilled" ? 'selected' : '' }} value="Unskilled">Unskilled</option>
                                                <option {{ $userProfile->mother_occupation == "Semi-skilled job" ? 'selected' : '' }} value="Semi-skilled job">Semi-skilled job</option>
                                                <option {{ $userProfile->mother_occupation == "High skilled job" ? 'selected' : '' }} value="High skilled job">High skilled job</option>
                                                <option {{ $userProfile->mother_occupation == "Business" ? 'selected' : '' }} value="Business">Business</option>
                                                <option {{ $userProfile->mother_occupation == "Prefer not to say" ? 'selected' : '' }} value="Prefer not to say">Prefer not to say</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="parents_retired">Parents Retired</label><br>
                                            <div class="form-check form-check-inline">
                                                <input id="father" class="form-check-input" type="radio" id="parents_retired" @if($userProfile->parents_retired=="Father") checked @endif
                                                name="parents_retired" value="Father" class="btn-check
                                                @error('parents_retired') is-invalid @enderror">
                                                <label for="father" class="form-check-label">Father</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input id="mother" class="form-check-input" type="radio" id="parents_retired" @if($userProfile->parents_retired=="Mother") checked @endif
                                                name="parents_retired" value="Mother" class="btn-check
                                                @error('parents_retired') is-invalid @enderror">
                                                <label for="mother" class="form-check-label">Mother</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input id="both" class="form-check-input" type="radio" @if($userProfile->parents_retired=="Both")
                                                checked @endif
                                                id="parents_retired" name="parents_retired" value="Both"
                                                class="btn-check @error('parents_retired') is-invalid
                                                @enderror">
                                                <label for="both" class="form-check-label">Both</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input id="neither" class="form-check-input" type="radio" @if($userProfile->parents_retired=="Neither")
                                                checked @endif
                                                id="parents_retired" name="parents_retired" value="Neither"
                                                class="btn-check @error('parents_retired') is-invalid
                                                @enderror">
                                                <label for="neither" class="form-check-label">Neither</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="gender">Gender</label><br>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->gender=="Male")
                                                checked @endif name="gender" type="radio" id="Male"
                                                value="Male">
                                                <label class="form-check-label" for="Male">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->gender=="Female")
                                                checked @endif name="gender" type="radio" id="Female"
                                                value="Female">
                                                <label class="form-check-label" for="Female">Female</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->gender=="Other")
                                                checked @endif name="gender" type="radio" id="Other"
                                                value="Other">
                                                <label class="form-check-label" for="Other">Other</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group">
                                            <label for="profession">Profession</label><br>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->profession=="Student") checked @endif
                                                name="profession" type="radio" id="Student" value="Student">
                                                <label class="form-check-label" for="Student">Student</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->profession=="University Student") checked @endif
                                                name="profession" type="radio" id="University Student"
                                                value="University Student">
                                                <label class="form-check-label" for="University Student">
                                                    University Student
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->profession=="Professional") checked @endif
                                                name="profession" type="radio" id="Professional"
                                                value="Professional">
                                                <label class="form-check-label" for="Professional">
                                                    Professional
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if($user->profession=="Parent")
                                                checked @endif name="profession" type="radio" id="Parent"
                                                value="Parent">
                                                <label class="form-check-label" for="Parent">
                                                    Parent Looking for Child
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mt-3">
                                        <input type="submit" value="save changes" class="btn btn-success btn-sm">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ update profile -->
</div>
@endsection

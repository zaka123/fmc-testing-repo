@extends('layouts.admin',['pagename'=>'Edit Profile'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Users') }}
    </h2>
</x-slot>

<div class="container bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-12">
            <div class="main">
                <div class="floating"></div>
                <div class="p-0 p-lg-4">
                    <div class="card border-0 bg-transparent">
                        <div class="card-body">
                            <div class="inner p-3">
                                <form action="{{route('admin.professional.update',$user->id)}}" method="post">
                                    @csrf @method('PUT')
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <div class="text-end">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name" value="{{old('name') ?? $user->name ?? '' }}" class="@error('name') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" name="last_name" id="last_name" value="{{old('last_name') ?? $user->last_name ?? '' }}" class="@error('last_name') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" id="email" value="{{old('email') ?? $user->email ?? '' }}" class="@error('email') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="form-group">
                                                <label for="gender">Gender</label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->gender=="Male")
                                                    checked @endif name="gender" type="radio" id="Male"
                                                    value="Male">
                                                    <label class="form-check-label" for="Male">Male</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->gender=="Female")
                                                    checked @endif name="gender" type="radio" id="Female"
                                                    value="Female">
                                                    <label class="form-check-label" for="Female">Female</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->gender=="Other")
                                                    checked @endif name="gender" type="radio" id="Other"
                                                    value="Other">
                                                    <label class="form-check-label" for="Other">Other</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 mb-3">
                                            <div class="form-group">
                                                <label for="profession">Profession</label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->profession=="Student") checked @endif
                                                    name="profession" type="radio" id="Student" value="Student">
                                                    <label class="form-check-label" for="Student">Student</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->profession=="University Student") checked @endif
                                                    name="profession" type="radio" id="University Student"
                                                    value="University Student">
                                                    <label class="form-check-label" for="University Student">University Student</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->profession=="Professional") checked @endif
                                                    name="profession" type="radio" id="Professional"
                                                    value="Professional">
                                                    <label class="form-check-label" for="Professional">Professional</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" @if($user->profession=="Parent")
                                                    checked @endif name="profession" type="radio" id="Parent"
                                                    value="Parent">
                                                    <label class="form-check-label" for="Parent">Parent Looking for
                                                        Child</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="date_of_birth">Date of Birth</label>
                                                <input type="date" name="date_of_birth" id="date_of_birth" value="{{old('date_of_birth') ?? $userProfessional->date_of_birth ?? '' }}" class="@error('date_of_birth') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" name="city" id="city" value="{{old('city') ?? $userProfessional->city ?? '' }}" class="@error('city') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="experience">Experience</label>
                                                <input type="text" name="experience" id="experience" value="{{old('experience') ?? $userProfessional->experience ?? '' }}" class="@error('experience') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="education_level">Education Level</label>
                                                <input type="text" name="education_level" id="education_level" value="{{old('education_level') ?? $userProfessional->education_level ?? '' }}" class="@error('education_level') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="organization_name">Organization Name</label>
                                                <input type="text" name="organization_name" id="organization_name" value="{{old('organization_name') ?? $userProfessional->organization_name ?? '' }}" class="@error('organization_name') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="role">Role</label>
                                                <input type="text" name="role" id="role" value="{{old('role') ?? $userProfessional->role ?? '' }}" class="@error('role') is-invalid @enderror form-control shadow-other">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3 justify-content-center">
                                            <input type="submit" value="Save" class="btn btn-success btn-sm">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ update profile -->
</div>
@endsection
<div class="card border-0 h-100 shadow-other">
    <h4 class="card-header text-bg-dark">Family Information</h4>
    <div class="card-body">
        @if ($familyInfo && count($familyInfo) > 0)
        <div class="row gy-2" style="font-weight: 500;">
            <div class="col-6">Father Education</div>
            <div class="col-6">{{ $userProfile->father_education }}</div>
            <div class="col-6">Father Occupation</div>
            <div class="col-6">{{ $userProfile->father_occupation }}</div>
            <div class="col-6">Mother Education</div>
            <div class="col-6">{{ $userProfile->mother_education }}</div>
            <div class="col-6">Mother Occupation</div>
            <div class="col-6">{{ $userProfile->mother_occupation }}</div>
            <div class="col-6">Retired Parents</div>
            <div class="col-6">{{ $userProfile->parents_retired }}</div>
            <div class="col-12 fw-bold fs-6">Family Assistance</div>
            <div class="col-3 col-md-6">Father</div>
            <div class="col-9 col-md-6">
                @if ($familyInfo["father"] > 100)
                    Not Applicable
                @else
                    <div class="progress bg-light rounded-0">
                        <div class="progress-bar text-warning bg-warning" style="width: {{ $familyInfo["father"] }}%"></div>
                    </div>
                @endif
            </div>
            <div class="col-3 col-md-6">Mother</div>
            <div class="col-9 col-md-6">
                @if ($familyInfo["mother"] > 100)
                    Not Applicable
                @else
                    <div class="progress bg-light rounded-0">
                        <div class="progress-bar text-warning bg-warning" style="width: {{ $familyInfo["mother"] }}%"></div>
                    </div>
                @endif
            </div>
            <div class="col-3 col-md-6">Siblings</div>
            <div class="col-9 col-md-6">
                @if ($familyInfo["siblings"] > 100)
                    Not Applicable
                @else
                    <div class="progress bg-light rounded-0">
                        <div class="progress-bar text-warning bg-warning" style="width: {{ $familyInfo["siblings"] }}%"></div>
                    </div>
                @endif
            </div>
            <div class="col-3 col-md-6">Relatives</div>
            <div class="col-9 col-md-6">
                @if ($familyInfo["relatives"] > 100)
                    Not Applicable
                @else
                    <div class="progress bg-light rounded-0">
                        <div class="progress-bar text-warning bg-warning" style="width: {{ $familyInfo["relatives"] }}%"></div>
                    </div>
                @endif
            </div>
            <div class="col-3 col-md-6">Others</div>
            <div class="col-9 col-md-6">
                @if ($familyInfo["other_persons"] > 100)
                    Not Applicable
                @else
                    <div class="progress bg-light rounded-0">
                        <div class="progress-bar text-warning bg-warning" style="width: {{ $familyInfo["other_persons"] }}%"></div>
                    </div>
                @endif
            </div>
        </div>
        @else
        <div class="alert alert-info" role="alert">No data found</div>
        @endif
    </div>
</div>

<div class="card border-0 h-100 shadow-other">
    <h4 class="card-header text-bg-dark">Future Self Reflection</h4>
    <div class="card-body">
        @if ($futureSelfReflection && $futureSelfReflection->count() > 0)
            <div class="row gy-2" style="font-weight: 500;">
                <div class="col-md-7">5-years Forecast:&nbsp;&nbsp;&nbsp;{{ $forcast }}</div>
                @if ($forcast != 'Other')
                    <div class="col-12 fw-bold">Reason:</div>
                    <div class="col-12">
                        @foreach ($futureSelfReflection[0]->answer as $reasonObject)
                            <ul>
                                @foreach ($reasonObject as $index => $value)
                                    <li>{{ $reasons[$index] }}</li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                @endif

                <div class="col-12 fw-bold fs-6">Decision Factors</div>
                <div class="col-md-7">Parents expectation:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['parents_expectations'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['parents_expectations'] }}%"></div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Friends Influence:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['friends_influence'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['friends_influence'] }}%"></div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">School/University Grades:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['school_college_university'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['school_college_university'] }}%">
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Favorite Subject:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['favorite_subject_importance'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['favorite_subject_importance'] }}%">
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Talent/skill:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['talent_skills'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['talent_skills'] }}%"></div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Hobbies:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['hobbies'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['hobbies'] }}%"></div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Scope/Market demand of occupation:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['scope_market_demand'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['scope_market_demand'] }}%"></div>
                        </div>
                    @endif
                </div>
                <div class="col-md-7">Financial Support for education or training:</div>
                <div class="col-md-5">
                    @if ($futureSelfReflection[1]->answer['financial_support_for_edu'] > 100)
                        Not Applicable
                    @else
                        <div class="progress bg-light rounded-0">
                            <div class="progress-bar text-special bg-special"
                                style="width: {{ $futureSelfReflection[1]->answer['financial_support_for_edu'] }}%">
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @else
            <div class="alert alert-info" role="alert">No data found</div>
        @endif
    </div>
</div>

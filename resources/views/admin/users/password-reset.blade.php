@extends('layouts.admin',['pagename'=>"Password Reset"])
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 rounded shadow bg-white p-4">
            <form action="{{route('admin.password.change')}}" class="needs-validation" novalidate method="post">
                @csrf
                <div class="form-floating mb-3">
                    <input type="text" name="password" id="password" required class="form-control" placeholder="...">
                    <label for="password">New Password</label>
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Password can't be empty!</div>
                </div>
                <input type="hidden" name="user_id" value="{{$user_id}}">
                <div class="form-floating mb-3">
                    <input type="text" name="password_confirmation" required id="password_confirmation" class=" form-control" placeholder="...">
                    <label for="password">Confirm Password</label>
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please confirm password!</div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@extends('layouts.admin', ['pagename' => 'Students'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="container bg-white px-4 py-4 rounded">
        <div class="row">
            {{-- <div class="col-md-12 justify-content-end">
                <a href="{{ route('admin.trashedUsers') }}" class="btn btn-outline-danger" title="deleted users">
                    Trashed Users
                </a>
            </div> --}}
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped" id="admin_users_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                {{-- <th>Phone</th>
                                <th>DOB</th>
                                <th>Country</th>
                                <th>Gender</th>
                                <th>Profession</th>

                                <th>Experience</th>
                                <th>Education level</th>
                                <th>Organization working in</th>
                                <th>Role</th> --}}

                                <th>Status</th>
                                <th class="no-sort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $user->name ?? '' }}</td>
                                    <td>{{ $user->email ?? '' }}</td>
                                    {{-- <td>{{ $user->phone ?? 'N/A' }}</td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional?->date_of_birth : $user->userProfile?->date_of_birth ?? '' }}
                                    </td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional->city : $user->userProfile->city ?? '' }}
                                    </td>
                                    <td>{{ $user->gender ?? 'N/A' }}</td>
                                    <td>{{ $user->profession ?? 'N/A' }}</td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional->experience : '' }}</td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional->education_level : '' }}</td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional->organization_name : '' }}</td>
                                    <td>{{ $user->profession == 'Professional' ? $user->userProfessional->role : '' }}</td> --}}
                                    <td>
                                        <input data-id="{{ $user->id }}" class="toggle-class" type="checkbox"
                                            data-onstyle="success" data-offstyle="danger" data-toggle="toggle"
                                            data-on="Activate" data-off="Deactivate" {{ $user->status ? 'checked' : '' }}>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{ route('admin.users.show', $user->id) }}"
                                                class="btn btn-outline-success" type="button">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            {{-- <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a> --}}
                                            <a href="{{ route('admin.users.changeUserPassword', $user->id) }}"
                                                class="btn btn-outline-primary" type="button">
                                                <i class="fa fa-key"></i>
                                            </a>
                                            <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal"
                                                data-bs-target="#deleteUser{{ $user->id }}Modal">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                        {{-- delete user modal --}}
                                        <div class="modal fade" id="deleteUser{{ $user->id }}Modal"
                                            data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
                                            aria-labelledby="deleteUser{{ $user->id }}ModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5"
                                                            id="deleteUser{{ $user->id }}ModalLabel">Attention!</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('admin.users.destroy', $user->id) }}"
                                                        method="post">
                                                        @csrf @method('DELETE')
                                                        <div class="modal-body">
                                                            Are you sure you want to delete this user?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary"
                                                                data-bs-dismiss="modal">No</button>
                                                            <button type="submit"
                                                                class="btn btn-sm btn-danger">Yes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {
            $('.toggle-class').change(function() {
                var status = $(this).prop('checked') == true ? 1 : 0;
                var user_id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '{{ route('admin.users.changeUserStatus') }}',
                    data: {
                        'status': status,
                        'user_id': user_id
                    },
                    success: function(data) {
                        console.log(data.msg);
                    }
                });
            });

            $("#admin_users_table").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "ordering": true,
                "buttons": [{
                    extend: "excel",
                    className: "btn-primary",
                    titleAttr: 'Export in Excel',
                    text: 'Export To Excel',
                    exportOptions: {
                        // columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                        columns: [0, 1, 2, ]
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary')
                    }
                }, {
                    text: 'Deleted Records', // Button text
                    action: function(e, dt, node, config) {
                        // Define the URL you want to redirect to
                        var customUrl = '{{ route('admin.trashedUsers') }}';

                        // Create an anchor tag and set its href attribute
                        var anchor = document.createElement('a');
                        anchor.href = customUrl;

                        // Simulate a click on the anchor tag to navigate to the URL
                        anchor.click();
                    },
                    className: 'btn-danger mx-2', // Add custom CSS class
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary')
                    }
                }],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    // {
                    //     "targets": [4, 6, 8, 9, 10, 11], // Hide columns 1, 2, 3 etc
                    //     "visible": false
                    // }
                ]
            }).buttons().container().appendTo('#admin_users_table_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush

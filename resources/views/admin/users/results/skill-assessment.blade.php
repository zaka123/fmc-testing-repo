<div>
    <b>In&nbsp;Meeting&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-success" role="progressbar" style="width: {{ $skillAssessmentTestResults['inMeetings'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['inMeetings'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Outside&nbsp;Meetings&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-primary" role="progressbar" style="width: {{ $skillAssessmentTestResults['outsideMeetings'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['outsideMeetings'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Speaking&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ $skillAssessmentTestResults['speaking'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['speaking'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Writing&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-info" role="progressbar" style="width: {{ $skillAssessmentTestResults['writing'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['writing'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Organizing&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $skillAssessmentTestResults['organizing'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['organizing'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Creativity&nbsp;:&nbsp;</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-dark" role="progressbar" style="width: {{ $skillAssessmentTestResults['creativity'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
            {{ round($skillAssessmentTestResults['creativity'], 0) }}%
        </div>
    </div>
</div>

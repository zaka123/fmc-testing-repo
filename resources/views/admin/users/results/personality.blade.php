@php
    $wordCombination='';
    $personalityTestResults['extroversion'] > 20 ? $wordCombination = "S" : $wordCombination = "R";
    $personalityTestResults['neuroticism'] > 20 ? $wordCombination .= "L" : $wordCombination .= "C";
    $personalityTestResults['conscientiousness'] > 20 ? $wordCombination .= "O" : $wordCombination = $wordCombination."U"; 
    $personalityTestResults['agreeableness'] > 20 ? $wordCombination .= "A" : $wordCombination .= "E";
    $personalityTestResults['opennessToExperience'] > 20 ? $wordCombination .= "I" : $wordCombination .="N";
@endphp

<div>
    <b>Extroversion</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar" style="width: {{ $personalityTestResults['extroversion'] }}%">
            {{ round($personalityTestResults['extroversion'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Neuroticism</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar text-white bg-warning" style="width: {{ $personalityTestResults['neuroticism'] }}%">
            {{ round($personalityTestResults['neuroticism'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Conscientiousness</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-info" style="width: {{ $personalityTestResults['conscientiousness'] }}%">
            {{ round($personalityTestResults['conscientiousness'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Agreeableness</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-secondary" style="width: {{ $personalityTestResults['agreeableness'] }}%">
            {{ round($personalityTestResults['agreeableness'], 0) }}%
        </div>
    </div>
</div>
<hr>
<div>
    <b>Openness To Experience</b>
    <div class="progress rounded shadow-sm">
        <div class="progress-bar bg-success" style="width: {{ $personalityTestResults['opennessToExperience']}}%">
            {{ round($personalityTestResults['opennessToExperience'], 0) }}%
        </div>
    </div>
</div>
<hr>
<section>
    <hgroup>
        <h5>Description</h5>
        <p id="description"></p>
    </hgroup>
</section>
<section>
    <hgroup>
        <h5>Career I should Choose</h5>
        <p id="favoured_careers"></p>
    </hgroup>
</section>
<section>
    <hgroup>
        <h5>Career I shouldn't Choose</h5>
        <p id="disFavoured_careers"></p>
    </hgroup>
</section>

@if($user->getTestResults(4) > 99)
@push('script')
<script>
    $(document).ready(() => {
        var url = "{{ route('benefit') }}" + "/{{ $wordCombination }}";
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                $('#description').text(data.description);
                $('#favoured_careers').text(data.favoured_careers);
                $('#disFavoured_careers').text(data.disFavoured_careers);
            }, //end of success
            error: function(data) {
                $(window).scrollTop(0);
                toastr.error(data);
            }
        });
    });
</script>
@endpush
@endif

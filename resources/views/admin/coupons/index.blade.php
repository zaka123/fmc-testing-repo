@extends('layouts.admin', ['pagename' => 'Coupons'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Coupons') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow px-4 py-4 rounded">
        <div class="row">
            <div class="col-md-12 justify-content-end">
                <a href="{{ route('admin.coupons.create') }}" class="btn btn-outline-primary" data-bs-toggle="tooltip"
                    data-bs-title="Create Coupon Code">
                    <i class="fa fa-solid fa-plus"></i>
                </a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped" id="example1">
                    <thead class="text-center">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Coupon Code</th>
                            <th>Off Type</th>
                            <th>Amount Off</th>
                            <th>Currency</th>
                            {{-- <th>Category</th>
                            <th>Remaining Redemptions</th>
                            <th>Redeem By Date</th>
                            <th>Note</th> --}}
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($coupons as $key => $coupon)
                            @php
                                $discount = 0;
                                $category = '';
                                if ($coupon->discount_type == 'Fixed') {
                                    $discount = round($coupon->discount, 0);
                                } else {
                                    $discount = round($coupon->discount, 0) . ' %';
                                }

                                if ($coupon->category != '') {
                                    if ($coupon->category == 'personalityTest') {
                                        $category = 'Personality Test';
                                    } elseif ($coupon->category == 'careerExpectationsTest') {
                                        $category = 'Career Expectations Test';
                                    } elseif ($coupon->category == 'skillAssessmentTest') {
                                        $category = 'Skill Assessment Test';
                                    } elseif ($coupon->category == 'sessionPayment') {
                                        $category = 'Sessions';
                                    } elseif ($coupon->category == 'testsPayment') {
                                        $category = 'Tests';
                                    }
                                }

                            @endphp
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $coupon->coupon_name }}</td>
                                <td>{{ $coupon->coupon_code }}</td>
                                <td>{{ $coupon->discount_type }}</td>
                                <td>{{ $discount }}</td>
                                {{-- <td>{{ $category }}</td>
                                <td>
                                    @if (!isset($coupon->max_redemptions))
                                        Unlimited
                                    @else
                                        {{ $coupon->max_redemptions }}
                                    @endif
                                </td>
                                <td>
                                    @if (!isset($coupon->redeem_by_date))
                                        Unlimited
                                    @else
                                        {{ $coupon->redeem_by_date }}
                                    @endif
                                </td>

                                <td>{{ $coupon->note == '' ? 'NIL' : $coupon->note }}</td> --}}
                                <td>{{ $coupon->currency_type }}</td>
                                <td class="text-center">
                                    @if ($coupon->expired)
                                        <span class="text-danger">Expired</span>
                                    @else
                                        <input data-id="{{ $coupon->id }}" class="toggle-class" type="checkbox"
                                            data-onstyle="success" data-offstyle="danger" data-toggle="toggle"
                                            data-on="Activated" data-off="Deactivated"
                                            {{ $coupon->status ? 'checked' : '' }}>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Actions">
                                        <a href="{{ route('admin.coupons.show', $coupon->id) }}" class="btn btn-outline-success" type="button"
                                        title="Details" data-bs-toggle="tooltip">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a type="button" class="btn btn-outline-secondary" title="Edit"
                                            data-bs-toggle="tooltip" href="{{ route('admin.coupons.edit', $coupon->id) }}">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button type="submit" form="delete{{ $coupon->id }}"
                                            class="btn btn-outline-danger" title="Delete" data-bs-toggle="tooltip">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                    <!-- delete coupon with id ? -->
                                    <form action="{{ route('admin.coupons.destroy', $coupon->id) }}" method="post" hidden
                                        id="delete{{ $coupon->id }}">
                                        @csrf @method('DELETE')
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {
            $('.toggle-class').change(function() {
                var status = $(this).prop('checked') == true ? 1 : 0;
                var coupon_id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '{{ route('admin.coupons.changeCouponStatus') }}',
                    data: {
                        'status': status,
                        'coupon_id': coupon_id
                    },
                    success: function(data) {
                        console.log(data.msg);
                    }
                });
            })
        })
    </script>
@endpush

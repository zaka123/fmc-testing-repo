@extends('layouts.admin', ['pagename' => 'Coupons'])

@section('css')
    <style>
        input+i {
            top: 41px;
            left: 18px;
        }
    </style>
@endsection

@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Coupon') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow p-4 rounded mb-3">
        <form action="{{ route('admin.coupons.update', $coupon) }}" method="post" class="row g-3">
            @csrf @method('PUT')
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="coupon_name">Coupon Name<span class="text-danger">*</span></label>
                    <input id="coupon_name" name="coupon_name" value="{{ $coupon->coupon_name ?? old('coupon_name') }}"
                        class="form-control @error('coupon_name') is-invalid @enderror" required>
                    <span id="couponNameError" class="text-danger"></span>
                    @error('coupon_name')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="coupon_code">Coupon Code<span class="text-danger">*</span></label>
                    <input id="coupon_code" name="coupon_code" value="{{ $coupon->coupon_code ?? old('coupon_code') }}"
                        class="form-control @error('coupon_code') is-invalid @enderror" required>
                        @error('coupon_code')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="discount_type" class="form-label">Discount Type<span class="text-danger">*</span></label>
                    <select name="discount_type" id="discount_type"
                        class="form-select @error('discount_type') is-invalid @enderror" required>
                        <option value="">Please select a Type</option>
                        <option value="Fixed" @if ($coupon->discount_type == 'Fixed') selected @endif>Fixed</option>
                        <option value="Percent" @if ($coupon->discount_type == 'Percent') selected @endif>Percent</option>
                    </select>
                    <span id="discountTypeError" class="text-danger"></span>
                    @error('discount_type')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="discount" class="form-label" id="discount_label">
                        @if ($coupon->discount_type == 'Percent')
                            Percent Off
                        @elseif ($coupon->discount_type == 'Fixed')
                            Amount Off
                        @endif
                        <span class="text-danger">*</span>
                    </label>
                    <input id="discount" name="discount" type="number" value="{{ $coupon->discount }}"
                        class="form-control  @error('discount') is-invalid @enderror" required>
                    <span id="discountError" class="text-danger"></span>
                    @error('discount')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="currency_type" class="form-label">Currency<span class="text-danger">*</span></label>
                    <select name="currency_type" id="currency_type"
                        class="form-select @error('currency_type') is-invalid @enderror" required>
                        <option value="">Please select a currency</option>
                        @foreach ($currencies as $currency)
                            <option value="{{ $currency->code }}" @if ($currency->code == $coupon->currency_type) selected @endif>{{ $currency->code }}</option>
                        @endforeach
                    </select>
                    <span id="currencyTypeError" class="text-danger"></span>
                    @error('currency_type')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="max_redemptions" class="form-label">Max Redemptions</label>
                    <input name="max_redemptions" type="number" id="max_redemptions"
                        value="{{ $coupon->max_redemptions ?? old('max_redemptions') }}"
                        class="form-control @error('max_redemptions') is-invalid @enderror">
                        @error('max_redemptions')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category" class="form-label">Category<span class="text-danger">*</span></label>
                    <select name="category" id="category" class="form-select @error('category') is-invalid @enderror"
                        required>
                        <option value="">Please select a category</option>
                        <option value="sessionPayment" @if ($coupon->category == 'sessionPayment') selected @endif>Session Payment
                        </option>
                        <option value="testsPayment" @if ($coupon->category != 'sessionPayment') selected @endif>Test Payment</option>
                    </select>
                    @error('category')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div id="test_category_div" class="col-md-6 @if ($coupon->category == 'sessionPayment') d-none @endif ">
                <div class="form-group">
                    <label for="test_category" class="form-label">Test Category</label>
                    <select name="test_category" id="test_category"
                        class="form-select @error('test_category') is-invalid @enderror">
                        <option value="">Please select a Test</option>
                        <option value="personalityTest" @if ($coupon->category == 'personalityTest') selected @endif>Personality Test
                        </option>
                        <option value="careerExpectationsTest" @if ($coupon->category == 'careerExpectationsTest') selected @endif>Career
                            Expectations Test</option>
                        <option value="skillAssessmentTest" @if ($coupon->category == 'skillAssessmentTest') selected @endif>Skill
                            Assessment Test</option>
                    </select>
                    @error('test_category')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="redeem_by_date" class="form-label">Redeem By (Date)</label>
                    <input name="redeem_by_date" id="redeem_by_date" type="date"
                        class="form-control @error('redeem_by_date') is-invalid @enderror"
                        value="{{ $coupon->redeem_by_date ?? old('redeem_by_date') }}">
                    @error('redeem_by_date')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="status" class="form-label">Status<span class="text-danger">*</span></label>
                    <select name="status" id="status" class="form-select @error('status') is-invalid @enderror"
                        required>
                        @if ($coupon->expired)
                            <option value="{{ $coupon->status }}" selected>Expired</option>
                        @else
                            <option value="1" @if ($coupon->status == 1) selected @endif>Active</option>
                            <option value="0" @if ($coupon->status == 0) selected @endif>InAactive</option>
                        @endif
                    </select>
                    @error('status')<span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group position-relative">
                    <label for="note" class="form-label">Note (Optional)</label>
                    <textarea name="note" cols="30" rows="1" class="form-control @error('note') is-invalid @enderror">{!! $coupon->note ?? old('note') !!}</textarea>
                    @error('note')<span class="text-danger">{{ $message }}</span>@enderror
                </div>

            </div>


            <div class="col-12">
                <button class="btn btn-secondary">Update</button>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {

            // Validate coupon_name
            $("#couponNameError").html('');

            $("#coupon_name").keyup(function() {
                validateCouponName();
            });

            function validateCouponName() {
                let couponNameValue = $("#coupon_name").val();
                if (couponNameValue.length == "") {
                    $("#couponNameError").html('Coupon Name is required');
                    return false;
                } else if (couponNameValue.length > 30) {
                    $("#couponNameError").html('The name must have fewer than 30 characters');
                    return false;
                } else {
                    $("#couponNameError").html('');
                }
            }

            // Validate discount
            $("#discountError").html('');
            $("#discountTypeError").html('');

            $("#discount").keyup(function() {
                validateDiscountValue();
            });

            function validateDiscountValue() {
                let discountType = $("#discount_type").val();
                let discountValue = $("#discount").val();

                if (discountType == "") {
                    $("#discountTypeError").html('Please select discount type first.');
                    $("#discount").val('');
                    $("#discount_type").focus();
                    return false;
                } else if (discountType == "Percent") {
                    if (discountValue.length == "") {
                        $("#discountError").html('Discount is required');
                        return false;
                    } else if (discountValue < 1 || discountValue > 100) {
                        $("#discountError").html('The discount value must be from 1% to 100%');
                        $("#discount").val('');
                        return false;
                    } else {
                        $("#discountError").html('');
                    }
                } else if (discountType == "Fixed") {
                    if (discountValue.length == "") {
                        $("#discountError").html('Discount is required');
                        return false;
                    } else if (discountValue < 1 || discountValue > 5000) {
                        $("#discountError").html('The discount value must be from 1 to 5000');
                        $("#discount").val('');
                        return false;
                    } else {
                        $("#discountError").html('');
                    }
                } else {
                    $("#discountError").html('');
                }
            }

            $('#discount_type').change(function() {

                $("#discountError").html('');
                $('#discount').val('');
                var value = $('#discount_type').val();
                if (value == '') {
                    $("#discountTypeError").html('Please select a discount type.');
                    $('#discount_label').html('Discount<span class="text-danger">*</span>');
                } else if (value == 'Fixed') {
                    $('#discount_label').html('Amount Off<span class="text-danger">*</span>');
                    $("#discountTypeError").html('');
                } else if (value == 'Percent') {
                    $('#discount_label').html('Percent Off<span class="text-danger">*</span>');
                    $("#discountTypeError").html('');
                }

            })

            $('#category').change(function() {

                var value = $('#category').val();
                if (value == 'testsPayment') {
                    $('#test_category_div').removeClass('d-none');
                } else if (value == 'sessionPayment') {
                    $('#test_category_div').addClass('d-none');
                    $('#test_category').val('');
                }

            });

        });
    </script>
@endpush

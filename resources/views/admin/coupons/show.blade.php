@extends('layouts.admin', ['pagename' => 'Coupon Details'])
@section('content')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Coupons') }}
        </h2>
    </x-slot>
    <div class="container bg-white shadow px-4 py-4 rounded">
        <div class="row">

            <div class="col-md-12">
                <table class="table">

                    <tbody class="">
                        @php
                            $discount = 0;
                            $category = '';
                            if ($coupon->discount_type == 'Fixed') {
                                $discount = round($coupon->discount, 0);
                            } else {
                                $discount = round($coupon->discount, 0) . ' %';
                            }

                            if ($coupon->category != '') {
                                if ($coupon->category == 'personalityTest') {
                                    $category = 'Personality Test';
                                } elseif ($coupon->category == 'careerExpectationsTest') {
                                    $category = 'Career Expectations Test';
                                } elseif ($coupon->category == 'skillAssessmentTest') {
                                    $category = 'Skill Assessment Test';
                                } elseif ($coupon->category == 'sessionPayment') {
                                    $category = 'Sessions';
                                } elseif ($coupon->category == 'testsPayment') {
                                    $category = 'Tests';
                                }
                            }

                        @endphp
                        <tr>
                            <th>Coupon ID :</th>
                            <td>{{ $coupon->id }}</td>
                            <th>Title :</th>
                            <td>{{ $coupon->coupon_name }}</td>
                        </tr>
                        <tr>
                            <th>Coupon Code :</th>
                            <td>{{ $coupon->coupon_code }}</td>
                                <th>Type :</th>
                                <td>{{ $coupon->discount_type }}</td>
                        </tr>
                        <tr>
                            <th>Amount :</th>
                            <td>{{ $discount }}</td>
                            <th>Currency :</th>
                            <td>{{ $coupon->currency_type }}</td>
                        </tr>
                        <tr>
                            <th>Category :</th>
                            <td>{{ $category }}</td>
                            <th>Remaining Redemptions :</th>
                            <td>
                                @if (!isset($coupon->max_redemptions))
                                    Unlimited
                                @else
                                    {{ $coupon->max_redemptions }}
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Redeem By Date :</th>
                            <td>
                                @if (!isset($coupon->redeem_by_date))
                                    Unlimited
                                @else
                                    {{ $coupon->redeem_by_date }}
                                @endif
                            </td>
                            <th>Status :</th>
                            <td>
                                @if ($coupon->expired)
                                    <span class="">Expired</span>
                                @elseif ($coupon->status)
                                    <span class="">Activated</span>
                                @else
                                    <span class="">Deactivated</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Note :</th>
                            <td>{{ $coupon->note == '' ? 'NIL' : $coupon->note }}</td>
                            <th></th><td></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


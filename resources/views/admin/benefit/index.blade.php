@extends('layouts.admin',['pagename'=>'Benefits'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Benefits') }}
    </h2>
</x-slot>
<div class="container bg-white shadow px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12 justify-content-end">
            <a href="{{route('admin.benefits.create')}}" class="btn btn-outline-primary" title="add Benefit">
                <i class="fa fa-solid fa-plus"></i>
            </a>
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Title</td>
                        <td>Description</td>
                        <td>Favoured</td>
                        <td>Dis Favoured</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($benefits as $key=>$benefit)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{ $benefit->title ?? '' }}</td>
                        <td>{!! $benefit->description ?? '' !!}</td>
                        <td>{!! $benefit->favoured_careers ?? '' !!}</td>
                        <td>{!! $benefit->disFavoured_careers ?? '' !!}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Actions">
                                <a type="button" class="btn btn-outline-secondary" href="{{route('admin.benefits.edit',$benefit->id)}}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <button type="submit" form="delete{{ $benefit->id }}" class="btn btn-outline-danger">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <!-- delete user with id ? -->
                            <form action="{{ route('admin.benefits.destroy',$benefit->id) }}" method="post" hidden id="delete{{ $benefit->id }}">
                                @csrf @method('DELETE')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
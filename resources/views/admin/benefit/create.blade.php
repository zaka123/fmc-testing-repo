@extends('layouts.admin',['pagename'=>'Benefit'])
@section('content')
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Benefit Create') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="bg-white px-4 py-4 rounded">
        <div class="row justify-content-center">
            <form action="{{route('admin.benefits.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <div class="row mb-4">
                        <div class="col-md-2">
                            <label for="title" class="form-label">Title</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}">
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-2">
                            <label for="description">Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="description" cols="30" rows="10" class="summernote @error('description') is-invalid @enderror">{!!old('description') !!}</textarea>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-2">
                            <label for="favoured_careers">Favoured Careers</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="favoured_careers" cols="30" rows="10" class="summernote @error('favoured_careers') is-invalid @enderror">{!!old('favoured_careers') !!}</textarea>
                        </div>
                    </div>
                    <div class="row  mb-4">
                        <div class="col-md-2">
                            <label for="disFavoured_career">Dis Favoured Careers</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="disFavoured_career" cols="30" rows="10" class="summernote @error('disFavoured_career') is-invalid @enderror">{!!old('disFavoured_career') !!}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 offset-6">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
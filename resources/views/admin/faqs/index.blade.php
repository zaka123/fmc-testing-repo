@extends('layouts.admin',['pagename'=>'Faqs'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12 justify-content-end">
            <button type="button" class="bg-primary btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-solid fa-plus"></i>
            </button>
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="example1">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Question</td>
                        <td>Answer</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($faqs as $faq)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $faq->question }}</td>
                            <td>{!! $faq->answer !!}</td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('admin.faqs.edit', $faq->id) }}" class="btn btn-outline-secondary" type="button" title="confirm">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    <button class="btn btn-outline-danger" form="delete{{ $faq->id }}" type="submit" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <form action="{{ route('admin.faqs.destroy',$faq->id) }}" method="post" hidden id="delete{{ $faq->id }}">
                                    @csrf @method('DELETE')
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('modal')
<form action="{{ route('admin.faqs.store') }}" method="post" style="overflow-y: scroll;">
    @csrf
    <div class="modal-body text-sm">
        <div class="form-group">
            <div class="row g-3">
                <div class="col-md-10 offset-md-1">
                    <label for="question" class="form-label">Question</label>
                    <textarea class="form-control @error('question') is-invalid @enderror" name="question" id="question" cols="30">{{ old('question') }}</textarea>
                </div>
                <div class="col-md-10 offset-md-1">
                    <label for="answer">Answer</label>
                    <textarea id="answer" name="answer" class="form-control summernote @error('answer') is-invalid @enderror" rows="10">{{ old('answer') }}</textarea>
                </div>
                <div class="col-12 text-center">
                    <button type="button" class="bg-secondary btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="bg-primary btn btn-primary">Add</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@include('extra.modal',['modal_title'=>'Add Faq'])
@endsection

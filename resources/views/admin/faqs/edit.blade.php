@extends('layouts.admin',['pagename'=>'Faqs'])
@section('content')
<div class="container shadow mb-3 bg-white px-4 py-4 rounded">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route("admin.faqs.update", $faq->id) }}" method="post">
                @csrf
                @method("put")
                <div class="form-group">
                    <label for="question">Question</label>
                    <textarea name="question" class="form-control summernote @error('question') is-invalid @enderror" id="question" cols="30" rows="10">{{ old("question") ?? $faq->question }}</textarea>
                </div>
                <div class="form-group">
                    <label for="answer">Answer</label>
                    <textarea name="answer" class="form-control summernote @error('answer') is-invalid @enderror" id="answer" cols="30" rows="10">{{ old("answer") ?? $faq->answer }}</textarea>
                </div>
                <div class="form-group">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary">Cancel</a>
                    <input type="submit" value="Update" class="btn btn-sm btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

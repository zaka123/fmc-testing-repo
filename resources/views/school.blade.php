@extends('layouts.navbar', ['title' => "School/College Student - "])
@section('css')
<style>
body {
    background-color: rgba(108, 187, 222, 20%) !important;
    background-image: url('/assets/icons/index.svg');
    background-size: cover;
}
.changerLook:hover{
    background-color:black!important;
}
</style>
@endsection
@section('content')
<div class="container pt-lg-5 mb-3 mb-lg-0" style="min-height:86vh">
    <div class="row align-items-center gx-3">
        <div class="col-lg-7">
            <h3 class="text-special text-uppercase">
                explore your future education and career pathways with our career experts!
            </h3>
            <p class="fw-light text-justify">
                The early years of high school are the best time to start thinking about your career
                and future aspirations. This will help you to identify the suitable course and
                institutes aligned with your abilities and interest. FIND ME CAREER help you to make
                an informed decision about your career and future education studies. Our experts
                make a personalized plan with set targets and provide relevant advice to counsel
                and motivate you.
            </p>
            <p>
                <a href="{{url('register','Student')}}" class="d-lg-none btn btn-sm bg-special rounded-3 changerLook">
                    Let's get Started
                </a>
                <a href="{{url('register','Student')}}" class="d-none d-lg-inline-block btn bg-special rounded-3 changerLook">
                    Let's get Started
                </a>
            </p>
        </div>
        <div class="col-lg-5">
            <img src="{{asset('assets/img/6461.png')}}" class="img-fluid" alt="job interviewer">
        </div>
    </div>
</div>
@include('newsletter_form')
@endsection

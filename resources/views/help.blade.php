{{-- @extends('layouts.navbar', ['title' => 'Help - ']) --}}
@extends('layouts.new_app', ['title' => 'Help - '])
@section('content')
    <style>
        .changerLook:hover {
            background-color: black !important;
        }

        form {
            border: 0!important;
            box-shadow: none!important;
        }

        .container::-webkit-scrollbar {
            width: 5px;
            height: 8px;
            background-color: #F3F5F7;
        }

        .container::-webkit-scrollbar-thumb {
            background-color: #749BC2;
            border-radius: 100px !important;
        }

        @media(max-width:768px) {
            .container {
                margin-top: 40px !important;
            }
        }

        nav {
            position: fixed;
            z-index: +99;
            width: 92%;
        }
    </style>
    <div class="container pb-5 mt-5"
        style="height: 80vh!important;overflow:auto; scrollbar-width: thin;
scrollbar-color: #749BC2 transparent;">
        <form class="row g-3  w-100" action="{{ route('help.store') }}" method="post">
            @csrf
            {{-- <div class="col-md-6 text-end">
                <img src="{{ asset('assets/img/experts.png') }}" class="w-50" alt="experts">
            </div> --}}
            <div class="col-md-7">
                <div class="fs-3">Let our Expert Teammates know your problem</div>
                <div class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla id consequuntur
                    excepturi quasi provident delectus consectetur neque, aspernatur qui, alias, dicta modi deserunt, autem
                    iure eligendi praesentium? Facere, dolorem incidunt.</div>
            </div>
            <div class="col-md-7">
                <label for="email" class="form-label">1) Email address</label>
                <input type="text" name="email" value="{{ auth()->user()->email ?? '' }}" readonly
                    class="form-control">
            </div>
            <div class="col-md-7">
                <label for="phone" class="form-label">2) Phone</label>
                <input type="text" name="phone" value="{{ auth()->user()->phone ?? '' }}" readonly
                    class="form-control">
            </div>
            <div class="col-md-7">
                <label for="question" class="form-label">3) Type your question below<span
                        class="text-danger">*</span></label>
                <textarea name="question" id="question" class="form-control" rows="3" cols="30"
                    placeholder="Type your question here" required></textarea>
                @error('question')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-7">
                <label for="info" class="form-label">4) How did this problem occured?<span
                        class="text-danger">*</span></label>
                <textarea name="info" id="info" class="form-control" rows="3" cols="30"
                    placeholder="Please type your explanation here" required></textarea>
                @error('info')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-7">
                <label for="date" class="form-label">5) How long have you been facing this problem?<span
                        class="text-danger">*</span></label>
                <input type="date" name="date" id="date" required class="form-control">
                @error('date')
                    <div class="text-danger">{{ $message }}</div>
                @enderror

            </div>
            <div class="col-md-7">
                <label for="category" class="form-label">6) Which category best suites your problem?<span
                        class="text-danger">*</span></label>
                <select name="category" id="category" class="form-select" required>
                    <option>Payment Problems</option>
                    <option>Session Problems</option>
                    <option>Video Call Problems</option>
                    <option>Download Reports Problems</option>
                    <option>Test Results Problems</option>
                    <option>Not Applicable</option>
                </select>
                @error('category')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-7">
                <div class="form-label">7) Did <a href="{{ url('faqs') }}" class="text-decoration-none">FAQs</a> helped
                    you?<span class="text-danger">*</span></div>
                <label class="form-label">
                    <input type="radio" value="Yes" name="faqs" class="form-check-input" required> Yes
                </label>
                <label class="ms-3 form-label">
                    <input type="radio" value="No" name="faqs" class="form-check-input" required> No
                </label>
                @error('faqs')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-sm text-white changerLook" style="background-color:#749BC2">Submit
                    Request</button>
            </div>
        </form>
    </div>
@endsection

<style>
    .blackChanger:hover{
        background-color:black!important;
    }
</style>
<div class="modal fade" id="changePictureModal" tabindex="-1" role="dialog" aria-labelledby="changePictureModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-cropImage" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePictureModalLabel">Crop Image Before Upload</h5>
                {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <form id="changePictureForm" action="{{ route('counsellor.profile.picture.store') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-8 mainParent">
                                <img id="image" class="cropImage" src="https://avatars0.githubusercontent.com/u/3456749">
                                <input type="hidden" name="image_base64">
                            </div>
                            <div class="col-md-4">
                                <div class="previewCropImage"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{ url('dashboard') }}" class="btn btn-secondary" data-bs-dismiss="changePictureModal">Cancel</a>
                {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-primary" id="crop">Upload</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-2 offcanvas-start offcanvas-lg" id="sidebar"
    style="--bs-offcanvas-width:auto;box-shadow: 1px 0px 0px 1px rgba(0, 0, 0, 0.15);">
    <div class="sticky-top side-bar overflow-auto vh-100">
        <div class="px-3">
            <div class="my-3">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/img/logo.png') }}" class="img-fluid" alt="logo">
                </a>
            </div>
            <div class="pe-3 ps-2 pb-2">
                <div class="mt-5 mb-2 text-center position-relative">
                    <img src="{{ asset(auth()->user()->profile_photo) }}" alt="profile pic"
                        class="shadow-sm rounded-circle" width="150" height="150">
                    {{-- <span class="position-absolute bottom-0 rounded-circle bg-white shadow translate-middle p-1"
                        style="left: 133px;cursor:pointer" data-bs-toggle="modal"
                        data-bs-target="#changeProfilePicture">
                        <img src="{{ asset('assets/icons/camera.svg') }}" alt="camera">
                    </span> --}}
                    <span class="position-absolute bottom-0 rounded-circle bg-white shadow translate-middle p-1"
                        style="left: 133px; cursor: pointer">
                        <label for="imageInput" style="cursor: pointer">
                            <img src="{{ asset('assets/icons/camera.svg') }}" alt="camera">
                        </label>
                        <input type="file" id="imageInput" name="image" class="image" style="display: none" accept="image/*">

                    </span>

                    {{-- <a class="position-absolute bottom-0 rounded-circle bg-white shadow translate-middle p-1"
                        style="left: 133px;cursor:pointer" href="{{ route('counsellor.profile.change.picture') }}">
                        <img src="{{ asset('assets/icons/camera.svg') }}" alt="camera">
                    </a> --}}
                </div>
                <div class="text-center">
                    <span class="fw-bold me-1">
                        {{ auth()->user()->fullname }}
                    </span>
                    <br>
                    <a href="{{ url('counsellor/edit/profile') }}" class="btn btn-sm bg-special text-white blackChanger">
                        Edit Profile
                    </a>
                    <hr class="">
                </div>
                <div class="text-capitalize">
                    <a href="{{ url('dashboard') }}" class="text-decoration-none dashboard text-dark fw-bold">
                        <i class="fa-solid fa-home me-1"></i>
                        Home
                    </a>
                </div>
                <hr>
                <div class="text-capitalize">
                    <a href="{{ url('counsellor/profile') }}" class="text-decoration-none profile text-dark fw-bold">
                        <i class="fa-solid fa-user me-1"></i>
                        Profile
                    </a>
                </div>
                <hr>
                <div class="text-capitalize">
                    <a href="#" class="fw-bold text-decoration-none text-dark reports">
                        <i class="fa-solid fa-file-lines me-2"></i>
                        Reports
                    </a>
                    <div class="ms-md-4">
                        <a class="text-muted text-decoration-none session-report"
                            href="{{ route('counsellor.session.report.index') }}">Session&nbsp;Report</a>
                    </div>
                </div>
                <hr>
                <div class="text-capitalize">
                    <a href="#" class="fw-bold text-decoration-none text-dark sessions">
                        <i class="fa-solid fa-file-lines me-2"></i>
                        Sessions
                    </a>
                    <div class="ms-md-4">
                        <a class="text-muted text-decoration-none time-management"
                            href="{{ url('counsellor/sessions') }}">Time&nbsp;management</a>
                        <br>
                        <a class="text-muted text-decoration-none session-history"
                            href="{{ route('counsellor.session.history') }}">Session&nbsp;History</a>
                        <br>
                        <a class="text-muted text-decoration-none bookings"
                            href="{{ route('counsellor.session.bookings') }}">Bookings</a>
                    </div>
                </div>
                <hr>
                <div class="text-capitalize">
                    <a class="text-decoration-none text-dark fw-bold help" href="{{ route('help') }}">
                        <i class="fa-regular fa-circle-question me-2"></i>Help
                    </a>
                </div>
                <hr>
                <div class="text-capitalize">
                    <a class="text-decoration-none text-dark fw-bold faqs" href="{{ route('faqs') }}">
                        <i class="fa-regular fa-circle-question text-dark me-2"></i>FAQ's
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light mb-5 pt-3 sticky-top">
    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{asset('assets/img/logo.png')}}" class="fluid-img" alt="career-counselling brand">
        </a>
        <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse pt-3 pt-lg-0" id="navbarNav">
            <ul class="navbar-nav ms-auto nav-pills">
                <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a class="nav-link" href="{{ route('about') }}">About us</a>
                </li>
                {{-- <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a class="nav-link" href="{{ route('faqs') }}">FAQ's</a>
                </li> --}}
                <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a class="nav-link" href="{{ url('contact') }}">Contact us</a>
                </li>
                @auth
                <li class="nav-item dropdown mb-2 mb-lg-0  me-0 me-lg-3">
                    <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="{{url('dashboard')}}"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}" type="submit" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa-solid fa-right-from-bracket me-2"></i>{{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" hidden>@csrf</form>
                        </li>
                    </ul>
                </li>
                @endauth
                @guest
                {{-- <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a href="{{url('counsellor-apply')}}" class="nav-link text-bg-dark">Become Coach</a>
                </li> --}}
                @if (Route::has('login'))
                <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                    <a href="{{route('login')}}" class="nav-link text-bg-dark">Sign in</a>
                </li>
                @endif
                @endguest
            </ul>
        </div>
    </div>
</nav>

@php
    $zoomAccount = App\Models\ZoomToken::all();
@endphp
<style>
    .blackChanger:hover {
        background-color: black !important;
        color: white !important;
    }
</style>

<nav class="pt-3">
    @if($zoomAccount->isEmpty() && auth()->user()->email == 'noreply@findmecareer.com')
        <a href="{{ route('zoom.account.connect') }}" class="btn btn-sm btn-primary" data-bs-toggle="tooltip"
            data-bs-title="Connect zoom account to attend sessions.">
            Connect Zoom
        </a>
    @endif
    <a href="{{ url('help') }}" class="rounded border text-decoration-none px-2 py-1 text-dark blackChanger"><i
            class="fa-regular fa-circle-question me-1"></i>Help</a>
    <a class="rounded border text-decoration-none px-2 py-1 text-dark blackChanger" style="cursor: pointer;"
        onclick="$('#logout').submit();"><i class="fa-solid fa-right-from-bracket me-2"></i>{{ __('Logout') }}</a>
    <form action="{{ route('logout') }}" id="logout" method="POST">@csrf
    </form>
</nav>

{{-- <style>
    .changerBack:hover {
        background-color: black !important;
        color: white !important;
    }

</style> --}}


<nav class="pt-3 d-flex" style="gap: 10px">
    {{-- <div class="dropdown w-100">
        <button class="btn  border dropdown-toggle py-2 changerBack w-100" type="button" id="dropdownMenu2" data-bs-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            {{ auth()->user()->name }}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <a href="{{ url('user/payments') }}" class="rounded dropdown-item  changerBack text-decoration-none px-2 py-1 text-dark"
                style="cursor: pointer;"><i
                    class="fa fa-credit-card me-2"></i>
                    {{ __('Payments') }}
            </a>
            <a class="rounded dropdown-item  changerBack text-decoration-none px-2 py-1 text-dark"
                style="cursor: pointer;" onclick="$('#logout').submit();"><i
                    class="fa-solid fa-right-from-bracket me-2"></i>
                    {{ __('Logout') }}
            </a>
            <form action="{{ route('logout') }}" id="logout" method="POST">@csrf
            </form>
        </div>
    </div> --}}
    <div class="dropdown ms-auto">
        <button class="btn  border-0 rounded-circle w-100 d-flex flex-column align-items-center" type="button" id="dropdownMenu2" data-bs-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            {{-- {{ auth()->user()->name }} --}}
            <img src="{{ asset(auth()->user()->profile_photo) }}" class="rounded-circle" alt="" style="width: 35px">
            <small>Me<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
              </svg></small>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <a href="{{ url('user/payments') }}" class="rounded dropdown-item  changerBack text-decoration-none px-2 py-1 text-dark"
                style="cursor: pointer;"><i
                    class="fa fa-credit-card me-2"></i>
                    {{ __('Payments') }}
            </a>
            <a class="rounded dropdown-item  changerBack text-decoration-none px-2 py-1 text-dark"
                style="cursor: pointer;" onclick="$('#logout').submit();"><i
                    class="fa-solid fa-right-from-bracket me-2"></i>
                    {{ __('Logout') }}
            </a>
            <form action="{{ route('logout') }}" id="logout" method="POST">@csrf
            </form>
        </div>
    </div>
</nav>

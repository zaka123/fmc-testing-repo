@extends('layouts.navbar',  ['title' => "Get Started"])
@section('css')
<style>
    html, body {
        min-height: 100% !important;
    }
    body {
        background-image: url('/assets/icons/index.svg')!important;
        background-size: cover;
        background-color: rgba(108, 187, 222, 0.3);
        display: flex !important;
        flex-direction: column !important;
    }

    body > nav:first-child {
        margin-bottom: 0 !important;
    }

    nav + div {
        background-color: transparent !important
    }

</style>
@endsection
@section('content')

@include('extra.whoAreYou')

@endsection

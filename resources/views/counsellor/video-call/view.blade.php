@extends('layouts.counsellor',['pagename'=>'Counsellor'])
@section('counsellor-content')
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>                    
                    </button>
                    <div class="pb-3 ms-auto">
                        @include('navbars.counsellor')
                    </div>
                </div>
            <div class="main" style="min-height:83.2vh">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="floating"></div>
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="inner py-5">
                                    <div class="row">
                                        <div class="col-md-10 offset-md-1">
                                            <div class="card shadow" style="--bs-card-border-color:var(--special)">
                                                <h5 class="card-header text-center text-white bg-special">
                                                    Your Meeting is ready
                                                </h5>
                                                <div class="card-body">
                                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                </div>
                                                <div class="card-footer text-center bg-special">
                                                    <a href="{{ $videoCall->starting_link }}" target="_blank" class="btn btn-dark btn-sm">
                                                        Start Now
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection

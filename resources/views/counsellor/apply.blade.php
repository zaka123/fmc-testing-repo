@extends('layouts.new_app', ['pagename' => 'Apply'])
@section('css')
    <style>
        .row {
            width: 100%;
            margin-top: 90px !important;
            padding-left: 50px;
        }

        @media(max-width:768px) {
            .row {
                flex-direction: column-reverse;
                margin-top: 20px !important;
                padding-left: 0px !important;
            }
        }

        .apply_text h1 {
            font-family: myFont;
            margin-bottom: 10px;
        }

        .text-special {
            color: #749BC2 !important;
        }

        .apply_text p {
            text-align: left;
            font-size: 17px;
            font-weight: 400;
        }

        @media(max-width:768px) {
            .apply_text p {
                text-align: justify;
            }
        }

        .apply_text a {
            border: 0;
            padding: 5px 35px;
            color: white;
            font-size: 14px;
            font-weight: 500;
            cursor: pointer;
            text-decoration: none;
            display: inline-block;
            background-color: #749BC2;
        }

        .apply_text a:hover {
            background-color: #000000;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-6 apply_text">
            <h1 class="text-special"><b>Become a Career Coach</b></h1>
            <p>Are you an educator,skilled profisional or a career counsellor with the same vision and interested in joining
                our mission?
                We're looking for experienced career coaches and mentors from different profisions who share our vision of
                providing a world-class coaching and mentoring to young generation for their better future. we offer a
                comprehensive package,benifit and above all an amazing team worth being a part of.</p>
            <a href="{{ route('coach.apply') }}">Apply</a>
        </div>
        <div class="col-12 col-md-6 " style="height: 400px">
            <div style="width: 100%;height:100%">
                <img src="{{ asset('new_design_assets/img/Career.svg') }}" alt="" width="100%" height="100%"
                    style="max-width: 100%;object-fit:contain">
            </div>
        </div>
    </div>
@endsection

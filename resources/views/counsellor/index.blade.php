@extends('layouts.counsellor', ['pagename' => 'Coach'])
@section('css')
    <style>
        .dashboard {
            color: var(--special) !important;
        }

        .shadow-other {
            box-shadow: 0px 0px 3px grey;
        }
        .blackkChanger:hover{
            color: black!important;
        }
        @media(min-width:1700px){
    .onTheFloor{
        position: fixed!important;
        bottom: 0;
        width:100%!important;
    }
   }
    </style>
@endsection
@section('counsellor-content')
    @php
        $timezones = App\Models\Timezone::all();
    @endphp
    <!-- timezone update modal -->
    <div class="modal fade" id="changeTimezoneModal" tabindex="-1" role="dialog" aria-labelledby="changeTimezoneModalTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-black" id="changeTimezoneModalTitle">Change Timezone</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('user.timezone.update', auth()->id()) }}" class="needs-validation"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body border-0 ">
                        <select class="form-select select2bs4" name="timezone" id="timezone" required>
                            <option value="">Select timezone</option>
                            @foreach ($timezones as $timezone)
                                <option value="{{ $timezone->timezone }}" @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                    {{ $timezone->timezone }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div class="row g-0">

            <!-- side bar -->
            @include('side-bar.counsellor')

            <div class="col-lg-10 main">
                <div class="row g-0">
                    <div class="col-12 bg-white sticky-top d-flex px-3">
                        <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebar">
                            <i class="fa-solid fa-bars fs-4"></i>
                        </button>
                        <div class="pb-3 ms-auto">
                            @include('navbars.counsellor')
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="p-3 pb-5" style='min-height: 83.2vh'>
                            <div class="floating"></div>
                            <div class="row g-3">
                                <div class="col-lg-4">
                                    <div class="card border-0 h-100 shadow" style="background-color: #70707014;">
                                        <h4 class="card-header text-bg-dark border-0">Personal Information</h4>
                                        <div class="card-body">
                                            <div class="d-flex text-truncate">
                                                <div class="fw-bold">
                                                    <div class="mb-2">Name:</div>
                                                    <div class="mb-2">Phone:</div>
                                                    <div class="mb-2">Email:</div>
                                                    <div class="mb-2">Linkedin:</div>
                                                    <div class="mb-2">Country:</div>
                                                    <div class="mb-2">Timezone:</div>
                                                    <div class="mb-2">Specialized in:</div>
                                                </div>
                                                <div class="ps-1">
                                                    <div class="mb-2">
                                                        {{ $counsellor->full_name }}
                                                    </div>
                                                    <div class="mb-2">
                                                        {{ $counsellorProfile->i_phone_number ?? '' }}
                                                    </div>
                                                    <div class="mb-2" data-bs-toggle="tooltip"
                                                        title="{{ $counsellor->email }}">
                                                        {{ Str::limit($counsellor->email, 21, '...') }}
                                                    </div>
                                                    <div class="mb-2">
                                                        <a href="{{ $counsellorProfile->linked_in ?? '#' }}" target="_blank"
                                                            class="text-decoration-none text-primary" title=""click
                                                            here to view linkedin profile>
                                                            View Profile
                                                        </a>
                                                    </div>
                                                    <div class="mb-2">
                                                        {{ $counsellorProfile->country ?? '' }}
                                                    </div>
                                                    <div class="mb-2 text-capitalize">
                                                        {{ $counsellor->timezone ?? 'N/A' }}
                                                        <a href="#" title="Change Timezone" class="text-info fs-6"
                                                            data-bs-toggle="modal" data-bs-target="#changeTimezoneModal">
                                                            <small>
                                                                <i class="fa-regular fa-pen-to-square" style="color: #729CC4"></i>
                                                            </small>
                                                        </a>
                                                    </div>
                                                    <div class="mb-2 text-capitalize">
                                                        {{ $counsellorProfile->interested_in ?? '' }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card border-0 h-100 shadow" style="background-color: #70707014;">
                                        <h4 class="card-header border-0 text-white bg-special">Latest Users</h4>
                                        <div class="card-body p-2">
                                            <div class="row g-2 row-cols-1 row-cols-lg-2 row-cols-lg-3">
                                                @forelse($sessions as $session)
                                                    @if ($session->user->id != null)
                                                    <div class="col">
                                                        <div class="card h-100 bg-light">
                                                            <h6 class="card-header border-0 bg-special text-white">
                                                                {{ $session->user->profession }}</h6>
                                                            <div class="card-body text-center p-1">
                                                                <div class="mb-2">
                                                                    <img src="{{ asset($session->user->profile_photo) }}"
                                                                        class="rounded-circle border"
                                                                        alt="{{ $session->user->full_name ?? 'N/A' }} picture"
                                                                        width="100" height="100">
                                                                </div>
                                                                <a href="{{ route('counsellor.user.show', $session->user->id) }}"
                                                                    class="text-decoration-none text-truncate fw-bold text-dark">
                                                                    {{ $session->user->name ?? 'N/A' }}
                                                                </a>
                                                                <div class="card-text">
                                                                    {{ $session->user->gender }}
                                                                    {!! $session->user->gender == 'Male'
                                                                        ? '<i class="fa-solid fa-person"></i>'
                                                                        : ($session->user->gender == 'Female'
                                                                            ? '<i class="fa-solid fa-person-dress"></i>'
                                                                            : '') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @empty
                                                    <div class="col p-4">
                                                        <i class="fa-solid fa-arrow-rotate-left"></i> No users Found
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <div class="card border-0 shadow latest-sessions position-relative"
                                        style="background-color: #70707014;">
                                        <h4 class="card-header text-bg-dark">Latest Sessions</h4>
                                        <div class="card-body py-2">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" class="text-center">#</th>
                                                        <th scope="col" class="text-center">Date</th>
                                                        <th scope="col" class="text-center">User</th>
                                                        <th scope="col" class="text-center">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-group-divider">
                                                    @forelse($latestSessions as $key => $latestSession)
                                                        <tr>
                                                            <th class="text-center" scope="row">{{ ++$key }}
                                                            </th>
                                                            <td class="text-center">
                                                                {{ date('m-d-Y', strtotime($latestSession->session->session_timing)) }}
                                                            </td>
                                                            <td class="text-center">{{ $latestSession->user->fullname }}
                                                            </td>
                                                            <td class="text-center">{!! $latestSession->session_status !!}</td>
                                                        </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="4" align="center">
                                                                You don't have any session yet.
                                                            </td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                        <a href="{{ route('counsellor.session.history') }}"
                                            class="btn card-link btn-sm shadow-other bg-special text-white px-3 position-absolute blackkChanger"
                                            style="top:100%;left: 50%;transform: translate(-50%, -50%);">View All ></a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card overflow-auto upcoming-sessions border-0 shadow"
                                        style="background-color: #70707014;">
                                        <h4 class="card-header sticky-top text-white bg-special">Upcoming Sessions</h4>
                                        <div class="card-body">
                                            @forelse($upcomingSessions as $coming)
                                                <div class="d-flex justify-content-start mb-3">
                                                    <div class="px-3"><i class="fa-solid fa-circle-notch"></i></div>
                                                    <div>
                                                        One meeting a head,<br>
                                                        <span class="text-truncate">User name:
                                                            {{ $coming->user->fullname }}</span><br>
                                                        Date:
                                                        {{ date('m-d-Y', strtotime($coming->session->session_timing)) }}
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="d-flex justify-content-start mb-3">
                                                    <div class="px-3"><i class="fa-solid fa-circle-notch"></i></div>
                                                    <div>
                                                        You don't have any upcoming session yet.<br>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="text-bg-dark px-4 py-3 onTheFloor">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                                            class="d-inline d-md-none"><br></span>All rights reserved</div>
                                    <div class="col-md-6 text-center text-md-end">Powered by <a href="https://www.theeducatics.com/"
                                            class="text-decoration-none lookBack text-white">The Educatics</a> | Made with <i
                                            class="fa-solid fa-heart text-danger"></i> by <a href="https://lastwavetechnology.com"
                                            class="text-decoration-none lookBack text-white" target="_blank">LWT</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('style/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                dropdownParent: $("#changeTimezoneModal")
            });
        });
    </script>
@endpush

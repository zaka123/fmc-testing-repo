@extends('layouts.counsellor',['pagename'=>'User Profile'])

@section('css')
<style>
    .progress {
        --bs-progress-height: 1.5rem;
        --bs-progress-bg: #fff;
    }
</style>
@endsection

@section('counsellor-content')
<div class="container-fluid">
    <div class="row g-0">
            @include('side-bar.counsellor')
        <div class="col-lg-10 main">
            <div class="row g-0">
                <div class="col-12 bg-white sticky-top d-flex px-3">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <div class="pb-3 ms-auto">
                        @include('navbars.counsellor')
                    </div>
                </div>
                <div class="col-12">
                    <div class="p-3 pb-5">
                        <div class="floating"></div>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <div class="card border-0 h-100 shadow-other" style="background-color: #70707014;">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="text-center">
                                                <img class="rounded-circle border" src="{{asset($user->profile_photo)}}" alt="User profile picture" width="150" height="150">
                                                <h4>{{$user->fullName ?? 'N/A' }}</h4>
                                                {{-- <div class="text-center">({{ $user->email }})</div> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{-- <div class="col-md-6">@include('counsellor.users.personal-info.family-info')</div>
                            <div class="col-md-6">@include('counsellor.users.personal-info.future-self-reflection')</div>
                            <div class="col-md-6">@include('counsellor.users.personal-info.career-ambitions')</div>
                            <div class="col-md-6">@include('counsellor.users.personal-info.study-habits-and-routine')</div> --}}



                            <!-- Personality Test Results for all users -->
                            <div class="col-md-9">
                                @include('counsellor.users.results.personality')
                            </div>

                            {{-- @if($user->profession=="Professional") --}}
                            <!-- Only for professionals -->
                            <div class="col-12">
                                @php
				                    $careerExpectationsTestResults = $user->careerExpectationsTestResults();
				                    $skillAssessmentTestResults = $user->skillAssessmentTestResults();
				                @endphp
                                @include('counsellor.users.results.career-ambitions')
                            </div>
                            <div class="col-12">
                                @include('counsellor.users.results.skill-assessment')
                            </div>
                            {{-- @endif --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

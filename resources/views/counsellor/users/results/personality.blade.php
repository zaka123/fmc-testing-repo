<div class="card border-0 h-100 shadow-other" style="background-color: #70707014;">
    <h4 class="card-header text-center bg-special text-white py-3">PERSONALITY TEST
        RESULTS</h4>
    <div class="card-body">
        @if($personalityTestResults > 99)
        @php
            $wordCombination='';
            $personalityTestResults['extroversion'] > 20 ? $wordCombination = "S" : $wordCombination = "R";
            $personalityTestResults['neuroticism'] > 20 ? $wordCombination .= "L" : $wordCombination .= "C";
            $personalityTestResults['conscientiousness'] > 20 ? $wordCombination .= "O" : $wordCombination .= "U";
            $personalityTestResults['agreeableness'] > 20 ? $wordCombination .= "A" : $wordCombination .= "E";
            $personalityTestResults['opennessToExperience'] > 20 ? $wordCombination .= "I" : $wordCombination .= "N";
        @endphp
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Extroversion</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar" role="progressbar" style="background-color:#80610C;width: {{ $personalityTestResults['extroversion'] ?? '0' }}%;">
                        {{ round($personalityTestResults['extroversion'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Neuroticism</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $personalityTestResults['neuroticism'] ?? '0' }}%;">
                        {{ round($personalityTestResults['neuroticism'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Conscientiousness</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ $personalityTestResults['conscientiousness'] ?? '0' }}%;">
                        {{ round($personalityTestResults['conscientiousness'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Agreeableness</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ $personalityTestResults['agreeableness'] ?? '0' }}%;">
                        {{ round($personalityTestResults['agreeableness'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Openness To Experience</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $personalityTestResults['opennessToExperience'] ?? '0' }}%;">
                        {{ round($personalityTestResults['opennessToExperience'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <section>
            <hgroup>
                <h5>Description</h5>
                <p id="description"></p>
            </hgroup>
        </section>
        <section>
            <hgroup>
                <h5>Career I should Choose</h5>
                <p id="favoured_careers"></p>
            </hgroup>
        </section>
        <section>
            <hgroup>
                <h5>Career I shouldn't Choose</h5>
                <p id="disFavoured_careers"></p>
            </hgroup>
        </section>
        @elseif($personalityTestResults > 0 && $personalityTestResults < 100)
            Not completed yet!
        @else
            Not attempted yet!
        @endif
    </div>
</div>

@if($user->getTestResults(4) > 99)
@push('script')
<script>
    $(document).ready(() => {
        console.log('document loaded')
        var url = "{{url('counsellor/benefit')}}" + "/{{$wordCombination}}";
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                console.log(data)
                $('#description').text(data.description);
                $('#favoured_careers').text(data.favoured_careers);
                $('#disFavoured_careers').text(data.disFavoured_careers);
            }, //end of success
            error: function(data) {
                $(window).scrollTop(0);
            }
        });
    });
</script>
@endpush
@endif

<div class="card border-0 h-100 shadow-other" style="background-color: #70707014;">
    <h4 class="card-header text-white bg-special text-center py-3">SKILL ASSESSMENT TEST
        RESULTS
    </h4>
    <div class="card-body">
        @if($skillAssessmentTestResults > 99)
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">In Meeting</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $skillAssessmentTestResults['inMeetings'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['inMeetings'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Outside Meetings</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-primary" role="progressbar" style="width: {{ $skillAssessmentTestResults['outsideMeetings'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['outsideMeetings'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Speaking</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ $skillAssessmentTestResults['speaking'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['speaking'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Writing</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ $skillAssessmentTestResults['writing'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['writing'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Organizing</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $skillAssessmentTestResults['organizing'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['organizing'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">Creativity</div>
            <div class="col-md-7">
                <div class="progress">
                    <div class="progress-bar bg-dark" role="progressbar" style="width: {{ $skillAssessmentTestResults['creativity'] ?? '0' }}%">
                        {{ round($skillAssessmentTestResults['creativity'], 0) . "%" }}
                    </div>
                </div>
            </div>
        </div>
        @elseif($skillAssessmentTestResults > 0 && $skillAssessmentTestResults < 100)
        Not completed yet!
        @else
        Not attempted yet!
        @endif
    </div>
</div>

<div class="card border-0 h-100 shadow-other" style="background-color: #70707014;">
    <h4 class="card-header text-white bg-special text-center py-3">CAREER EXPECTATIONS
        QUESTIONNAIRE</h4>
    <div class="card-body">
        @if(is_array($careerExpectationsTestResults))
        <p>Congratulations on your outstanding career expectations test results! Your hard work and dedication have truly paid off. Your future looks bright with such impressive results. Keep up the great work and I have no doubt that you will achieve all of your career goals.</p>
        @php
            $colors = ["bg-success", "bg-primary", "bg-secondary", "bg-info", "bg-warning", "bg-dark", "bg-blue", "bg-yellow"];
        @endphp
        @foreach($careerExpectationsTestResults as $key=>$value)
        <div class="row g-3">
            <div class="col-md-5 col-lg-3 fw-bold">{!! $key !!}</div>
            <div class="col-md-7 col-lg-8">
                <div class="progress shadow-sm">
                    <div class="progress-bar {{ $colors[$loop->index] }}" role="progressbar" style="width: {{ $value ?? '0'}}%;">
                        {{ round($value, 0) }}%
                    </div>
                </div>
            </div>
        </div>
        <hr>
        @endforeach
        <div class="row g-3">
            <hgroup class="col-12">
                <h5>Competition</h5>
                <p>The idea of a career as a contest competing with others and with definite success
                    indicators is important to you. You need to have recognition of your achievements – if not
                    you will feel dissatisfied and frustrated.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Freedom</h5>
                <p>You expect to have considerable autonomy over your day to day work, in how you
                    approach it and setting priorities. You expect to be evaluated on ultimate achievements
                    rather than detailed methods. You would not take kindly to someone watching closely
                    over your shoulder.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Management</h5>
                <p>It is important for you to be able to use a range of generalist skills to achieve results
                    through and with others. Position, title and status (along with the rewards) are important
                    to you and you need to achieve a position of responsibility quickly.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Life Balance</h5>
                <p>For you the right balance between work and the rest of your life is important. You will
                    seek opportunities that allow you to develop this balance and will feel resentment if (in
                    your eyes) unacceptable demands are placed upon you that intrude into your non-work
                    space. You expect give and take, and flexible working practices.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Organisation Membership</h5>
                <p>You will identify strongly with organisational goals and values. You are a person who will
                    enjoy being seen as an “organisational man or woman” and your needs and values will fit
                    very closely with the organisation. You are likely to put company needs before your own.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Expertise</h5>
                <p>You need to have the opportunity to develop your expertise and to become more self-
                    confident about your personal value. Your security comes from the opportunity to
                    specialise and you would find any attempt to make you a generalist rather unsettling,
                    feeling vulnerable and exposed.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Learning</h5>
                <p>You will thrive on being challenged and learning to overcome difficulties through
                    acquisition of new skills and expertise. Your positive attitude will allow you to tackle issues
                    5and problems that may seem daunting to others. If you are not constantly challenged with
                    new learning opportunities you will feel a lack of direction.</p>
            </hgroup>
            <hgroup class="col-12">
                <h5>Entrepreneurship</h5>
                <p>Risk taking is your lifeblood and source of stimulation rather than being frightened and
                    resistant. Self-conviction, self-determination, self-control are essential to you whether they
                    exist through self-employment or an organisation that allows you this scope.</p>
            </hgroup>
        </div>
        @elseif($careerExpectationsTestResults > 0 && $careerExpectationsTestResults < 100)
            No completed yet!
        @else
            No attempted yet!
        @endif
    </div>
</div>

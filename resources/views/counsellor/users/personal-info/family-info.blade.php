<div class="card border-0 h-100 shadow-other">
    <h4 class="card-header text-bg-dark">Family Information</h4>
    <div class="card-body">
        <div class="row gy-2" style="font-weight: 500;">
            <div class="col-6">Father Education</div>
            <div class="col-6">{{ $userProfile->father_education ?? '' }}</div>
            <div class="col-6">Father Occupation</div>
            <div class="col-6">{{ $userProfile->father_occupation ?? '' }}</div>
            <div class="col-6">Mother Education</div>
            <div class="col-6">{{ $userProfile->mother_education ?? '' }}</div>
            <div class="col-6">Mother Occupation</div>
            <div class="col-6">{{ $userProfile->mother_occupation ?? '' }}</div>
            <div class="col-6">Retired Parents</div>
            <div class="col-6">{{ $userProfile->parents_retired ?? '' }}</div>
            <div class="col-12 fw-bold fs-6">Family Assistance</div>
            <div class="col-3 col-md-6">Father</div>
            <div class="col-9 col-md-6">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-warning bg-warning" id="personal6'father" style="width: {{ $familyInfo["father"] }}%">
                        {{ $familyInfo["father"] }}%
                    </div>
                </div>
            </div>
            <div class="col-3 col-md-6">Mother</div>
            <div class="col-9 col-md-6">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-warning bg-warning" id="personal6'mother" style="width: {{ $familyInfo["mother"] }}%">
                        {{ $familyInfo["mother"] }}%
                    </div>
                </div>
            </div>
            <div class="col-3 col-md-6">Siblings</div>
            <div class="col-9 col-md-6">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-warning bg-warning" id="personal6'siblings" style="width: {{ $familyInfo["siblings"] }}%">
                        {{ $familyInfo["siblings"] }}
                    </div>
                </div>
            </div>
            <div class="col-3 col-md-6">Relatives</div>
            <div class="col-9 col-md-6">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-warning bg-warning" id="personal6'relatives" style="width: {{ $familyInfo["relatives"] }}%">
                        {{ $familyInfo["relatives"] }}
                    </div>
                </div>
            </div>
            <div class="col-3 col-md-6">Others</div>
            <div class="col-9 col-md-6">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-warning bg-warning" id="personal6'other_persons" style="width: {{ $familyInfo["other_persons"] }}%">
                        {{ $familyInfo["other_persons"] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

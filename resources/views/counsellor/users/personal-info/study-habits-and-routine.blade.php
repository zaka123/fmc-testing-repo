<div class="card border-0 h-100 shadow-other">
    <h4 class="card-header text-bg-dark">Study Habits And Routine</h4>
    <div class="card-body">
        <div class="row gy-2" style="font-weight: 500;">
            <div class="col-7 col-lg-6">Morning study hours</div>
            <div class="col-5 col-lg-6">{{ $studyHabitsAndRoutine[0]['answer'] }} {{ Str::plural("hour", $studyHabitsAndRoutine[0]['answer']) }}</div>
            <div class="col-7 col-lg-6">Evening study hours</div>
            <div class="col-5 col-lg-6">{{ $studyHabitsAndRoutine[1]['answer'] }} {{ Str::plural("hour", $studyHabitsAndRoutine[1]['answer']) }}</div>
            <div class="col-12 fw-bold">Reason for home study</div>
            <div class="col-12">
                <table class="table table-hover">
                    <tr>
                        <td>Interesting content</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["interesting_content"] }}</td>
                    </tr>
                    <tr>
                        <td>Upcoming exam or test</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["upcoming_exam_or_test"] }}</td>
                    </tr>
                    <tr>
                        <td>Parents pressure</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["parents_pressure"] }}</td>
                    </tr>
                    <tr>
                        <td>Homework or assignment</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["homework_or_assignment"] }}</td>
                    </tr>
                    <tr>
                        <td>Classmate or peer pressure</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["classmate_or_peer_pressure"] }}</td>
                    </tr>
                    <tr>
                        <td>My daily routine</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["my_daily_routine"] }}</td>
                    </tr>
                    <tr>
                        <td>Other reasons</td>
                        <td>{{ $studyHabitsAndRoutine[2]->answer["other_reasons"] }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-12 fw-bold">Reason for no home study</div>
            <div class="col-12">
                <table class="table table-hover">
                    <tr>
                        <td>No time to study</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["no_time_to_study"] }}</td>
                    </tr>
                    <tr>
                        <td>No interesting content</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["no_interesting_content"] }}</td>
                    </tr>
                    <tr>
                        <td>No upcoming exam or test</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["no_upcoming_exam_or_test"] }}</td>
                    </tr>
                    <tr>
                        <td>No pressure from anyone</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["no_pressure_from_anyone"] }}</td>
                    </tr>
                    <tr>
                        <td>No homework or assignment</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["no_homework_or_assignment"] }}</td>
                    </tr>
                    <tr>
                        <td>None of my classmate study</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["none_of_my_classmate_study"] }}</td>
                    </tr>
                    <tr>
                        <td>I never study</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["i_never_study"] }}</td>
                        </tr>
                    <tr>
                        <td>Other reasons</td>
                        <td>{{ $studyHabitsAndRoutine[3]->answer["other_reasons"] }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

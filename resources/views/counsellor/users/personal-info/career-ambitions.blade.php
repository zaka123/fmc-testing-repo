<div class="card border-0 h-100 shadow-other">
    <h4 class="card-header text-bg-dark">Career Ambitions</h4>
    <div class="card-body">
        <div class="row gy-2">
            <div class="col-md-7 fw-bold">Immediate next step:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 1)->first()->answer }}</div>
            <div class="col-md-7 fw-bold">Most influence personality:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 2)->first()->answer }}</div>
            <div class="col-md-7 fw-bold">Same profession as:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 4)->first()->answer }}</div>
            <div class="col-md-7 fw-bold">Parents expectation are same:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 5)->first()->answer }}</div>
            @if ($careerAmbitions->where('question_no', 2)->first()->answer != "Nobody" && $careerAmbitions->where('question_no', 2)->first()->answer != "Prefer not to say")
            <div class="col-md-7 fw-bold">Degree of person who influenced you:</div>
            <div class="col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar" role="progressbar" style="width: {{ $careerAmbitions->where('question_no', 3)->first()->answer }}%"></div>
                </div>
            </div>
            @endif
            <div class="col-md-7 fw-bold">Reason for career choice:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 6)->first()->answer }}</div>
            <div class="col-md-7 fw-bold">Reason for university degree choice:</div>
            <div class="col-md-5">{{ $careerAmbitions->where('question_no', 7)->first()->answer }}</div>
            <div class="col-12 fw-bold">Reason for getting a job:</div>
            <div class="col-12">
                <ul>
                    @foreach ($careerAmbitions->where('question_no', 8)->first()->answer as $index => $value)
                        <li>{{ $reasonsForGettingJob[$index] }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 fw-bold">Acquired skills:</div>
            <div class="col-12">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <td>Job or education</td>
                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer["job_or_edu"] }}</td>
                        </tr>
                        <tr>
                            <td>Resume creation</td>
                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer["resume_creation"] }}</td>
                        </tr>
                        <tr>
                            <td>Cover letter creation</td>
                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer["cover_letter_creation"] }}</td>
                        </tr>
                        <tr>
                            <td>Interview preparation</td>
                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer["interview_prep"] }}</td>
                        </tr>
                        <tr>
                            <td>Presentation</td>
                            <td>{{ $careerAmbitions->where('question_no', 9)->first()->answer["presentation"] }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-7 fw-bold fs-6">Employability skills</div>

            <div class="col-6 col-md-7">Communication skill:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[0]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Teamwork skill:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[1]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Problem Solving:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[2]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Initiative and Enterprise Skills:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[3]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7"> Planning and Organizing Skill:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[4]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Self-management Skills:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[5]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Learning Skills:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[6]['answer'] }}%"></div>
                </div>
            </div>

            <div class="col-6 col-md-7">Technology Skills:</div>
            <div class="col-6 col-md-5">
                <div class="progress bg-light rounded-0">
                    <div class="progress-bar text-special bg-special" style="width: {{ $decision[7]['answer'] }}%"></div>
                </div>
            </div>

        </div>
    </div>
</div>

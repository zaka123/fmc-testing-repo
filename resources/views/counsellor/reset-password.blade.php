@extends('layouts.navbar')
@section('css')
<style>
    label {
    color:var(--special) !important
 }
 .blackChanger:hover{
    background-color:black!important;
 }
</style>
@endsection
@section('content')
<div class="container pb-5 d-flex align-items-center justify-content-center" style="min-height:82vh">
    <div class="card" style="width:35rem;--bs-card-border-color:var(--special)">
        <h5 class="card-header bg-special">Update your Password</h5>
        <div class="card-body">
            <div class="card-text mb-3 text-muted">
                Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Repellat omnis, nulla harum corporis illum nobis fuga alias delectus repellendus animi deserunt molestiae at est modi corrupti. Earum sed explicabo expedita.
            </div>
            <form method="POST" action="{{ route('counsellor.password.update') }}" id="resetPasswordForm">
                @csrf
                <div class="form-group mb-3">
                    <label for="password" class="form-label">Password:</label>
                    <input id="password" class="form-control" type="password" name="password" required autofocus>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Password Confirmation:</label>
                    <input id="password_confirmations" class="form-control" type="password" name="password_confirmation" required>
                </div>
            </form>
        </div>
        <button class="card-footer btn btn-sm bg-special blackChanger" form="resetPasswordForm">Update Password</button>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>Zoom Meeting SDK Sample JavaScript</title>

    <!-- For Client View -->
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.15.2/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.15.2/css/react-select.css" />

    <!-- Origin Trials to enable Gallery View in Chrome/Edge -->
    <!-- More Info: https://developers.zoom.us/docs/meeting-sdk/web/gallery-view/ -->
    <!-- SharedArrayBuffers in non-isolated pages on Desktop platforms -->
    <meta http-equiv="origin-trial" content="">

    <style>
        html,
        body {
            min-width: 0 !important;
        }

        #zmmtg-root {
            display: none;
        }

        main {
            width: 70%;
            margin: auto;
            text-align: center;
        }

        main button {
            margin-top: 20px;
            background-color: #2D8CFF;
            color: #ffffff;
            text-decoration: none;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 40px;
            padding-right: 40px;
            display: inline-block;
            border-radius: 10px;
            cursor: pointer;
            border: none;
            outline: none;
        }

        main button:hover {
            background-color: #2681F2;
        }
    </style>
</head>

<body>

    <main>
        <h1>FIND ME CAREER - Zoom One on One Session</h1>

        <!-- For Component View -->
        <div id="meetingSDKElement">
            <!-- Zoom Meeting SDK Rendered Here -->
        </div>

        {{-- <button onclick="getSignature()">Join Meeting</button> --}}
        <button onclick="startMeeting()">Start Session</button>
    </main>

    <!-- For Component and Client View -->
    <script src="https://source.zoom.us/2.15.2/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/2.15.2/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/2.15.2/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/2.15.2/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/2.15.2/lib/vendor/lodash.min.js"></script>

    <!-- For Client View -->
    {{-- <script src="https://source.zoom.us/zoom-meeting-2.16.0.min.js"></script> --}}
    <script src="https://source.zoom.us/zoom-meeting-2.15.2.min.js"></script>
    <script>
        ZoomMtg.setZoomJSLib('https://source.zoom.us/2.15.2/lib', '/av')

        ZoomMtg.preLoadWasm()
        ZoomMtg.prepareWebSDK()
        // loads language files, also passes any error messages to the ui
        ZoomMtg.i18n.load('en-US')
        ZoomMtg.i18n.reload('en-US')

        // var authEndpoint = 'https://c3d1-2407-d000-503-e1c1-dd8b-2ffb-fdd8-2cac.ngrok-free.app/testjwttoken';
        var sdkKey = '{{ getenv('ZOOM_SDK_ID') }}';
        var meetingNumber = '{{ $videoCall->meeting_id }}';
        var passWord = 'secret1234';
        var role = {{ $role }};
        var userName = '{{ auth()->user()->name }}';
        var userEmail = '';
        var registrantToken = '';
        var zakToken = '{{ $zakToken['token'] }}';
        var leaveUrl = '{{ url("/counsellor/bookings") }}';
        var signature = '{{ $jwtToken }}';

        // function getSignature() {
        //     fetch(authEndpoint, {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'application/json'
        //         },
        //         body: JSON.stringify({
        //             meetingNumber: meetingNumber,
        //             role: role
        //         })
        //     }).then((response) => {
        //         return response.json()
        //     }).then((data) => {
        //         console.log(data)
        //         startMeeting(data.signature)
        //     }).catch((error) => {
        //         console.log(error)
        //     })
        // }

        function startMeeting() {

            document.getElementById('zmmtg-root').style.display = 'block'

            ZoomMtg.init({
                leaveUrl: leaveUrl,
                disablePreview: true,
                success: (success) => {
                    console.log(success)
                    console.log(signature);
                    ZoomMtg.join({
                        signature: signature,
                        sdkKey: sdkKey,
                        meetingNumber: meetingNumber,
                        passWord: passWord,
                        userName: userName,
                        userEmail: userEmail,
                        tk: registrantToken,
                        zak: zakToken,
                        success: (success) => {
                            console.log(success)
                        },
                        error: (error) => {
                            console.log(error)
                        },
                    })
                },
                error: (error) => {
                    console.log(error)
                }
            })
        }
    </script>

</body>

</html>

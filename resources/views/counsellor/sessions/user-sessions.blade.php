@extends('layouts.counsellor', ['pagename' => 'Bookings'])
@section('css')
    <style>
        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        /* round start and ends of all rows */
        table.payments tbody td:first-child {
            border-top-left-radius: var(--bs-border-radius);
            border-bottom-left-radius: var(--bs-border-radius);
        }

        table.payments tbody td:last-child {
            border-top-right-radius: var(--bs-border-radius);
            border-bottom-right-radius: var(--bs-border-radius);
        }

        .bookings {
            color: var(--special) !important;
        }

        .title::before {
            content: "Booking List"
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('counsellor-content')
    <div class="container-fluid px-0">
        <div class="row g-0">
            <!-- side bar -->
            @include('side-bar.counsellor')
            <div class="col-md-10 bg-light smallResponsive">
                <div class="col-12 bg-white sticky-top d-flex px-3">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <div class="pb-3 ms-auto">
                        @include('navbars.counsellor')
                    </div>
                </div>
                <div class="row g-0">
                    <div class="col-12">
                        <div class="main" style="min-height:83.2vh">
                            <div class="floating"></div>
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row inner py-5">
                                                <div class="col-10 offset-1">
                                                    <table class="table table-borderless payments mt-5"
                                                        style="vertical-align:middle">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th class="text-center">User</th>
                                                                <th class="text-center">Session Date</th>
                                                                <th class="text-center">Session Time</th>
                                                                {{-- <th class="text-center">Timezone</th> --}}
                                                                <th class="text-center">Status</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($user_sessions as $key => $session)
                                                                <tr>
                                                                    <td class="text-center">{{ ++$key }}</td>
                                                                    <td class="text-center">{{ $session->user->full_name }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{-- {{ $session->session->date }} --}}
                                                                        {{ (new DateTime($session->session->session_timing, new DateTimeZone('UTC')))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y') ?? '' }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{-- {{ $session->session->time }} --}}
                                                                        {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone ?? 'UTC')))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('h:i A') ?? '' }}
                                                                    </td>
                                                                    {{-- <td class="text-center">{{ $session->session->timezone ?? '' }}</td> --}}
                                                                    <td class="text-center">{!! $session->session_status ?? '' !!}</td>
                                                                    <td class="text-center">
                                                                        <div
                                                                        {{-- id="session{{ $session->id }}" --}}
                                                                            class="btn-group btn-group-sm">
                                                                            {{-- <a href="{{url('counsellor/status',['id'=>$session->id,'status'=>'Confirm'])}}" class="btn btn-success">
                                                                        <i class="fa fa-check"></i>
                                                                    </a>
                                                                    <a href="{{url('counsellor/status',['id'=>$session->id,'status'=>'Pending'])}}" class="btn btn-info">
                                                                        <i class="fa fa-times"></i>
                                                                    </a> --}}
                                                                            <a href="{{ route('counsellor.user.show', $session->user_id) }}"
                                                                                data-bs-toggle="tooltip"
                                                                                title="view student profile"
                                                                                class="btn btn-primary">
                                                                                <i class="fa fa-user"></i>
                                                                            </a>
                                                                            @if ($session->is_starting)
                                                                                <button class="btn btn-dark"
                                                                                    data-bs-toggle="tooltip"
                                                                                    title="wait for session time to arrive">
                                                                                    <i class="fa fa-info"></i>
                                                                                </button>
                                                                            @elseif($session->has_started)
                                                                                @if ($session->videoCall->id ?? '')
                                                                                    <a
                                                                                    {{-- id="call{{ $session->id }}" --}}
                                                                                        href="{{ route('counsellor.video-call.show', $session->videoCall->id) }}"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="join now"
                                                                                        class="btn btn-success">
                                                                                        <i class="fa fa-phone"></i>
                                                                                    </a>
                                                                                @else
                                                                                    <button class="btn btn-dark"
                                                                                        data-bs-toggle="tooltip"
                                                                                        title="the session was attempted with a fake email. Please try again with a valid email.">
                                                                                        <i class="fa fa-info"></i>
                                                                                    </button>
                                                                                @endif
                                                                            @elseif($session->has_ended)
                                                                                <button class="btn btn-dark"
                                                                                    data-bs-toggle="tooltip"
                                                                                    title="the session ended">
                                                                                    <i class="fa fa-info"></i>
                                                                                </button>
                                                                            @endif

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        setInterval(function() {
            checkAndReloadPage();
        }, 60000);

        // Function to reload the page if the current time is equal to or greater than the target datetime
        function checkAndReloadPage() {
            var inputTime = '';
            var targetDatetime = '';
            @if ($user_sessions)
                @foreach ($user_sessions as $session)
                    @if ($session->session->session_timing)
                        // inputTime = '{{ $session->session->session_timing }}';
                        inputTime =
                            '{{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone))->format('m-d-Y H:i') }}';
                        // console.log(inputTime);

                        // Convert to javascript date
                        targetDatetime = new Date(inputTime);
                        var currentDatetime = new Date();
                        var oneHourLater = new Date(targetDatetime.getTime() + 60 * 60 *
                            1000); // Add 1 hour to the current time
                        if (currentDatetime >= targetDatetime && currentDatetime < oneHourLater) {
                            // clearInterval(intervalId); // Clear the interval
                            location.reload();
                        }
                    @endif
                @endforeach
            @endif
        }
    </script>
@endpush

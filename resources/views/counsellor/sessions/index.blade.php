@extends('layouts.counsellor', ['pagename' => 'Sessions'])
@section('counsellor-content')
@section('css')
    <style>
        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
            vertical-align: middle;
        }

        /* round start and ends of all rows */
        table.payments tbody td:first-child {
            border-top-left-radius: var(--bs-border-radius);
            border-bottom-left-radius: var(--bs-border-radius);
        }

        table.payments tbody td:last-child {
            border-top-right-radius: var(--bs-border-radius);
            border-bottom-right-radius: var(--bs-border-radius);
        }

        .time-management {
            color: var(--special) !important;
        }

        .title::before {
            content: "Time Management"
        }

        .blackChanger:hover {
            background-color: black !important;
            color: white !important;
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
    </style>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"
        integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw=="
    crossorigin="anonymous"
        referrerpolicy="no-referrer" />
@endsection
@php
    $timezones = App\Models\Timezone::all();
@endphp
<!-- timezone update modal -->
<div class="modal fade" id="changeTimezoneModal" tabindex="-1" role="dialog" aria-labelledby="changeTimezoneModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-black" id="changeTimezoneModalTitle">Change Timezone</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post" action="{{ route('user.timezone.update', auth()->id()) }}" class="needs-validation"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-body border-0 ">
                    <select class="form-select select2bs4" name="timezone" id="timezone" required>
                        <option value="">Select timezone</option>
                        @foreach ($timezones as $timezone)
                            <option value="{{ $timezone->timezone }}" @if (auth()->user()->timezone == $timezone->timezone) selected @endif>
                                {{ $timezone->timezone }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeChanger" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveChanger">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" style="--bs-modal-width: 450px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Session</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form autocomplete="off" action="{{ route('counsellor.sessions.store') }}" method="post">
                @csrf
                <div class="modal-body text-sm">
                    @if (auth()->user()->timezone)
                        <div class="form-group mb-3">
                            <label for="date" class="form-label">Session Date</label>
                            <input name="date" id="date" type="date"
                            {{-- min="{{ date('Y-m-d') }}" --}}
                                min="{{ now()->addDays(2)->format('Y-m-d') }}"
                                class="form-control @error('date') is-invalid @enderror" value="{{ old('date') }}"
                                required style="box-shadow:none">
                            @error('date')
                                <div class="invalid-feedback">{{ __($message) }}</div>
                            @enderror
                        </div>

                        {{-- <div class="form-group mb-3">
                            <label for="time" class="form-label">Session Time</label>
                            <input name="time" id="time" list="timeList" type="time"
                                pattern="([01][0-9]|2[0-3]):(00|30)" step="1800"
                                class="form-control @error('time') is-invalid @enderror" value="{{ old('time') }}"
                                required>
                            @error('time')
                                <div class="invalid-feedback">{{ __($message) }}</div>
                            @enderror
                        </div> --}}


                        <div class="form-group mb-5">
                            <label for="time" class="form-label">Session Time</label>
                            <div
                                style="display:flex;width:100%;background-color:white;border:1px solid #ced4da;border-radius: 0.375rem;">
                                <select id="hours" class="form-control"
                                    style="width: 33%;border:1px solid white;box-shadow:none
                                    "
                                    required>
                                    @for ($hour = 1; $hour <= 12; $hour++)
                                        <option value="{{ $hour }}">{{ sprintf('%02d', $hour) }}</option>
                                    @endfor
                                </select>
                                <select id="minutes" class="form-control"
                                    style="width: 33%;border:1px solid white;box-shadow:none" required>
                                    @php
                                        $minutes = [0, 30];
                                    @endphp
                                    @foreach ($minutes as $minute)
                                        <option value="{{ $minute }}">{{ sprintf('%02d', $minute) }}</option>
                                    @endforeach
                                </select>
                                <select id="period" class="form-control"
                                    style="width:43%;border:1px solid white;box-shadow:none" required>
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group d-none">
                            <label for="convertedTime">Converted Time:</label>
                            <input type="time" id="convertedTime" name="time" class="form-control" readonly>
                        </div>
                    @else
                        <div class="alert alert-warning" role="alert">
                            <h4 class="alert-heading">Warning!</h4>
                            <p class="mb-0">Please set your timezone first and then try again.</p>
                        </div>
                    @endif
                </div>
                <div class="modal-footer justify-content-between">
                    <div>
                        <strong>Timezone: </strong> {{ auth()->user()->timezone ?? 'N/A' }}
                        <a href="#" title="Change Timezone" class="text-decoration-none fs-6"
                            data-bs-toggle="modal" data-bs-target="#changeTimezoneModal">
                            <small>
                                <i class="fa-regular fa-pen-to-square"></i>
                            </small>
                        </a>
                    </div>
                    <div>
                        <button type="button" class="btn btn-sm btn-secondary"
                            data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm bg-special text-white">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light smallResponsive">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="inner row">
                                    <div class="col-12">
                                        <div class="px-2 py-4 p-lg-4">
                                            <button class="btn btn-sm bg-special text-white blackChanger"
                                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                Add Availability
                                            </button>
                                            <table class="table payments table-borderless" align="center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-center">Session Date</th>
                                                        <th class="text-center">Session Time</th>
                                                        <th class="text-center">Timezone</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($sessions as $key => $session)
                                                        <tr>
                                                            <td class="text-center">{{ ++$key }}</td>
                                                            <td class="text-center">{{ $session->date }}
                                                            </td>
                                                            <td class="text-center">{{ $session->time }}</td>
                                                            <td class="text-center">
                                                                {{ auth()->user()->timezone ?? $session->timezone }}
                                                            </td>
                                                            <td class="text-center">
                                                                @if ($session->userCounsellorSession->isEmpty())
                                                                    <div class="btn-group btn-group-sm">
                                                                        <a href="{{ route('counsellor.sessions.edit', $session->id) }}"
                                                                            class="btn btn-outline-secondary"
                                                                            data-bs-toggle="tooltip" title="Edit">
                                                                            <i class="fa fa-pencil-alt"></i>
                                                                        </a>
                                                                        <button type="submit"
                                                                            form="delete_session_{{ $session->id }}"
                                                                            class="btn btn-outline-danger"
                                                                            data-bs-toggle="tooltip" title="Delete">
                                                                            <i class="fa fa-trash"></i>
                                                                        </button>
                                                                    </div>

                                                                    <form
                                                                        action="{{ route('counsellor.sessions.destroy', $session->id) }}"
                                                                        method="post"
                                                                        id="delete_session_{{ $session->id }}">
                                                                        @csrf @method('DELETE')
                                                                    </form>
                                                                @else
                                                                    <span class="btn btn-outline-secondary"
                                                                        data-bs-toggle="tooltip"
                                                                        title="The session is booked">
                                                                        <i class="fa fa-info"></i>
                                                                    </span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const hoursSelect = document.getElementById('hours');
        const periodSelect = document.getElementById('period');
        const minutesSelect = document.getElementById('minutes');
        const convertedTimeInput = document.getElementById('convertedTime');

        function updateConvertedTime() {
            const hours = hoursSelect.value;
            const period = periodSelect.value;
            const minutes = minutesSelect.value;
            const hourIn24Format = (parseInt(hours) % 12) + (period === 'PM' ? 12 : 0);
            const timeString =
                `${hourIn24Format.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
            convertedTimeInput.value = timeString;
        }

        hoursSelect.addEventListener('change', updateConvertedTime);
        periodSelect.addEventListener('change', updateConvertedTime);
        minutesSelect.addEventListener('change', updateConvertedTime);

        // Call the function once initially to set the initial value
        updateConvertedTime();
    });
</script>
@endsection
@push('script')
<script src="{{ asset('style/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $(function() {
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4',
            dropdownParent: $("#changeTimezoneModal")
        });
    });
</script>
@endpush

@extends('layouts.counsellor',['pagename'=>'Session Report'])
@section('css')
<style>
    table.payments tbody td {
        background-color: var(--bs-white) !important;
    }

    table.payments {
        border-collapse: separate !important;
        border-spacing: 0px .5rem !important;
    }

    /* round start and ends of all rows */
    table.payments tbody td:first-child {
        border-top-left-radius: var(--bs-border-radius);
        border-bottom-left-radius: var(--bs-border-radius);
    }

    table.payments tbody td:last-child {
        border-top-right-radius: var(--bs-border-radius);
        border-bottom-right-radius: var(--bs-border-radius);
    }

    body>div.container-fluid.px-0>div>div.col-md-2>div>div>div.pe-3.ps-2.pb-2>div:nth-child(9)>div>a:nth-child(2) {
        color: var(--special) !important
    }
</style>
@endsection
@section('counsellor-content')
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="row inner py-3">
                                        <div class="col-10 offset-1 mb-3 fw-bold">Upload Session Report</div>
                                    <div class="col-10 offset-1">
                                        <form action="{{url('counsellor/session-history')}}" class="needs-validation" enctype="multipart/form-data" method="post" novalidate>
                                            @csrf
                                            <div class="form-group mb-3">
                                                <label for="Report Statement">Report Statement <small>(Optional)</small></label>
                                                <textarea name="reportStatement" id="extra" cols="30" rows="10" class="form-control @error('reportStatement') is-invalid @enderror">{{old('reportStatement')}}</textarea>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="report">Report File</label>
                                                <input type="file" name="report" id="report" class="form-control @error('report') is-invalid @enderror" accept="docx, application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
                                            </div>
                                            <input type="hidden" name="session_id" value="{{$session_id ?? ''}}">
                                            <input type="submit" value="Upload" class="btn bg-special text-white">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection

@extends('layouts.counsellor', ['pagename' => 'Sessions'])
@section('counsellor-content')
@section('css')
    <style>
        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        /* round start and ends of all rows */
        table.payments tbody td:first-child {
            border-top-left-radius: var(--bs-border-radius);
            border-bottom-left-radius: var(--bs-border-radius);
        }

        table.payments tbody td:last-child {
            border-top-right-radius: var(--bs-border-radius);
            border-bottom-right-radius: var(--bs-border-radius);
        }

        body>div.container-fluid.px-0>div>div.col-md-2.bg-light>div>div>div.pe-3.ps-2.pb-2>div:nth-child(9)>div>a:nth-child(1) {
            color: var(--bs-primary) !important;
        }

        @media(max-width:768px) {
            .schedule {
                display: none;
            }
        }

        .schedule {
            height: 50%;
            width: 50%;
        }
    </style>
@endsection
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"
    integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw=="
    crossorigin="anonymous"
    referrerpolicy="no-referrer" />
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="inner row">
                                    <div class="col-md-10 offset-md-1">
                                        <div class="row justify-content-center">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="px-2 py-4 p-lg-4">
                                                    <h5>Update Schedule</h5>
                                                    <form
                                                        action="{{ route('counsellor.sessions.update', $session->id) }}"
                                                        method="post">
                                                        @csrf @method('PUT')
                                                        <div class="form-group mb-3">
                                                            <label for="date" class="form-label">Session
                                                                Date</label>
                                                            <input name="date" id="date" type="date"
                                                                {{-- min="{{ date('Y-m-d') }}" --}}
                                                                min="{{ now()->addDays(2)->format('Y-m-d') }}"
                                                                class="form-control @error('date') is-invalid @enderror"
                                                                value="{{ old('date') ?? $session->date_for_HTML }}"
                                                                required style="box-shadow:none">
                                                            @error('date')
                                                                <div class="invalid-feedback">{{ __($message) }}</div>
                                                            @enderror
                                                        </div>
                                                        {{-- <div class="form-group mb-3">
                                                            <label for="time" class="form-label">Session Time</label>
                                                            <input name="time" id="time" type="time" class="form-control @error('time') is-invalid @enderror" value="{{ old('time') ?? $session->time_for_HTML }}" required>
                                                            @error('time')
                                                                <div class="invalid-feedback">{{ __($message) }}</div>
                                                            @enderror
                                                        </div> --}}
                                                        <div class="form-group mb-3">
                                                            <label for="time" class="form-label">Session
                                                                Time</label>
                                                            <div
                                                                style="display:flex;width:100%;background-color:white;border:1px solid #ced4da;border-radius: 0.375rem;">
                                                                <select id="hours" class="form-control"
                                                                    style="width: 33%;border:1px solid white;box-shadow:none
                                                                    "
                                                                    required>
                                                                    @for ($hour = 1; $hour <= 12; $hour++)
                                                                        <option value="{{ $hour }}"
                                                                            @if ($hour == $hours) selected @endif>
                                                                            {{ sprintf('%02d', $hour) }}</option>
                                                                    @endfor
                                                                </select>
                                                                <select id="minutes" class="form-control"
                                                                    style="width: 33%;border:1px solid white;box-shadow:none"
                                                                    required>
                                                                    @php
                                                                        $interval = [0, 30];
                                                                    @endphp
                                                                    @foreach ($interval as $minute)
                                                                        <option value="{{ $minute }}"
                                                                            @if ($minute == $minutes) selected @endif>
                                                                            {{ sprintf('%02d', $minute) }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select id="period" class="form-control"
                                                                    style="width:43%;border:1px solid white;box-shadow:none"
                                                                    required>
                                                                    <option value="AM"
                                                                        @if ($ampm == 'AM') selected @endif>
                                                                        AM
                                                                    </option>
                                                                    <option value="PM"
                                                                        @if ($ampm == 'PM') selected @endif>
                                                                        PM
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="display: none">
                                                            <label for="convertedTime">Converted Time:</label>
                                                            <input type="time" id="convertedTime" name="time"
                                                                class="form-control"
                                                                value="{{ $session->time_for_HTML }}" readonly>
                                                        </div>
                                                        <div class="form-group mb-3">
                                                            <label for="timezone">Timezone:</label>
                                                            <input type="text" class="form-control" name="timezone"
                                                                value="{{ auth()->user()->timezone }}" readonly />

                                                        </div>
                                                        <a href="{{ url()->previous() }}"
                                                            class="btn btn-sm btn-secondary">Cancel changes</a>
                                                        <button type="submit"
                                                            class="btn btn-sm bg-special text-white">Save
                                                            changes</button>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"
    integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function() {
        $('input[type="datetime"]').datetimepicker({
            formatDate: "Y-m-d",
            formatTime: "h:i A",
            minDate: new Date(),
        });
    });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const hoursSelect = document.getElementById('hours');
        const periodSelect = document.getElementById('period');
        const minutesSelect = document.getElementById('minutes');
        const convertedTimeInput = document.getElementById('convertedTime');

        function updateConvertedTime() {
            const hours = hoursSelect.value;
            const period = periodSelect.value;
            const minutes = minutesSelect.value;
            const hourIn24Format = (parseInt(hours) % 12) + (period === 'PM' ? 12 : 0);
            const timeString =
                `${hourIn24Format.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
            convertedTimeInput.value = timeString;
        }

        hoursSelect.addEventListener('change', updateConvertedTime);
        periodSelect.addEventListener('change', updateConvertedTime);
        minutesSelect.addEventListener('change', updateConvertedTime);

        // Call the function once initially to set the initial value
        // updateConvertedTime();
    });
</script>
@endpush

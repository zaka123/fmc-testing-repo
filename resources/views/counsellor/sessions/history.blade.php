@extends('layouts.counsellor', ['pagename' => 'Session History'])
@section('css')
    <style>
        table.payments tbody td {
            background-color: var(--bs-white) !important;
        }

        table.payments {
            border-collapse: separate !important;
            border-spacing: 0px .5rem !important;
        }

        /* round start and ends of all rows */
        table.payments tbody td:first-child {
            border-top-left-radius: var(--bs-border-radius);
            border-bottom-left-radius: var(--bs-border-radius);
        }

        table.payments tbody td:last-child {
            border-top-right-radius: var(--bs-border-radius);
            border-bottom-right-radius: var(--bs-border-radius);
        }

        .session-history {
            color: var(--special) !important
        }

        .title::before {
            content: "Session History"
        }

        @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
    </style>
@endsection
@section('counsellor-content')
    <div class="container-fluid px-0">
        <div class="row g-0">
            <!-- side bar -->
            @include('side-bar.counsellor')

            <div class="col-md-10 bg-light smallResponsive">
                <div class="col-12 bg-white sticky-top d-flex px-3">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <div class="pb-3 ms-auto">
                        @include('navbars.counsellor')
                    </div>
                </div>
                <div class="row g-0">
                    <div class="col-12">
                        <div class="main" style="min-height:83.2vh">
                            <div class="floating"></div>
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="card border-0 pt-3 bg-transparent">
                                        <div class="card-body">
                                            <div class="row inner py-5">
                                                <div class="col-10 offset-1">
                                                    <table class="table table-borderless payments"
                                                        style="vertical-align:middle">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th class="text-center">User</th>
                                                                <th class="text-center">Date </th>
                                                                <th class="text-center">Time</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($sessions as $key => $session)
                                                                <tr>
                                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                                    <td class="text-center">{{ $session->user->full_name }}</td>
                                                                    <td class="text-center">
                                                                        {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('d-m-Y') ?? '' }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        {{ (new DateTime($session->session->session_timing, new DateTimeZone($session->session->timezone)))->setTimezone(new DateTimeZone(auth()->user()->timezone ?? 'UTC'))->format('h:i A') ?? '' }}
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <div class="btn-group btn-group-sm">
                                                                            <a href="{{ route('counsellor.user.show', $session->user->id) }}"
                                                                                data-bs-toggle="tooltip"
                                                                                title="view profile"
                                                                                class="btn btn-outline-primary">
                                                                                <i class="fa fa-user"></i>
                                                                            </a>
                                                                            @if ($session->report_uploaded)
                                                                                <a href="{{ route('counsellor.session.report.show', ['session' => $session->id, 'report' => $session->report->id]) }}"
                                                                                    data-bs-toggle="tooltip"
                                                                                    title="view report"
                                                                                    class="btn btn-outline-success">
                                                                                    <i class="fa-solid fa-eye"></i>
                                                                                </a>
                                                                            @else
                                                                                <a href="{{ route('counsellor.session.report.create', ['session' => $session->id]) }}"
                                                                                    data-bs-toggle="tooltip"
                                                                                    title="upload report"
                                                                                    class="btn btn-outline-dark">
                                                                                    <i class="fa-solid fa-upload"></i>
                                                                                </a>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('footer.user')
                </div>
            </div>

        </div>
    </div>
@endsection

@extends('layouts.counsellor',['pagename'=>'Session Report'])
@section('css')
<style>
    table.payments tbody td {
        background-color: var(--bs-white) !important;
    }

    table.payments {
        border-collapse: separate !important;
        border-spacing: 0px .5rem !important;
    }

    /* round start and ends of all rows */
    table.payments tbody td:first-child {
        border-top-left-radius: var(--bs-border-radius);
        border-bottom-left-radius: var(--bs-border-radius);
    }

    table.payments tbody td:last-child {
        border-top-right-radius: var(--bs-border-radius);
        border-bottom-right-radius: var(--bs-border-radius);
    }

    body>div.container-fluid.px-0>div>div.col-md-2>div>div>div.pe-3.ps-2.pb-2>div:nth-child(9)>div>a:nth-child(2) {
        color: var(--special) !important
    }

.text-justify {
text-align: justify;
}
</style>
@endsection
@section('counsellor-content')
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="row justify-content-center inner py-3">
                                    <h3 class="col-lg-10 mb-3">Session Review</h3>
                                    <div class="col-lg-10">
                                        <hgroup>
                                            <h6>Reflections from previous session</h6>
                                            <p class="text-muted text-justify">{{ $report->reflections ?? 'N/A' }}</p>
                                        </hgroup>
                                        <hgroup>
                                            <h6>Warm up - What's in your mind</h6>
                                            <p class="text-muted text-justify">{{ $report->warm_up ?? 'N/A' }}</p>
                                        </hgroup>
                                        <hgroup>
                                            <h6>Challenges:</h6>
                                            <p class="text-muted text-justify">{{ $report->challenges ?? 'N/A' }}</p>
                                        </hgroup>
                                        <hgroup>
                                            <h6>Goal setting:</h6>
                                            <p class="text-muted text-justify">{{ $report->goal_setting ?? 'N/A' }}</p>
                                        </hgroup>
                                        <hgroup>
                                            <h6>Commitments:</h6>
                                            <p class="text-muted text-justify">{{ $report->commitments ?? 'N/A' }}</p>
                                        </hgroup>
                                        <hgroup>
                                            <h6>Additional notes:</h6>
                                            <p class="text-muted text-justify">{{ $report->additional_notes ?? 'N/A' }}</p>
                                        </hgroup>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection


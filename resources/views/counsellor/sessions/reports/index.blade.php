@extends('layouts.counsellor',['pagename'=>'Sessions'])
@section('counsellor-content')
@section('css')
<style>
    table.payments tbody td {
        background-color: var(--bs-white) !important;
    }

    table.payments {
        border-collapse: separate !important;
        border-spacing: 0px .5rem !important;
    }

    /* round start and ends of all rows */
    table.payments tbody td:first-child {
        border-top-left-radius: var(--bs-border-radius);
        border-bottom-left-radius: var(--bs-border-radius);
    }

    table.payments tbody td:last-child {
        border-top-right-radius: var(--bs-border-radius);
        border-bottom-right-radius: var(--bs-border-radius);
    }

    .session-report {
        color: var(--special) !important;
    }

    .title::before {
        content: "Session Report"
    }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">
@endsection
<div class="container-fluid px-0">
    <div class="row g-0">
        <!-- side bar -->
        @include('side-bar.counsellor')
        
        <div class="col-lg-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                    data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row g-0">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="inner row">
                                    <div class="col-lg-10 offset-lg-1">
                                        <div class="p-2 p-lg-4">
                                            <table style="vertical-align:middle;" class="table payments table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-center">User</th>
                                                        <th class="text-center">Session Date</th>
                                                        <th class="text-center">Session Time</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($reports as $key => $report)
                                                    <tr>
                                                        <td class="text-center">{{ ++$key }}</td>
                                                        <td class="text-center">
                                                            <a href="{{ route("counsellor.user.show", $report->student->id) }}" class="text-decoration-none">
                                                                {{ $report->student->fullname }}
                                                            </a>
                                                        </td>
                                                        <td class="text-center">{{ date('d-m-Y', strtotime($report->session->session->session_timing)) }}</td>
                                                        <td class="text-center">{{ date('h:i:s A', strtotime($report->session->session->session_timing)) }}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group btn-group-sm">
                                                                <a href="{{ route("counsellor.session.report.show", ['session' => $report->session->id, 'report' => $report->id]) }}" class="btn btn-outline-success" data-bs-toggle="tooltip" title="view report">
                                                                    <i class="fa-solid fa-eye"></i>
                                                                </a>
                                                                <a href="{{ route("counsellor.session.report.edit", ['session' => $report->session->id, 'report' => $report->id]) }}" class="btn btn-outline-secondary" data-bs-toggle="tooltip" title="edit report">
                                                                    <i class="fa-solid fa-pencil"></i>
                                                                </a>
                                                                 <button type="submit" onclick="return confirm('Are you sure you want to delete this report?');" class="btn btn-outline-danger" form="delete_{{ $report->id }}_form">
                                                                    <i class="fa-solid fa-trash"></i>
                                                                 </button>
                                                            </div>
                                                            <form action="{{ route("counsellor.session.report.destroy", ['session' => $report->session->id, 'report' => $report->id]) }}" method="post" hidden id="delete_{{ $report->id }}_form">
                                                                @csrf
                                                                @method("DELETE")
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection

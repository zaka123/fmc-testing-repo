@extends('layouts.counsellor',['pagename'=>'Session Report'])
@section('css')
<style>
    table.payments tbody td {
        background-color: var(--bs-white) !important;
    }

    table.payments {
        border-collapse: separate !important;
        border-spacing: 0px .5rem !important;
    }

    /* round start and ends of all rows */
    table.payments tbody td:first-child {
        border-top-left-radius: var(--bs-border-radius);
        border-bottom-left-radius: var(--bs-border-radius);
    }

    table.payments tbody td:last-child {
        border-top-right-radius: var(--bs-border-radius);
        border-bottom-right-radius: var(--bs-border-radius);
    }

    body>div.container-fluid.px-0>div>div.col-md-2>div>div>div.pe-3.ps-2.pb-2>div:nth-child(9)>div>a:nth-child(2) {
        color: var(--special) !important
    }
</style>
@endsection
@section('counsellor-content')
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light">
            <div class="col-12 bg-white sticky-top d-flex px-3">
                <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                    <i class="fa-solid fa-bars fs-4"></i>
                </button>
                <div class="pb-3 ms-auto">
                    @include('navbars.counsellor')
                </div>
            </div>
            <div class="main" style="min-height:83.2vh">
                <div class="floating"></div>
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="card border-0 pt-3 bg-transparent">
                            <div class="card-body">
                                <div class="row justify-content-center inner py-3">
                                    <h3 class="col-lg-10 mb-3">Session Review</h3>
                                    <div class="col-lg-10">
                                        <form action="{{ route("counsellor.session.report.update", ['session' => $session->id, 'report' => $report->id]) }}" class="needs-validation" method="post">
                                            @csrf
                                            @method("PUT")
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="reflections">Reflections from previous session</label>
                                                <textarea autofocus placeholder="What was the most useful for you?" name="reflections" id="reflections" cols="30" rows="3" class="form-control @error('reflections') is-invalid @enderror">{{ old('reflections') ?? $report->reflections }}</textarea>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="warm_up">Warm up - What's in your mind</label>
                                                <textarea placeholder="What's on your mind?&#10;... and what else?" name="warm_up" id="warm_up" cols="30" rows="3" class="form-control @error('warm_up') is-invalid @enderror" required>{{ old('warm_up') ?? $report->warm_up }}</textarea>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="challenges">Challenges:</label>
                                                <textarea placeholder="What's the real challenge here?&#10;If you're saying &quot;yes&quot; to this, what are you&#10;saying &quot;no&quot; to?&#10;... and what else?" name="challenges" id="challenges" cols="30" rows="5" class="form-control @error('challenges') is-invalid @enderror" required>{{ old('challenges') ?? $report->challenges }}</textarea>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="goal_setting">Goal setting:</label>
                                                <textarea placeholder="What do you want?&#10;... and what else?&#10;... SMART goal setting" name="goal_setting" id="goal_setting" cols="30" rows="3" class="form-control @error('goal_setting') is-invalid @enderror" required>{{ old('goal_setting') ?? $report->goal_setting }}</textarea>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="commitments">Commitments:</label>
                                                <textarea placeholder="Every time [situation or circumstance], I will&#10;[action meant to instill new habit or skill]. I will&#10;be mindful of [old habit or existing emotional&#10;barrier]." name="commitments" id="commitments" cols="30" rows="5" class="form-control @error('commitments') is-invalid @enderror" required>{{ old('commitments') ?? $report->commitments }}</textarea>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label class="fs-5 fw-500" for="extra">Additional notes:</label>
                                                <textarea name="extra" id="extra" cols="30" rows="3" class="form-control @error('extra') is-invalid @enderror" required>{{ old('extra') ?? $report->extra }}</textarea>
                                            </div>
                                            <a href="{{ url()->previous() }}" class="btn btn-secondary">Cancel</a>
                                            <input type="submit" value="Save" class="btn bg-special text-white">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footer.user')
        </div>
    </div>
</div>
@endsection


@extends('layouts.counsellor',['pagename'=>'Profile'])
@section('css')
<style>
    .profile {
        color: var(--special) !important
    }

    .shadow-other {
        box-shadow: 0px 0px 3px grey;
    }
    @media(max-width:991px) {
            .smallResponsive {
                width: 100% !important;
            }
        }
</style>
@endsection
@section('counsellor-content')
<div class="container-fluid px-0">
    <div class="row g-0">
        @include('side-bar.counsellor')
        <div class="col-md-10 bg-light smallResponsive">
            <div class="row g-0">
                <div class="col-12 bg-white sticky-top d-flex px-3">
                    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#sidebar">
                        <i class="fa-solid fa-bars fs-4"></i>
                    </button>
                    <div class="pb-3 ms-auto">
                        @include('navbars.counsellor')
                    </div>
                </div>
                <div class="col-12">
                    <div class="p-3 main pb-5" style="min-height: 83.2vh">
                        <div class="px-md-5 pt-lg-5">
                            <div class="floating"></div>
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="card h-100 border-0 shadow bg-transparent">
                                        <h4 class="card-header text-bg-dark">Personal Information</h4>
                                        <div class="card-body inner">
                                            <div class="row g-3">
                                                <div class="col-4 col-lg-6"><strong>Name:</strong></div>
                                                <div class="col-8 col-lg-6">{{ $counsellorProfile['counsellor']?->full_name }}</div>
                                                <div class="col-4 col-lg-6"><strong>Country:</strong></div>
                                                <div class="col-8 col-lg-6">{{ $counsellorProfile['profile']?->country }}</div>
                                                <div class="col-4 col-lg-6"><strong>Email:</strong></div>
                                                <div class="col-8 col-lg-6">{{ Str::limit($counsellorProfile['counsellor']?->email, 21, '...') }}</div>
                                                <div class="col-4 col-lg-6"><strong>Linkedin:</strong></div>
                                                <div class="col-8 col-lg-6">
                                                    <a class="text-decoration-none" target="_blank" href="{{$counsellorProfile['profile']?->linked_in }}">View Profile</a>
                                                </div>
                                                <div class="col-4 col-lg-6"><strong>Gender:</strong></div>
                                                <div class="col-8 col-lg-6">{{ $counsellorProfile['counsellor']?->gender }}</div>
                                                <div class="col-4 col-lg-6"><strong>Phone Number:</strong></div>
                                                <div class="col-8 col-lg-6">{{ $counsellorProfile['profile']?->i_phone_number }}</div>
                                                <div class="col-4 col-lg-6"><strong>Specialized in:</strong></div>
                                                <div class="col-8 col-lg-6">{{ $counsellorProfile['profile']?->interested_in }}</div>
                                                {{-- <div class="col-4 col-lg-6"><strong>Profession</strong></div>
                                                <div class="col-8 col-lg-6">
                                                    {!! $counsellorProfile['profile']->certified !!}</div> --}}
                                                {{-- <div class="col-12">
                                                    <a href="{{asset($counsellorProfile['profile']->resume_upload ?? '#')}}" class="btn btn-sm btn-primary" download="">
                                                        Download Your Resume
                                                    </a>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card h-100 border-0 shadow bg-transparent">
                                        <h4 class="card-header text-bg-dark">About you!
                                        </h4>
                                        <div class="card-body inner">
                                            <div class="row g-3">
                                                <div class="col-12">
                                                    <div class="card-text">
                                                        {!! $counsellorProfile['profile']?->career_couch !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
</div>
@endsection

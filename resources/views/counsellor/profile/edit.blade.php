@extends('layouts.counsellor', ['pagename' => 'Profile'])
@section('counsellor-content')
<style>
    .saveChanger:hover{
        background-color:#06acf4!important;
    }
    .cancelChanger:hover{
        background-color:black!important;
    }
    .deleteChanger:hover{
        background-color: red!important;
    }
</style>
    <div class="container-fluid px-0">
        <div class="row g-0">
            @include('side-bar.counsellor')
            <div class="col-md-10 bg-light">
                <div class="row g-0 h-100">
                    <div class="col-12 bg-white sticky-top d-flex px-3">
                        <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
                            data-bs-target="#sidebar">
                            <i class="fa-solid fa-bars fs-4"></i>
                        </button>
                        <div class="pb-3 ms-auto">
                            @include('navbars.counsellor')
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="p-3 main h-100">
                            <div class="floating"></div>
                            <form action="{{ url('counsellor/profile/update') }}" class="needs-validation" novalidate
                                method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="inner card">
                                    <div class="card-body">
                                        <div class="row g-3">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">First Name</label>
                                                    <input type="text" name="name"
                                                        value="{{ old('name') ?? $counsellorProfile['counsellor']->first_name }}"
                                                        class="form-control @error('name') is-invalid @enderror" required>
                                                    <div class="invalid-feedback">Please fill in this field.</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="last_name">Last Name</label>
                                                    <input type="text" name="last_name"
                                                        value="{{ old('last_name') ?? $counsellorProfile['counsellor']->last_name }}"
                                                        class="form-control @error('last_name') is-invalid @enderror"
                                                        required>
                                                    <div class="invalid-feedback">Please fill in this field.</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" name="email"
                                                        value="{{ old('email') ?? $counsellorProfile['counsellor']?->email }}"
                                                        class="form-control @error('email') is-invalid @enderror" required>
                                                    <div class="invalid-feedback">Please fill in this field.</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 d-flex">
                                                <div class="form-group">
                                                    <label for="dial_code" class="form-label fw-500">Phone</label>
                                                    <input name="dial_code"
                                                        value="{{ old('dial_code') ?? $counsellorProfile['profile']?->dial_code }}"
                                                        list="dial_code_list" id="dial_code" placeholder="+92"
                                                        class="form-control @error('dial_code') is-invalid @enderror"
                                                        style="width: 5rem" autocomplete="country-code">
                                                    <datalist id="dial_code_list">
                                                        @foreach ($countryDialCodes as $code)
                                                            <option> {{ $code }}</option>
                                                        @endforeach
                                                    </datalist>
                                                    </select>
                                                </div>
                                                <div class="ps-2 w-100">
                                                    <label for="phone_number" class="form-label fw-500"
                                                        style="visibility: hidden;">Cell</label>
                                                    <input type="number" name="phone_number"
                                                        value="{{ old('phone_number') ?? $counsellorProfile['profile']?->phone_number }}"
                                                        class="form-control @error('phone_number') is-invalid @enderror">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="linked_in">Linkedin</label>
                                                    <input type="text" name="linked_in"
                                                        value="{{ old('linked_in') ?? $counsellorProfile['profile']?->linked_in }}"
                                                        class="form-control @error('linked_in') is-invalid @enderror">
                                                    {{-- <div class="invalid-feedback">Please enter a valid url.</div> --}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country</label>
                                                    <select name="country"
                                                        class="form-select @error('country') is-invalid @enderror" required>
                                                        <option value="">Please select your country</option>
                                                        @foreach ($countries as $country)
                                                            <option @selected($country == (old('country') ?? $counsellorProfile['profile']?->country))>{{ $country }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please fill in this field.</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Gender">Gender</label>
                                                    <br>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Male" name="gender"
                                                            @checked($counsellorProfile['counsellor']->gender == 'Male') required> &nbsp; Male
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Female" name="gender"
                                                            @checked($counsellorProfile['counsellor']->gender == 'Female') required> &nbsp; Female
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Other" name="gender"
                                                            @checked($counsellorProfile['counsellor']->gender == 'Other') required> &nbsp; Other
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="interested_in">Specialized In:</label>
                                                    <br>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Coaching" name="interested_in"
                                                            @checked($counsellorProfile['profile']?->interested_in == 'Coaching') required> &nbsp; Coaching
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Mentoring" name="interested_in"
                                                            @checked($counsellorProfile['profile']?->interested_in == 'Mentoring') required> &nbsp; Mentoring
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" value="Both Coaching and Mentoring"
                                                            name="interested_in" @checked($counsellorProfile['profile']?->interested_in == 'Both Coaching and Mentoring') required>
                                                        &nbsp; Both
                                                    </label>
                                                </div>
                                            </div>
                                            {{-- <div class="col-md-6 mt-3">
                                            <div class="form-group">
                                                <label for="coaching_certification">Do You Have Career/life Coaching Certification?</label>
                                                <br>
                                                <label class="radio-inline">
                                                    <input type="radio" class="@error('coaching_certification') is-invalid @enderror" value="on" name="coaching_certification" @checked($counsellorProfile['profile']?->coaching_certification=="on")> &nbsp; Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" value="off" class="@error('coaching_certification') is-invalid @enderror" name="coaching_certification" @checked($counsellorProfile['profile']?->coaching_certification=="off")> &nbsp; No
                                                </label>
                                            </div>
                                        </div> --}}
                                            <div class="col-md-12">
                                                <label for="career_couch">Tell us about yourself</label>
                                                <textarea name="career_couch" id="career_couch" class="form-control @error('career_couch') is-invalid @enderror"
                                                    required rows="5">{!! $counsellorProfile['profile']->career_couch ?? old('career_couch') !!}</textarea>
                                                <div class="invalid-feedback">Please fill in this field.</div>
                                            </div>
                                            <div class="col-md-12 d-flex justify-content-start">
                                                <a class="btn btn-secondary text-white me-2 cancelChanger"
                                                    href="{{ url()->previous() }}">Cancel Changes</a>
                                                <button class="btn bg-special text-white saveChanger">Save Changes</button>
                                                <!-- Delete Account Button -->
                                                <button type="submit" form="deletAccountForm"
                                                class="btn btn-danger btn-sm text-white ms-4 delete-confirm deleteChanger"
                                                data-name="{{ auth()->user()->name }}"
                                                data-original-title="Delete Account">
                                                Delete Account
                                            </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- Delete Account Form -->
                            <form id="deletAccountForm"
                            action="{{ route('counsellor.account.destroy', auth()->user()->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                        </div>
                    </div>
                </div>
                @include('footer.user')
            </div>
        </div>
    </div>
    {{-- Sweet Alert JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        // for post method
        $('.delete-confirm').click(function(event) {
            // var form = $(this).closest("form");
            var form = $('#deletAccountForm');
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Are you sure to delete your account permanantly?`,
                    text: "You won't be able to recover your account!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection

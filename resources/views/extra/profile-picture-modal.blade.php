<style>
    .changerPrev:hover{
        background-color:rgb(15, 15, 230)!important;
    }
    .lookBetter:hover{
        background-color:black!important;
    }
</style>
<div class="modal fade" id="profilePictureModal" tabindex="-1" aria-labelledby="profilePictureModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="profilePictureModalLabel">Upload Picture</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0">
                <form id="profilePictureForm" method="post" action="{{url('user/profile-picture')}}" class="needs-validation"
                    enctype="multipart/form-data" novalidate>
                    @csrf
                    <input class="form-control" type="file" id="photo" name="photo" required accept=".png,.jpg,.jpeg">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary lookBetter btn-sm" data-bs-dismiss="modal">Close</button>
                <button type="submit" form="profilePictureForm" class="btn bg-special text-white btn-sm changerPrev">Upload</button>
            </div>
        </div>
    </div>
</div>
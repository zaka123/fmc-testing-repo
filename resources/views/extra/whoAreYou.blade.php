<div class="container-fluid py-5" data-section="who-are-you">
    <div class="container pb-5">
        <div class="row mb-3">
            <div class="col-12">
                <div class="text-center" style="font-family: Roboto;">
                    {{-- <h1 class="fw-bold"><a href="#who-are-you" class="text-decoration-none text-dark text-uppercase"><span class="text-special">who</span> are you</a></h1> --}}
                    <h2 class="fw-bold"><a href="#get-started" class="text-decoration-none text-dark text-uppercase">WANT TO <span class="text-special">KNOW</span> YOUR <span class="text-special">CAREER</span> PATHWAY</a></h2>
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <p class="fw-light">
                                Our career experts provide confidential support, advice and show the possible solutions, discuss various career plans and developmental needs tailored to your ambitions and skills. We provide our coaching services to range of individuals and institutes, please choice the below options that suits best for you.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center row-cols-1 row-cols-md-2 row-cols-lg-5 gy-4">
            <div class="col">
                <div class="card py-3 px-1 h-100 border-0 text-center">
                    <div class="card-body">
                        <img src="{{asset('assets/img/school_card_img.png')}}" class="w-50" alt="...">
                        <h5 class="card-title text-capitalize text-center">school/college students</h5>
                        <a href="{{url('school')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card py-3 px-2 h-100 border-0 text-center">
                    <div class="card-body">
                        <img src="{{asset('assets/img/university_card_img.png')}}" class="w-50" alt="...">
                        <h5 class="card-title text-capitalize text-center">University Students</h5>
                        <a href="{{url('university')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class=" col">
                <div class="card py-3 px-2 h-100 border-0 text-center">
                    <div class="card-body">
                        <img src="{{asset('assets/img/professional_card_img.png')}}" class="w-50" alt="...">
                        <h5 class="card-title text-capitalize text-center">Working professional</h5>
                        <a href="{{url('Professional')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div>
            {{-- <div class=" col-12 col-md-6 col-lg-3">
                <div class="card py-5 px-4 h-100 border-0 text-center">
                    <div class="card-body">
                        <img src="{{asset('assets/img/parents_card_img.png')}}" class="w-50" alt="...">
                        <h5 class="card-title">Parents Looking for your child</h5>
                        <a href="{{url('Parents')}}" class="stretched-link"></a>
                    </div>
                </div>
            </div> --}}
        </div>
    </div> <!-- container -->
</div> <!-- container-fluid -->

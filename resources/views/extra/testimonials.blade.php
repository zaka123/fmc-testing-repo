<div class="container-fluid pb-3 pt-1" data-section="reviews">
    <div class="container pb-3">
        <h1 class=" text-center mt-5 mb-3">
            <a href="#what-our-customer-are-saying" class="text-decoration-none text-dark">TESTIMONIALS</a>
        </h1>
        @php
        $testimonials = [
            [
                "name" => "Ateeq",
                "message" => "\"After a transformative session with my career coach, I now have absolute clarity on the subject for my master's program. Their expertise and guidance helped me uncover my true passion and align it with my academic pursuits. Grateful for the invaluable support!\""
            ],
            [
                "name" => "Khalid",
                "message" => "\"Thanks to my career coach, I am now confident about the subject for my master's program. Their thoughtful questioning and insightful advice helped me explore my interests, evaluate options, and make an informed decision. Excited to embark on this educational journey!\""
            ],
            [
                "name" => "Ayesha",
                "message" => "\"After an eye-opening session with my career coach, I finally discovered the perfect subject for my master's program. Their expertise and guidance helped me align my passions and strengths, giving me the confidence to pursue my academic goals.\""
            ],
            [
                "name" => "Minahil",
                "message" => "\"I cannot thank my career coach enough for helping me navigate the overwhelming choices for my master's program. With their support, I gained clarity and found the ideal subject that matches my interests and career aspirations. I'm excited to embark on this educational journey!\""
            ],
            [
                "name" => "Omer",
                "message" => "\"I had an incredible session with my career coach. Their personalized guidance and insights helped me gain clarity, set meaningful goals, and take confident steps towards achieving my professional aspirations. Highly recommend!\""
            ],
            [
                "name" => "Saira",
                "message" => "\"Working with my career coach was a game-changer! Their personalized guidance and insightful advice helped me gain clarity, set meaningful goals, and take confident steps towards my professional success.\""
            ]
        ];
        @endphp
        <div class="swiper px-3 p-5">
            <div class="swiper-wrapper">
                @foreach($testimonials as $testimonial)
                <div class="swiper-slide h-100">
                    <div class="card h-100 border border-info">
                        <div class="card-body pb-5">
                            <div class="d-flex mb-2 align-items-end">
                                <!-- <img src="{{asset('assets/img/benton.jpg')}}" alt="benton" class="rounded-circle" width="80" height="80"> -->
                                <h3 class="card-title ms-2">
                                    <div class="text-justify">{{ $testimonial['name'] }}</div>
                                    <!-- <div class="ratings">
                                        <div class="empty-stars"></div>
                                        <div class="full-stars" style="width:50%"></div>
                                    </div> -->
                                </h3>
                            </div>
                            <p class="card-text text-justify">
                            {{ $testimonial['message']}}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div> <!-- swiper wrapper -->
            <div class="swiper-pagination"></div>
        </div> <!-- swiper -->
    </div> <!-- container -->
</div> <!-- container-fluid -->
@push('script')
<script>
const swiper = new Swiper('.swiper', {
    loop: true,
    autoHeight: true,
    spaceBetween: 35,
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
        clickable: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        768: {
            slidesPerView: 2,
            spaceBetween: 50,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 50,
        }
    }
});

</script>
@endpush
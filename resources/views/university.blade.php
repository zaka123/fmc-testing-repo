@extends('layouts.navbar', ['title' => "University Student"])
@section('css')
<style>
    body {
        background-color: rgba(108, 187, 222, 20%) !important;
        background-image: url('/assets/icons/index.svg');
        background-size: cover;
    }
    .changerBackLook:hover{
        background-color:black!important;
    }
</style>
@endsection
@section('content')
<div>

    <div class="container pt-lg-5 mb-3 mb-lg-0" style="min-height:84vh">
        <div class="row">
            <div class="col-lg-7">
                <h3 class="text-special">
                    MAKE YOUR JOURNEY/TRANSITION FROM EDUCATION TO EMPLOYEMENT SEAMLESSLY WITH OUR CAREER EXPERTS!
                </h3>
                <p class="fw-light text-justify">
                    Acquiring the right carrer based on your skills, aptitude, and future goals immediately after
                    education is not straight forword.
                    <br>
                    <b>FIND ME CAREER</b> help you to make your transition smooth by connecting you with the small
                    expert in your filed to share inspiring thoughts, fresh perspective and discussion on topics that
                    matter you.
                    <br>
                    Our experts make personalized plan with set targets and provide relevent advice to counsel and
                    motivate you.
                </p>
                <p>
                    <a href="{{url('register','University Student')}}"
                        class="d-lg-none btn btn-sm changerBackLook bg-special rounded-3">Let's get Started</a>
                    <a href="{{url('register','University Student')}}"
                        class="d-none d-lg-inline-block changerBackLook btn bg-special rounded-3">Let's get Started</a>
                </p>
            </div>
            <div class="col-lg-5 text-center">
                <img src="{{asset('assets/img/university student.png')}}" class="img-fluid" alt="university students">
            </div>
        </div>
    </div>

</div>
@include('newsletter_form')
@endsection
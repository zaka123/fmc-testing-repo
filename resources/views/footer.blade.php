<style>
    .effectHover:hover{
        color: black!important;
    }
    .hoverChanger:hover{
        color: #6CBBDE!important;
    }
</style>
<footer id="footer" class="container-fluid bg-special">
    <div class="container pt-5 pb-2">
        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-4 g-3">
            <div class="col">
                <div class="text-center text-lg-start">
                    <p class="mb-3">
                        <a href="{{ url('/') }}">
                            <img class="img-fluid" src="{{ asset('assets/img/logo.png') }}" alt="">
                        </a>
                    </p>
                    <p class="text-capitalize">
                        make informed career decisions and new thinking perspectives for better future.
                    </p>
                    <div id="icon">
                        <a href="https://www.linkedin.com/company/findmecareer" target="_blank"
                            style="color: white"><span class="me-1"><i
                                    class="fa-brands fa-linkedin fa-2xl effectHover"></i></span></a>
                        <a href="https://www.facebook.com/profile.php?id=100092629940766" target="_blank"
                            style="color: white"><i class="fa-brands fa-square-facebook fa-2xl effectHover"></i></a>
                    </div>
                </div>
            </div>
            <div class="col">
                <p class="h4 mb-3">Company</p>
                <p>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('about') }}">About us</a>
                </p>
                <p>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('help') }}">Help center</a>
                </p>
                <p>
                    <a class="text-decoration-none text-capitalize text-white effectHover"
                        href="#what-our-customer-are-saying">testimonials</a>
                </p>
            </div>
            <div class="col">
                <p class="h4 mb-3">Links</p>
                <div>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('counsellor.apply') }}">Careers</a>
                </div>
                <div>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('faqs') }}">FAQs</a>
                </div>
                <div>
                    <a class="text-decoration-none text-white effectHover" href="{{ url('contact') }}">Contact us</a>
                </div>
                <div>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('privacy') }}">Privacy Policy</a>
                </div>
                <div>
                    <a class="text-decoration-none text-white effectHover" href="{{ route('terms-of-use') }}">Terms of use</a>
                </div>
            </div>
            <div class="col">
                <p class="h4 mb-3">Contact</p>
                <p>
                    <a class="text-decoration-none text-white effectHover" href="tel:+(44) 736 797 9379">+(44) 736 797 9379</a>
                </p>
                <p>
                    <a class="text-decoration-none text-white effectHover"
                        href="mailto:contact@findmecareer.com ">contact@findmecareer.com</a>
                </p>
                <p class="">
                    <a class="text-decoration-none text-white effectHover" href="https://goo.gl/maps/1YHpKmMeFnvT1MnE6">41 Robinson
                        street, Milton Keynes, United Kingdom</a>
                </p>
            </div>
        </div>
    </div>
</footer>
<footer class="text-bg-dark px-4 py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-7">&copy; Copyright {{ now()->format('Y') }} Find Me Career. <span
                    class="d-inline d-md-none"><br></span>All rights reserved</div>
            <div class="col-md-5 text-center text-md-end">Powered by <a href="https://www.theeducatics.com/"
                    class="text-decoration-none text-white hoverChanger">The Educatics</a> | Made with <i
                    class="fa-solid fa-heart text-danger"></i> by <a href="https://lastwavetechnology.com"
                    class="text-decoration-none text-white hoverChanger" target="_blank">LWT</a>
            </div>
        </div>
    </div>
</footer>

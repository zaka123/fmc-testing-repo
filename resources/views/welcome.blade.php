@extends('layouts.navbar')
@section('css')
    <style>
        #navbarNav>ul>li:nth-child(1)>a:not([class*="dark"]) {
            background-color: var(--special) !important;
            color: var(--bs-white) !important
        }

        .hoversEffect:hover {
            background-color:black!important;
        }
    </style>
@endsection
@section('content')
    <!-- paste your page contents here -->
    <div class="container settings">
        <div class="pt-3 mb-2">
            <div class="row g-0 align-items-center user-select-none">
                <div class="col-lg-6">
                    <hgroup>
                        <h1 class="text-uppercase display-5 fw-bold">
                            <span class="text-special">Make informed career decisions</span> with our career coaches.
                            {{-- <span class="text-special text-uppercase">the world</span> most advance <span class="text-special">career counselling</span> portal --}}
                        </h1>
                        <p class="fw-light ">
                            <span class="fw-bold">FIND ME CAREER</span> empowers you to achieve your academic and professional
                            aspirations through expert guidance. Our skilled coaches and mentors will guide you in
                            discovering your unique skills, talents and passions, identifying a fulfilling career path, and
                            set actionable goals to bring your vision to life. Join us to unlock your full potential and
                            create the life and career you truly desire.
                        </p>
                    </hgroup>
                </div>
                <div class="col-lg-6 text-center text-lg-end order-2 order-lg-1">
                    <img class="img-fluid" src="{{ asset('assets/img/main.png') }}" alt="main cover photo">
                </div>
                <div class="col-12 order-1 order-lg-2">
                    <a class="btn bg-special hoversEffect px-2 py-1" href="{{ route('who-are-you') }}">Get started</a>
                    <span class="visually-hidden" id="get-started"></span>
                </div>
            </div> <!-- row -->
        </div> <!-- padding box -->
    </div> <!-- container -->
    <!-- who are you section -->
    @include('extra.whoAreYou')
    <!-- / who are you -->
    <div class="container-fluid" data-section="best-coaches">
        <div class="container">
            <div class="row align-items-center mb-5 g-3">
                <div class="col-lg-5">
                    <div class="text-center">
                        <img src="{{ asset('assets/img/best_coaches.png') }}" class="img-fluid" alt="peoples">
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="ps-lg-5">
                        <hgroup>
                            {{-- <h1 class="text-center text-lg-start">Best Coach On <span class="text-special">Your Finger</span> Tips</h1> --}}
                            <h2 class="text-center fw-bold text-lg-start">CAREER <span class="text-special">COACHING</span>
                                PROGRAMME</h2>
                            <p class="fw-light">
                                Our <b>career coaches</b> are highly qualified and trained individuals with experience in
                                dealing range of education and work-related issues. They analyse the results, discuss with
                                you and advice you regarding your career ambitions. Moreover, they will create the
                                <b>SMART</b> career goals for you to follow
                            </p>
                        </hgroup>
                    </div>
                    <span class="visually-hidden" id="subscription-option"></span>
                </div>
                <span id="what-our-customer-are-saying"></span>
            </div> <!-- row -->
            <div class="row justify-content-around g-2 g-lg-4">
                {{-- <h1 class="text-center mb-5">
                <a href="#subscription-option" class="text-decoration-none text-dark"><span class="text-special">Subscription</span> Option</a>
            </h1>
            <div class="col-lg-5  pe-lg-0">
                <div class="card h-100 border border-info rounded-4 text-center position-relative">
                    <div class="card-body fw-100">
                        <h3 class="card-title text-center fw-bold text-special">Basic</h3>
                        <h3 class="text-center"><strong><sup>$</sup>50<sub>/month</sub></strong></h3>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <a href="#" type="button" class="btn bg-special btn-sm text-white">Buy now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 d-none d-lg-block">
            </div>
            <div class="col-lg-5  ps-lg-0">
                <div class="card h-100 border border-info rounded-4 text-center position-relative">
                    <div class="card-body fw-100">
                        <h3 class="card-title text-center fw-bold text-special">Premium</h3>
                        <h3 class="text-center"><strong><sup>$</sup>80<sub>/month</sub></strong></h3>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <p><i class="fa-solid fa-check"></i> Lorem ipsum dolor sit amet, consectetur</p>
                        <a href="#" type="button" class="btn bg-special btn-sm text-white">Buy now</a>
                    </div>
                </div>
            </div> --}}
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- container-fluid -->
    {{-- <div class="container-fluid pb-3 pt-1" data-section="reviews">
        <div class="container pb-3">
            <h1 class=" text-center mt-5 mb-3">
                <a href="#what-our-customer-are-saying" class="text-decoration-none text-dark">TESTIMONIALS</a>
            </h1>
            <div class="swiper px-3 p-5">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="card h-100 border border-info">
                            <div class="card-body pb-5">
                                <div class="d-flex mb-2 align-items-end">
                                    <img src="{{ asset('assets/img/benton.jpg') }}" alt="benton" class="rounded-circle"
                                        width="80" height="80">
                                    <h3 class="card-title ms-2">
                                        <div>Benton</div>
                                        <div class="ratings">
                                            <div class="empty-stars"></div>
                                            <div class="full-stars" style="width:50%"></div>
                                        </div>
                                    </h3>
                                </div>
                                <p class="card-text">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut veniam, ducimus voluptatum
                                    qui assumenda cupiditate debitis ea corrupti labore, iure quis. Sed tenetur soluta non
                                    quas ut nulla asperiores in.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card h-100 border border-info">
                            <div class="card-body pb-5">
                                <div class="d-flex mb-2 align-items-end">
                                    <img src="{{ asset('assets/img/parmer.jpg') }}" alt="parmer" class="rounded-circle"
                                        width="80" height="80">
                                    <h3 class="card-title ms-2">
                                        <div>Parmer</div>
                                        <div class="ratings">
                                            <div class="empty-stars"></div>
                                            <div class="full-stars" style="width:40%"></div>
                                        </div>
                                    </h3>
                                </div>
                                <p class="card-text">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut veniam, ducimus voluptatum
                                    qui assumenda cupiditate debitis ea corrupti labore, iure quis. Sed tenetur soluta non
                                    quas ut nulla asperiores in.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card h-100 border border-info">
                            <div class="card-body pb-5">
                                <div class="d-flex mb-2 align-items-end">
                                    <img src="{{ asset('assets/img/Ellipse 32.png') }}" alt="Ahmad"
                                        class="rounded-circle" width="80" height="80">
                                    <h3 class="card-title ms-2">
                                        <div>Ahmad</div>
                                        <div class="ratings">
                                            <div class="empty-stars"></div>
                                            <div class="full-stars" style="width:40%"></div>
                                        </div>
                                    </h3>
                                </div>
                                <p class="card-text">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut veniam, ducimus voluptatum
                                    qui assumenda cupiditate debitis ea corrupti labore, iure quis. Sed tenetur soluta non
                                    quas ut nulla asperiores in.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card h-100 border border-info">
                            <div class="card-body pb-5">
                                <div class="d-flex mb-2 align-items-end">
                                    <img src="{{ asset('assets/img/Ellipse 32.png') }}" alt="Ahmad"
                                        class="rounded-circle" width="80" height="80">
                                    <h3 class="card-title ms-2">
                                        <div>Harry</div>
                                        <div class="ratings">
                                            <div class="empty-stars"></div>
                                            <div class="full-stars" style="width:40%"></div>
                                        </div>
                                    </h3>
                                </div>
                                <p class="card-text">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut veniam, ducimus voluptatum
                                    qui assumenda cupiditate debitis ea corrupti labore, iure quis. Sed tenetur soluta non
                                    quas ut nulla asperiores in.
                                </p>
                            </div>
                        </div>
                    </div>
                </div> <!-- swiper wrapper -->
                <div class="swiper-pagination"></div>
            </div> <!-- swiper -->
        </div> <!-- container -->
    </div> --}}
    @include('extra.testimonials')
    <!-- newsletter subscription -->
    @include('newsletter_form')
@endsection
@push('script')
    <script>
        const swiper = new Swiper('.swiper', {
            loop: true,
            autoHeight: true,
            spaceBetween: 35,
            pagination: {
                el: '.swiper-pagination',
                dynamicBullets: true,
                clickable: true,
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 50,
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                }
            }
        });
    </script>
@endpush

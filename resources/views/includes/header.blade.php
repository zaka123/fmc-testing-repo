<div class="bg-white text-special pt-3 ps-3 d-flex align-items-center justify-content-between mt-3">
    <button class="btn btn-sm d-inline-block d-lg-none" data-bs-toggle="offcanvas"
        data-bs-target="#sidebar">
        <i class="fa-solid fa-bars fs-4"></i>
    </button>
    <h3 class="d-none pt-2 d-lg-inline">
        <span class="d-flex align-items-center special_monstrate_text text-dark gap-2">Welcome,
            <svg width="24" height="24" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill="#ffba47"
                    d="M17.924 17.315c-.057.174-.193.367-.416.432c-.161.047-5.488 1.59-5.652 1.633c-.469.125-.795.033-1.009-.156c-.326-.287-4.093-2.85-8.845-3.092c-.508-.025-.259-1.951 1.193-1.951c.995 0 3.904.723 4.255.371c.271-.272.394-1.879-.737-4.683L4.438 4.232a1.045 1.045 0 0 1 1.937-.781L8.361 8.37c.193.48.431.662.69.562c.231-.088.279-.242.139-.709L7.144 2.195A1.043 1.043 0 0 1 7.796.871a1.042 1.042 0 0 1 1.325.652l1.946 5.732c.172.504.354.768.642.646c.173-.073.161-.338.115-.569l-1.366-5.471a1.045 1.045 0 1 1 2.027-.506l1.26 5.042c.184.741.353 1.008.646.935c.299-.073.285-.319.244-.522l-.872-4.328a.95.95 0 0 1 1.86-.375l.948 4.711l.001.001v.001l.568 2.825c.124.533.266 1.035.45 1.527c1.085 2.889.519 5.564.334 6.143" />
            </svg>
        </span>
        <span class="spacial_bold_monstrate_text animate__animated d-block"
            id="userName">{{ auth()->user()->name }}</span>
    </h3>
    <div class="d-flex align-items-center justify-content-center gap-4 pe-3">
        {{-- the searching stuff is here --}}
        {{-- <div class="cursor-pointer">
            <svg width="30" height="30" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path fill="#496C8C"
                    d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396l1.414-1.414l-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8s3.589 8 8 8m0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6s-6-2.691-6-6s2.691-6 6-6" />
                <path fill="#496C8C"
                    d="M11.412 8.586c.379.38.588.882.588 1.414h2a3.977 3.977 0 0 0-1.174-2.828c-1.514-1.512-4.139-1.512-5.652 0l1.412 1.416c.76-.758 2.07-.756 2.826-.002" />
            </svg>
        </div> --}}
        {{-- the notifications bell is here --}}
        {{-- <div class="position-relative cursor-pointer">
            <svg width="20.49" height="24" viewBox="0 0 700 820" xmlns="http://www.w3.org/2000/svg">
                <path fill="#496C8C"
                    d="M579 372q0 24 9 45t25 37t37 25t45 9v93H0v-93q24 0 45-9t37-25t25-37t9-45V233q0-48 18-90t50-74t73-50t90-18t90 18t74 50t50 74t18 90zM347 696q-32 0-56-20t-33-49h179q-8 30-32 49t-58 20" />
            </svg>
            <span class="position-absolute bell">8</span>
        </div> --}}
        {{-- the user profile and small image is here --}}
        <div class="d-flex align-items-center gap-2 position-relative" style="margin-top: -7px">
            <img src="{{ asset(auth()->user()->profile_photo) }}" alt=".." class="rounded-circle"
                width="40px" height="40px" style="border: 2px solid #729CC4">
            <span class="text-dark special_monstrate_text cursor-pointer" id="userInfoButton"
                style="font-weight: 600">{{ auth()->user()->name }} <i class="bi bi-chevron-down"></i></span>
            <div class="position-absolute rounded-4" id="userInfo">
                <ul class="w-100 h-100 m-0 p-2 py-3">
                    {{-- for email --}}
                    <li class="special_monstrate_text">{{ auth()->user()->email }}</li>
                    <hr class="centered">
                    {{-- for profile  --}}
                    <li class="special_monstrate_text">
                        <a href="{{ route('user.settings.index') }}" class="text-dark gap-3">
                            <i class="fa-solid fa-user"></i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <hr class="centered">
                    {{-- for payments --}}
                    <li class="special_monstrate_text">
                        <a href="{{ url('user/payments') }}" class="text-dark gap-3">
                            <i class="fa-solid fa-credit-card"></i>
                            <span>Payments</span>
                        </a>
                    </li>
                    <hr class="centered">
                    {{-- for sessions --}}
                    <li class="special_monstrate_text">
                        <a href="{{ route('user.session.index') }}" class="text-dark gap-3">
                            <i class="fa-solid fa-clock"></i>
                            <span>Sessions</span>
                        </a>
                    </li>
                    <hr class="centered">
                    {{-- for logout --}}
                    <li class="special_monstrate_text">
                        <a href="javascript:void(0)" class="text-dark gap-3" onclick="$('#logout').submit();">
                            <i class="fa-solid fa-right-from-bracket"></i>
                            {{ __('Logout') }}
                        </a>
                        <form action="{{ route('logout') }}" id="logout" method="POST">@csrf
                        </form>
                    </li>
                    <hr class="centered">
                </ul>
            </div>
        </div>
    </div>
</div>

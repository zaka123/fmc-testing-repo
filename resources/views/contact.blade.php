@extends('layouts.new_app', ['pagename' => 'Contact us'])
@section('css')
    <style>
        .text-special {
            color: #749BC2 !important;
        }

        .row {
            width: 98vw;
        }

        .contact_text h3 {
            font-family: myFont;
            margin-bottom: 35px;
        }

        .contact_text p {
            text-align: justify;
            font-size: 17px;
            font-weight: 400;
        }

        .contact_form {
            display: flex;
            align-content: center;
            justify-content: end;
            margin-top: 80px;
        }

        @media(max-width:768px) {
            .contact_form {
                margin-top: 5px !important;
            }
        }

        .contact_form form {
            width: 47% !important;
            border-radius: 12px;
            background-color: white;
            box-shadow: 2px 2px 15px rgb(208, 207, 207);
            padding: 15px;
        }

        @media(max-width:768px) {
            .contact_form form {
                width: 100% !important;
            }
        }

        .contact_form form .form-group {
            width: 100%;
            max-width: 100%;
            margin-bottom: 10px;
        }

        .sendButton button {
            background: #749BC2;
            color: white;
        }

        .sendButton button:hover {
            background: #000000;
            color: white;
        }

        input {
            border: 1px solid #3d74ab !important;
            border-radius: 10px !important;
            height: 30px;
            padding-left: 32px;
            padding-right: 10px;
        }

        input:focus {
            outline: none !important;
            box-shadow: none !important;
        }

        textarea {
            border: 1px solid #3d74ab !important;
        }

        textarea:focus {
            outline: none !important;
            box-shadow: none !important;
        }

        .login {
            width: 50%;
            display: flex;
            flex-direction: column;
            justify-content: start;
            align-items: center;
        }

        @media(max-width:992px) {
            .login {
                width: 92vw;
                margin-left: 0px !important;
                margin-top: 0px !important;
                padding-top: 0px !important;
            }
        }

        .login p {
            font-size: 18px;
            font-weight: 400;
        }

        .login a {
            color: #749BC2;
            text-decoration: none;
        }

        form {
            border-radius: 12px;
            width: 293px !important;
            background-color: white;
            box-shadow: 2px 2px 15px rgb(208, 207, 207);
            display: flex;
            padding-top: 23px;
            padding-bottom: 22px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
        }

        form.active {
            z-index: -1;
        }

        @media(max-width:992px) {
            form {
                margin: auto;
            }
        }

        @media(max-width:330px) {
            form {
                width: 92vw !important;
            }
        }

        .input {
            display: flex;
            flex-direction: column;
            width: 244px !important;
            position: relative;
        }

        .active {
            margin-bottom: 10px;
        }

        .input input {
            width: 100% !important;
        }

        .input svg,
        .input img {
            position: absolute;
            top: 29px;
            left: 10px;
        }

        .psw_lock {
            margin-top: 2px;
            margin-left: 2px;
        }

        .eye {
            position: absolute;
            left: 90% !important;
            top: 32px !important;
            font-size: 12px;
            cursor: pointer;
            z-index: 2 !important;
        }

        .eye:hover {
            scale: 1.1;
        }

        .input label {
            color: #749BC2;
            font-weight: 500;
        }

        .input input {
            border: 1px solid #3d74ab;
            border-radius: 10px;
            height: 30px;
            width: 240px;
            padding-left: 32px;
            padding-right: 10px;
        }

        #password {
            padding-right: 30px !important;
        }

        #password_confirmation {
            padding-right: 30px !important;
        }

        .input input:focus {
            outline: none;
        }

        .checkbox {
            width: 82%;
            margin-bottom: 15px;
        }

        .checkbox input {
            accent-color: #749BC2;
        }

        .signup {
            width: 242px;
            border: none;
            background-color: #749BC2;
            color: white;
            padding: 5px;
            border-radius: 9px;
        }

        .signup:hover {
            background-color: #000000;
        }

        footer {
            display: flex;
            background-color: #E3EBF3;
            justify-content: space-between;
            align-items: center;
            padding: 10px;
        }

        @media(min-width:1300px) {
            footer {
                position: fixed;
                bottom: 0;
                width: 100vw;
            }
        }

        @media(max-width:768px) {
            footer {
                flex-direction: column;
            }
        }

        footer span {
            font-weight: 500;
        }

        .copyright {
            padding-left: 20px;
        }

        @media(max-width:768px) {
            footer {
                text-align: center;
            }
        }

        .privacy {
            padding-right: 40px;
        }

        .lwt:hover {
            color: #749BC2 !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-6 mt-md-5" style="height: 300px">
            <div class="mt-md-5" style="width: 100%;height:100%">
                <img src="{{ asset('new_design_assets/img/new/contact_us.svg') }}" alt="" width="100%" height="100%"
                    style="max-width: 100%;object-fit:contain">
            </div>
        </div>
        <div class="col-md-6 col-12 pt-md-4">
            <div class="login  m-auto">
                <h3><b>Contact us</b></h3>
                <form action="{{ route('contact-us.store') }}" method="POST">
                    @csrf
                    <div class="input active">
                        <label>Name<span class="text-danger">*</span></label>
                        <svg width="17" height="17" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"
                            style="margin-top: 1px;margin-left:1px">
                            <path fill="#749bc2"
                                d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0S96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z" />
                        </svg>
                        <input type="text" name="name" id="name" value="{{ old('name') }}">
                        @error('name')
                            <small class="text-danger">{{ $message }}</small>
                            <script>
                                document.querySelector('.active').classList.remove('active');
                            </script>
                        @enderror
                    </div>
                    <div class="input active">
                        <label>Email<span class="text-danger">*</span></label>
                        <svg width="20" height="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path fill="#749bc2"
                                d="M20 4H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2zm0 4.7l-8 5.334L4 8.7V6.297l8 5.333l8-5.333V8.7z" />
                        </svg>
                        <input type="email" name="email" id="email" value="{{ old('email') }}">
                        @error('email')
                            <small class="text-danger">{{ $message }}</small>
                            <script>
                                document.querySelector('.active').classList.remove('active');
                            </script>
                        @enderror
                    </div>
                    <div class="input">
                        <label>Phone</label>
                        <svg width="20" height="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path fill="#749bc2"
                                d="m20.487 17.14l-4.065-3.696a1.001 1.001 0 0 0-1.391.043l-2.393 2.461c-.576-.11-1.734-.471-2.926-1.66c-1.192-1.193-1.553-2.354-1.66-2.926l2.459-2.394a1 1 0 0 0 .043-1.391L6.859 3.513a1 1 0 0 0-1.391-.087l-2.17 1.861a1 1 0 0 0-.29.649c-.015.25-.301 6.172 4.291 10.766C11.305 20.707 16.323 21 17.705 21c.202 0 .326-.006.359-.008a.992.992 0 0 0 .648-.291l1.86-2.171a.997.997 0 0 0-.085-1.39" />
                        </svg>
                        <div class="d-flex align-items-center">
                            <input name="dial_code" list="dial_code_list" id="dial_code" placeholder="+92"
                                autocomplete="country-code"
                                style="width: 70px!important;border-top-right-radius:0px!important;border-bottom-right-radius:0px!important;border-right:0!important">
                            <input class="form-control"  type="tel" name="phone_number"
                                id="phone_number" value="{{ old('phone_number') }}"
                                style="border-top-left-radius:0px!important;border-bottom-left-radius:0px!important;padding-left:5px!important">
                        </div>
                        <datalist id="dial_code_list">
                            @foreach ($countryDialCodes as $code)
                                <option @selected(old('dial_code') == $code)>{{ $code }}</option>
                            @endforeach
                        </datalist>
                        @error('dial_code')
                            <small class="text-danger mt-0">{{ $message }}</small>
                        @enderror
                        @error('phone_number')
                            <small class="text-danger mt-0">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="input my-3">
                        <label>Message</label>
                        <textarea name="message" id="message" class="form-control">{{ old('message') }}</textarea>
                        @error('message')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <button class="signup">Send</button>
                </form>
            </div>
        </div>
    </div>
@endsection























































































{{-- <div class="col-12 col-md-6 contact_form">
            <form action="{{ route('contact-us.store') }}" method="POST" class="border">
                @csrf
                <div class="form-group">
                    <label class="form-label" for="name">Name<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                    @error('name')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="form-label" for="email">Email<span class="text-danger">*</span></label>
                    <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}">
                    @error('email')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="d-flex  w-100">
                    <div class="form-group">
                        <label class="form-label" for="phone_number">Phone</label>
                        <input name="dial_code" list="dial_code_list" id="dial_code" placeholder="+92" class="form-control"
                            style="width: 5rem" autocomplete="country-code">
                        <datalist id="dial_code_list">
                            @foreach ($countryDialCodes as $code)
                                <option @selected(old('dial_code') == $code)>{{ $code }}</option>
                            @endforeach
                        </datalist>
                    </div>
                    <div class="ms-3">
                        <label for="phone_number" class="form-label fw-500 text-white user-select-none">Phone</label>
                        <input class="form-control" placeholder="300 000 0000" type="tel" name="phone_number"
                            id="phone_number" value="{{ old('phone_number') }}">
                    </div>
                </div>
                @error('dial_code')
                    <span class="text-danger mt-0">{{ $message }}</span>
                @enderror
                @error('phone_number')
                    <span class="text-danger mt-0">{{ $message }}</span>
                @enderror
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" class="form-control">{{ old('message') }}</textarea>
                    @error('message')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="sendButton w-100">
                    <button class="w-100 btn bg-special">Send</button>
                </div>
            </form>
        </div> --}}

{{-- @extends('layouts.navbar', ["title" => "FAQ's - "]) --}}
@extends('layouts.new_app')
@section('css')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap');

        h1 {
            font-family: 'Roboto', sans-serif;
            font-size: 4rem;
        }

        @media(max-width:1200px) {
            h1 {
                font-size: calc(1.375rem + 1.5vw) !important;
            }
        }

        .accordion-button {
            font-family: 'Roboto', sans-serif;
            font-size: 1rem;
        }
        .accordion-body {
            font-family: 'Roboto', sans-serif;
            font-size: 1rem;
        }
        .row {
            overflow: auto
        }

        .row {
            width: 97vw !important;
            height: 80vh !important;
            scrollbar-width: thin;
            overflow-x: hidden;
            scrollbar-color: #749BC2 transparent;
        }

        .row::-webkit-scrollbar {
            width: 5px;
            height: 8px;
            background-color: #F3F5F7;
        }

        .row::-webkit-scrollbar-thumb {
            background-color: #749BC2;
            border-radius: 100px !important;
        }

        .row {
            --bs-gutter-x: 1rem !important;
        }
        #sidebarView{
            z-index: 9!important;
        }
    </style>
@endsection
@section('content')
    <div class="main pt-5">
        <div class="row g-5">
            <div class="col-12 border-bottom">
                <div class="text-center">
                    <h1>FAQs</h1>
                </div>
            </div>
            <div class="col-12">
                <div class="">
                    <div class="row g-1">
                        @foreach ($faqGroups as $faqGroup)
                            <div class="col-md-6">
                                <div class="accordion accordion-flush bg-transparent" id="testFAQS"
                                    style="--bs-accordion-bg:transparent;">
                                    @forelse($faqGroup as $faq)
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="test-heading{{ $faq->id }}">
                                                <button class="accordion-button collapsed" type="button"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target="#testQuestion{{ $faq->id }}" aria-expanded="false"
                                                    aria-controls="testQuestion{{ $faq->id }}">
                                                    {!! $faq->question !!}
                                                </button>
                                            </h2>
                                            <div id="testQuestion{{ $faq->id }}" class="accordion-collapse collapse"
                                                aria-labelledby="test-heading{{ $faq->id }}"
                                                data-bs-parent="#testFAQS">
                                                <div class="accordion-body">{!! $faq->answer !!}</div>
                                            </div>
                                        </div>
                                    @empty
                                        No questions found
                                    @endforelse
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
    .hoversEffect:hover{
        background-color:black!important;
    }
</style>
<div id="sign-in" class="container-fluid bg-white py-5">
    <div class="py-5">
        <div class="text-center user-select-none fw-500">
            <h3><a class="text-decoration-none text-dark" href="#sign-in">Join our community</a></h3>
            <div class="fs-2  text-capitalize">Subscribe <span class="text-special">To Our</span> Newsletter</div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5 col-xxl-3">
                <form action="{{ route("subscribe") }}" autocomplete="off" method="post">
                    @csrf
                    <div class="input-group border border-info p-1 rounded-pill align-items-center">
                        <label for="email"><i class="fa-solid fa-envelope fs-3 text-special my-1 mx-2"></i></label>
                        <input id="email" type="email" name="email" required class="form-control border-0 text-info me-1" placeholder="Enter Your Email" aria-label="Recipient's username">
                        <input type="submit" hidden id="submit">
                        <span onclick="document.getElementById('submit').click();" class="input-group-text py-2 hoversEffect rounded-pill btn bg-special">Subscribe</span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="{{ asset('assets/errors/style.css') }}">
    <link rel="stylesheet" href="{{ asset('style/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>
        @yield('title')
    </title>
    @yield('styles')
</head>

<body>
    {{-- @include('navbars.navbar') --}}
    {{-- <nav class="navbar navbar-expand-lg mb-1 navbar-light pt-3 sticky-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('assets/img/logo.png') }}" class="fluid-img" alt="career-counselling brand">
            </a>
            <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse pt-3 pt-lg-0" id="navbarNav">
                <ul class="navbar-nav ms-auto nav-pills">
                    <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                        <a class="nav-link" href="{{ route('about') }}">About us</a>
                    </li>
                    <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                        <a class="nav-link" href="{{ url('contact') }}">Contact us</a>
                    </li>
                    @auth
                        <li class="nav-item dropdown mb-2 mb-lg-0  me-0 me-lg-3">
                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" href="{{ url('dashboard') }}"><i
                                            class="fa fa-tachometer-alt"></i> Dashboard</a></li>
                                <li><a class="dropdown-item" href="{{ route('logout') }}" type="submit"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                            class="fa-solid fa-right-from-bracket me-2"></i>{{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" hidden>@csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endauth
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item mb-2 mb-lg-0 me-0 me-lg-3">
                                <a href="{{ route('login') }}" class="nav-link text-bg-dark">Sign in</a>
                            </li>
                        @endif
                    @endguest
                </ul>
            </div>
        </div>
    </nav> --}}

    @yield('message')

    {{-- <div class="mt-5">
        @include('footer.user')
    </div> --}}
    <script src="{{ asset('style/plugins/jquery/jquery.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
        integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous">
    </script>
</body>

</html>

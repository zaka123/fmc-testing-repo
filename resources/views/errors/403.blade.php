@extends('errors::layout')

@section('styles')
    <style>
        @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');

        * {
            margin: 0;
            padding: 0;
            outline: none;
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif;
        }

        body {
            height: 100vh;
            background: -webkit-repeating-linear-gradient(-45deg, #71b7e6, #69a6ce, #2ebbc8, #6ed0f1, #52abd1, #69a6ce, #4bb5d8);
            background-size: 400%;
        }

        #error-page {
            position: absolute;
            top: 10%;
            left: 15%;
            right: 15%;
            bottom: 10%;
            display: flex;
            align-items: center;
            justify-content: center;
            background: #fff;
            box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
        }

        #error-page .content {
            max-width: 600px;
            text-align: center;
        }

        .content h2.header {
            font-size: 18vw;
            line-height: 1em;
            position: relative;
        }

        .content h2.header:after {
            position: absolute;
            content: attr(data-text);
            top: 0;
            left: 0;
            right: 0;
            background: -webkit-repeating-linear-gradient(-45deg, #71b7e6, #69a6ce, #b98acc, #ee8176, #b98acc, #69a6ce, #9b59b6);
            background-size: 400%;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.25);
            animation: animate 10s ease-in-out infinite;
        }

        @keyframes animate {
            0% {
                background-position: 0 0;
            }

            25% {
                background-position: 100% 0;
            }

            50% {
                background-position: 100% 100%;
            }

            75% {
                background-position: 0% 100%;
            }

            100% {
                background-position: 0% 0%;
            }
        }

        .content h4 {
            font-size: 1.5em;
            margin-bottom: 20px;
            text-transform: uppercase;
            color: #000;
            font-size: 2em;
            max-width: 600px;
            position: relative;
        }

        .content h4:after {
            position: absolute;
            content: attr(data-text);
            top: 0;
            left: 0;
            right: 0;
            text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.4);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .content p {
            font-size: 1.2em;
            color: #0d0d0d;
        }

        .grandFather {
            position: relative;
        }

        .father {
            position: absolute;
            top: 10px;
            left: 10px;
        }

        @media(max-width:480px) {
            .father {
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
        }

        @media(max-width:480px) {
            .content {
                margin-top: 40px;
            }
        }
    </style>
@endsection

@section('title', __('Forbidden'))
@section('message')

    <body>
        <div id="error-page" class="grandFather">
            <div class="father">
                <a href="/"><img src="{{ asset('assets/img/logo.png') }}" alt=""></a>
            </div>
            <div class="content" style="padding: 0 10px;">
                <h2 class="header" data-text="403">
                    403
                </h2>
                <h4 data-text="Permission denied.">
                    Permission denied.
                </h4>
                <p style="text-align: center">
                    We are sorry, but you don't have permission to access this page.
                </p>
            </div>
        </div>
    </body>
@endsection

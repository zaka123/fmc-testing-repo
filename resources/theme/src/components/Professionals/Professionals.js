import React from "react";
import { Col, Button, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Mobile from "../../images/professionals.png"
import Svg from "../../fonts/left.svg"




const Professionals = () => {
  document.title = 'Professionals | Career Counselling'
  return (
    <div className="profession-section d-flex align-items-center">
      <Container className="text-white">
        <Row>
          <Col lg="6">
            <h1 className="fw-bold profession title">ENSURE  YOUR CHILDREN  ARE   SETUP  FOR  A  SUCCESSFUL  CAREER  WITH   OUR
              CAREER EXPERT! </h1>

            <p className="text-dark" >
              Are   you   worried   about   your   child   career?   Have   you   identified   your   child
              interests? Are you worried his interests does not match with your career choices
              or aspirations? Our experts help you to get rid of these worrying by talking to
              you and your child, identifying his interests and skills, finding the career path
              that matches his interest and your aspirations. Our experts make personalized
              plan   with   set   targets   and   provide   relevant   advice   to   you   and   your   child   for
              successful career.     </p>
            <Link to="/register?profession=Professional">
              <Button variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Lets Get Started</Button>
            </Link>
          </Col>
          <Col lg="6">
            <img src={Mobile} className="img-responsive mobile" alt="logo" />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Professionals;


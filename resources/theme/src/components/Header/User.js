import React from "react";
import { Container, Button, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

const Userheader = () => {

    return (

        <Navbar bg="transparent" expand="lg" className="p-3 fixed-nav">
            <Container>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end gap-5">
                    <Link to="" className="" variant="px-3"><i className="fa fa-3x   text-secondary fa-home" /></Link>
                    <Link to="" className="" variant="px-3"><i className="fa-3x fa-solid fa-envelope  text-secondary " /></Link>
                    <Link to="" className="" variant="px-3"><i className="fa-solid fa-chart-pie fa-3x  text-secondary" /></Link>
                    <Link to="" className="" variant="px-3"><i className="fa-solid fa-list-ul fa-3x  text-secondary" /></Link>
                    <Link to="" className="" variant="px-3"><i className="fa-solid fa-circle-question fa-3x  text-secondary" /></Link>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );


};

export default Userheader;

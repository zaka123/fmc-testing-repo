import React from "react";
import { Route, Redirect } from 'react-router';
import { BrowserRouter as Router, Routes, Switch, Link, useParams } from "react-router-dom";
import ReactDOM from 'react-dom';
import { Container, Button, Navbar } from "react-bootstrap";
import logo from '../../images/logo_2.png';
import Userheader from "./User";

class Header extends React.Component  {
  constructor(props) {
    super(props)
    this.state = { 
      isAuthenticated: false 
    };
    this.checkLogin = this.checkLogin.bind(this); 
    this.dashboard = this.dashboard.bind(this); 
  }

  dashboard()
  {
    window.location = "/dashboard"
  }

  checkLogin() {
    axios.post('checkLogin', packets)
      .then(
        success => {
          this.setState({ isAuthenticated : true})
        }
      )
      .catch(error => {
        this.setState({ isAuthenticated: false })
      });
  }
  render(){
    let Button;
    if (this.state.isAuthenticated===true)
    {
      Button = <Link to="login" className="btn home-bg-red m-2 rounded-0" variant="rounded-0 px-3">Login</Link>
    } else {
      Button = <Link to="#" onClick={this.dashboard} className="btn home-bg m-2 rounded-0" variant="rounded-0 px-3">Login</Link>
    }
  return (
    <Navbar bg="transparent" expand="lg" className="p-3 fixed-top">
      <Container>
        <Link to="/"><Navbar.Brand><img src={logo} className="img-responsive logo " alt="logo" /></Navbar.Brand></Link>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Link to="/register?profession=counsellor" className="btn  button-1 m-2 rounded-1" variant="  rounded-0 px-3">Become Career Counselor</Link>
          {Button}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
  }
};

export default Header;

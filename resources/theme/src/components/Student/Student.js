import React from "react";
import { Col, Button, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Mobile from "../../images/students.png"
import Svg from "../../fonts/left.svg"
import "./Student.css";

const Student = () => {
  document.title = 'Student | Career Counselling'
  return (

    <div className="student-section d-flex align-items-center">
      <Container className="text-white">
        <Row>
          <Col lg="6">
            <h1 className="fw-bold main-color title">Here We Will Write <span className="main-color-2">Tagline</span> For Students</h1>

            <p className="text-dark" >
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
            <Link to="/register?profession=student">
              <Button variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Lets Get Started</Button>
            </Link>
          </Col>
          <Col lg="6">
            <img src={Mobile} className="img-responsive mobile" alt="logo" />
          </Col>
        </Row>


      </Container>
    </div>
  );
};

export default Student;

import React from "react";
import { Col, Button, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Mobile from "../../images/parents.png"
import Svg from "../../fonts/left.svg"
import "./Parents.css"



const Parents = () => {
  document.title = 'Parent Registration | Career Counselling'
  return (

    <div className="parents-section-1 d-flex align-items-center">
      <Container className="text-white">
        <Row>
          <Col lg="6">
            <h1 className="fw-bold main-parents-1 title">GET HELP FOR CAREER TRANSITION AND GROWTH FROM OUR CAREER EXPERTS</h1>

            <p className="text-dark" >
              Are you bored in your current role and looking for career transition or your role
              doesn't fit with your interest or attitude? Our career experts help you to find the
              suitable   role   based   on   your   skills   and  interest   and   provide   your   step-by-step
              guidance to achieve your career aspirations.
            </p>
            <Link to="/register?profession=Parent">
              <Button variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Lets Get Started</Button>
            </Link>
          </Col>
          <Col lg="6">
            <img src={Mobile} className="img-responsive mobile" alt="logo" />
          </Col>
        </Row>


      </Container>
    </div>
  );
};

export default Parents;

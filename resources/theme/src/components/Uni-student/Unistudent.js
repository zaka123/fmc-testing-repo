import React from "react";
import { Col, Button, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Mobile from "../../images/unistudents.png"
import Svg from "../../fonts/left.svg"
// import "./App.css";

const Unistudent = () => {
  document.title = 'University Student | Career Counselling'
  return (

    <div className="student-section d-flex align-items-center">
      <Container className="text-white">
        <Row>
          <Col lg="6">
            <h1 className="fw-bold main-color title">MAKE YOUR JOURNEY/TRANISITION FROM EDUCATION TO EMPLOYMENT
              SEAMLESSLY WITH OUR CAREER EXPERTS!</h1>
            <p className="text-dark" >
              Acquiring   the   right   career   based   on   your   skills,   aptitude,   and   future   goals
              immediately after education is not straight forward. FIND ME CAREER help you to
              make your transition smooth by connecting you with the smart experts in your
              filed to share inspiring thoughts, fresh perspective and discussion on topics that
              matter you. Our experts make personalized plan with set targets and provide
              relevant advice to counsel and motivate you.
            </p>
            <Link to="/register?profession=University Student">
              <Button variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Lets Get Started</Button>
            </Link>
          </Col>
          <Col lg="6">
            <img src={Mobile} className="img-responsive mobile" alt="logo" />
          </Col>
        </Row>


      </Container>
    </div>
  );
};

export default Unistudent;

import React from 'react';
import { Container, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


const Footer = () => {
    if (window.location.pathname === '/userdashboard') return null;
    return (
        <div>
            <div >
                <Container fluid>
                    <Row>
                        <Col className="footer-btns" lg="12">
                            <Link to="/services">
                                <Button variant="dark rounded-0 fw-light ms-1 mt-4">About Us</Button>
                            </Link>
                            <Link to="/services">
                                <Button variant="dark rounded-0  fw-light ms-1 mt-4">Career Couching</Button>
                            </Link>

                            <Link to="/services">
                                <Button variant="dark rounded-0 fw-light ms-1 mt-4">Testimonials</Button>
                            </Link>
                            <Link to="/contacts">
                                <Button variant="dark rounded-0 fw-light ms-1 mt-4">Contact</Button>
                            </Link>
                        </Col>

                    </Row>
                </Container>
                <Container fluid className="bg-dark footer  text-white text-start w-100 py-3" >

                    {/* Stack the columns on mobile by making one full-width and the other half-width */}
                    <Row>
                        <Col xs={12} md={12}>
                            <p className="mb-0">&copy; Copyright 2022 Find me Career. All rights reserved.</p>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
};

export default Footer;
import React from "react";
import { Col, Container, Row, Button } from "react-bootstrap";
import { BrowserRouter as Router, Link } from "react-router-dom";
import Mobile from "../../images/career.png"
import "./Hero.css";

const Hero = () => {
  return (
    <div className="hero-section d-flex align-items-center">
      <Container className="text-white">
        <Row>
          <Col lg="6">
            <h1 className="fw-bold title">FIND ME CAREER will help you to achieve    <span className="home-red">Your Career Goal</span> </h1>
            <h1 className="fw-bold title">by providing personalized coaching and mentorship.</h1>
            {/* <Router> */}
            <Link to="/student">
              <Button className="home-bg-color" variant=" rounded-3 fw-light ms-1 mt-4">School/College Students</Button>
            </Link>
            <Link to="/Unistudent">
              <Button className="home-bg-color-3" variant="dark rounded-3  fw-light ms-1 mt-4">University Student / Grad</Button>
            </Link>
            <Link to="/Professionals">
              <Button className="home-bg-color-1" variant="dark rounded-3 fw-light ms-1 mt-4">Working Professionals</Button>
            </Link>
            <Link to="/Parents">
              <Button className="home-bg-color-1" variant="dark rounded-3 fw-light ms-1 mt-4">Parents Looking for there child</Button>
            </Link>
            {/* </Router> */}
          </Col>
          <Col lg="6">
            <img src={Mobile} className="img-responsive mobile" alt="logo" />
          </Col>
        </Row>


      </Container>
    </div>
  );
};

export default Hero;

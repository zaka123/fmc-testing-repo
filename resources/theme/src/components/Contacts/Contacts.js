import React from 'react'
import { Col, Button, Container, Row, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
// import Svg from "../fonts/left.svg"

export const Contacts = () => {
  document.title = 'Contact | Career Counselling'
  return (
    <div className="signup d-flex align-items-center">
      <Container className="text-white">
        <Row>

          <Col lg="6" className='mx-auto'>
            <h1 className="fw-bold main-parents title">Contact US</h1>
            <Form>
              <Row>
                <Col lg="6">
                  <Form.Group className="mb-3 " controlId="formBasicEmail">
                    <Form.Control className='new-form-control' type="text" placeholder="First Name" />
                  </Form.Group>
                </Col>
                <Col lg="6">
                  <Form.Group className="mb-3" controlId="formBasicPassword">

                    <Form.Control className='new-form-control' type="Text" placeholder="Last Name" />
                  </Form.Group>
                </Col>


                <Col lg="12">
                  <Form.Group className="mb-3 " controlId="formBasicEmail">
                    <Form.Control className='new-form-control' type="email" placeholder="Email" />
                  </Form.Group>
                </Col>
                <Col lg="12">
                  <Form.Group className="mb-3" controlId="formBasicPassword">

                    <textarea
                      className="form-control"
                      id="exampleFormControlTextarea1"
                      rows="5"
                      placeholder='Your Message'
                    />
                  </Form.Group>
                </Col>


              </Row>
            </Form>
          </Col>
        </Row>


      </Container >
    </div >
  )
}


export default Contacts;

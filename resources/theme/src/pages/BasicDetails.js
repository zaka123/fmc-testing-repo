import React from 'react'
import { Col, Button, Container, Row, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Svg from "../fonts/left.svg"

export const BasicDetails = () => {
    return (
        <div className="profession-section d-flex align-items-center">
            <Container className="text-white">
                <Row>
                    <Col lg="6">
                        <h1 className="fw-bold profession title">This is Our Basic Details Page</h1>

                        <p className="text-dark" >
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>

                    </Col>
                    <Col lg="6">
                        {/* <h1 className="fw-bold main-parents title">Register Yourself</h1> */}
                        <Form>
                            <Row>
                                <Col lg="6">
                                    <Form.Group className="mb-3 " controlId="formBasicEmail">
                                        <Form.Select className='new-form-control' aria-label="Default select example">
                                            <option>Education Level</option>
                                            <option value="1">Metric</option>
                                            <option value="2">Inter</option>
                                            <option value="3">Master</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col lg="6">
                                    <Form.Group className="mb-3" controlId="formBasicPassword">

                                        <Form.Select className='new-form-control' aria-label="Default select example">
                                            <option>Gender</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col lg="6">
                                    <Form.Group className="mb-3" controlId="formBasicPassword">
                                        <Form.Label className='text-dark fw-bold'>Date OF Birth</Form.Label>
                                        <Form.Control className='new-form-control' type="date" placeholder="Date of Birth" />
                                    </Form.Group>
                                </Col>
                                <Col lg="6">
                                    <Form.Group className="mb-3" controlId="formBasicPassword">

                                        {/* <Form.Control type="Text" placeholder="Last Name" /> */}
                                    </Form.Group>
                                </Col>

                                <Col lg="12">
                                    <Form.Group className="mb-3" controlId="formBasicPassword">

                                        <Form.Select className='new-form-control' aria-label="Default select example">
                                            <option>Have you ever been to career counselor</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Link to="/services">
                                    <Button variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Lets Get Started</Button> <img className="icon-left mt-4" src={Svg} alt="" />
                                </Link>

                            </Row>
                        </Form>
                    </Col>
                </Row>


            </Container >
        </div >
    )
}

import * as React from 'react';
import { Route, Redirect } from 'react-router'
import ReactDOM from 'react-dom';
import { Col, Button, Container, Row, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Svg from "../fonts/left.svg";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()
export class Signin extends React.Component {
    login(event) {
        event.preventDefault();

        const packets = {
            email: event.target.email.value,
            password: event.target.password.value
        };
        axios.post('login', packets)
            .then(
                success => {
                    window.location = "/dashboard"
                }
            )
            .catch(error => {
                toast.error(error.response.data.message);
            });
    }


    render() {
        document.title = 'Login | Career Counselling'
        return (
            <div className="signup d-flex align-items-center">

                <Container className="text-white">
                    <Row>
                        <Col lg="6">
                            <h1 className="fw-bold main-parents title">This is Our Sign In Page</h1>

                            <p className="text-dark">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </Col>
                        <Col lg="6">
                            <h1 className="fw-bold main-parents title">Sign In To Your Account</h1>
                            <Form onSubmit={this.login}>
                                <Row>
                                    <Col lg="7">
                                        <Form.Group className="mb-3 " controlId="formBasicEmail">
                                            <Form.Control name="email" className='email new-form-control' type="email" placeholder="Email" />
                                        </Form.Group>
                                    </Col>
                                    <Col lg="7">
                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Control name="password" className='password new-form-control' type="password" placeholder="Password" />
                                        </Form.Group>
                                    </Col>
                                    <div>
                                        <Button type="submit" variant="dark rounded-0 fw-bold ms-1 px-4 mt-4">Sign In</Button> <img className="icon-left mt-4" src={Svg} alt="" />
                                    </div>

                                </Row>
                            </Form>
                        </Col>
                    </Row>
                </Container >
            </div >
        );
    }
}


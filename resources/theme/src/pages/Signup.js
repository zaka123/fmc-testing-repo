import * as React from 'react';
import { Route, Redirect } from 'react-router'
import { BrowserRouter as Router, Routes, Switch, Link, useParams } from "react-router-dom";
import ReactDOM from 'react-dom';
import { Col, Button, Container, Row, Form } from "react-bootstrap";
import Svg from "../fonts/left.svg";
import Recaptcha from "react-recaptcha";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

export class Signup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isVerified: false,
            fields: {},
            errors: {},
            history: {},
        };
        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
        this.register = this.register.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
    }

    recaptchaLoaded() {
        console.log('Recaptcha loaded');
    }

    verifyCallback(response) {
        if (response) {
            this.setState({
                isVerified: true,
            });
        }
    }

    validateFields() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        // First Name
        if (!fields["first_name"]) {
            formIsValid = false;
            errors["first_name"] = "Cannot be empty";
        }

        if (typeof fields["first_name"] !== "undefined") {
            if (!fields["first_name"].match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                errors["first_name"] = "Only letters";
            }
        }

        if (!fields["last_name"]) {
            formIsValid = false;
            errors["last_name"] = "Cannot be empty";
        }

        if (typeof fields["last_name"] !== "undefined") {
            if (!fields["last_name"].match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                errors["last_name"] = "Only letters";
            }
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf("@");
            let lastDotPos = fields["email"].lastIndexOf(".");

            if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    fields["email"].indexOf("@@") == -1 &&
                    lastDotPos > 2 &&
                    fields["email"].length - lastDotPos > 2
                )
            ) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        if (!fields["phone"]) {
            formIsValid = false;
            errors["phone"] = "Cannot be empty";
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Cannot be empty";
        }

        if (!fields["confirmation_password"]) {
            formIsValid = false;
            errors["confirmation_password"] = "Cannot be empty";
        }

        if (!fields["confirmation_password"] || fields["confirmation_password"] != fields["password"]) {
            formIsValid = false;
            errors["confirmation_password"] = "Password does not match";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }


    register(event) {
        event.preventDefault();
        const queryParams = new URLSearchParams(window.location.search)
        const profession = queryParams.get("profession")
        let role = "Student"; 
        if(profession=="counsellor"){
            role = "Counsellor"
        }else {
            role = "Student"
        }
        // console.log(role);
        if (!this.state.isVerified) {
            toast.error('Please Verify the Captcha');
            return false;
        }
        this.validateFields();
        if (!this.validateFields()) {
            toast.error('Please enter the correct data');
            return false;
        }
        const packets = {
            first_name: event.target.first_name.value,
            last_name: event.target.last_name.value,
            email: event.target.email.value,
            phone: event.target.phone.value,
            password: event.target.password.value,
            password_confirmation: event.target.confirmation_password.value,
            gender: event.target.gender.value,
            role:role,
            profession: profession,
        };

        axios.post('/register', packets)
            .then(
                success => {
                    window.location = "/dashboard"
                }
            )
            .catch(error => {
                toast.error(error.response.data.message);
            });
    };

    render() {
        document.title = 'Register | Career Counselling'
        return (
            <div className="signup d-flex align-items-center">
                <Container className="text-white">
                    <Row>
                        <Col lg="6">
                            <h1 className="fw-bold main-parents title">This is Our SignUP Page</h1>

                            <p className="text-dark" >
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>

                        </Col>
                        <Col lg="6">
                            <h1 className="fw-bold main-parents title">Register Yourself</h1>
                            <Form onSubmit={this.register}>
                                <Row>
                                    <Col lg="6">
                                        <Form.Group className="mb-3 " controlId="formBasicEmail">
                                            <Form.Control name="first_name" onChange={this.handleChange.bind(this, "first_name")} value={this.state.fields["first_name"]} className='new-form-control' type="text" placeholder="First Name" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["first_name"]}</span>

                                    </Col>
                                    <Col lg="6">
                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Control name="last_name" onChange={this.handleChange.bind(this, "last_name")} value={this.state.fields["last_name"]} className='new-form-control' type="Text" placeholder="Last Name" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["last_name"]}</span>

                                    </Col>
                                    <Col lg="6">
                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Control name="email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} className='new-form-control' type="email" placeholder="john@gmail.com" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["email"]}</span>

                                    </Col>
                                    <Col lg="6">
                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Control name="phone" onChange={this.handleChange.bind(this, "phone")} value={this.state.fields["phone"]} className='new-form-control' type="tel" placeholder="00000000" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["phone"]}</span>

                                    </Col>
                                    <Col lg="6">
                                        <Form.Group className="mb-3 " controlId="formBasicEmail">
                                            <Form.Control name="password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} className='new-form-control' type="password" placeholder="Password" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["password"]}</span>

                                    </Col>
                                    <Col lg="6">
                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Control name="confirmation_password" onChange={this.handleChange.bind(this, "confirmation_password")} value={this.state.fields["confirmation_password"]} className='new-form-control' type="password" placeholder="Confirm Password" />
                                        </Form.Group>
                                        <span style={{ color: "red" }}>{this.state.errors["confirmation_password"]}</span>

                                    </Col>
                                    <div onChange={this.handleChange.bind(this, "gender")} style={{ color: "#19787C" }}>
                                        <input type="radio" checked value="Male" name="gender" /> Male
                                        <input type="radio" value="Female" name="gender" /> Female
                                        <input type="radio" value="Other" name="gender" /> Other
                                    </div>
                                    <Recaptcha
                                        sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
                                        render="explicit"
                                        onloadCallback={this.recaptchaLoaded}
                                        verifyCallback={this.verifyCallback}
                                    />
                                    <Col lg="12">
                                        <Form.Check
                                            type="checkbox"
                                            id={`default-checkbox`}
                                            label={`Accept Terms & Condition`}
                                            name="terms"
                                            className='text-dark'
                                        />
                                    </Col>
                                    <div>
                                        <Button
                                            type="submit" onClick={this.setRole}
                                            variant="dark rounded-0 fw-bold ms-1  px-4 mt-4">Lets Get Started</Button> <img className="icon-left mt-4" src={Svg} alt="" />
                                    </div>
                                </Row>
                            </Form>
                        </Col>
                    </Row>
                </Container >
            </div>
        );
    }
}

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import User from "./components/Header/User";
import Home from "./components/Home/Home";
import Parents from "./components/Parents/Parents";
import Professionals from "./components/Professionals/Professionals";
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom";
import Unavailable from "./components/Unavailable/Unavailable";
import About from "./components/Student/Student";
import Contacts from "./components/Contacts/Contacts";
import Unistudent from "./components/Uni-student/Unistudent";
import { Signup } from "./pages/Signup";
import { Signin } from "./pages/Signin.js";
import { BasicDetails } from "./pages/BasicDetails";
import { Userdashboard } from "./user/Userdashboard";
function App() {
  return (
    <div>
      <Router>
        <Header />
        <Switch>

          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route exact path="/Parents">
            <Parents />
          </Route>
          <Route exact path="/Professionals">
            <Professionals />
          </Route>
          <Route exact path="/Unistudent">
            <Unistudent />
          </Route>
          <Route exact path="/Student">
            <About />
          </Route>
          <Route exact path="/contacts">
            <Contacts />
          </Route>
            <Route path="/register" component={Signup}></Route>
          <Route path="/login">
            <Signin />
          </Route>
          <Route path="/Basic-detail">
            < BasicDetails />
          </Route>

          <Route path="/userdashboard">
            <Userdashboard />
          </Route>

          <Route path="*">
            <Unavailable />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
